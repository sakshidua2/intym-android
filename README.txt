Ionic:

   Ionic CLI                     : 6.11.0 (/usr/local/lib/node_modules/@ionic/cli)
   Ionic Framework               : @ionic/angular 5.3.1
   @angular-devkit/build-angular : 0.901.12
   @angular-devkit/schematics    : 9.1.12
   @angular/cli                  : 9.1.12
   @ionic/angular-toolkit        : 2.3.0

Cordova:

   Cordova CLI       : 9.0.0 (cordova-lib@9.0.1)
   Cordova Platforms : android 8.1.0, ios 5.1.1
   Cordova Plugins   : cordova-plugin-ionic-keyboard 2.2.0, cordova-plugin-ionic-webview 4.2.1, (and 16 other plugins)

Utility:

   cordova-res (update available: 0.15.1) : 0.14.0
   native-run (update available: 1.1.0)   : 1.0.0

System:

   ios-sim : 8.0.2
   NodeJS  : v14.6.0 (/usr/local/bin/node)
   npm     : 6.14.7
   OS      : macOS Catalina
   Xcode   : Xcode 11.3 Build version 11C29