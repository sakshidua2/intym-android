this.onmessage = function (event) {
  const request = event.data;
  this.startApiCall(request);
};

function startApiCall(request) {
  const { url, type, data, headers } = request;
  const headersKeys = Object.keys(headers);
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      postMessage({ success: true, response: this.responseText });
    } else if (this.readyState == 4 && this.status != 200) {
      postMessage({ success: false, response: this.responseText });
    }
  };
  xhttp.open(type, url, true);
  for (let i = 0; i < headersKeys.length; i++) {
    const key = headersKeys[i];
    xhttp.setRequestHeader(key, headers[key]);
  }
  xhttp.send(data);
}
