import { Component } from "@angular/core";
import { APP_CONSTANTS } from "../config/app.constants";
import { Router } from "@angular/router";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  public isSupervisor: boolean = false;
  public showViewNow: boolean = false;
  notificationCount: string | number = 0;
  constructor(
    private router: Router,
    private coreService: CoreService,
    private modalController: ModalController,
  ) { }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.showViewNow = true;
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
