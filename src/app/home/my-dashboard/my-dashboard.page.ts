import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { Chart } from "chart.js";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { debounceTime } from "rxjs/operators";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { HolidaysListService } from "src/app/services/db/holidays-list.service";
import { HolidaysModel } from "src/app/local-db-models/holidays_list";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { Platform } from "@ionic/angular";

@Component({
  selector: "app-my-dashboard",
  templateUrl: "./my-dashboard.page.html",
  styleUrls: ["./my-dashboard.page.scss"],
})
export class MyDashboardPage implements OnInit {
  @ViewChild("lineCanvas") lineCanvas: ElementRef;
  @ViewChild("doughnutCanvas") doughnutCanvas: ElementRef;
  public leavesCount = {
    PendingCount: 0,
    ApprovedCount: 0,
    RejectedCount: 0,
    CanceledCount: 0,
  };
  request: any;
  holidayDateRange: any;
  customerVisit = {
    ApprovedCount: 0,
    RejectedCount: 0,
    Total: 0
  };

  fromMin: any;
  fromMax: any;
  toMin: any;
  toMax: any;

  holidaysList: any[];
  labelList: any[] = [];
  graphData: any[] = [];
  upComingLeaves: any;
  defaultValues: { min: string; max: string };
  private lineChart: Chart;

  constructor(
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService,
    private holidayService: HolidaysListService,
    private platform: Platform
  ) { }

  ngOnInit() {
    this.upComingLeaves = [];
  }

  ionViewDidEnter() {
    const maxDate: any = this.coreService.getMomentNow();
    const minDate: any = this.coreService.getMomentNow();
    minDate.year(2000).startOf("year");
    this.fromMax = this.toMax = maxDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    this.fromMin = this.toMin = minDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    this.defaultValues = {
      max: this.fromMax,
      min: this.fromMin,
    };
    this.setDates(false);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.loadLeavesStatusCounter();
      this.myUpcomingLeaves();
      this.getHolidayLit();
      this.getCustomerVisitStatisticsCounter();
    } else {
      this.getHolidaysFromLocal();
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getCustomerVisitStatisticsCounter() {
    this.apiService
      .getCustomerVisitStatisticsCounter({
        FromDate: this.request.FromDate,
        ToDate: this.request.ToDate,
      }, APP_CONSTANTS.DATA_TYPE_MY)
      .subscribe((res: any) => {
        this.customerVisit = res.data;
      });
  }

  getHolidayLit(e?: any) {
    const header = {
      headers: {
        [APP_CONSTANTS.HTTP_LOADER]: e ? "true" : "false",
      },
    };
    this.apiService.getHolidayList(header).subscribe(
      (res: any) => {
        const processedRes = this.processHolidayDate(res);
        const holidays = [...processedRes];
        this.holidaysAdjustment(holidays);
        if (this.platform.is("cordova")) {
          this.holidayService.storeHolidaysForOffline([...res]);
        }
      },
      (err) => {
        this.holidaysList = [];
        this.coreService.displayToast("Something went wrong", "danger");
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      },
      () => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      }
    );
  }

  getHolidaysFromLocal(e?) {
    this.holidayService
      .getData()
      .then((res: HolidaysModel[]) => {
        const processedRes = this.processHolidayDate(res);
        this.holidaysAdjustment([...processedRes]);
      })
      .catch((err) => {
        this.coreService.displayToast(
          "You are offline and no data found for user locally",
          "warning"
        );
      })
      .finally(() => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      });
  }

  holidaysAdjustment(holidays) {
    holidays.sort(
      (a, b) => a.genericHolidaydate.unix() - b.genericHolidaydate.unix()
    );
    const now = this.coreService.getMomentObject().unix();
    holidays = holidays.filter((o) => o.genericHolidaydate.unix() > now);
    this.holidaysList = holidays.splice(0, 3);
  }

  setDates(isFromDatePicker, type?, e?: any, StopLoader?: boolean) {
    if (!isFromDatePicker) {
      const fromDate = this.coreService.getMomentObject().subtract(4, "day");
      const toDate = this.coreService.getMomentObject();
      this.request = {
        FromDate: fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        ToDate: toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        Type: "my",
      };
    } else {
      const fromDate = this.coreService.getMomentObject(this.request.FromDate);
      const toDate = this.coreService.getMomentObject(this.request.ToDate);
      let fromDt = this.request.FromDate;
      let toDt = this.request.ToDate;
      if (type === "from" && fromDate.unix() > toDate.unix()) {
        toDt = fromDt;
      } else if (type === "to" && toDate.unix() < fromDate.unix()) {
        fromDt = toDt;
      }
      this.request.FromDate = this.coreService
        .getMomentObject(fromDt)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
      this.request.ToDate = this.coreService
        .getMomentObject(toDt)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    }
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.getGraphDetails(e, StopLoader);
      this.loadLeavesStatusCounter(e, StopLoader);
      this.getCustomerVisitStatisticsCounter();
    } else if (isFromDatePicker) {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getGraphDetails(e?: any, StopLoader?: boolean) {
    this.apiService
      .getWorkHoursGraph({ ...this.request, StopLoader })
      .pipe(debounceTime(500))
      .subscribe(
        (res: any) => {
          if (res) {
            this.labelList = [];
            this.graphData = [];
            res.forEach((ele: any) => {
              const dt = this.coreService
                .getMomentByDate(ele.Date)
                .toISOString();
              this.labelList.push(dt);
              if (!ele.OutTime) {
                ele.OutTime = ele.InTime;
              }
              this.graphData.push([
                this.getMilliseconds(ele.InTime),
                this.getMilliseconds(ele.OutTime),
              ]);
            });
            this.createGraph();
          }
        },
        () => {
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        },
        () => {
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        }
      );
  }

  getMilliseconds(date: string) {
    if (!date || !date.trim()) {
      return 0;
    }
    const splitedDateTime = date.split(" ");
    const splitedDate = splitedDateTime[0].trim().split("/");
    const splitedTime = splitedDateTime[1].trim().split(":");
    const meridiem = splitedDateTime[2].trim().toLocaleLowerCase();
    const month =
      parseInt(splitedDate[0]) < 10 ? `0${splitedDate[0]}` : splitedDate[0];
    const day =
      parseInt(splitedDate[1]) < 10 ? `0${splitedDate[1]}` : splitedDate[1];
    const year = splitedDate[2];
    const ISOFormatDate = `${year}-${month}-${day}`;
    let hour = parseInt(splitedTime[0]);
    const minute = parseInt(splitedTime[1]);
    const second = parseInt(splitedTime[2]);
    if (meridiem === "pm") {
      const hr = parseInt(splitedTime[0]);
      if (hr < 12) {
        hour = 12 + hr;
      }
    }
    const momentNow = this.coreService.getMomentObject(ISOFormatDate);
    momentNow.set({ hour, minute, second, millisecond: 0 });
    const momentNowLocalTimezone = this.coreService.convertMomentToLocalTimezone(
      momentNow
    );
    const momentDayStart = this.coreService.getMomentObject(ISOFormatDate);
    momentDayStart.startOf("day");
    return momentNowLocalTimezone.valueOf() - momentDayStart.valueOf();
  }

  msToTime(s, isTooltip?: boolean) {
    const ms = s % 1000;
    s = (s - ms) / 1000;
    const secs = s % 60;
    s = (s - secs) / 60;
    const mins = s % 60;
    const hrs = (s - mins) / 60;

    return !isTooltip
      ? `${hrs < 10 ? "0" + hrs : hrs > 23 ? 23 : hrs}:${hrs >= 23 ? 59 : "00"}`
      : `${hrs < 10 ? "0" + hrs : hrs > 23 ? 23 : hrs}:${mins < 10 ? "0" + mins : mins
      }`;
  }

  createGraph() {
    if (this.lineChart) {
      this.lineChart.destroy();
    }
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "horizontalBar",
      data: {
        labels: this.labelList,
        datasets: [
          {
            label: "Work Hours",
            maxBarThickness: 8,
            minBarLength: 2,
            data: this.graphData,
            borderColor: "rgba(255,0,0,1)",
            backgroundColor: "rgba(255,0,0,1)",
            barPercentage: 0.06,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              type: "time",
              time: {
                unit: "day",
                round: "day",
                displayFormats: {
                  day: "D-MMM",
                },
              },
              offset: true,
            },
          ],
          xAxes: [
            {
              ticks: {
                maxTicksLimit: 6,
                source: "data",
                beginAtZero: true,
                beforeBuildTicks: (e) => {
                  console.log("beforeBuildTicks", e);
                },
                callback: (value) => {
                  if (value) {
                    return this.msToTime(value);
                  }
                  return "00:00";
                },
              },
            },
          ],
        },
        tooltips: {
          mode: "label",
          callbacks: {
            title: (tooltipItem) => {
              const d = JSON.parse(tooltipItem[0].value);
              tooltipItem[0].value = `${this.msToTime(
                d[0],
                true
              )}-${this.msToTime(d[1], true)}`;

              const data = this.coreService
                .getMomentByDate(tooltipItem[0].xLabel)
                .format(APP_CONSTANTS.DATE_FORMAT_DD_MMM_YYYY);
              return data;
            },
          },
        },
      },
    });
  }

  loadLeavesStatusCounter(e?: any, StopLoader?: boolean) {
    const leavesCounterPayload = {
      FromDate: this.request.FromDate,
      ToDate: this.request.ToDate,
      Type: "my",
      NoOfRows: 0,
      StopLoader,
    };
    this.apiService.getLeaveStatusCounter(leavesCounterPayload).subscribe(
      (res: any) => {
        this.leavesCount = { ...res.data };
      },
      (err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      },
      () => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      }
    );
  }

  myUpcomingLeaves(e?: any, StopLoader?: boolean) {
    this.holidayDateRange = {
      fromDate: this.coreService.getMomentObject(),
      toDate: this.coreService.getMomentObject().add(30, 'days')
    };
    this.apiService
      .getUpcomingLeaves({
        FromDate: this.holidayDateRange.fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        ToDate: this.holidayDateRange.toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        NoOfRows: 5,
        type: "my",
        StopLoader
      })
      .subscribe(
        (res: any) => {
          if (res) {
            this.upComingLeaves = res;
            this.upComingLeaves.sort(
              (a, b) =>
                this.coreService.getMomentByDate(a.FromDate).unix() -
                this.coreService.getMomentByDate(b.FromDate).unix()
            );

            const data = this.upComingLeaves.filter(
              (ele) =>
                this.coreService.getMomentByDate(ele.FromDate).unix() >
                this.coreService.getMomentObject().unix() &&
                ele.StatusId === 2
            );
            this.upComingLeaves = data.splice(0, 3);
          } else {
            this.upComingLeaves = [];
          }
        },
        (err) => {
          this.upComingLeaves = [];
          this.coreService.displayToast("Something went wrong", "danger");
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        },
        () => {
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        }
      );
  }

  doRefresh(e) {
    this.fromMin = this.toMax = this.defaultValues.max;
    this.fromMin = this.toMin = this.defaultValues.min;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.setDates(false, undefined, e, true);
      this.getHolidayLit(e);
      this.loadLeavesStatusCounter(e, true);
      this.myUpcomingLeaves(e, true);
      this.getCustomerVisitStatisticsCounter();
    } else {
      this.getHolidaysFromLocal(e);
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  processHolidayDate(res: any[any]) {
    return res.map((o) => {
      const splitedDate = o.HolidayDate ? o.HolidayDate.trim().split("/") : [];
      const year = splitedDate[2];
      const month =
        parseInt(splitedDate[0]) < 10 ? `0${splitedDate[0]}` : splitedDate[0];
      const day =
        parseInt(splitedDate[1]) < 10 ? `0${splitedDate[1]}` : splitedDate[1];
      const genericHolidaydate = this.coreService.getMomentObject(
        `${year}-${month}-${day}`
      );
      return { ...o, genericHolidaydate };
    });
  }

  changeDateRender(date: string) {
    const splitedDate = date.split("-");
    return `${splitedDate[1]}/${splitedDate[2]}/${splitedDate[0]}`;
  }
}
