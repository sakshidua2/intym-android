import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { MyDashboardPageRoutingModule } from "./my-dashboard-routing.module";

import { MyDashboardPage } from "./my-dashboard.page";
import { SharedModule } from "src/app/shared/shared.module";
import { DateTimeFormatPipe } from "src/app/pipes/date-pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyDashboardPageRoutingModule,
    SharedModule,
  ],
  declarations: [MyDashboardPage],
  providers: [DatePipe, DateTimeFormatPipe],
})
export class MyDashboardPageModule {}
