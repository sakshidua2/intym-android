import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { HolidaysListService } from "src/app/services/db/holidays-list.service";
import { HolidaysModel } from "src/app/local-db-models/holidays_list";
import { CoreService } from "src/app/services/core.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { Platform, ModalController } from "@ionic/angular";
import { SettingsPage } from "../../settings/settings.page";

@Component({
  selector: "app-holidays",
  templateUrl: "./holidays.page.html",
  styleUrls: ["./holidays.page.scss"],
})
export class HolidaysPage implements OnInit {
  public isSupervisor: boolean = false;
  holidayList: any[];
  constructor(
    private apiService: ApiService,
    private networkService: NetworkService,
    private holidaysService: HolidaysListService,
    private coreService: CoreService,
    private platform: Platform,
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getHolidaysFromLocal();
    } else {
      this.getHolidayList();
    }
  }

  getHolidayList(e?: any) {
    const header = {
      headers: {
        [APP_CONSTANTS.HTTP_LOADER]: e ? "true" : "false",
      },
    };
    this.apiService.getHolidayList(header).subscribe((res: any) => {
      const processedRes = this.processHolidayDate(res);
      this.holidayList = [...processedRes];
      this.holidayList.sort(
        (a, b) => a.genericHolidaydate.unix() - b.genericHolidaydate.unix()
      );
      if (e && e.target && e.target.complete) {
        e.target.complete();
      }
      if (this.platform.is("cordova")) {
        this.holidaysService.storeHolidaysForOffline([...res]);
      }
    });
  }

  getHolidaysFromLocal(e?) {
    this.holidaysService
      .getData()
      .then((res: HolidaysModel[]) => {
        const processedRes = this.processHolidayDate(res);
        this.holidayList = [...processedRes];
        this.holidayList.sort(
          (a, b) => a.genericHolidaydate.unix() - b.genericHolidaydate.unix()
        );
      })
      .catch((err) => {
        this.coreService.displayToast(
          "You are offline and no data found for user locally",
          "warning"
        );
      })
      .finally(() => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      });
  }

  doRefresh(e) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getHolidaysFromLocal(e);
    } else {
      this.getHolidayList(e);
    }
  }

  processHolidayDate(res: any[any]) {
    return res.map((o) => {
      const splitedDate = o.HolidayDate ? o.HolidayDate.trim().split("/") : [];
      const year = splitedDate[2];
      const month =
        parseInt(splitedDate[0]) < 10 ? `0${splitedDate[0]}` : splitedDate[0];
      const day =
        parseInt(splitedDate[1]) < 10 ? `0${splitedDate[1]}` : splitedDate[1];
      const genericHolidaydate = this.coreService.getMomentObject(
        `${year}-${month}-${day}`
      );
      return { ...o, genericHolidaydate };
    });
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
