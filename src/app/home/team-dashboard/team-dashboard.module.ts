import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamDashboardPageRoutingModule } from './team-dashboard-routing.module';

import { TeamDashboardPage } from './team-dashboard.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TeamDashboardPageRoutingModule,
    SharedModule
  ],
  declarations: [TeamDashboardPage]
})
export class TeamDashboardPageModule {}
