import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Chart } from "chart.js";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import {
  ConnectionStatus,
  NetworkService,
} from "src/app/services/network.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";

@Component({
  selector: "app-team-dashboard",
  templateUrl: "./team-dashboard.page.html",
  styleUrls: ["./team-dashboard.page.scss"],
})
export class TeamDashboardPage implements OnInit {
  @ViewChild("barCanvas") barCanvas: ElementRef;
  public leavesCount = {
    PendingCount: 0,
    ApprovedCount: 0,
    RejectedCount: 0,
    CanceledCount: 0,
  };
  public attendanceCorrectionCount = {
    PendingCount: 0,
    ApprovedCount: 0,
    RejectedCount: 0,
    CanceledCount: 0,
  };
  customerVisit = {
    ApprovedCount: 0,
    RejectedCount: 0,
    Total: 0
  };
  holidayDateRange: any;
  private barChart: Chart;
  body: any;
  present: number[] = [];
  absent: number[] = [];
  labels: string[] = [];

  fromMinDate: string;
  fromMaxDate: string;
  toMinDate: string;
  toMaxDate: string;
  TeamLeaves: any;
  canLoadGraph: boolean;
  defaultValues: { FromDate: string; ToDate: string };
  constructor(
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    const maxDate: any = this.coreService.getMomentNow();
    const minDate: any = this.coreService.getMomentNow();
    minDate.year(2000).startOf("year");
    this.fromMaxDate = this.toMaxDate = maxDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    this.fromMinDate = this.toMinDate = minDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
  }

  ionViewDidEnter() {
    const fromDate = this.coreService.getMomentObject().subtract(4, "day");
    const toDate = this.coreService.getMomentObject();
    this.canLoadGraph = false;
    this.body = {
      FromDate: fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
    };
    this.defaultValues = { ...this.body };
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.loadLeavesStatusCounter();
      this.loadAttendanceCorrectionCounter();
      this.loadAttendGraph();
      this.myUpcomingLeaves();
      this.getCustomerVisitStatisticsCounter();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getCustomerVisitStatisticsCounter() {
    this.apiService.getCustomerVisitStatisticsCounter({
      FromDate: this.body.FromDate,
      ToDate: this.body.ToDate,
    }, APP_CONSTANTS.DATA_TYPE_TEAM).subscribe((res: any) => {
      console.log("res", res);
      this.customerVisit = res.data;
    });
  }

  loadAttendGraph(e?: any, StopLoader?: boolean) {
    const data = {
      FromDate: this.body.FromDate,
      ToDate: this.body.ToDate,
      type: "team",
      StopLoader,
    };
    this.apiService.getAttendanceGraph(data).subscribe(
      (res: any) => {
        if (res) {
          this.present = [];
          this.absent = [];
          this.labels = [];
          res.forEach((element) => {
            const dt = this.coreService
              .getMomentByDate(element.Date)
              .toISOString();
            this.present.push(element.Present);
            this.absent.push(element.Absent);
            this.labels.push(dt);
          });
          this.plotGraph();
        }
      },
      (err) => {
        const message =
          err &&
            err.error &&
            (err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"])
            ? err.error["Status"] ||
            err.error["ErrorMessage"] ||
            err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      },
      () => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      }
    );
  }

  plotGraph() {
    if (this.barChart) {
      this.barChart.destroy();
    }
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: this.labels,
        datasets: [
          {
            label: "Present",
            maxBarThickness: 8,
            minBarLength: 2,
            data: this.present,
            backgroundColor: "rgba(79, 223, 241, 1)",
            borderColor: "rgba(180, 242, 250, 1)",
          },
          {
            label: "Absent",
            data: this.absent,
            maxBarThickness: 8,
            minBarLength: 2,
            backgroundColor: "rgba(238, 46, 39, 1)",
            borderColor: "rgba(248, 187, 188,1)",
          },
        ],
      },
      options: {
        tooltips: {
          mode: "label",
          callbacks: {
            title: (tooltipItem) => {
              const data = this.coreService
                .getMomentByDate(tooltipItem[0].xLabel)
                .format(APP_CONSTANTS.DATE_FORMAT_DD_MMM_YYYY);
              return data;
            },
          },
        },
        legend: {
          display: true,
          align: "end",
          labels: {
            usePointStyle: true,
            fontSize: 10,
          },
        },
        title: {
          display: true,
          text: "Attendance Summary",
        },
        scales: {
          xAxes: [
            {
              // stacked: true,
              gridLines: {
                color: "rgba(0, 0, 0, 0)",
                drawBorder: false,
                display: false,
              },
              ticks: {
                padding: 1,
                beginAtZero: true,
                callback: (value) => {
                  const data = this.coreService
                    .getMomentByDate(value)
                    .format(APP_CONSTANTS.DATE_FORMAT_DD_MMM);
                  return data;
                },
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                drawBorder: false,
              },
              ticks: {
                padding: 1,
                beginAtZero: true,
              },
            },
          ],
        },
      },
    });
  }

  loadLeavesStatusCounter(e?: any, StopLoader?: boolean) {
    const payload = {
      FromDate: this.body.FromDate,
      ToDate: this.body.ToDate,
      Type: "team",
      NoOfRows: 0,
      StopLoader,
    };
    this.apiService.getLeaveStatusCounter(payload).subscribe(
      (res: any) => {
        this.leavesCount = { ...res.data };
      },
      (err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      },
      () => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      }
    );
  }

  loadAttendanceCorrectionCounter(e?: any, StopLoader?: boolean) {
    const payload = {
      FromDate: this.body.FromDate,
      ToDate: this.body.ToDate,
      Type: "team",
      NoOfRows: 0,
      StopLoader,
    };
    this.apiService.getAttendanceCorrectionCounter(payload).subscribe(
      (res: any) => {
        this.attendanceCorrectionCount = { ...res };
      },
      (err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      },
      () => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      }
    );
  }

  dateAdjust(type: string) {
    const fromDate = this.coreService.getMomentObject(this.body.FromDate);
    const toDate = this.coreService.getMomentObject(this.body.ToDate);
    let fromDt = this.body.FromDate;
    let toDt = this.body.ToDate;
    if (type === "from" && fromDate.unix() > toDate.unix()) {
      toDt = fromDt;
    } else if (type === "to" && toDate.unix() < fromDate.unix()) {
      fromDt = toDt;
    }
    this.body.FromDate = this.coreService
      .getMomentObject(fromDt)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    this.body.ToDate = this.coreService
      .getMomentObject(toDt)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.loadAttendGraph();
      this.myUpcomingLeaves();
      this.loadLeavesStatusCounter();
      this.loadAttendanceCorrectionCounter();
      this.getCustomerVisitStatisticsCounter();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  myUpcomingLeaves(e?: any, StopLoader?: boolean) {
    this.holidayDateRange = {
      fromDate: this.coreService.getMomentObject(),
      toDate: this.coreService.getMomentObject().add(30, 'days')
    };
    this.apiService
      .getUpcomingLeaves({
        FromDate: this.holidayDateRange.fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        ToDate: this.holidayDateRange.toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        NoOfRows: 5,
        type: "team",
        StopLoader
      })
      .subscribe(
        (res: any) => {
          if (res) {
            res.sort(
              (a, b) =>
                this.coreService.getMomentByDate(a.FromDate).unix() -
                this.coreService.getMomentByDate(b.FromDate).unix()
            );
            const data = res.filter(
              (ele) =>
                this.coreService.getMomentByDate(ele.FromDate).unix() >
                this.coreService.getMomentObject().unix() &&
                ele.StatusId === 2
            );

            this.TeamLeaves = data.splice(0, 3);
          } else {
            this.TeamLeaves = [];
          }
        },
        (err) => {
          this.TeamLeaves = [];
          this.coreService.displayToast("Something went wrong", "danger");
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        },
        () => {
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
        }
      );
  }

  doRefresh(e) {
    this.canLoadGraph = false;
    this.body = { ...this.defaultValues };
    setTimeout(() => {
      this.canLoadGraph = true;
    }, 600);

    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.myUpcomingLeaves(e, true);
      this.loadLeavesStatusCounter(e, true);
      this.loadAttendanceCorrectionCounter(e, true);
      this.loadAttendGraph(e, true);
      this.getCustomerVisitStatisticsCounter();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  changeDateRender(date: string) {
    const splitedDate = date.split("-");
    return `${splitedDate[1]}/${splitedDate[2]}/${splitedDate[0]}`;
  }
}
