import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: '',
        redirectTo: 'my-dashboard',
        pathMatch: 'full'
      },
      {
        path: 'my-dashboard',
        children: [
          {
            path: '',
            loadChildren: () => import('./my-dashboard/my-dashboard.module').then(m => m.MyDashboardPageModule)
          }
        ]
      },
      {
        path: 'team-dashboard',
        children: [
          {
            path: '',
            loadChildren: () => import('./team-dashboard/team-dashboard.module').then(m => m.TeamDashboardPageModule)
          }
        ]
      }
    ],
  },
  {
    path: 'holidays',
    loadChildren: () => import('./holidays/holidays.module').then( m => m.HolidaysPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }
