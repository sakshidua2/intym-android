export class AttendanceLogModel {
  id: number;
  EmpAttnTransactionId: number;
  EmployeeId: number;
  EmployeeName: string;
  TimeStamp: string;
  AttendanceStatus: number;
  DeviceSN: string;
  ImageURL: string;
  Latitude: number;
  Longitude: number;
  CustomerName: string;
  CustomerAddress: string;
  ApprovalStatus: number;
  IsMyAttendanceLog: boolean;
}
