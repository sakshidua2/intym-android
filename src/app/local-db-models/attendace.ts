export class AttendanceModel {
  Id: number;
  AttendanceDate: string;
  CorrectionRequest: string;
  Department: string;
  EmployeeId: number;
  EmployeeName: string;
  InTime: string;
  InTimeStatus: string;
  OutTimeStatus: String;
  OutTime: string;
  Pin: string;
  Shift: string;
  Status: string;
  SpentHours: string;
  IsMyAttendance: number;
}
