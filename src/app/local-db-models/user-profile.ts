export class UserProfileModel {
  EmployeeId: number;
  Pin: string;
  UserName: string;
  FirstName: string;
  LastName: string;
  MailId: string;
  Role: string;
  Shift: string;
  Gender: string;
  Location: string;
  Department: string;
  Designation: string;
  EmployeeType: string;
  JoinDate: string;
  Address: string;
  City: string;
  Country: string;
  Zipcode: string;
  PhoneNo: string;
  ProfilePic: string;
}
