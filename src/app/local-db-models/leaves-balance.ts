export class LeavesBalanceModel {
  LeaveTypeId: number;
  LeaveName: string;
  Balance: number;
}
