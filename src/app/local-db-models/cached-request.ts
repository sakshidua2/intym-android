export class CachedRequestModel {
  id: string;
  url: string;
  type: string;
  data: any;
  headers: any;
  time: number;
  dependentLocalRecordId: number;
  dependentPayload: string;
}
