export class HolidaysModel {
  Id: number;
  DayValue: string;
  HolidayDate: string;
  HolidayDesc: string;
  HolidayId: number;
  LocationName: string;
  Optional: boolean;
}
