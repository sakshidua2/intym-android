export class LeavesModel {
  Id: number;
  EmployeeId: string;
  EmpLeaveId: number;
  EmployeeName: string;
  Username: string;
  FromDate: string;
  ToDate: string;
  LeaveTypeId: number;
  LeaveType: string;
  StatusId: number;
  StatusName: string;
  Comments: string;
  Reason: string;
  AuthorizedBy: number;
  SupervisorName: string;
  IsDeleted: boolean;
  LeaveCount: string;
  AppliedDate: string;
  IsMyLeave: boolean;
}
