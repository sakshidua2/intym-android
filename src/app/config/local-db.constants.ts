export const LOCAL_DB_CONSTANTS = {
  DB_NAME: "in-tym-local.db",
  TBL_CACHED_REQUESTS: "tbl_cached_requests",
  TBL_USER_PROFILE: "tbl_user_profile",
  TBL_LEAVES: "tbl_leaves",
  TBL_LEAVES_BALANCE: "tbl_leaves_balance",
  TBL_ATTENDANCE: "tbl_attendance",
  TBL_ATTENDANCE_LOG: "tbl_attendance_log",
  TBL_HOLIDAYS_LIST: "tbl_holidays_list",
};
