export const NOTIFY_OBSERVABLE = {
  PROFILE_CHANGE: "profileChange",
  LOGOUT_ACTION: "logout_action",
  LOCALSTORAGE_UPDATED: "localstorage_updated",
  TEAM_VISITS_LIST: "team_visits_list",
  GET_REGULARIZATON: 'get-regularization',
  STOP_CAMERA: 'stop-camera',
  START_CAMERA: 'start-camera',
  GET_NOTIFICATION_COUNT: 'get-notification-count',
  NOTIFICATION_COUNT: 'notification-count',
  ADD_FCM_TOKEN: 'add-fcm-token'
};
