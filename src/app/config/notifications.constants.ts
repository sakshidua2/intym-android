export const NOTIFICATION_MESSAGE = {
  CustomerVisit: "Customer Visit- ",
  LeaveRequest: "Leave Request",
  AttendanceCorrection: "Attendance Correction Request- ",
  OutDuty: "OutDuty",
  InDuty: "InDuty",
  Report: "Report",
};

export const NOTIFICATION_SUPERVISOR_REDIRECT = {
  CustomerVisit: "/customer-visit-history/team-visit",
  LeaveRequest: "/leaves/team-leaves",
  AttendanceCorrection: "/attendance-history/team-attendance",
};

export const NOTIFICATION_EMPLOYEE_REDIRECT = {
  CustomerVisit: "/customer-visit-history/my-visit",
  LeaveRequest: "/leaves/my-leaves",
  AttendanceCorrection: "/attendance-history/my-attendance",
};
export const NOTIFICATION_LABLES = {
  LEAVE_REQUEST_RECEIVED: "You have received ##type## from ##user## on ##date##",
  REPORT_REQUEST_RECEIVED: "Please click here to download your report",
  REQUEST_RECEIVED: "You have received ##type## ##user## checked in/out ##cust## on ##date##",
  VISIT_REQUEST_PROCESSED:
    "Your ##type## with ##user## on ##date## at ##address## has been ##status##",
  OTHER_REQUEST_PROCESSED: "Your ##type## for ##date## has been ##status##",
};
