export const LABEL_CONSTANTS = {
  OFFLINE_CHECK_CONNECTION:
    "You are offline! Please check internet connection.",
  OFFLINE_NO_LOCAL_DATA: "You are offline and no data found for user locally",
  OFFLINE_INSUFF_DATA:
    "You are offline do not have sufficient data to apply for leave",
  EXPORT_SUCCESS:
    "Report is being generated. Please check notification screen to download report",
  ENABLE_LOCATION: "Unable to load map. Please try again",
};
