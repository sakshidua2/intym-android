import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { AlertController, ModalController, NavController, Platform } from "@ionic/angular";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";
import { AddUpdateFormPage } from '../add-update-form/add-update-form.page';
@Component({
  selector: "app-my-visit",
  templateUrl: "./my-visit.page.html",
  styleUrls: ["./my-visit.page.scss"],
})
export class MyVisitPage implements OnInit {
  public calendarComponentTypeMy = APP_CONSTANTS.DATA_TYPE_MY;
  userDetails: any;
  body: any;
  defaultFilterValue: any;
  public allowRemoteLogin = false;
  screenType: string;
  public salesVisitData: any;

  VisitsList: any[];
  public visits: any[];
  visitsCopy: any[];
  visitsFilterForm: FormGroup;
  filterForm: FormGroup;
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  attendanceFilter: { min: any; max: any };

  isMaxMonth: boolean;
  isMinMonth: boolean;
  public ATTENDANCE_STATUS_CHECK_IN = 3;
  public ATTENDANCE_STATUS_CHECK_OUT = 4;

  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  isSupervisor: boolean;
  public isLeavesListReadyToRender = false;
  public monthFormatter = APP_CONSTANTS.MONTH_FORMATED;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef,
    private alertCntrl: AlertController
  ) { }

  ngOnInit() {
    this.userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (this.userDetails && this.userDetails.Role === "Supervisor") {
      this.isSupervisor = true;
    }
    this.body = {};
    this.attendanceFilter = {
      min: "",
      max: "",
    };
    this.isMaxMonth = false;
    this.isMinMonth = false;
    this.attendanceFilter.max = this.coreService
      .getMomentNow()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    this.attendanceFilter.min = this.coreService.getMomentObject("2000-01-01");
    this.visitsFilterForm = this.fb.group({
      FromDate: [""],
      ToDate: [""],
      Status: ["all"],
    });

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }
    this.isLeavesListReadyToRender = false;
    if (this.userDetails) {
      try {
        this.allowRemoteLogin = this.userDetails.AllowRemoteLogin;
      } catch (error) {
        this.allowRemoteLogin = false;
      }
    } else {
      this.allowRemoteLogin = false;
    }
    this.body = {
      FromDate: this.coreService.getMomentNow(),
      ToDate: this.coreService.getMomentNow(),
    };
    this.isMaxMonth = true;
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
    };
    this.visits = [];
    this.visitsFilterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getSalesVisits();
    }
  }

  handleMyCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getSalesVisits();
      if (!payload.sameMonth) { this.getSalesVisitCalendar(); }
    }
  }

  getSalesVisitCalendar() {
    this.salesVisitData = {};
    this.apiService
      .getSalesVisitCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe((res: any) => {
        [...res].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          this.salesVisitData = {
            ...this.salesVisitData,
            [date]: {
              Total: o.Total,
              Productivity: o.Productivity,
              NonProductivity: o.NonProductivity,
            },
          };
          return true;
        });
      });
  }

  getSalesVisits(e?: any, StopLoader?: boolean) {
    this.apiService
      .getSalesVisits(
        {
          FromDate: this.body.FromDate.format(this.yearFormatted),
          ToDate: this.body.ToDate.format(this.yearFormatted),
          StopLoader,
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe(
        (res: any) => {
          console.log("res", res);
          this.visits = res;
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  async handleView(visit) {
    const modalRef = await this.modalCtrl.create({
      component: AddUpdateFormPage,
      cssClass: "custom-filter-modal",
      componentProps: {
        visitFilterForm: this.filterForm,
        defaultValues: this.defaultFilterValue,
        visitData: visit,
        isView: true,
        heading: "View Sales Visit"
      },
      mode: "md",
    });
    await modalRef.present();
  }

  async handleEdit(visit) {
    const modalRef = await this.modalCtrl.create({
      component: AddUpdateFormPage,
      cssClass: "custom-filter-modal",
      componentProps: {
        visitFilterForm: this.filterForm,
        defaultValues: this.defaultFilterValue,
        visitData: visit,
        heading: "Edit Sales Visit"
      },
      mode: "md",
    });
    await modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          this.apiService
            .updateSalesVisit({ ...res.data.salesVisit, SalesId: res.data.SalesId })
            .subscribe(
              (responce: any) => {
                this.body.FromDate = this.coreService.getMomentObject(res.data.dates.FromDate);
                this.body.ToDate = this.coreService.getMomentObject(res.data.dates.ToDate);
                this.body.CalendarFromDate = this.coreService.getMomentObject().set('date', 1).format(this.yearFormatted);
                this.body.CalendarToDate = this.coreService.getMomentObject().format(this.yearFormatted);
                this.coreService.displayToast("Sales Visit updated successfully.", "success");
                this.getSalesVisitCalendar();
                this.getSalesVisits();
              },
              (err) => {
                const message =
                  err &&
                    err.error &&
                    (err.error["Status"] ||
                      err.error["ErrorMessage"] ||
                      err.error["Message"])
                    ? err.error["Status"] ||
                    err.error["ErrorMessage"] ||
                    err.error["Message"]
                    : "Something went wrong! please try later.";
                this.coreService.displayToast(message, "danger");
              }
            );
        }
      }
    });
  }

  async handleDelete(id) {
    const ActionAlert = await this.alertCntrl.create({
      header: "Confirmation",
      message: "Are you sure to delete.",
      buttons: [
        {
          text: "No",
          role: "cancel",
        },
        {
          text: "Yes",
          handler: () => {
            this.deleteSalesVisit(id);
          },
        },
      ],
    });
    await ActionAlert.present();
  }

  deleteSalesVisit(id) {
    this.apiService
      .deleteSalesVisit(id)
      .subscribe(
        (res: any) => {
          this.coreService.displayToast(res, "success");
          this.getSalesVisits();
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }


  notificationRequest(data: { id: number; date: string }) {
    const date = this.coreService.getMomentObject(data.date);
    this.apiService
      .getAttendanceLog(
        {
          FromDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
          ToDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe((res: any) => {
        let list = [...res];
        list = list.filter(
          (ele: any) => ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
        );
        if (list && list.length) {
          this.navigate(list[0].visitDetails);
        }
      });
    history.state.data = undefined;
  }

  openNotificationPopup(notificationVisit: any) {
    if (notificationVisit) {
      const item: any = {
        EmployeeId: notificationVisit.EmployeeId,
        EmployeeName: notificationVisit.EmployeeName,
        checkInId: notificationVisit.EmpAttnTransactionId,
        customerAddress: notificationVisit.CustomerName,
        customerName: notificationVisit.CustomerAddress,
      };
      if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
      ) {
        item["checkInStatus"] = notificationVisit.ApprovalStatus;
        item["checkInTime"] = notificationVisit.TimeStamp;
        item["checkInTimeStamp"] = notificationVisit.TimeStamp;
        item["checkInId"] = notificationVisit.EmpAttnTransactionId;
      } else if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_OUT
      ) {
        item["checkOutStatus"] = notificationVisit.ApprovalStatus;
        item["checkOutTime"] = notificationVisit.TimeStamp;
        item["checkOutTimeStamp"] = notificationVisit.TimeStamp;
        item["checkOutId"] = notificationVisit.EmpAttnTransactionId;
      }
      this.navigate([item]);
    }
  }

  async navigate(item) {
    if (item && item.length) {
      this.navCntrl.navigateForward("/customer-visit-history/visit-by-date", {
        state: { data: item },
      });
    } else {
      this.coreService.displayToast(
        "There are no visits on that date.",
        "danger"
      );
    }
  }

  async addSales() {
    const modalRef = await this.modalCtrl.create({
      component: AddUpdateFormPage,
      cssClass: "custom-filter-modal",
      componentProps: {
        visitFilterForm: this.filterForm,
        defaultValues: this.defaultFilterValue,
        heading: "Add Sales Visit"
      },
      mode: "md",
    });
    await modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          this.apiService
            .createSalesVisit(res.data.salesVisit)
            .subscribe(
              (responce: any) => {
                this.body.FromDate = this.coreService.getMomentObject(res.data.dates.FromDate);
                this.body.ToDate = this.coreService.getMomentObject(res.data.dates.ToDate);
                this.body.CalendarFromDate = this.coreService.getMomentObject().set('date', 1).format(this.yearFormatted);
                this.body.CalendarToDate = this.coreService.getMomentObject().format(this.yearFormatted);
                this.coreService.displayToast("Sales Visit added successfully.", "success");
                this.getSalesVisitCalendar();
                this.getSalesVisits();
              },
              (err) => {
                const message =
                  err &&
                    err.error &&
                    (err.error["Status"] ||
                      err.error["ErrorMessage"] ||
                      err.error["Message"])
                    ? err.error["Status"] ||
                    err.error["ErrorMessage"] ||
                    err.error["Message"]
                    : "Something went wrong! please try later.";
                this.coreService.displayToast(message, "danger");
              }
            );

        }
      }
    });
  }

  isVisible(date) {
    const today = this.coreService.getMomentObject().set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    const tableDate = this.coreService.getMomentObject(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    if (tableDate.format() >= today.format()) {
      return true;
    }
    return false;
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getSalesVisitCalendar();
    }
  }

}
