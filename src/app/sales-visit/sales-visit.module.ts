import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalesVisitPageRoutingModule } from './sales-visit-routing.module';

import { SalesVisitPage } from './sales-visit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SalesVisitPageRoutingModule
  ],
  declarations: [SalesVisitPage]
})
export class SalesVisitPageModule { }
