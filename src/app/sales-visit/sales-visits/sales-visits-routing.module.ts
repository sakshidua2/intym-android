import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SalesVisitsPage } from "./sales-visits.page";

const routes: Routes = [
  {
    path: "",
    component: SalesVisitsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalesVisitsPageRoutingModule { }
