import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SalesVisitsPage } from './sales-visits.page';

describe('CustomerVisitsPage', () => {
  let component: SalesVisitsPage;
  let fixture: ComponentFixture<SalesVisitsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SalesVisitsPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SalesVisitsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
