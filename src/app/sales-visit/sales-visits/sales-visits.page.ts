import { Component, OnInit } from "@angular/core";
import { NavController, ModalController } from "@ionic/angular";
import { SettingsPage } from "../../settings/settings.page";
import { Router } from "@angular/router";
import { CoreService } from "../../services/core.service";
import { NOTIFY_OBSERVABLE } from "../../config/notify-observable.constants";
import { APP_CONSTANTS } from "../../config/app.constants";

@Component({
  selector: "app-sales-visits",
  templateUrl: "./sales-visits.page.html",
  styleUrls: ["./sales-visits.page.scss"],
})
export class SalesVisitsPage implements OnInit {
  public isSupervisor: boolean = false;
  visits: any[] = [];
  notificationCount: string | number = 0;
  constructor(
    private navcntrl: NavController,
    private modalController: ModalController,
    private router: Router,
    private coreService: CoreService,
  ) { }

  ngOnInit() {
    if (history.state.data) {
      this.visits = [...history.state["data"]];
      if (this.visits.length <= 0) {
        this.navcntrl.navigateRoot("/sales-visit-history/sales-visits");
      }
    } else {
      this.navcntrl.navigateRoot("/sales-visit-history/sales-visits");
    }
  }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
