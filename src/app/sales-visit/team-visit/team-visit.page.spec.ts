import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeamVisitPage } from './team-visit.page';

describe('TeamVisitPage', () => {
  let component: TeamVisitPage;
  let fixture: ComponentFixture<TeamVisitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamVisitPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TeamVisitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
