import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AlertController, ModalController, NavController, Platform } from "@ionic/angular";
import { VisitFilterComponent } from "../visit-filter/visit-filter.component";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";
import { NOTIFY_OBSERVABLE } from "src/app/config/notify-observable.constants";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";

@Component({
  selector: "app-team-visit",
  templateUrl: "./team-visit.page.html",
  styleUrls: ["./team-visit.page.scss"],
})
export class TeamVisitPage implements OnInit {
  public calendarComponentTypeTeam = APP_CONSTANTS.DATA_TYPE_TEAM;
  public ATTENDANCE_STATUS_CHECK_IN = 3;
  public ATTENDANCE_STATUS_CHECK_OUT = 4;
  public PENDING_APPROVAL_STATUS = 1;
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  body: any;
  public visits = [];
  visitsCopy = [];
  filterForm: FormGroup;
  usersList: any[];
  defaultFilterValue: any;
  screenType: string;
  public salesVisitData: any;

  isMaxMonth: boolean;
  isMinMonth: boolean;
  attendanceFilter: { min: any; max: any };
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public isLeavesListReadyToRender = false;

  public monthFormatter = APP_CONSTANTS.MONTH_FORMATED;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef,
    private alertCntrl: AlertController
  ) { }

  ngOnInit() {
    this.coreService.notifyObservable$.subscribe((res: any) => {
      if (res.option === NOTIFY_OBSERVABLE.TEAM_VISITS_LIST && res.value) {
        this.getSalesVisits();
      }
    });
    this.body = {};
    this.attendanceFilter = {
      min: "",
      max: "",
    };
    this.attendanceFilter.max = this.coreService
      .getMomentNow()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    this.attendanceFilter.min = this.coreService.getMomentObject("2000-01-01");
    this.filterForm = this.fb.group({
      User: [""],
    });

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }
    this.isLeavesListReadyToRender = false;
    this.body = {
      FromDate: this.coreService.getMomentNow(),
      ToDate: this.coreService.getMomentNow(),
    };
    this.isMaxMonth = true;
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
      User: 0,
    };
    this.isMaxMonth = true;
    this.filterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getSalesVisits();
    }
  }

  handleTeamCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getSalesVisits();
      if (!payload.sameMonth) { this.getSalesVisitCalendar(); }
    }
  }

  getSalesVisitCalendar() {
    this.salesVisitData = {};
    this.apiService
      .getSalesVisitCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          Filters: this.body.Filters,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe((res: any) => {
        [...res].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          this.salesVisitData = {
            ...this.salesVisitData,
            [date]: {
              Total: o.Total,
              Productivity: o.Productivity,
              NonProductivity: o.NonProductivity,
            },
          };
          return true;
        });
      });
  }

  getSalesVisits() {
    this.apiService
      .getSalesVisits(
        {
          FromDate: this.body.FromDate.format(this.yearFormatted),
          ToDate: this.body.ToDate.format(this.yearFormatted),
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe(
        (res: any) => {
          console.log("res", res);
          this.visits = res;
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  handleEdit() {
    console.log("edit clicked");
  }

  async handleDelete(id) {
    const ActionAlert = await this.alertCntrl.create({
      header: "Confirmation",
      message: "Are you sure to delete.",
      buttons: [
        {
          text: "No",
          role: "cancel",
        },
        {
          text: "Yes",
          handler: () => {
            this.deleteSalesVisit(id);
          },
        },
      ],
    });
    await ActionAlert.present();
  }

  deleteSalesVisit(id) {
    this.apiService
      .deleteSalesVisit(id)
      .subscribe(
        (res: any) => {
          this.coreService.displayToast(res, "success");
          this.getSalesVisits();
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  notificationRequest(data: { id: number; date: string }) {
    const date = this.coreService.getMomentObject(data.date);
    this.apiService
      .getAttendanceLog(
        {
          FromDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
          ToDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe((res: any) => {
        let list = [...res];
        list = list.filter(
          (ele: any) => ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
        );
        if (list && list.length) {
          this.navigate(list[0].employeeVisitDetails[0].visitDetails);
        }
        if (list && list.length) {
          this.navigate(list[0].employeeVisitDetails[0].visitDetails);
        }
      });
    history.state.data = undefined;
  }

  openNotificationPopup(notificationVisit: any) {
    if (notificationVisit) {
      const item: any = {
        EmployeeId: notificationVisit.EmployeeId,
        EmployeeName: notificationVisit.EmployeeName,
        checkInId: notificationVisit.EmpAttnTransactionId,
        customerAddress: notificationVisit.CustomerName,
        customerName: notificationVisit.CustomerAddress,
      };
      if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
      ) {
        item["checkInStatus"] = notificationVisit.ApprovalStatus;
        item["checkInTime"] = notificationVisit.TimeStamp;
        item["checkInTimeStamp"] = notificationVisit.TimeStamp;
        item["checkInId"] = notificationVisit.EmpAttnTransactionId;
      } else if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_OUT
      ) {
        item["checkOutStatus"] = notificationVisit.ApprovalStatus;
        item["checkOutTime"] = notificationVisit.TimeStamp;
        item["checkOutTimeStamp"] = notificationVisit.TimeStamp;
        item["checkOutId"] = notificationVisit.EmpAttnTransactionId;
      }
      this.navigate([item]);
    }
  }

  async navigate(item: any) {
    if (item.length) {
      this.navCntrl.navigateForward("/customer-visit-history/customer-visits", {
        state: { data: item },
      });
    } else {
      this.coreService.displayToast(
        "There are no visits on that date.",
        "danger"
      );
    }
  }

  async handleFilter() {
    const modalRef = await this.modalCtrl.create({
      component: VisitFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        visitFilterForm: this.filterForm,
        defaultValues: this.defaultFilterValue,
      },
      mode: "md",
    });
    await modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          this.body.FromDate = this.coreService.getMomentObject(res.data.dates.FromDate);
          this.body.ToDate = this.coreService.getMomentObject(res.data.dates.ToDate);
          this.body.CalendarFromDate = this.coreService.getMomentObject().set('date', 1).format(this.yearFormatted);
          this.body.CalendarToDate = this.coreService.getMomentObject().format(this.yearFormatted);
          this.getSalesVisitCalendar();
          this.getSalesVisits();
        }
      }
    });
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getSalesVisitCalendar();
    }
  }
}
