import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamVisitPageRoutingModule } from './team-visit-routing.module';
import { CustomCalendarComponent } from '../calendar/calendar.component';
import { VisitFilterComponent } from '../visit-filter/visit-filter.component';
import { TeamVisitPage } from './team-visit.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgCalendarModule } from 'ionic2-calendar';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NgCalendarModule,
    TeamVisitPageRoutingModule,
    SharedModule
  ],
  declarations: [TeamVisitPage, CustomCalendarComponent, VisitFilterComponent]
})
export class TeamVisitPageModule { }
