import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AddUpdateFormPage } from "./add-update-form.page";
import { SharedModule } from "src/app/shared/shared.module";
import { AddUpdateFormRoutingModule } from './add-update-form-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AddUpdateFormRoutingModule,
        ReactiveFormsModule,
        SharedModule,
    ],
    declarations: [
        AddUpdateFormPage,
    ],
})
export class AddUpdateFormModule { }
