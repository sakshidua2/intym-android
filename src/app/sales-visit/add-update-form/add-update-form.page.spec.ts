import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddUpdateFormPage } from './add-update-form.page';

describe('AddUpdateFormPage', () => {
  let component: AddUpdateFormPage;
  let fixture: ComponentFixture<AddUpdateFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddUpdateFormPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddUpdateFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
