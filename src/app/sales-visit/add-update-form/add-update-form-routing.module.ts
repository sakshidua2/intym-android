import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddUpdateFormPage } from './add-update-form.page';

const routes: Routes = [
  {
    path: '',
    component: AddUpdateFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddUpdateFormRoutingModule { }
