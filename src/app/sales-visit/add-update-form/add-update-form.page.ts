import { Component, OnInit, Input, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ModalController, AlertController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { ApiService } from 'src/app/services/api.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";

@Component({
  selector: "app-add-update-form",
  templateUrl: "./add-update-form.component.html",
  styleUrls: ["./add-update-form.component.scss"],
})
export class AddUpdateFormPage implements OnInit {
  salesVisitForm: FormGroup;
  userList: any[] = [];
  productList: any[] = [];
  correctItem: any;
  @Input() defaultValues: any;
  @Input() visitData: any;
  @Input() isView: boolean;
  @Input() heading: boolean;
  isSubmitted: boolean;
  isOrderCloser: boolean;
  isQuerySelected: boolean;
  isAddressPatternError: boolean;
  Coordinate = { Lat: '', Long: '' };
  filertUser: any;
  selectedProduct: string;
  minDate: any;
  constructor(
    public modalCtrl: ModalController,
    public coreService: CoreService,
    public apiService: ApiService,
    public networkService: NetworkService,
    private cdr: ChangeDetectorRef,
    private alertCtrl: AlertController,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.salesVisitForm = new FormGroup({
      InTime: new FormControl("", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      OutTime: new FormControl("", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      DealerName: new FormControl(this.visitData ? this.visitData.NameofSIorDealer : "", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      OwnerName: new FormControl(this.visitData ? this.visitData.OwnerName : "", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      ContactNumber: new FormControl(this.visitData ? this.visitData.ContactNumber : "", [
        Validators.required,
        Validators.pattern("[0-9 ]{10}"),
        Validators.minLength(10),
        Validators.maxLength(10),
      ]),
      Categories: new FormControl(this.visitData && this.visitData.SICategory25kplus ? "yes" : "no", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      Location: new FormControl(this.visitData ? this.visitData.Location : "", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      City: new FormControl(this.visitData ? this.visitData.City : "", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      Address: new FormControl(this.visitData ? this.visitData.CompleteAddress : "", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
        this.coreService.addressPatternValidation,
      ]),
      TSM_ASM: new FormControl(this.visitData && this.visitData.TSMorNSMAccomplished ? 'yes' : "no", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      MeetingType: new FormControl(this.visitData && this.visitData.MeetingType ? this.visitData.MeetingType : "System Integrator", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      OrderClosure: new FormControl(this.visitData && this.visitData.OrderClosure ? 'yes' : "no", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
      Product: new FormControl(this.visitData ? this.visitData.ProductId : "", [
        Validators.required,
      ]),
      QueryGenerated: new FormControl(this.visitData && this.visitData.QueryGenerated ? "yes" : "no"),
      ClosedOrderValue: new FormControl(this.visitData ? this.visitData.ClosedOrderValue : ""),
      ExpectedClosureValue: new FormControl(this.visitData ? this.visitData.ExpectedClosureValue : ""),
      ExpectedClosureDate: new FormControl(this.visitData ? this.visitData.ExpectedClosureDate : ""),
      RDName: new FormControl(this.visitData ? this.visitData.RDName : ""),
      ProductDescription: new FormControl(this.visitData ? this.visitData.ProjectDescription : ""),
      DiscussionPoints: new FormControl(this.visitData ? this.visitData.DiscussionPoints : ""),
    });

    this.correctItem = {
      InTime: this.coreService.getMomentNow(),
    };

    this.salesVisitForm
      .get("InTime")
      .setValue(
        this.visitData
          ? this.coreService.getMomentObject(this.visitData.CheckIn).format('HH:mm')
          : this.correctItem.InTime.format('HH:mm')
      );
    this.salesVisitForm
      .get("OutTime")
      .setValue(
        this.visitData
          ? this.coreService.getMomentObject(this.visitData.CheckOut).format('HH:mm')
          : this.correctItem.InTime.add(30, 'minutes').format('HH:mm')
      );
    this.minDate = this.coreService.getMomentNow().format('YYYY');
    this.isOrderCloser = this.visitData ? this.visitData.OrderClosure : false;
    this.isQuerySelected = this.visitData ? this.visitData.QueryGenerated : false;
    this.selectedProduct = this.visitData ? this.visitData.Product : "Select Product";
    this.handleOrderClosure(
      { detail: { value: this.visitData && this.visitData.OrderClosure ? 'yes' : "no" } }
    );
    this.handleQuerySelected(
      { detail: { value: this.visitData && this.visitData.QueryGenerated ? 'yes' : "no" } }
    );
    this.isAddressPatternError = false;
  }

  numberOnlyValidation(event: any) {
    const pattern = /^[0-9]*$/;
    if (!pattern.test(event.detail.value)) {
      // invalid character, prevent input
      event.preventDefault();
      this.salesVisitForm
        .get("ContactNumber")
        .setValue(event.detail.value.slice(0, -1));
      this.cdr.detectChanges();
      return false;
    }
  }

  async onContactNumberBlur(event: any) {
    const pattern = /^[0-9]*$/;
    if (pattern.test(event.target.value) && event.target.value.length === 10 && !this.isView) {
      this.apiService
        .getSalesvisitCustomerDataByContect(event.target.value)
        .subscribe(async (responce: any) => {
          console.log("responce", responce);
          if (responce && responce.ContectNumber) {
            this.salesVisitForm.get("DealerName").setValue(responce.NameofSIorDealer);
            // this.salesVisitForm.get("MeetingType").setValue(responce.CustomerType);
            this.salesVisitForm.get("OwnerName").setValue(responce.OwnerName);
            this.salesVisitForm.get("Address").setValue(responce.CompleteAddress);
            this.salesVisitForm.get("City").setValue(responce.City);
          } else {
            const exitAlert = await this.alertCtrl.create({
              header: "No Data Warning!",
              message: "Sales visit customer details not found, would you like to proceed and fill up new customer information?",
              buttons: [
                {
                  text: "Cancel",
                  cssClass: "secondary",
                  handler: () => {
                    this.navigate();
                  },
                }, {
                  text: "Ok",
                  role: "cancel",
                },
              ],
            });
            await exitAlert.present();
          }
        }, async (err) => {
          this.salesVisitForm.get("DealerName").setValue("");
          // this.salesVisitForm.get("MeetingType").setValue("System Integrator");
          this.salesVisitForm.get("OwnerName").setValue("");
          this.salesVisitForm.get("Address").setValue("");
          this.salesVisitForm.get("City").setValue("");
          const exitAlert = await this.alertCtrl.create({
            header: "No Data Warning!",
            message: "Sales visit customer details not found, would you like to proceed and fill up new customer information?",
            buttons: [
              {
                text: "Cancel",
                cssClass: "secondary",
                handler: () => {
                  this.navigate();
                },
              }, {
                text: "Ok",
                role: "cancel",
              },
            ],
          });
          await exitAlert.present();
        });
    }
  }

  ionViewDidEnter() {
    if (
      this.networkService.getCurrentNetworkStatus() !== ConnectionStatus.Offline
    ) {
      this.getProductList();
      this.checkGPSPermission();
      if (!this.isView && !this.visitData) {
        this.getCurrentPosition();
      }
    }
  }

  getCurrentPosition() {
    this.geolocation
      .getCurrentPosition()
      .then((res: any) => {
        this.apiService
          .getAddressByLatLong(`${res.coords.latitude},${res.coords.longitude}`)
          .subscribe((responce: any) => {
            if (responce
              && responce.addresses.length > 0
              && responce.addresses[0].address
              && responce.addresses[0].address.freeformAddress
            ) {
              this.Coordinate.Lat = res.coords.latitude;
              this.Coordinate.Long = res.coords.longitude;
              this.salesVisitForm
                .get("Location")
                .setValue(
                  this.visitData
                    ? this.visitData.Location
                    : responce.addresses[0].address.freeformAddress
                );
            }
          }, (err) => {
            console.log("error", err);
          });
      }).catch((err) => {
        console.log("error", err);
        this.coreService.displayToast("Please enable location to add a sales visit", "warning");
        this.navigate();
      });
  }

  checkGPSPermission() {
    this.androidPermissions
      .checkPermission(
        this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
      )
      .then(
        (result) => {
          console.log("result", result);
          if (result.hasPermission) {
            //If having permission show 'Turn On GPS' dialogue
            this.askToTurnOnGPS();
          } else {
            //If not having permission ask for permission
            this.requestGPSPermission();
          }
        },
        (err) => {
          console.log("err", err);
        }
      );
  }
  askToTurnOnGPS() {
    this.locationAccuracy
      .canRequest()
      .then((res) => {
        if (res) {
          this.locationAccuracy
            .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            .then(
              () => {
                // When GPS Turned ON call method to get Accurate location coordinates
                if (!this.isView && !this.visitData) {
                  this.getCurrentPosition();
                }
              },
              (error) => {
                this.coreService.displayToast("Please enable location to add a sales visit", "warning");
                this.navigate();
              }
              // alert('Error requesting location permissions ' + JSON.stringify(error))
            );
        }
      })
      .catch((err) => {
        console.log("gps is not available in this device");
      });
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions
          .requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
          )
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            (error) => {
              // this.requestGPSPermission();
              // console.log(error);

              //Show alert if user click on 'No Thanks'
              // alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  dateAdjust(type: string) {
    this.salesDetailCntrls.ExpectedClosureDate.
      setValue(this.coreService.getMomentObject(this.salesDetailCntrls.ExpectedClosureDate.value).toISOString());
  }


  getProductList() {
    this.apiService
      .getProductList()
      .subscribe(
        (res: any) => {
          this.productList = res;
        },
        (err) => {
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  handleOrderClosure(event) {
    this.isOrderCloser = event.detail.value === 'yes';
    this.salesVisitForm
      .get("QueryGenerated")
      .setValue("no");
    const ClosedOrderValueControl = this.salesVisitForm.get('ClosedOrderValue');
    const RDNameControl = this.salesVisitForm.get('RDName');
    const ProductDescriptionControl = this.salesVisitForm.get('ProductDescription');
    const ExpectedClosureValueControl = this.salesVisitForm.get('ExpectedClosureValue');
    const ExpectedClosureDateControl = this.salesVisitForm.get('ExpectedClosureDate');
    const DiscussionPointsControl = this.salesVisitForm.get('DiscussionPoints');

    if (this.isOrderCloser) {
      ClosedOrderValueControl.setValidators([Validators.required]);
      RDNameControl.setValidators([Validators.required]);
      ProductDescriptionControl.setValidators([Validators.required]);
      ExpectedClosureValueControl.setValidators(null);
      ExpectedClosureDateControl.setValidators(null);
      DiscussionPointsControl.setValidators(null);

    }
    if (!this.isOrderCloser) {
      ClosedOrderValueControl.setValidators(null);
      RDNameControl.setValidators(null);
      ProductDescriptionControl.setValidators(null);
      ExpectedClosureValueControl.setValidators(null);
      ExpectedClosureDateControl.setValidators(null);
      DiscussionPointsControl.setValidators(null);
    }
    ClosedOrderValueControl.updateValueAndValidity();
    RDNameControl.updateValueAndValidity();
    ProductDescriptionControl.updateValueAndValidity();
    ExpectedClosureValueControl.updateValueAndValidity();
    ExpectedClosureDateControl.updateValueAndValidity();
    DiscussionPointsControl.updateValueAndValidity();
    this.cdr.detectChanges();
  }

  handleQuerySelected(event) {
    this.isQuerySelected = event.detail.value === 'yes';
    const ExpectedClosureValueControl = this.salesVisitForm.get('ExpectedClosureValue');
    const ExpectedClosureDateControl = this.salesVisitForm.get('ExpectedClosureDate');
    const DiscussionPointsControl = this.salesVisitForm.get('DiscussionPoints');

    if (!this.isQuerySelected) {
      ExpectedClosureValueControl.setValidators(null);
      ExpectedClosureDateControl.setValidators(null);
      DiscussionPointsControl.setValidators(null);

    }
    if (this.isQuerySelected) {
      ExpectedClosureValueControl.setValidators([Validators.required]);
      ExpectedClosureDateControl.setValidators([Validators.required]);
      DiscussionPointsControl.setValidators([Validators.required]);
    }
    ExpectedClosureValueControl.updateValueAndValidity();
    ExpectedClosureDateControl.updateValueAndValidity();
    ExpectedClosureDateControl.setValue(this.coreService.getMomentObject());
    DiscussionPointsControl.updateValueAndValidity();
    this.cdr.detectChanges();
  }

  async timeAdjust(event, type) {
    if (type === 'in' && this.salesVisitForm.value.InTime) {
      this.salesVisitForm
        .get("OutTime")
        .setValue(
          this.coreService.getMomentObject(
            this.coreService.getMomentByTime(this.salesVisitForm.value.InTime).toString()
          ).add(30, 'minutes').format('HH:mm')
        );
    }
    if (type === 'out'
      && this.salesVisitForm.value.OutTime
      && this.salesVisitForm.value.InTime > this.salesVisitForm.value.OutTime
    ) {
      this.salesVisitForm
        .get("OutTime")
        .setValue(
          this.coreService.getMomentObject(
            this.coreService.getMomentByTime(this.salesVisitForm.value.InTime).toString()
          ).add(30, 'minutes').format('HH:mm')
        );
      this.coreService.displayToast("Out time should be more than In Time", "danger");
    }
  }


  dismissModal() {
    this.modalCtrl.dismiss(undefined, "close");
  }

  async onSubmit() {
    if (this.salesVisitForm.valid) {
      const data = this.salesVisitForm.getRawValue();
      // console.log(this.coreService.getMomentObject(data.InTime).format("HH:mm"));
      // console.log(this.coreService.getMomentByTime(this.coreService.getMomentObject(data.InTime).format("HH:mm")));
      const salesVisit = {
        CheckIn: data.InTime,
        CheckOut: data.OutTime,
        NameofSIorDealer: data.DealerName,
        OwnerName: data.OwnerName,
        ContactNumber: data.ContactNumber,
        SICategory25kplus: data.Categories === 'yes',
        Location: data.Location,
        City: data.City,
        CompleteAddress: data.Address,
        TSMorNSMAccomplished: data.TSM_ASM === 'yes',
        OrderClosure: data.OrderClosure === 'yes',
        ProductId: data.Product,
        MeetingType: data.MeetingType,
        Lat: this.Coordinate.Lat,
        Long: this.Coordinate.Long,
      };

      if (data.OrderClosure === 'yes') {
        salesVisit['ClosedOrderValue'] = data.ClosedOrderValue;
        salesVisit['RDName'] = data.RDName;
        salesVisit['ProjectDescription'] = data.ProductDescription;
      } else {
        salesVisit['QueryGenerated'] = data.QueryGenerated === 'yes';
        if (data.QueryGenerated === 'yes') {
          salesVisit['ExpectedClosureValue'] = data.ExpectedClosureValue;
          salesVisit['ExpectedClosureDate'] = this.coreService.getMomentObject(data.ExpectedClosureDate).set({ hour: 12, minute: 0 });
          salesVisit['DiscussionPoints'] = data.DiscussionPoints;
        }
      }
      console.log(salesVisit);
      this.modalCtrl.dismiss({ salesVisit, dates: this.defaultValues, SalesId: this.visitData ? this.visitData.ID : 0 }, "apply");
    } else {
      this.coreService.displayToast(
        "firstly fill all required filled",
        "danger"
      );
    }
  }

  get salesDetailCntrls() {
    return this.salesVisitForm.controls;
  }

  navigate() {
    this.salesVisitForm.reset();
    this.modalCtrl.dismiss();
  }
}
