import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: "app-visit-filter",
  templateUrl: "./visit-filter.component.html",
  styleUrls: ["./visit-filter.component.scss"],
})
export class VisitFilterComponent implements OnInit {
  @Input() visitFilterForm: FormGroup;
  userList: any[] = [];
  @Input() defaultValues: any;
  isSubmitted: boolean;
  filertUser: any;
  constructor(
    public modalCtrl: ModalController,
    public coreService: CoreService,
    public apiService: ApiService
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.getEmployeeList();
  }

  get fControls() {
    return this.visitFilterForm.controls;
  }

  getEmployeeList() {
    this.apiService.getEmployeeList().subscribe((res: any) => {
      if (res.L1) {
        this.userList = this.userList.concat(res.L1);
      }
      if (res.L2) {
        this.userList = this.userList.concat(res.L2);
      }
      if (res.L3) {
        this.userList = this.userList.concat(res.L3);
      }
    });
  }

  dismissModal() {
    this.modalCtrl.dismiss(undefined, "close");
  }

  onSubmit() {
    if (this.visitFilterForm.valid) {
      const data = this.visitFilterForm.getRawValue();
      const canEmit = Object.values(data).filter((ele) => ele);
      if (canEmit && canEmit.length) {
        this.modalCtrl.dismiss({ dates: this.defaultValues }, "apply");
        return;
      }
      this.modalCtrl.dismiss(undefined, "apply");
    }
  }

  resetValue() {
    this.visitFilterForm.reset();
    this.visitFilterForm.patchValue(this.defaultValues);
    this.onSubmit();
  }
}
