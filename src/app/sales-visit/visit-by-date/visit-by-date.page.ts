import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-visit-by-date",
  templateUrl: "./visit-by-date.page.html",
  styleUrls: ["./visit-by-date.page.scss"],
})
export class VisitByDatePage implements OnInit {
  public visits: any = [];
  constructor(private navcntrl: NavController) { }

  ngOnInit() {
    if (history.state.data) {
      this.visits = [...history.state["data"]];
      if (this.visits.length <= 0) {
        this.navcntrl.navigateRoot("/customer-visit-history/team-visit");
      }
    } else {
      this.navcntrl.navigateRoot("/customer-visit-history/team-visit");
    }
  }
}
