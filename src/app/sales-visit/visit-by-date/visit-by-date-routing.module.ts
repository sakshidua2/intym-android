import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitByDatePage } from './visit-by-date.page';

const routes: Routes = [
  {
    path: '',
    component: VisitByDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisitByDatePageRoutingModule {}
