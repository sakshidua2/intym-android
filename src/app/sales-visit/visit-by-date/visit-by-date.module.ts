import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisitByDatePageRoutingModule } from './visit-by-date-routing.module';

import { VisitByDatePage } from './visit-by-date.page';
import { VisitListItemBlockComponent } from 'src/app/components/visit-list-item-block/visit-list-item-block.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisitByDatePageRoutingModule,
    SharedModule
  ],
  declarations: [VisitByDatePage, VisitListItemBlockComponent]
})
export class VisitByDatePageModule {}
