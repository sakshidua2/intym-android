import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SalesVisitPage } from './sales-visit.page';

describe('SalesVisitPage', () => {
  let component: SalesVisitPage;
  let fixture: ComponentFixture<SalesVisitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SalesVisitPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SalesVisitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
