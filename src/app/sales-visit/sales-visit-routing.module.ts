import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesVisitPage } from './sales-visit.page';
import { VisitsGuard } from '../guard/visits.guard';

const routes: Routes = [
  {
    path: '',
    component: SalesVisitPage,
    children: [
      {
        path: '',
        redirectTo: 'my-visit',
        pathMatch: 'full'
      },
      {
        path: 'my-visit',
        children: [
          {
            path: '',
            loadChildren: () => import('./my-visit/my-visit.module').then(m => m.MyVisitPageModule)
          }
        ],
        data: { isMyVisit: true },
        canActivate: [VisitsGuard]
      },
      {
        path: 'team-visit',
        children: [
          {
            path: '',
            loadChildren: () => import('./team-visit/team-visit.module').then(m => m.TeamVisitPageModule)
          }
        ],
        data: { isMyVisit: false },
        canActivate: [VisitsGuard]
      }
    ]
  },
  {
    path: 'visit-by-date',
    loadChildren: () => import('./visit-by-date/visit-by-date.module').then(m => m.VisitByDatePageModule)
  },
  {
    path: 'customer-visits',
    loadChildren: () => import('./sales-visits/sales-visits.module').then(m => m.SalesVisitsPageModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./add-update-form/add-update-form.module').then(m => m.AddUpdateFormModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalesVisitPageRoutingModule { }
