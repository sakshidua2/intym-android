import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StaticContentPage } from './static-content.page';

const routes: Routes = [
  {
    path: '',
    component: StaticContentPage,
    children: [
      {
        path: 'eula',
        children: [
          {
            path: '',
            loadChildren: () => import('./eula/eula.module').then(m => m.EulaPageModule)
          }
        ]
      },
      {
        path: 'privacy',
        children: [
          {
            path: '',
            loadChildren: () => import('./privacy/privacy.module').then(m => m.PrivacyPageModule)
          }
        ]
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaticContentPageRoutingModule { }
