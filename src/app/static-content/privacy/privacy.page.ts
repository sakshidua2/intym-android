import { Component, OnInit } from "@angular/core";
import { CoreService } from "src/app/services/core.service";
import { ApiService } from "src/app/services/api.service";

@Component({
  selector: "app-privacy",
  templateUrl: "./privacy.page.html",
  styleUrls: ["./privacy.page.scss"],
})
export class PrivacyPage implements OnInit {
  public pageTitle = "";
  public title = "";
  public desc = "";
  public date = "";
  constructor(
    private coreService: CoreService,
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.getPageContent();
  }

  getPageContent() {
    this.apiService.getStaticPageContent().subscribe(
      (res: any) => {
        const pageDetail = res.find(d => {
          return 'Privacy' === d.pagetype;
        });
        this.title = pageDetail.Description.Tile;
        this.desc = pageDetail.Description.Body.replace(/\n/g, "<br />");
        this.date = pageDetail.Description.EffectiveDate;
      },
      (err) => {
        const message =
          err &&
            err.error &&
            (err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"] ||
              err.error["MessageDetail"])
            ? err.error["MessageDetail"]
              ? err.error["MessageDetail"]
              : err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  ionViewDidEnter() { }

}
