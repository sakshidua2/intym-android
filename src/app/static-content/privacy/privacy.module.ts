import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PrivacyPageRoutingModule } from "./privacy-routing.module";

import { PrivacyPage } from "./privacy.page";
import { SharedModule } from "src/app/shared/shared.module";
import { DateTimeFormatPipe } from "src/app/pipes/date-pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivacyPageRoutingModule,
    SharedModule,
  ],
  declarations: [PrivacyPage],
  providers: [DatePipe, DateTimeFormatPipe],
})
export class PrivacyPageModule { }
