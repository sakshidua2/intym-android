import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { EulaPageRoutingModule } from "./eula-routing.module";

import { EulaPage } from "./eula.page";
import { SharedModule } from "src/app/shared/shared.module";
import { DateTimeFormatPipe } from "src/app/pipes/date-pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EulaPageRoutingModule,
    SharedModule,
  ],
  declarations: [EulaPage],
  providers: [DatePipe, DateTimeFormatPipe],
})
export class EulaPageModule { }
