import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";
import { APP_CONSTANTS } from "../config/app.constants";

@Component({
  selector: "app-static-content",
  templateUrl: "./static-content.page.html",
  styleUrls: ["./static-content.page.scss"],
})
export class StaticContentPage implements OnInit {
  public pageTitle = "";
  public title = "";
  public desc = "";
  public url = "";
  public phone = "";
  public phonenum = "";
  public email = "";
  public date = "";
  isTermPage = false;
  notificationCount: string | number = 0;
  isSupervisor: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private coreService: CoreService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.activatedRoute.queryParams.subscribe(
      (params) => {
        this.resetData();
        if (params.pageId) {
          this.getPageContent(params.pageId);
        } else {
          this.pageTitle = "Legal & EULA";
          this.isTermPage = true;
        }
      },
      (error) => {
        this.router.navigate(["/dashboard"], { replaceUrl: true });
      }
    );
    const userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (userDetails && ["Admin", "Supervisor"].includes(userDetails.Role)) {
      this.isSupervisor = true;
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  getPageContent(pageId) {
    this.apiService.getStaticPageContent().subscribe(
      (res) => {
        this.setPageContent(res, pageId);
      },
      (err) => {
        const message =
          err &&
          err.error &&
          (err.error["Status"] ||
            err.error["ErrorMessage"] ||
            err.error["Message"] ||
            err.error["MessageDetail"])
            ? err.error["MessageDetail"]
              ? err.error["MessageDetail"]
              : err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
        this.router.navigate(["/dashboard"], { replaceUrl: true });
      }
    );
  }

  async setPageContent(data, pageId) {
    let pageDetail = null;
    switch (pageId) {
      case "about-us":
        pageDetail = await this.getPageDescription(data, "AboutUs");
        this.pageTitle = "About Us";
        this.title = pageDetail.CompanyName;
        this.desc = pageDetail.Details.replace(/\n/g, "<br />");
        this.url = pageDetail.Website;
        break;
      case "contact-us":
        pageDetail = await this.getPageDescription(data, "ContactUs");
        this.pageTitle = "Contact Us";
        this.title = pageDetail.CompanyName;
        this.phone = pageDetail.PhoneNo;
        this.phonenum = pageDetail.PhoneNo.split(" : ")[1];
        this.email = pageDetail.Email;
        this.url = pageDetail.SurveyLink;
        break;
      case "help":
        this.pageTitle = "Help & Feedback";
        break;
      default:
        this.pageTitle = "";
        break;
    }
  }

  getPageDescription(data, page) {
    const pageDetail = data.find((d) => {
      return page === d.pagetype;
    });
    return pageDetail.Description;
  }

  resetData() {
    this.pageTitle = "";
    this.title = "";
    this.desc = "";
    this.url = "";
    this.phone = "";
    this.phonenum = "";
    this.email = "";
    this.date = "";
    this.isTermPage = false;
  }

  ionViewDidLeave() {
    this.resetData();
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
