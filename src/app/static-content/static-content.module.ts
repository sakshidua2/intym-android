import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StaticContentPageRoutingModule } from './static-content-routing.module';

import { StaticContentPage } from './static-content.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    StaticContentPageRoutingModule
  ],
  declarations: [StaticContentPage]
})
export class StaticContentPageModule { }
