import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StaticContentPage } from './static-content.page';

describe('StaticContentPage', () => {
  let component: StaticContentPage;
  let fixture: ComponentFixture<StaticContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticContentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StaticContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
