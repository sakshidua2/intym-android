import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { APP_CONSTANTS } from '../config/app.constants';
import * as moment from 'moment';

@Pipe({
    name: 'timeFormat',
})
export class TimeFormatPipe extends DatePipe implements PipeTransform {
    transform(value: any, format?: 'time24' | 'time12') {
        const formatter = { time12: APP_CONSTANTS.TIME_12_FORMAT, time24: APP_CONSTANTS.TIME_24_FORMAT };
        if (value && value !== '-') {
            try {
                const momentObj = this.getMomentByDate(value);
                if (momentObj) {
                    return super.transform(momentObj, formatter[format]);
                }
                return value;
            } catch (error) {
                return value;
            }
        }
        return value;
    }

    private getMomentByDate(time: string) {
        const timeArr = time.split(':');
        const hours = timeArr[0] ? parseInt(timeArr[0]) : 0;
        const mins = timeArr[1] ? parseInt(timeArr[1]) : 0;
        return moment().hour(hours).minute(mins);
    }
}
