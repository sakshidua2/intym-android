import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { APP_CONSTANTS } from '../config/app.constants';
import * as moment from 'moment';

@Pipe({
    name: 'dateTimeFormat',
})
export class DateTimeFormatPipe extends DatePipe implements PipeTransform {
    transform(value: any, format?: 'date' | 'dateTime' | 'time12' | 'time24') {
        if (value && value !== '-') {
            const formatter = {
                date: APP_CONSTANTS.DATE_FORMAT,
                dateTime: APP_CONSTANTS.DATE_TIME_FORMAT,
                time12: APP_CONSTANTS.TIME_12_FORMAT,
                time24: APP_CONSTANTS.TIME_24_FORMAT
            };
            return super.transform(this.getMomentByDate(value), formatter[format]);
        }
        return value;
    }

    private getMomentByDate(date) {
        if (typeof date === 'string' && date.search(/[zZ]/g) < 0) {
            date += 'Z';
        }
        return moment(date);
    }
}
