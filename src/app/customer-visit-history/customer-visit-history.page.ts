import { Component, OnInit } from "@angular/core";
import { APP_CONSTANTS } from "../config/app.constants";
import { Router } from "@angular/router";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-customer-visit-history",
  templateUrl: "./customer-visit-history.page.html",
  styleUrls: ["./customer-visit-history.page.scss"],
})
export class CustomerVisitHistoryPage implements OnInit {
  public isSupervisor: boolean;
  public allowRemoteLogin: boolean;
  title: string;
  notificationCount: string | number = 0;
  constructor(
    private router: Router,
    private coreService: CoreService,
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.allowRemoteLogin = user.AllowRemoteLogin;
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
      this.title = this.allowRemoteLogin
        ? "Customer Visits"
        : "Team Visits";
    } else {
      this.isSupervisor = false;
      this.title = "Customer Visits";
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }
  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
