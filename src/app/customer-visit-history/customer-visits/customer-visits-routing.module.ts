import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CustomerVisitsPage } from "./customer-visits.page";

const routes: Routes = [
  {
    path: "",
    component: CustomerVisitsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerVisitsPageRoutingModule { }
