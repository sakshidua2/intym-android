import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerVisitsPageRoutingModule } from './customer-visits-routing.module';

import { CustomerVisitsPage } from './customer-visits.page';
import { VisitListItemBlockComponent } from 'src/app/components/visit-list-item-block/visit-list-item-block.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CustomerVisitsPageRoutingModule,
    SharedModule
  ],
  declarations: [CustomerVisitsPage, VisitListItemBlockComponent]
})
export class CustomerVisitsPageModule { }
