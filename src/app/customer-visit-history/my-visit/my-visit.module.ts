import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { MyVisitPageRoutingModule } from "./my-visit-routing.module";
import { CustomCalendarComponent } from "../calendar/calendar.component";
import { VisitListItemBlockComponent } from "src/app/components/visit-list-item-block/visit-list-item-block.component";

import { MyVisitPage } from "./my-visit.page";
import { SharedModule } from "src/app/shared/shared.module";
import { NgCalendarModule } from "ionic2-calendar";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyVisitPageRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    NgCalendarModule,
  ],
  declarations: [
    MyVisitPage,
    CustomCalendarComponent,
    VisitListItemBlockComponent,
  ],
})
export class MyVisitPageModule {}
