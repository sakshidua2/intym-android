import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { ModalController, NavController, Platform } from "@ionic/angular";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { LeaveFilterComponent } from "src/app/leaves/leave-filter/leave-filter.component";
import { AttendanceLogService } from "../../services/db/attendance-log.service";
import { AttendanceLogModel } from "src/app/local-db-models/attendace_log";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";
@Component({
  selector: "app-my-visit",
  templateUrl: "./my-visit.page.html",
  styleUrls: ["./my-visit.page.scss"],
})
export class MyVisitPage implements OnInit {
  public calendarComponentTypeMy = APP_CONSTANTS.DATA_TYPE_MY;
  userDetails: any;
  body: any;
  defaultFilterValue: any;
  public allowRemoteLogin = false;
  screenType: string;
  public customerVisitData: any;

  VisitsList: any[];
  public visits: any[];
  visitsCopy: any[];
  visitsFilterForm: FormGroup;
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  attendanceFilter: { min: any; max: any };

  isMaxMonth: boolean;
  isMinMonth: boolean;
  public ATTENDANCE_STATUS_CHECK_IN = 3;
  public ATTENDANCE_STATUS_CHECK_OUT = 4;

  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  isSupervisor: boolean;
  public isLeavesListReadyToRender = false;
  public monthFormatter = APP_CONSTANTS.MONTH_FORMATED;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private attendanceLogService: AttendanceLogService,
    private networkService: NetworkService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (this.userDetails && this.userDetails.Role === "Supervisor") {
      this.isSupervisor = true;
    }
    this.body = {};
    this.attendanceFilter = {
      min: "",
      max: "",
    };
    this.isMaxMonth = false;
    this.isMinMonth = false;
    this.attendanceFilter.max = this.coreService
      .getMomentNow()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    this.attendanceFilter.min = this.coreService.getMomentObject("2000-01-01");
    this.visitsFilterForm = this.fb.group({
      FromDate: [""],
      ToDate: [""],
      Status: ["all"],
    });

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }
    this.isLeavesListReadyToRender = false;
    if (this.userDetails) {
      try {
        this.allowRemoteLogin = this.userDetails.AllowRemoteLogin;
      } catch (error) {
        this.allowRemoteLogin = false;
      }
    } else {
      this.allowRemoteLogin = false;
    }
    this.body = {
      FromDate: this.coreService.getMomentNow(),
      ToDate: this.coreService.getMomentNow(),
    };
    this.isMaxMonth = true;
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
      Status: "all",
    };
    this.visits = [];
    this.visitsFilterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal();
    } else {
      this.getVisits();
    }
  }

  handleMyCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal();
    } else {
      this.getVisits(null, false);
      if (!payload.sameMonth) { this.getCustomerVisitCalendar(); }
    }
  }

  getCustomerVisitCalendar() {
    this.customerVisitData = {};
    this.apiService
      .getCustomerVisitCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe((res: any) => {
        [...res.data].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          if (
            date === this.coreService.getMomentFormatDate(this.yearFormatted)
          ) {
            this.customerVisitData = {
              ...this.customerVisitData,
              [date]: { InMeeting: o.InMeeting, DoneMeeting: o.DoneMeeting },
            };
          } else {
            this.customerVisitData = {
              ...this.customerVisitData,
              [date]: {
                ApprovedVisits: o.ApprovedVisits,
                RejectedVisits: o.RejectedVisits,
                TotalVisits: o.TotalVisits,
              },
            };
          }
          return true;
        });
      });
  }

  getVisits(e?: any, StopLoader?: boolean) {
    this.apiService
      .getAttendanceLog(
        {
          FromDate: this.body.FromDate.format(this.yearFormatted),
          ToDate: this.body.ToDate.format(this.yearFormatted),
          StopLoader,
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe(
        (res: any) => {
          const isMyAttendanceLog = true;
          const rawData = [...res].map((o) => ({
            ...o,
            IsMyAttendanceLog: isMyAttendanceLog,
          }));
          const result = this.processVisitsRawData([...rawData]);
          this.visits = result;
          this.visitsCopy = result;
          const ToDate = this.body.ToDate;
          ToDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          if (ToDate.isSame(this.attendanceFilter.max)) {
            this.isMaxMonth = true;
          } else {
            this.isMaxMonth = false;
          }
          const FromDate = this.body.FromDate;
          FromDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          if (FromDate.isSame(this.attendanceFilter.min)) {
            this.isMinMonth = true;
          } else {
            this.isMinMonth = false;
          }
          if (this.body.Status && this.body.Status !== "all") {
            this.visits.forEach(({ visitDetails }, i) => {
              if (visitDetails && visitDetails.length) {
                this.visits[i].filterVisitDetails = this.visits[
                  i
                ].visitDetails.filter(
                  (ele) =>
                    ele.checkInStatus === this.STATUS_CODES[this.body.Status] ||
                    ele.checkOutStatus === this.STATUS_CODES[this.body.Status]
                );
              }
            });
            this.visits = this.visits.filter(
              ({ filterVisitDetails }) =>
                filterVisitDetails && filterVisitDetails.length > 0
            );
          }
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
          this.isLeavesListReadyToRender = true;
          if (history.state.data) {
            const notificationData = history.state["data"];
            const date = this.coreService.getMomentObject(
              notificationData.date
            );
            setTimeout(() => {
              if (
                date.isSame(this.body.FromData, "month") &&
                date.isSame(this.body.FromData, "year")
              ) {
                let list = JSON.parse(JSON.stringify(this.visits));
                list = list.filter(
                  (ele: any) =>
                    ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
                );
                if (list && list.length) {
                  this.navigate(list[0].visitDetails);
                }
              } else {
                this.notificationRequest(notificationData);
              }
              history.state.data = undefined;
            }, 500);
          }

          if (this.platform.is("cordova")) {
            this.attendanceLogService.storeAttendanceLogForOffline(
              [...rawData],
              isMyAttendanceLog
            );
          }
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  notificationRequest(data: { id: number; date: string }) {
    const date = this.coreService.getMomentObject(data.date);
    this.apiService
      .getAttendanceLog(
        {
          FromDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
          ToDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe((res: any) => {
        let list = [...res];
        list = list.filter(
          (ele: any) => ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
        );
        if (list && list.length) {
          this.navigate(list[0].visitDetails);
        }
      });
    history.state.data = undefined;
  }

  openNotificationPopup(notificationVisit: any) {
    if (notificationVisit) {
      const item: any = {
        EmployeeId: notificationVisit.EmployeeId,
        EmployeeName: notificationVisit.EmployeeName,
        checkInId: notificationVisit.EmpAttnTransactionId,
        customerAddress: notificationVisit.CustomerName,
        customerName: notificationVisit.CustomerAddress,
      };
      if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
      ) {
        item["checkInStatus"] = notificationVisit.ApprovalStatus;
        item["checkInTime"] = notificationVisit.TimeStamp;
        item["checkInTimeStamp"] = notificationVisit.TimeStamp;
        item["checkInId"] = notificationVisit.EmpAttnTransactionId;
      } else if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_OUT
      ) {
        item["checkOutStatus"] = notificationVisit.ApprovalStatus;
        item["checkOutTime"] = notificationVisit.TimeStamp;
        item["checkOutTimeStamp"] = notificationVisit.TimeStamp;
        item["checkOutId"] = notificationVisit.EmpAttnTransactionId;
      }
      this.navigate([item]);
    }
  }
  getAttendanceLogLocal(e?: any) {
    // this.coreService.showLoader();
    const fromDate = this.coreService.getMomentObject(this.body.FromDate);
    const toDate = this.coreService.getMomentObject(this.body.ToDate);
    toDate.add(1, "day");
    const params = {
      FromDate: fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      IsMyAttendanceLog: true,
    };
    this.attendanceLogService
      .getData(params)
      .then((res: AttendanceLogModel[]) => {
        const ToDate = this.body.ToDate;
        ToDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        if (ToDate.isSame(this.attendanceFilter.max)) {
          this.isMaxMonth = true;
        } else {
          this.isMaxMonth = false;
        }
        const FromDate = this.body.FromDate;
        FromDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        if (FromDate.isSame(this.attendanceFilter.min)) {
          this.isMinMonth = true;
        } else {
          this.isMinMonth = false;
        }
        const result = this.processVisitsRawData([...res]);
        this.visits = result;
        this.visitsCopy = result;
        if (this.body.Status && this.body.Status !== "all") {
          this.visits.forEach(({ visitDetails }, i) => {
            if (visitDetails && visitDetails.length) {
              this.visits[i].filterVisitDetails = this.visits[
                i
              ].visitDetails.filter(
                (ele) =>
                  ele.checkInStatus === this.STATUS_CODES[this.body.Status] ||
                  ele.checkOutStatus === this.STATUS_CODES[this.body.Status]
              );
            }
          });
          this.visits = this.visits.filter(
            ({ filterVisitDetails }) =>
              filterVisitDetails && filterVisitDetails.length > 0
          );
        }
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
      })
      .catch((err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_NO_LOCAL_DATA,
          "warning"
        );
      });
  }

  processVisitsRawData(rawData) {
    const visitsArr = [
      this.ATTENDANCE_STATUS_CHECK_IN,
      this.ATTENDANCE_STATUS_CHECK_OUT,
    ];
    const filteredVisits = rawData.filter((o) =>
      visitsArr.includes(o.AttendanceStatus)
    );
    filteredVisits.sort(
      (a, b) =>
        this.coreService.getMomentByDate(a.TimeStamp).unix() -
        this.coreService.getMomentByDate(b.TimeStamp).unix()
    );
    const dateRange = this.coreService.getMomentDatesRange(
      this.body.FromDate,
      this.body.ToDate
    );
    const dateRangeLength = dateRange.length;
    const filteredVisitsLength = filteredVisits.length;
    const result = [];
    for (let index = 0; index < dateRangeLength; index++) {
      const rangeDate = dateRange[index];
      const visitDetails = [];
      let totalVisits = 0;
      let totalMins = 0;
      for (
        let filterIndex = 0;
        filterIndex < filteredVisitsLength;
        filterIndex++
      ) {
        const filterObj = filteredVisits[filterIndex];
        const foramtedFilterDate = this.coreService
          .getMomentByDate(filterObj.TimeStamp)
          .format(APP_CONSTANTS.DATE_FORMATER);
        if (rangeDate === foramtedFilterDate) {
          const customerName = filterObj.CustomerName;
          const customerIndex = visitDetails.findIndex(
            (o) =>
              o.customerName.toLocaleLowerCase() ===
              customerName.toLocaleLowerCase()
          );
          if (
            customerIndex < 0 ||
            filterObj.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
          ) {
            const visitDetail = {
              checkInId: filterObj.EmpAttnTransactionId,
              checkOutId: 0,
              customerName,
              customerAddress: filterObj.CustomerAddress,
              checkInTimeStamp: filterObj.TimeStamp,
              checkOutTimeStamp: "",
              checkInTime: filterObj.TimeStamp,
              checkOutTime: "",
              checkInStatus: filterObj.ApprovalStatus,
              checkOutStatus: 0,
              spentHrs: 0,
              spentMins: 0,
              spentTime: "",
              EmployeeId: filterObj.EmployeeId,
              EmployeeName: filterObj.EmployeeName,
            };
            visitDetails.push(visitDetail);
            totalVisits++;
          } else {
            const customerIndexForPunchOut = visitDetails.findIndex(
              (o) =>
                o.customerName.toLocaleLowerCase() ===
                customerName.toLocaleLowerCase() && o.checkOutTime === ""
            );
            if (customerIndexForPunchOut >= 0) {
              const customerExistingObj =
                visitDetails[customerIndexForPunchOut];
              customerExistingObj.checkOutId = filterObj.EmpAttnTransactionId;
              customerExistingObj.checkOutTimeStamp = filterObj.TimeStamp;
              customerExistingObj.checkOutTime = filterObj.TimeStamp;
              customerExistingObj.checkOutStatus = filterObj.ApprovalStatus;
              const spentHours = this.coreService.calculateMomentDateDiff(
                customerExistingObj.checkInTimeStamp,
                customerExistingObj.checkOutTimeStamp
              );
              customerExistingObj.spentHrs = spentHours.hours;
              customerExistingObj.spentMins = spentHours.minutes;
              customerExistingObj.spentTime = spentHours.label;
              customerExistingObj["momentLabel"] = spentHours;
              visitDetails[customerIndexForPunchOut] = customerExistingObj;
              totalMins += spentHours.hours * 60 + spentHours.minutes;
            }
          }
        }
      }
      const spentHrs = Math.floor(totalMins / 60);
      const spentMins = Math.floor(totalMins % 60);
      const overview = {
        date: rangeDate,
        visits: totalVisits,
        spentHrs,
        spentMins,
        spentTime: `${spentHrs}h ${spentMins}min`,
        visitDetails,
      };
      result.push(overview);
    }
    return result.filter(
      ({ visitDetails }) => visitDetails && visitDetails.length
    );
  }

  nextMonthAttend() {
    if (this.isMaxMonth) {
      return;
    }
    const date = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    date.add(1, "M");
    date.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });
    this.body.Status = "all";
    if (this.attendanceFilter.max.unix() > date.unix()) {
      if (date.month() - this.body.FromDate.month() !== 1) {
        date.subtract(1, "M");
      }
      date.startOf("month");
      this.body.FromDate = this.coreService.getMomentByDate(date.toISOString());
      date.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(date.toISOString());

      this.visitsFilterForm.patchValue({
        FromDate: this.body.FromDate.toISOString(),
        ToDate: this.body.ToDate.toISOString(),
        Status: this.body.Status,
      });
      this.isMinMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAttendanceLogLocal();
      } else {
        this.getVisits();
      }
    } else {
      const max = this.coreService.getMomentByDate(
        this.attendanceFilter.max.toISOString()
      );
      if (this.body.ToDate.unix() !== max.unix()) {
        this.isMaxMonth = true;
        this.body.ToDate = this.coreService.getMomentByDate(max.toISOString());
        max.startOf("month");
        this.body.FromDate = this.coreService.getMomentByDate(
          max.toISOString()
        );

        this.visitsFilterForm.patchValue({
          FromDate: this.body.FromDate.toISOString(),
          ToDate: this.body.ToDate.toISOString(),
          Status: this.body.Status,
        });
        this.isMaxMonth = true;
        if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Offline
        ) {
          this.getAttendanceLogLocal();
        } else {
          this.getVisits();
        }
      }
    }
  }

  prevMonthAttend() {
    if (this.isMinMonth) {
      return;
    }
    const date = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    this.body.Status = "all";
    date.subtract(1, "months");
    if (this.attendanceFilter.min.unix() <= date.unix()) {
      this.body.FromDate = this.coreService.getMomentByDate(date.toISOString());
      date.endOf("month");
      this.body.ToDate = date;
      this.visitsFilterForm.patchValue({
        FromDate: this.body.FromDate.toISOString(),
        ToDate: this.body.ToDate.toISOString(),
        Status: this.body.Status,
      });
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAttendanceLogLocal();
      } else {
        this.getVisits();
      }
      this.isMaxMonth = false;
    } else {
      this.isMinMonth = true;
    }
  }

  async navigate(item) {
    if (item && item.length) {
      this.navCntrl.navigateForward("/customer-visit-history/visit-by-date", {
        state: { data: item },
      });
    } else {
      this.coreService.displayToast(
        "There are no visits on that date.",
        "danger"
      );
    }
  }

  async handleFilter() {
    const max = this.coreService
      .getMomentNow()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format(this.yearFormatted);
    const min = this.coreService
      .getMomentObject("2000-01-01")
      .format(this.yearFormatted);
    const modalRef = await this.modalCtrl.create({
      component: LeaveFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        type: "visits",
        leaveFilterForm: this.visitsFilterForm,
        defaultValues: this.defaultFilterValue,
        minMaxDates: { min, max },
        isVisitsPage: true,
      },
      mode: "md",
    });

    await modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          const FromDate = this.coreService
            .getMomentByDate(this.body.FromDate.toISOString())
            .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          const ToDate = this.coreService
            .getMomentByDate(this.body.ToDate.toISOString())
            .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          if (
            (res.data.FromDate &&
              FromDate.unix() !== res.data.FromDate.unix()) ||
            (res.data.ToDate && ToDate.unix() !== res.data.ToDate.unix())
          ) {
            this.body = res.data;
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getAttendanceLogLocal();
            } else {
              this.getVisits();
            }
            return;
          }
          this.body = res.data;
          let list = JSON.parse(JSON.stringify(this.visitsCopy));
          if (res.data.Status && res.data.Status !== "all") {
            list.forEach(({ visitDetails }, i) => {
              if (visitDetails && visitDetails.length) {
                list[i].filterVisitDetails = list[i].visitDetails.filter(
                  (ele) =>
                    ele.checkInStatus === this.STATUS_CODES[res.data.Status] ||
                    ele.checkOutStatus === this.STATUS_CODES[res.data.Status]
                );
              }
            });
            list = list.filter(
              ({ filterVisitDetails }) =>
                filterVisitDetails && filterVisitDetails.length > 0
            );
          }
          this.visits = [...list];
        } else {
          this.body = {
            FromDate: this.coreService.getMomentNow().set("date", 1),
            ToDate: this.coreService.getMomentNow(),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAttendanceLogLocal();
          } else {
            this.getVisits();
          }
        }
      }
    });
  }

  navigateToManual() {
    let attendance = this;
    const attendanceLog = localStorage.getItem(APP_CONSTANTS.ATTENDANCE_LOG);
    const token = localStorage.getItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
    const userDetail = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    const auth_logged_in = localStorage.getItem(APP_CONSTANTS.AUTH_LOGGED_IN);
    const option = {
      ATTENDANCE_LOG: attendanceLog,
      TOKEN: token,
      USER_DETAIL: userDetail,
      AUTH_LOGGED_IN: auth_logged_in,
    };
    window["plugins"].newActivityPlugin.new_activity(
      option,
      function (res) {
        if (res === "qr") {
          attendance.navCntrl.navigateForward("/attendance-screen");
        } else if (res === "dashboard") {
          attendance.navCntrl.navigateForward("");
        }
      },
      function (err) { }
    );
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal(e);
    } else {
      this.getVisits(e, true);
      this.getCustomerVisitCalendar();
    }
  }
  generatePdf() {
    if (this.isSupervisor) {
      const userDetails = JSON.parse(
        localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
      );
      let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Employee Customer Visit Report</h3>
      </div>
      <div>
        <table class="info-table">
          <tr>
            <td>Employee: ${userDetails.FirstName + userDetails.LastName}</td>
            <td>From Date: ${this.body.FromDate.toLocaleDateString()}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.toLocaleDateString()}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Date</th>
              <th>Employee</th>
              <th>Customer Name</th>
              <th>Customer Address</th>
              <th>In Time </th>
              <th>In Time Status</th>
              <th>Out Time</th>
              <th>Out Time Status</th>
              <th>Spent Hours</th>
            </tr>
          </thead>
          <tbody>
    `;
      const CODES = {
        1: "pending",
        2: "approved",
        3: "rejected",
        4: "cancelled",
      };
      this.visits
        .filter(({ visitDetails }) => visitDetails && visitDetails.length)
        .forEach(({ visitDetails, ...item }) => {
          visitDetails.forEach((ele) => {
            const row = `
      <tr>
        <td>${item.date ? item.date : "-"}</td>
        <td>${ele.EmployeeName ? ele.EmployeeName : "-"}</td>
        <td>${ele.customerName ? ele.customerName : "-"}</td>
        <td>${ele.customerAddress ? ele.customerAddress : "-"}</td>
        <td>${ele.checkInTime ? ele.checkInTime : "-"}</td>
        <td>${ele.checkInStatus ? CODES[ele.checkInStatus] : "-"}</td>
        <td>${ele.checkOutTime ? ele.checkOutTime : "-"}</td>
        <td>${ele.checkOutStatus ? CODES[ele.checkOutStatus] : "-"}</td>
        <td>${ele.spentTime}</td>
      </tr>`;
            html += row;
          });
        });
      const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                      <p>Report Generated By: ${this.userDetails.FirstName} ${this.userDetails.LastName
        } : ${this.coreService.getMomentNow(APP_CONSTANTS.PDF_INFO_FORMAT)}</p>
                    </div>
                  </div>
                </body>
              </html>`;
      html += endOfHtml;
      this.pdfGenerator
        .fromData(html, {
          documentSize: "A4",
          fileName: `my-visit_report${this.coreService.getMomentNow(
            APP_CONSTANTS.FILE_EXTENSION_FORMAT
          )}.pdf`,
          type: "share",
        })
        .then((res) => {
          console.log("res =>", res);
        })
        .catch((err) => {
          console.log("error =>", err);
        });
    }
  }
}
