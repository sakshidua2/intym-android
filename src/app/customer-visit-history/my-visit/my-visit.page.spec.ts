import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyVisitPage } from './my-visit.page';

describe('MyVisitPage', () => {
  let component: MyVisitPage;
  let fixture: ComponentFixture<MyVisitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyVisitPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyVisitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
