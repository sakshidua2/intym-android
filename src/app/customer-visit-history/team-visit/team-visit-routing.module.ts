import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamVisitPage } from './team-visit.page';
const routes: Routes = [
  {
    path: '',
    component: TeamVisitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamVisitPageRoutingModule {}
