import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ModalController, NavController, Platform } from "@ionic/angular";
import { VisitFilterComponent } from "../visit-filter/visit-filter.component";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { AttendanceLogService } from "src/app/services/db/attendance-log.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { AttendanceLogModel } from "src/app/local-db-models/attendace_log";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";
import { NOTIFY_OBSERVABLE } from "src/app/config/notify-observable.constants";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";

@Component({
  selector: "app-team-visit",
  templateUrl: "./team-visit.page.html",
  styleUrls: ["./team-visit.page.scss"],
})
export class TeamVisitPage implements OnInit {
  public calendarComponentTypeTeam = APP_CONSTANTS.DATA_TYPE_TEAM;
  public ATTENDANCE_STATUS_CHECK_IN = 3;
  public ATTENDANCE_STATUS_CHECK_OUT = 4;
  public PENDING_APPROVAL_STATUS = 1;
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  body: any;
  public visits = [];
  visitsCopy = [];
  filterForm: FormGroup;
  usersList: any[];
  defaultFilterValue: {
    User?: number;
  };
  screenType: string;
  public customerVisitData: any;

  isMaxMonth: boolean;
  isMinMonth: boolean;
  attendanceFilter: { min: any; max: any };
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public isLeavesListReadyToRender = false;

  public monthFormatter = APP_CONSTANTS.MONTH_FORMATED;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private attendanceLogService: AttendanceLogService,
    private networkService: NetworkService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.coreService.notifyObservable$.subscribe((res: any) => {
      if (res.option === NOTIFY_OBSERVABLE.TEAM_VISITS_LIST && res.value) {
        this.getVisits();
      }
    });
    this.body = {};
    this.attendanceFilter = {
      min: "",
      max: "",
    };
    this.attendanceFilter.max = this.coreService
      .getMomentNow()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    this.attendanceFilter.min = this.coreService.getMomentObject("2000-01-01");
    this.filterForm = this.fb.group({
      User: [""],
      Projects: [""],
      Cities: [""],
      Regions: [""],
    });

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }
    this.isLeavesListReadyToRender = false;
    this.body = {
      FromDate: this.coreService.getMomentNow(),
      ToDate: this.coreService.getMomentNow(),
    };
    this.isMaxMonth = true;
    this.defaultFilterValue = {
      User: 0,
    };
    this.isMaxMonth = true;
    this.filterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal();
    } else {
      this.getVisits();
    }
  }

  handleTeamCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal();
    } else {
      this.getVisits(null, false);
      if (!payload.sameMonth) { this.getCustomerVisitCalendar(); }
    }
  }

  getCustomerVisitCalendar() {
    this.customerVisitData = {};
    this.apiService
      .getCustomerVisitCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          Filters: this.body.Filters,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe((res: any) => {
        [...res.data.L1].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          if (
            date === this.coreService.getMomentFormatDate(this.yearFormatted)
          ) {
            this.customerVisitData = {
              ...this.customerVisitData,
              [date]: { InMeeting: o.InMeeting, DoneMeeting: o.DoneMeeting },
            };
          } else {
            this.customerVisitData = {
              ...this.customerVisitData,
              [date]: {
                ApprovedVisits: o.ApprovedVisits,
                RejectedVisits: o.RejectedVisits,
                TotalVisits: o.TotalVisits,
              },
            };
          }
          return true;
        });
        [...res.data.L2].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          if (
            date === this.coreService.getMomentFormatDate(this.yearFormatted)
          ) {
            if (this.customerVisitData[date]) {
              this.customerVisitData[date].InMeeting += o.InMeeting;
              this.customerVisitData[date].DoneMeeting += o.DoneMeeting;
            } else {
              this.customerVisitData = {
                ...this.customerVisitData,
                [date]: {
                  InMeeting: o.InMeeting,
                  DoneMeeting: o.DoneMeeting,
                },
              };
            }
          } else {
            if (this.customerVisitData[date]) {
              this.customerVisitData[date].ApprovedVisits += o.ApprovedVisits;
              this.customerVisitData[date].RejectedVisits += o.RejectedVisits;
              this.customerVisitData[date].TotalVisits += o.TotalVisits;
            } else {
              this.customerVisitData = {
                ...this.customerVisitData,
                [date]: {
                  ApprovedVisits: o.ApprovedVisits,
                  RejectedVisits: o.RejectedVisits,
                  TotalVisits: o.TotalVisits,
                },
              };
            }
          }
          return true;
        });
        [...res.data.L3].map((o) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            o.Date
          );
          if (
            date === this.coreService.getMomentFormatDate(this.yearFormatted)
          ) {
            if (this.customerVisitData[date]) {
              this.customerVisitData[date].InMeeting += o.InMeeting;
              this.customerVisitData[date].DoneMeeting += o.DoneMeeting;
            } else {
              this.customerVisitData = {
                ...this.customerVisitData,
                [date]: {
                  InMeeting: o.InMeeting,
                  DoneMeeting: o.DoneMeeting,
                },
              };
            }
          } else {
            if (this.customerVisitData[date]) {
              this.customerVisitData[date].ApprovedVisits += o.ApprovedVisits;
              this.customerVisitData[date].RejectedVisits += o.RejectedVisits;
              this.customerVisitData[date].TotalVisits += o.TotalVisits;
            } else {
              this.customerVisitData = {
                ...this.customerVisitData,
                [date]: {
                  ApprovedVisits: o.ApprovedVisits,
                  RejectedVisits: o.RejectedVisits,
                  TotalVisits: o.TotalVisits,
                },
              };
            }
          }
          return true;
        });
      });
  }

  getVisits(e?: any, StopLoader?: boolean) {
    this.apiService
      .getAttendanceLog(
        {
          FromDate: this.body.FromDate.format(this.yearFormatted),
          ToDate: this.body.ToDate.format(this.yearFormatted),
          StopLoader,
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe(
        (res: any) => {
          const isMyAttendanceLog = false;
          const response = res.L1.concat(res.L2, res.L3);
          const rawData = [...response].map((o) => ({
            ...o,
            IsMyAttendanceLog: isMyAttendanceLog,
          }));
          const result = this.processVisitsRawData([...rawData]);
          this.visits = JSON.parse(JSON.stringify(result));
          // this.customerVisitData = JSON.parse(JSON.stringify(result));
          this.visitsCopy = JSON.parse(JSON.stringify(result));
          const ToDate = this.body.ToDate;
          ToDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          if (ToDate.isSame(this.attendanceFilter.max)) {
            this.isMaxMonth = true;
          } else {
            this.isMaxMonth = false;
          }
          const FromDate = this.body.FromDate;
          FromDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
          if (FromDate.isSame(this.attendanceFilter.min)) {
            this.isMinMonth = true;
          } else {
            this.isMinMonth = false;
          }
          if (this.body.User && this.body.User.length !== 0) {
            this.visits.forEach(({ employeeVisitDetails }, i) => {
              if (employeeVisitDetails && employeeVisitDetails.length) {
                this.visits[i].employeeVisitDetails = this.visits[i].employeeVisitDetails.filter(
                  (ele) => this.body.User.indexOf(ele.employeeId) > -1
                );
              }
            });
          }
          if (this.body.Status && this.body.Status !== "all") {
            this.visits.forEach(({ employeeVisitDetails }, i) => {
              if (employeeVisitDetails && employeeVisitDetails.length) {
                employeeVisitDetails.forEach(({ visitDetails }, index) => {
                  this.visits[i].employeeVisitDetails[
                    index
                  ].visitDetails = this.visits[i].employeeVisitDetails[
                    index
                  ].visitDetails.filter(
                    (ele) =>
                      ele.checkInStatus ===
                      this.STATUS_CODES[this.body.Status] ||
                      ele.checkOutStatus === this.STATUS_CODES[this.body.Status]
                  );
                });
                this.visits[
                  i
                ].employeeVisitDetails = employeeVisitDetails.filter(
                  ({ visitDetails }) => visitDetails.length > 0
                );
              }
            });
            this.visits = this.visits.filter(
              ({ employeeVisitDetails }) =>
                employeeVisitDetails.length > 0 && employeeVisitDetails
            );
          }
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
          this.isLeavesListReadyToRender = true;
          if (history.state.data) {
            const notificationData = history.state["data"];
            const date = this.coreService.getMomentObject(
              notificationData.date
            );
            setTimeout(() => {
              if (
                date.isSame(this.body.FromData, "month") &&
                date.isSame(this.body.FromData, "year")
              ) {
                let list = JSON.parse(JSON.stringify(this.visits));
                list = list.filter(
                  (ele: any) =>
                    ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
                );
                if (list && list.length) {
                  this.navigate(list[0].employeeVisitDetails[0].visitDetails);
                }
              } else {
                this.notificationRequest(notificationData);
              }
              history.state.data = undefined;
            }, 500);
          }

          if (this.platform.is("cordova")) {
            this.attendanceLogService.storeAttendanceLogForOffline(
              [...rawData],
              isMyAttendanceLog
            );
          }
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  notificationRequest(data: { id: number; date: string }) {
    const date = this.coreService.getMomentObject(data.date);
    this.apiService
      .getAttendanceLog(
        {
          FromDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
          ToDate: date.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe((res: any) => {
        let list = [...res];
        list = list.filter(
          (ele: any) => ele.date === date.format(APP_CONSTANTS.DATE_FORMATER)
        );
        if (list && list.length) {
          this.navigate(list[0].employeeVisitDetails[0].visitDetails);
        }
        if (list && list.length) {
          this.navigate(list[0].employeeVisitDetails[0].visitDetails);
        }
      });
    history.state.data = undefined;
  }

  openNotificationPopup(notificationVisit: any) {
    if (notificationVisit) {
      const item: any = {
        EmployeeId: notificationVisit.EmployeeId,
        EmployeeName: notificationVisit.EmployeeName,
        checkInId: notificationVisit.EmpAttnTransactionId,
        customerAddress: notificationVisit.CustomerName,
        customerName: notificationVisit.CustomerAddress,
      };
      if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
      ) {
        item["checkInStatus"] = notificationVisit.ApprovalStatus;
        item["checkInTime"] = notificationVisit.TimeStamp;
        item["checkInTimeStamp"] = notificationVisit.TimeStamp;
        item["checkInId"] = notificationVisit.EmpAttnTransactionId;
      } else if (
        notificationVisit.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_OUT
      ) {
        item["checkOutStatus"] = notificationVisit.ApprovalStatus;
        item["checkOutTime"] = notificationVisit.TimeStamp;
        item["checkOutTimeStamp"] = notificationVisit.TimeStamp;
        item["checkOutId"] = notificationVisit.EmpAttnTransactionId;
      }
      this.navigate([item]);
    }
  }

  getAttendanceLogLocal(e?: any) {
    // this.coreService.showLoader();
    const fromDate = this.coreService.getMomentObject(this.body.FromDate);
    const toDate = this.coreService.getMomentObject(this.body.ToDate);
    toDate.add(1, "day");
    const params = {
      FromDate: fromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: toDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      IsMyAttendanceLog: false,
    };
    this.attendanceLogService
      .getData(params)
      .then((res: AttendanceLogModel[]) => {
        const ToDate = this.body.ToDate;
        ToDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        if (ToDate.isSame(this.attendanceFilter.max)) {
          this.isMaxMonth = true;
        } else {
          this.isMaxMonth = false;
        }
        const FromDate = this.body.FromDate;
        FromDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        if (FromDate.isSame(this.attendanceFilter.min)) {
          this.isMinMonth = true;
        } else {
          this.isMinMonth = false;
        }
        const result = this.processVisitsRawData([...res]);

        this.visits = JSON.parse(JSON.stringify(result));
        this.visitsCopy = JSON.parse(JSON.stringify(result));

        if (this.body.User && this.body.User.length !== 0) {
          this.visits.forEach(({ employeeVisitDetails }, i) => {
            if (employeeVisitDetails && employeeVisitDetails.length) {
              this.visits[i].employeeVisitDetails = this.visits[i].employeeVisitDetails.filter(
                (ele) => this.body.User.indexOf(ele.employeeId) > -1
              );
            }
          });
        }
        if (this.body.Status && this.body.Status !== "all") {
          this.visits.forEach(({ employeeVisitDetails }, i) => {
            if (employeeVisitDetails && employeeVisitDetails.length) {
              employeeVisitDetails.forEach(({ visitDetails }, index) => {
                this.visits[i].employeeVisitDetails[
                  index
                ].visitDetails = this.visits[i].employeeVisitDetails[
                  index
                ].visitDetails.filter(
                  (ele) =>
                    ele.checkInStatus === this.STATUS_CODES[this.body.Status] ||
                    ele.checkOutStatus === this.STATUS_CODES[this.body.Status]
                );
              });
              this.visits[i].employeeVisitDetails = employeeVisitDetails.filter(
                ({ visitDetails }) => visitDetails.length > 0
              );
            }
          });
          this.visits = this.visits.filter(
            ({ employeeVisitDetails }) =>
              employeeVisitDetails.length > 0 && employeeVisitDetails
          );
        }
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
      })
      .catch((err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_NO_LOCAL_DATA,
          "warning"
        );
      });
  }

  processVisitsRawData(rawData) {
    const ogRawData = [...rawData];
    const visitsArr = [
      this.ATTENDANCE_STATUS_CHECK_IN,
      this.ATTENDANCE_STATUS_CHECK_OUT,
    ];
    this.usersList = [];
    const ids = [];

    rawData
      .filter(({ AttendanceStatus }) => visitsArr.includes(AttendanceStatus))
      .map((ele) => {
        if (!ids.includes(ele.EmployeeId) && ele) {
          ids.push(ele.EmployeeId);
          this.usersList.push(ele);
        }
      });

    const filteredVisits = rawData.filter(
      (o) => visitsArr.indexOf(o.AttendanceStatus) >= 0
    );
    filteredVisits.sort(
      (a, b) =>
        this.coreService.getMomentByDate(a.TimeStamp).unix() -
        this.coreService.getMomentByDate(b.TimeStamp).unix()
    );
    const dateRange = this.coreService.getMomentDatesRange(
      this.body.FromDate,
      this.body.ToDate
    );
    const dateRangeLength = dateRange.length;
    const filteredVisitsLength = filteredVisits.length;
    const result = [];
    for (let index = 0; index < dateRangeLength; index++) {
      const rangeDate = dateRange[index];
      const employeeVisitDetails = [];
      for (
        let filterIndex = 0;
        filterIndex < filteredVisitsLength;
        filterIndex++
      ) {
        const filterObj = filteredVisits[filterIndex];
        const foramtedFilterDate = this.coreService
          .getMomentByDate(filterObj.TimeStamp)
          .format(APP_CONSTANTS.DATE_FORMATER);
        if (rangeDate === foramtedFilterDate) {
          const employeeId = filterObj.EmployeeId;
          const employeeIndex = employeeVisitDetails.findIndex(
            (o) => o.employeeId === employeeId
          );
          if (
            employeeIndex < 0 &&
            filterObj.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
          ) {
            const employeeVisitDetail = {
              employeeId,
              employeeName: filterObj.EmployeeName,
              visits: 1,
              totalMins: 0,
              spentHrs: 0,
              spentMins: 0,
              spentTime: "",
              pendingApproval:
                filterObj.ApprovalStatus === this.PENDING_APPROVAL_STATUS
                  ? 1
                  : 0,
              visitDetails: [
                {
                  checkInId: filterObj.EmpAttnTransactionId,
                  checkOutId: 0,
                  customerName: filterObj.CustomerName,
                  customerAddress: filterObj.CustomerAddress,
                  checkInTimeStamp: filterObj.TimeStamp,
                  checkOutTimeStamp: "",
                  checkInTime: filterObj.TimeStamp,
                  checkOutTime: "",
                  checkInStatus: filterObj.ApprovalStatus,
                  checkOutStatus: 0,
                  spentHrs: 0,
                  spentMins: 0,
                  spentTime: "",
                },
              ],
            };
            employeeVisitDetails.push(employeeVisitDetail);
          } else if (employeeIndex >= 0) {
            const employeeVisitDetail = employeeVisitDetails[employeeIndex];
            const employeeClientDetails = employeeVisitDetail.visitDetails;
            const customerName = filterObj.CustomerName;
            const customerIndex = employeeClientDetails.findIndex(
              (o) =>
                o.customerName.toLocaleLowerCase() ===
                customerName.toLocaleLowerCase()
            );
            if (
              customerIndex < 0 ||
              filterObj.AttendanceStatus === this.ATTENDANCE_STATUS_CHECK_IN
            ) {
              const clientDetail = {
                checkInId: filterObj.EmpAttnTransactionId,
                checkOutId: 0,
                customerName,
                customerAddress: filterObj.CustomerAddress,
                checkInTimeStamp: filterObj.TimeStamp,
                checkOutTimeStamp: "",
                checkInTime: filterObj.TimeStamp,
                checkOutTime: "",
                checkInStatus: filterObj.ApprovalStatus,
                checkOutStatus: 0,
                spentHrs: 0,
                spentMins: 0,
                spentTime: "",
              };
              const newEmployeeClientDetails = [
                ...employeeClientDetails,
                clientDetail,
              ];
              employeeVisitDetails[
                employeeIndex
              ].visitDetails = newEmployeeClientDetails;
              employeeVisitDetails[employeeIndex].visits += 1;
              employeeVisitDetails[employeeIndex].pendingApproval +=
                filterObj.ApprovalStatus === this.PENDING_APPROVAL_STATUS
                  ? 1
                  : 0;
            } else {
              const customerIndexForPunchOut = employeeClientDetails.findIndex(
                (o) =>
                  o.customerName.toLocaleLowerCase() ===
                  customerName.toLocaleLowerCase() && o.checkOutTime === ""
              );
              if (customerIndexForPunchOut >= 0) {
                const customerExistingObj =
                  employeeClientDetails[customerIndexForPunchOut];
                customerExistingObj.checkOutId = filterObj.EmpAttnTransactionId;
                customerExistingObj.checkOutTimeStamp = filterObj.TimeStamp;
                customerExistingObj.checkOutTime = filterObj.TimeStamp;
                customerExistingObj.checkOutStatus = filterObj.ApprovalStatus;
                employeeVisitDetails[employeeIndex].pendingApproval +=
                  filterObj.ApprovalStatus === this.PENDING_APPROVAL_STATUS
                    ? 1
                    : 0;
                const spentHours = this.coreService.calculateMomentDateDiff(
                  customerExistingObj.checkInTimeStamp,
                  customerExistingObj.checkOutTimeStamp
                );
                customerExistingObj.spentHrs = spentHours.hours;
                customerExistingObj.spentMins = spentHours.minutes;
                customerExistingObj.spentTime = spentHours.label;
                employeeClientDetails[
                  customerIndexForPunchOut
                ] = customerExistingObj;
                employeeVisitDetails[
                  employeeIndex
                ].visitDetails = employeeClientDetails;
                // employeeVisitDetails[employeeIndex].visits += 1;
                const totalMins = employeeVisitDetails[employeeIndex].totalMins;
                const newTotalMins =
                  totalMins + spentHours.hours * 60 + spentHours.minutes;
                const spentHrs = Math.floor(newTotalMins / 60);
                const spentMins = Math.floor(newTotalMins % 60);
                employeeVisitDetails[employeeIndex].totalMins = newTotalMins;
                employeeVisitDetails[employeeIndex].spentHrs = spentHrs;
                employeeVisitDetails[employeeIndex].spentMins = spentMins;
                employeeVisitDetails[
                  employeeIndex
                ].spentTime = `${spentHrs}h ${spentMins}min`;
              }
            }
          }
        }
      }
      const overview = {
        date: rangeDate,
        employeeVisitDetails,
        Collapsed: true,
      };
      result.push(overview);
    }
    return result.filter(
      ({ employeeVisitDetails }) =>
        employeeVisitDetails && employeeVisitDetails.length
    );
  }

  async navigate(item: any) {
    if (item.length) {
      this.navCntrl.navigateForward("/customer-visit-history/customer-visits", {
        state: { data: item },
      });
    } else {
      this.coreService.displayToast(
        "There are no visits on that date.",
        "danger"
      );
    }
  }

  nextMonthAttend() {
    if (this.isMaxMonth) {
      return;
    }
    const date = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    date.add(1, "M");
    date.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });

    this.body.Status = "all";
    this.body.User = 0;

    if (this.attendanceFilter.max.unix() > date.unix()) {
      if (date.month() - this.body.FromDate.month() !== 1) {
        date.subtract(1, "M");
      }
      date.startOf("month");
      this.body.FromDate = this.coreService.getMomentByDate(date.toISOString());
      date.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(date.toISOString());
      this.filterForm.patchValue({
        User: this.body.User,
      });
      this.isMinMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAttendanceLogLocal();
      } else {
        this.getVisits();
      }
    } else {
      const max = this.coreService.getMomentByDate(
        this.attendanceFilter.max.toISOString()
      );
      if (this.body.ToDate.unix() !== max.unix()) {
        this.isMaxMonth = true;
        this.body.ToDate = this.coreService.getMomentByDate(max.toISOString());
        max.startOf("month");
        this.body.FromDate = this.coreService.getMomentByDate(
          max.toISOString()
        );
        this.filterForm.patchValue({
          User: this.body.User,
        });
        this.isMaxMonth = true;
        if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Offline
        ) {
          this.getAttendanceLogLocal();
        } else {
          this.getVisits();
        }
      }
    }
  }

  prevMonthAttend() {
    if (this.isMinMonth) {
      return;
    }
    const date = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    date.subtract(1, "months");

    this.body.Status = "all";
    this.body.User = 0;
    if (this.attendanceFilter.min.unix() <= date.unix()) {
      this.body.FromDate = this.coreService.getMomentByDate(date.toISOString());
      date.endOf("month");
      this.body.ToDate = date;
      this.filterForm.patchValue({
        User: this.body.User,
      });
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAttendanceLogLocal();
      } else {
        this.getVisits();
      }
      this.isMaxMonth = false;
    } else {
      this.isMinMonth = true;
    }
  }

  async handleFilter() {
    const modalRef = await this.modalCtrl.create({
      component: VisitFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        visitFilterForm: this.filterForm,
        defaultValues: this.defaultFilterValue,
      },
      mode: "md",
    });
    await modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          if (!this.body.CalendarFromDate || !this.body.CalendarToDate) {
            this.body.CalendarFromDate = this.coreService
              .getMomentNow()
              .set("date", 1)
              .format(this.yearFormatted);
            this.body.CalendarToDate = this.coreService.getMomentNow().format(this.yearFormatted);
          }
          this.body.Filters = { EmployeeId: res.data.User };
          this.body.User = res.data.User;
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAttendanceLogLocal();
          } else {
            this.getVisits();
            this.getCustomerVisitCalendar();
          }
          let list = JSON.parse(JSON.stringify(this.visitsCopy));
          list = list.filter(
            ({ employeeVisitDetails }) =>
              employeeVisitDetails && employeeVisitDetails.length
          );
          this.visits = [...list];
        } else {
          this.body = {
            FromDate: this.coreService.getMomentObject(this.body.FromDate),
            ToDate: this.coreService.getMomentObject(this.body.ToDate),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAttendanceLogLocal();
          } else {
            this.getVisits();
            this.getCustomerVisitCalendar();
          }
        }
      }
    });
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAttendanceLogLocal(e);
    } else {
      this.getVisits(e, true);
      this.getCustomerVisitCalendar();
    }
  }

  generatePdf() {
    const userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    let EmployeeName = "";
    let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Team Customer Vistis Report</h3>
      </div>
      <div>
        <table class="info-table">
          <tr>
            <td>Employee: ${this.body.User && EmployeeName ? EmployeeName : "All"
      }</td>
            <td>From Date: ${this.body.FromDate.format(
        APP_CONSTANTS.DATE_FORMATER
      )}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.format(
        APP_CONSTANTS.DATE_FORMATER
      )}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Date</th>
              <th>Employee</th>
              <th>Customer Name</th>
              <th>Customer Address</th>
              <th>In Time </th>
              <th>In Time Status</th>
              <th>Out Time</th>
              <th>Out Time Status</th>
              <th>Spent Hours</th>
            </tr>
          </thead>
          <tbody>
    `;
    const CODES = {
      1: "pending",
      2: "approved",
      3: "rejected",
      4: "cancelled",
    };
    this.visits
      .filter(
        ({ employeeVisitDetails }) =>
          employeeVisitDetails && employeeVisitDetails.length
      )
      .forEach(({ employeeVisitDetails, ...item }) => {
        employeeVisitDetails.forEach((ele: any) => {
          if (ele.visitDetails && ele.visitDetails.length) {
            ele.visitDetails.forEach((visit: any) => {
              const row = `
            <tr>
              <td>${item.date ? item.date : "-"}</td>
              <td>${ele.employeeName ? ele.employeeName : "-"}</td>
              <td>${visit.customerName ? visit.customerName : "-"}</td>
              <td>${visit.customerAddress ? visit.customerAddress : "-"}</td>
              <td>${visit.checkInTime ? visit.checkInTime : "-"}</td>
              <td>${visit.checkInStatus ? CODES[visit.checkInStatus] : "-"}</td>
              <td>${visit.checkOutTime ? visit.checkOutTime : ""}</td>
              <td>${visit.checkOutStatus ? CODES[visit.checkOutStatus] : "-"
                }</td>
              <td>${ele.spentTime}</td>
            </tr>`;
              html += row;
            });
          }
        });
      });
    const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                      <p>Report Generated By: ${userDetails.FirstName} ${userDetails.LastName
      } : ${this.coreService.getMomentNow(APP_CONSTANTS.PDF_INFO_FORMAT)}</p>
                    </div>
                  </div>
                </body>
              </html>`;
    html += endOfHtml;
    this.pdfGenerator
      .fromData(html, {
        documentSize: "A4",
        fileName: `team-visit_report${this.coreService.getMomentNow(
          APP_CONSTANTS.FILE_EXTENSION_FORMAT
        )}.pdf`,
        type: "share",
      })
      .then((res) => {
        console.log("res =>", res);
      })
      .catch((err) => {
        console.log("error =>", err);
      });
  }
}
