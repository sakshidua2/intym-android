import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerVisitHistoryPageRoutingModule } from './customer-visit-history-routing.module';

import { CustomerVisitHistoryPage } from './customer-visit-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CustomerVisitHistoryPageRoutingModule
  ],
  declarations: [CustomerVisitHistoryPage]
})
export class CustomerVisitHistoryPageModule { }
