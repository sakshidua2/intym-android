import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisitByDatePage } from './visit-by-date.page';

describe('VisitByDatePage', () => {
  let component: VisitByDatePage;
  let fixture: ComponentFixture<VisitByDatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitByDatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisitByDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
