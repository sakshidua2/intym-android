import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerVisitHistoryPage } from './customer-visit-history.page';
import { VisitsGuard } from '../guard/visits.guard';

const routes: Routes = [
  {
    path: '',
    component: CustomerVisitHistoryPage,
    children: [
      {
        path: '',
        redirectTo: 'my-visit',
        pathMatch: 'full'
      },
      {
        path: 'my-visit',
        children: [
          {
            path: '',
            loadChildren: () => import('./my-visit/my-visit.module').then(m => m.MyVisitPageModule)
          }
        ],
        data: {isMyVisit: true},
        canActivate: [VisitsGuard]
      },
      {
        path: 'team-visit',
        children: [
          {
            path: '',
            loadChildren: () => import('./team-visit/team-visit.module').then(m => m.TeamVisitPageModule)
          }
        ],
        data: {isMyVisit: false},
        canActivate: [VisitsGuard]
      }
    ]
  },
  {
    path: 'visit-by-date',
    loadChildren: () => import('./visit-by-date/visit-by-date.module').then( m => m.VisitByDatePageModule)
  },
  {
    path: 'customer-visits',
    loadChildren: () => import('./customer-visits/customer-visits.module').then( m => m.CustomerVisitsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerVisitHistoryPageRoutingModule { }
