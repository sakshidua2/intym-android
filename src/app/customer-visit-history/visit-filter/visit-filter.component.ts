import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: "app-visit-filter",
  templateUrl: "./visit-filter.component.html",
  styleUrls: ["./visit-filter.component.scss"],
})
export class VisitFilterComponent implements OnInit {
  @Input() visitFilterForm: FormGroup;
  userList: any[] = [];
  userListCopy: any[] = [];
  projectList: any[] = [];
  regionList: any[] = [];
  cityList: any[] = [];
  @Input() defaultValues: any;
  isSubmitted: boolean;
  filertUser: any;
  constructor(
    public modalCtrl: ModalController,
    public coreService: CoreService,
    public apiService: ApiService
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.filertUser = {
      Cities: [],
      Regions: [],
      Projects: [],
    };
    this.getEmployeeList();
    this.getProjects();
    this.getRegions();
  }

  get fControls() {
    return this.visitFilterForm.controls;
  }

  getEmployeeList() {
    this.apiService.getEmployeeList().subscribe((res: any) => {
      if (res.L1) {
        this.userList = this.userList.concat(res.L1);
      }
      if (res.L2) {
        this.userList = this.userList.concat(res.L2);
      }
      if (res.L3) {
        this.userList = this.userList.concat(res.L3);
      }
      this.userListCopy = this.userList;
    });
  }
  getProjects() {
    this.apiService.getProjects().subscribe((res: any) => {
      this.projectList = res;
    });
  }
  getRegions() {
    this.apiService.getRegions().subscribe((res: any) => {
      res.map(region => {
        if (region.RegionType === 0) {
          this.cityList.push(region);
        } else {
          this.regionList.push(region);
        }
        return true;
      })
    });
  }

  onCityChange(event) {
    this.filertUser.Cities = event.target.value;
    this.filterUserList();
  }

  onRegionChange(event) {
    this.filertUser.Regions = event.target.value;
    this.filterUserList();
  }

  onProjectChange(event) {
    this.filertUser.Projects = event.target.value;
    this.filterUserList();
  }

  filterUserList() {
    this.userList = this.userListCopy;
    for (const project of this.filertUser.Projects) {
      this.userList = this.userList.filter(user => {
        if (user.ProjectId.indexOf(project) > -1) {
          return true;
        }
        return false;
      });
    }
    for (const city of this.filertUser.Cities) {
      this.userList = this.userList.filter(user => {
        if (user.RegionId.indexOf(city) > -1) {
          return true;
        }
        return false;
      });
    }
    for (const region of this.filertUser.Regions) {
      this.userList = this.userList.filter(user => {
        if (user.RegionId.indexOf(region) > -1) {
          return true;
        }
        return false;
      });
    }
  }

  dismissModal() {
    this.modalCtrl.dismiss(undefined, "close");
  }

  onSubmit() {
    if (this.visitFilterForm.valid) {
      const data = this.visitFilterForm.getRawValue();
      const canEmit = Object.values(data).filter((ele) => ele);
      if (canEmit && canEmit.length) {
        this.modalCtrl.dismiss(data, "apply");
        return;
      }
      this.modalCtrl.dismiss(undefined, "apply");
    }
  }

  resetValue() {
    this.visitFilterForm.reset();
    this.visitFilterForm.patchValue(this.defaultValues);
    this.onSubmit();
  }
}
