import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { AlertController } from "@ionic/angular";
import { forkJoin } from "rxjs";
import { CoreService } from "src/app/services/core.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { APP_CONSTANTS } from "../../config/app.constants";
import { NOTIFY_OBSERVABLE } from "src/app/config/notify-observable.constants";

@Component({
  selector: "app-visit-list-item-block",
  templateUrl: "./visit-list-item-block.component.html",
  styleUrls: ["./visit-list-item-block.component.scss"],
})
export class VisitListItemBlockComponent implements OnInit {
  static COMPONENT_TYPE_MY = "my";
  static COMPONENT_TYPE_TEAM = "team";
  @Input() componentType = VisitListItemBlockComponent.COMPONENT_TYPE_MY;
  public isSupervisor: boolean = false;
  InTime = false;
  OutTime = false;
  @Input() data: any;

  statusList: any = [];

  constructor(
    private apiService: ApiService,
    private alertCtrl: AlertController,
    private coreService: CoreService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.statusList = [
      {
        StatusId: 1,
        StatusDesc: "Pending",
      },
      {
        StatusId: 2,
        StatusDesc: "Approved",
      },
      {
        StatusId: 3,
        StatusDesc: "Rejected",
      },
      {
        StatusId: 4,
        StatusDesc: "Cancelled",
      },
    ];
    if (this.data) {
      this.addStatus(this.data);
    }
  }

  addStatus(data) {
    if (data.checkInStatus) {
      const status = this.statusList.filter(
        (el) => el.StatusId === data.checkInStatus
      );
      data["inStatus"] = status[0].StatusDesc;
    }

    if (data.checkOutStatus) {
      const status = this.statusList.filter(
        (el) => el.StatusId === data.checkOutStatus
      );

      data["outStatus"] = status[0].StatusDesc;
    }
  }

  get componentTypeMy() {
    return VisitListItemBlockComponent.COMPONENT_TYPE_MY;
  }
  get componentTypeTeam() {
    return VisitListItemBlockComponent.COMPONENT_TYPE_TEAM;
  }

  async handleAction() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      let msg = "";
      if (this.InTime && this.OutTime) {
        msg = "Do you want to reject both In Time & Out Time?";
      } else {
        if (this.InTime) {
          msg = "Do you want to reject In Time?";
        }

        if (this.OutTime) {
          msg = "Do you want to reject Out Time?";
        }
      }
      const ActionAlert = await this.alertCtrl.create({
        header: "Confirmation",
        message: msg,
        buttons: [
          {
            text: "No",
            role: "cancel",
          },
          {
            text: "Yes",
            handler: () => {
              this.handelReject();
            },
          },
        ],
      });
      await ActionAlert.present();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  handelReject() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const requestIn = {
        EmpAttnTransactionId: this.data.checkInId,
        ApprovalStatus: 3,
      };
      const requestOut = {
        EmpAttnTransactionId: this.data.checkOutId,
        ApprovalStatus: 3,
      };
      if (this.InTime && this.OutTime) {
        forkJoin([
          this.apiService.updateAttendanceStatus(requestIn),
          this.apiService.updateAttendanceStatus(requestOut),
        ]).subscribe((res: any) => {
          this.data.checkInStatus = 3;
          this.data.inStatus = "Rejected";
          this.data.checkOutStatus = 3;
          this.data.outStatus = "Rejected";
          this.coreService.displayToast(res[0].Status, "success");
          this.coreService.notifyOther({
            option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
            value: true,
          });
          this.InTime = this.OutTime = !this.InTime;
        });
      } else {
        if (this.InTime) {
          this.apiService
            .updateAttendanceStatus(requestIn)
            .subscribe((res: any) => {
              if (res) {
                this.data.checkInStatus = 3;
                this.data.inStatus = "Rejected";
                this.coreService.displayToast(res.Status, "success");
                this.coreService.notifyOther({
                  option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
                  value: true,
                });
                this.InTime = !this.InTime;
              }
            });
        }

        if (this.OutTime) {
          this.apiService
            .updateAttendanceStatus(requestOut)
            .subscribe((res: any) => {
              if (res) {
                this.data.checkOutStatus = 3;
                this.data.outStatus = "Rejected";
                this.coreService.displayToast(res.Status, "success");
                this.coreService.notifyOther({
                  option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
                  value: true,
                });
                this.OutTime = !this.OutTime;
              }
            });
        }
      }
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  handelApprove() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const requestIn = {
        EmpAttnTransactionId: this.data.checkInId,
        ApprovalStatus: 2,
      };
      const requestOut = {
        EmpAttnTransactionId: this.data.checkOutId,
        ApprovalStatus: 2,
      };

      if (this.InTime && this.OutTime) {
        forkJoin([
          this.apiService.updateAttendanceStatus(requestIn),
          this.apiService.updateAttendanceStatus(requestOut),
        ]).subscribe((res: any) => {
          this.data.checkInStatus = 2;
          this.data.inStatus = "Approved";
          this.data.checkOutStatus = 2;
          this.data.outStatus = "Approved";
          this.coreService.displayToast(res[0].Status, "success");
          this.coreService.notifyOther({
            option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
            value: true,
          });
          this.InTime = this.OutTime = !this.InTime;
        });
      } else {
        if (this.InTime) {
          this.apiService
            .updateAttendanceStatus(requestIn)
            .subscribe((res: any) => {
              if (res) {
                this.data.checkInStatus = 2;
                this.data.inStatus = "Approved";
                this.coreService.displayToast(res.Status, "success");
                this.coreService.notifyOther({
                  option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
                  value: true,
                });
                this.InTime = !this.InTime;
              }
            });
        }

        if (this.OutTime) {
          this.apiService
            .updateAttendanceStatus(requestOut)
            .subscribe((res: any) => {
              if (res) {
                this.data.checkOutStatus = 2;
                this.data.outStatus = "Approved";
                this.coreService.displayToast(res.Status, "success");
                this.coreService.notifyOther({
                  option: NOTIFY_OBSERVABLE.TEAM_VISITS_LIST,
                  value: true,
                });
                this.OutTime = !this.OutTime;
              }
            });
        }
      }
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }
}
