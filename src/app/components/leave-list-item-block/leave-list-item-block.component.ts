import { Component, OnInit, Input } from "@angular/core";
import { AlertController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { NetworkService, ConnectionStatus } from 'src/app/services/network.service';
import { LABEL_CONSTANTS } from 'src/app/config/labels.constants';

@Component({
  selector: "app-leave-list-item-block",
  templateUrl: "./leave-list-item-block.component.html",
  styleUrls: ["./leave-list-item-block.component.scss"],
})
export class LeaveListItemBlockComponent implements OnInit {
  static COMPONENT_TYPE_MY = "my";
  static COMPONENT_TYPE_TEAM = "team";
  static TEAM_ACTION_APPROVE = "approve";
  static TEAM_ACTION_REJECT = "reject";
  static MY_ACTION_EDIT = "edit";
  static MY_ACTION_CANCEL = "cancel";

  private teamActionTypeSelected = "";
  cancelBtn: boolean;
  editBtn: boolean;

  @Input() componentType = LeaveListItemBlockComponent.COMPONENT_TYPE_MY;
  @Input() teamLeaveActionCallback: (payload: any) => void;
  @Input() myLeaveActionCallback: (payload: any) => void;
  @Input() cancelLeaveActionCallBack: (payload: any) => void;
  @Input() data: any;

  public defaultProfileImg = "assets/icon/favicon.png";
  public showProfilePicPlaceHolder = true;

  constructor(
    private alertCtrl: AlertController,
    private coreService: CoreService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    switch (this.data.StatusName) {
      case "Pending":
        this.data.chipColor = "primary";
        this.cancelBtn = this.editBtn = true;
        break;
      case "Cancelled":
        this.data.chipColor = "primary";
        this.cancelBtn = this.editBtn = false;
        break;
      case "Rejected":
        this.data.chipColor = "primary";
        this.cancelBtn = this.editBtn = false;
        break;
      case "Approved":
        this.data.chipColor = "success";
        this.editBtn = false;
        if (
          this.componentType === this.componentTypeMy &&
          this.data.FromDate &&
          this.coreService.getMomentByDate(this.data.FromDate).unix() > this.coreService.getMomentNow().unix()
        ) {
          this.cancelBtn = true;
        } else {
          this.cancelBtn = false;
        }
        break;
      case "Applied":
        this.data.chipColor = "secondary";
        this.cancelBtn = this.editBtn = true;
        break;
    }
  }

  get componentTypeMy() {
    return LeaveListItemBlockComponent.COMPONENT_TYPE_MY;
  }
  get componentTypeTeam() {
    return LeaveListItemBlockComponent.COMPONENT_TYPE_TEAM;
  }
  get teamActionApprove() {
    return LeaveListItemBlockComponent.TEAM_ACTION_APPROVE;
  }
  get teamActionReject() {
    return LeaveListItemBlockComponent.TEAM_ACTION_REJECT;
  }
  get myActionEdit() {
    return LeaveListItemBlockComponent.MY_ACTION_EDIT;
  }
  get myActionCancel() {
    return LeaveListItemBlockComponent.MY_ACTION_CANCEL;
  }

  async handleAction(type) {
    if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
      this.teamActionTypeSelected = type;
      const teamActionAlert = await this.alertCtrl.create({
        header: "Add Comment",
        inputs: [
          {
            name: "Comments",
            type: "textarea",
            id: "leave-list-item-block-team-action-comment",
            placeholder: "Enter comment here!",
          },
        ],
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
          },
          {
            text: "Submit",
            handler: (data) => {
              if (data && data.Comments.trim() === '') {
                this.coreService.displayToast('Comment is required', 'danger');
                return false;
              }
              const payload = {
                StatusId: this.teamActionTypeSelected === 'reject' ? 3 : 2,
                EmpLeaveId: this.data.EmpLeaveId,
                ...data,
              };
              this.teamLeaveActionCallback(payload);
            },
          },
        ],
      });
      await teamActionAlert.present();
    } else {
      this.coreService.displayToast(LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION, 'warning');
    }
  }

  handleMyAction(type: any) {
    if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
      this.myLeaveActionCallback({ type });
    } else {
      this.coreService.displayToast(LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION, 'warning');
    }
  }

  async handleCancel(type: any) {
    if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online) {
      const cancelCntrol = await this.alertCtrl.create({
        header: "Cancel Leave",
        message: "Are you sure you want to cancel the leave?",
        buttons: [
          {
            text: "No",
            role: "cancel",
          },
          {
            text: "Yes",
            handler: (data) => {
              const payload = {
                EmpLeaveId: type,
                StatusId: 4,
                Comments: '',
                IsSyncOnline: this.data.IsSyncOnline ? this.data.IsSyncOnline : false
              };
              this.cancelLeaveActionCallBack(payload);
            },
          },
        ],
      });
      await cancelCntrol.present();
    } else {
      this.coreService.displayToast(LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION, 'warning');
    }
  }

  handleLoad() {
    this.showProfilePicPlaceHolder = false;
  }
}
