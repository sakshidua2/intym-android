import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CoreService } from "../services/core.service";
import { ApiService } from "../services/api.service";
import { PopoverController, NavController } from "@ionic/angular";
import { PopoverComponent } from "./popover/popover.component";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { LABEL_CONSTANTS } from "../config/labels.constants";
import { APP_CONSTANTS } from "../config/app.constants";
import { Router } from "@angular/router";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.page.html",
  styleUrls: ["./change-password.page.scss"],
})
export class ChangePasswordPage implements OnInit {
  public isSupervisor: boolean = false;
  public changePasswordForm: FormGroup;
  isSubmitted: boolean;
  showPassword: {
    oldP: boolean;
    newP: boolean;
    confP: boolean;
  };
  notificationCount: string | number = 0;
  constructor(
    private fb: FormBuilder,
    private coreService: CoreService,
    private apiService: ApiService,
    private popoverCtrl: PopoverController,
    private navCtrl: NavController,
    private networkService: NetworkService,
    private router: Router,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.showPassword = {
      oldP: false,
      newP: false,
      confP: false,
    };
    this.changePasswordForm = this.fb.group(
      {
        oldPassword: ["", [Validators.required]],
        newPassword: [
          "",
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(15),
          ],
        ],
        confirmPassword: [
          "",
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(15),
          ],
        ],
      },
      { validators: this.comparePassword }
    );
  }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  get oldPasswordCtrl() {
    return this.changePasswordForm.get("oldPassword");
  }
  get newPasswordCtrl() {
    return this.changePasswordForm.get("newPassword");
  }
  get confirmPasswordCtrl() {
    return this.changePasswordForm.get("confirmPassword");
  }

  handleSubmit() {
    this.isSubmitted = true;
    if (
      this.changePasswordForm.valid &&
      this.oldPasswordCtrl.value !== this.newPasswordCtrl.value &&
      this.confirmPasswordCtrl.value === this.newPasswordCtrl.value
    ) {
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Online
      ) {
        this.isSubmitted = false;
        const request = {
          OldPassword: this.changePasswordForm.value.oldPassword,
          NewPassword: this.changePasswordForm.value.newPassword,
        };
        this.apiService.changePassword(request).subscribe(
          (res: any) => {
            if (res) {
              this.coreService.displayToast(res.Status, "success");
              this.changePasswordForm.reset();
              const rememberdData = localStorage.getItem(
                APP_CONSTANTS.REMEMBER_ME_DETAILS
              );
              if (rememberdData) {
                const data = JSON.parse(
                  this.coreService.decryptData(
                    rememberdData,
                    APP_CONSTANTS.REMEMBER_ME_DETAILS_KEY
                  )
                );

                data.userDetails["password"] = request.NewPassword;
                const encrypted = this.coreService.encryptData(
                  JSON.stringify(data),
                  APP_CONSTANTS.REMEMBER_ME_DETAILS_KEY
                );

                localStorage.setItem(
                  APP_CONSTANTS.REMEMBER_ME_DETAILS,
                  encrypted
                );
              }

              this.coreService.notifyOther({
                option: NOTIFY_OBSERVABLE.LOGOUT_ACTION,
                value: { action: "change-password" },
              });
            }
          },
          (err) => {

            const message =
              err &&
                err.error &&
                (err.error["Status"] ||
                  err.error["ErrorMessage"] ||
                  err.error["Message"])
                ? err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"]
                : "Something went wrong! please try later.";
            this.coreService.displayToast(message, "danger");
          }
        );
      } else {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
          "warning"
        );
      }
    }
  }
  async presentPopover(event: any) {
    const popover = await this.popoverCtrl.create({
      component: PopoverComponent,
      cssClass: "my-custom-class",
      event,
      translucent: true,
    });
    return await popover.present();
  }
  ionViewDidLeave() {
    this.changePasswordForm.reset();
  }

  comparePassword(fb: FormGroup) {
    const oldPassword = fb.get("oldPassword").value;
    const newPassword = fb.get("newPassword").value;
    const confirmPassword = fb.get("confirmPassword").value;

    if (oldPassword === newPassword) {
      return { oldNewSame: true };
    }

    if (newPassword !== confirmPassword) {
      return { passwordMismatch: true };
    }
    return null;
  }

  navigate() {
    this.changePasswordForm.reset();
    this.navCtrl.navigateBack("/profile");
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
