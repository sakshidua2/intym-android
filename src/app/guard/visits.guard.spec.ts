import { TestBed } from '@angular/core/testing';

import { VisitsGuard } from './visits.guard';

describe('VisitsGuard', () => {
  let guard: VisitsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(VisitsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
