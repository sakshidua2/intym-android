import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { APP_CONSTANTS } from "../config/app.constants";

@Injectable({
  providedIn: "root",
})
export class VisitsGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const user = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (next.data && next.data.isMyVisit) {
      console.log("guard", user)
      if (
        user &&
        ["Admin", "Supervisor"].includes(user.Role) &&
        !user.AllowRemoteLogin
      ) {
        this.router.navigate(["customer-visit-history/team-visit"]);
        return false;
      }
      if (
        user &&
        !["Admin", "Supervisor"].includes(user.Role) &&
        !user.AllowRemoteLogin
      ) {
        this.router.navigate(["/dashboard"]);
        return false;
      }
      return true;
    } else if (user && !["Admin", "Supervisor"].includes(user.Role)) {
      this.router.navigate(["/dashboard"]);
      return false;
    }
    return true;
  }
}
