import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { CoreService } from "src/app/services/core.service";
import { ApiService } from "src/app/services/api.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { APP_CONSTANTS } from "src/app/config/app.constants";

@Component({
  selector: "app-correction-approve-reject",
  templateUrl: "./correction-approve-reject.component.html",
  styleUrls: ["./correction-approve-reject.component.scss"],
})
export class CorrectionApproveRejectComponent implements OnInit {
  correctionInfo: any;
  correctionReqForm: FormGroup;
  requests: any[];
  public defaultProfileImg = "assets/icon/favicon.png";
  InAction: boolean;
  OutAction: boolean;
  isAppRejScreen: boolean;
  public APPLIED_STATUS_ID = 1;
  public APPROVED_STATUS_ID = 2;
  public REJECTED_STATUS_ID = 3;
  public CANCELLED_STATUS_ID = 4;
  InComments: any;
  OutComments: any;
  constructor(
    public modalCtrl: ModalController,
    private coreService: CoreService,
    private apiService: ApiService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    this.requests = [];
    this.correctionReqForm = new FormGroup({
      Comments: new FormControl("", Validators.required),
    });
  }

  get fControls() {
    return this.correctionReqForm.controls;
  }

  ionViewDidEnter() {
    this.requests = [];
    const { CorrectionRequest } = this.correctionInfo;
    const requestIds = [];
    const requestComments = [];
    if (CorrectionRequest && CorrectionRequest.length) {
      this.isAppRejScreen = CorrectionRequest.filter(
        ({ StatusId }) => StatusId === this.APPLIED_STATUS_ID
      ).length
        ? true
        : false;
    }
    if (CorrectionRequest && CorrectionRequest.length) {
      CorrectionRequest.forEach((ele: any) => {
        if (ele.DayType === "First Half") {
          requestIds.push({
            id: ele.EmpAttnRegularizeId,
            DayType: 1,
            StatusId: ele.StatusId,
            InTimeRequested: ele.InTime,
            RegularizeCode: ele.AttnRegularizeCode,
          });
          this.InComments = {
            sNo: 1,
            commentLabel: "Log In Comments",
            comments: ele.UserComments,
            supervisorComments: ele.SupervisorComments,
          };
          if (ele.StatusId === this.APPLIED_STATUS_ID) {
            this.correctionReqForm.addControl(
              "InTimeStatus",
              new FormControl("")
            );
          }
        } else if (ele.DayType === "Second Half") {
          requestIds.push({
            id: ele.EmpAttnRegularizeId,
            StatusId: ele.StatusId,
            DayType: 2,
            OutTimeRequested: ele.OutTime,
            RegularizeCode: ele.AttnRegularizeCode,
          });

          this.OutComments = {
            sNo: 2,
            commentLabel: "Log Out Comments",
            comments: ele.UserComments,
            supervisorComments: ele.SupervisorComments,
          };
          if (ele.StatusId === this.APPLIED_STATUS_ID) {
            this.correctionReqForm.addControl(
              "OutTimeStatus",
              new FormControl("")
            );
          }
        }
      });
      this.requests = requestIds;
    }
  }

  get InTimeRequest() {
    return this.requests.filter(({ DayType }) => DayType === 1)[0];
  }
  get OutTimeRequest() {
    return this.requests.filter(({ DayType }) => DayType === 2)[0];
  }

  changeStatus(type: "in" | "out", action: boolean) {
    if (this.isAppRejScreen) {
      if (type === "in" && this.fControls.InTimeStatus) {
        const stauts =
          action === this.InAction && this.fControls.InTimeStatus.value
            ? ""
            : true;
        this.fControls.InTimeStatus.setValue(stauts);
        this.InAction = action;
      }
      if (type === "out" && this.fControls.OutTimeStatus) {
        const stauts =
          action === this.OutAction && this.fControls.OutTimeStatus.value
            ? ""
            : true;
        this.fControls.OutTimeStatus.setValue(stauts);
        this.OutAction = action;
      }
    }
  }
  handleSupervisorAction() {
    if (
      this.correctionReqForm.valid &&
      ((this.correctionReqForm.get("InTimeStatus") && this.correctionReqForm.get("InTimeStatus").value) ||
        (this.correctionReqForm.get("OutTimeStatus") && this.correctionReqForm.get("OutTimeStatus").value))
    ) {
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Online
      ) {
        const requests = JSON.parse(JSON.stringify(this.requests));
        requests.forEach((ele: any) => {
          if (
            ele.DayType === 1 &&
            ele.StatusId === this.APPLIED_STATUS_ID &&
            this.correctionReqForm.get("InTimeStatus") &&
            this.correctionReqForm.get("InTimeStatus").value
          ) {
            ele.StatusId = this.InAction ? 2 : 3;
          } else if (
            ele.DayType === 2 &&
            ele.StatusId === this.APPLIED_STATUS_ID &&
            this.correctionReqForm.get("OutTimeStatus") &&
            this.correctionReqForm.get("OutTimeStatus").value
          ) {
            ele.StatusId = this.OutAction ? 2 : 3;
          }
          ele.Comments = this.fControls.Comments.value;
        });
        const data = requests
          .filter((ele) =>
            [this.APPROVED_STATUS_ID, this.REJECTED_STATUS_ID].includes(
              ele.StatusId
            )
          )
          .map((ele: any) => ({
            StatusId: ele.StatusId,
            EmpAttnRegularizeId: ele.id,
            SupervisorComments: ele.Comments,
          }));
        this.apiService.updateAttendanceCorre(data).subscribe(
          (res: any) => {
            if (res) {
              this.coreService.displayToast("success", "success");
              this.modalCtrl.dismiss(true);
            } else {
              this.coreService.displayToast("Something went wrong!", "danger");
            }
          },
          (err) => {
            this.coreService.displayToast("Something went wrong!", "danger");
          }
        );
      } else {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
          "warning"
        );
      }
    } else {
      if (
        this.fControls.Comments &&
        ((this.fControls.InTimeStatus && this.fControls.InTimeStatus.value) ||
          (this.fControls.OutTimeStatus &&
            this.fControls.OutTimeStatus.value)) &&
        this.fControls.Comments.hasError("required")
      ) {
        this.coreService.displayToast("Please write the comment", "danger");
        return;
      }
      if (this.fControls.InTimeStatus && !this.fControls.InTimeStatus.value) {
        this.coreService.displayToast(
          "Approve or reject the InTime correction",
          "danger"
        );
        return;
      }
      if (this.fControls.OutTimeStatus && !this.fControls.OutTimeStatus.value) {
        this.coreService.displayToast(
          "Approve or reject the Outtime correction",
          "danger"
        );
        return;
      }
    }
  }
}
