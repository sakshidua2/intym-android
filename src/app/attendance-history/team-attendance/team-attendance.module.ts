import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamAttendancePageRoutingModule } from './team-attendance-routing.module';
import { AttendanceFilterComponent } from '../attendance-filter/attendance-filter.component';
import { TeamAttendancePage } from './team-attendance.page';
import { CorrectionApproveRejectComponent } from './correction-approve-reject/correction-approve-reject.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgCalendarModule } from 'ionic2-calendar';
import { CustomCalendarComponent } from '../calendar/calendar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TeamAttendancePageRoutingModule,
    SharedModule,
    NgCalendarModule
  ],
  declarations: [TeamAttendancePage, CorrectionApproveRejectComponent, CustomCalendarComponent, AttendanceFilterComponent],
  providers: [DatePipe]
})
export class TeamAttendancePageModule { }
