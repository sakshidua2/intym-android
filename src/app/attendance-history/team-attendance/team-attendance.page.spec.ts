import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeamAttendancePage } from './team-attendance.page';

describe('TeamAttendancePage', () => {
  let component: TeamAttendancePage;
  let fixture: ComponentFixture<TeamAttendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamAttendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TeamAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
