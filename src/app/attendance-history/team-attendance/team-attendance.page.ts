import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ModalController, Platform } from "@ionic/angular";
import { FormBuilder, FormGroup } from "@angular/forms";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { AuthGuardService } from "src/app/services/auth-guard.service";
import { ApiService } from "src/app/services/api.service";
import { AttendanceFilterComponent } from "../attendance-filter/attendance-filter.component";
import { CoreService } from "src/app/services/core.service";
import { CorrectionApproveRejectComponent } from "./correction-approve-reject/correction-approve-reject.component";
import { AttendanceService } from "src/app/services/db/attendance.service";
import { AttendanceModel } from "src/app/local-db-models/attendace";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";

@Component({
  selector: "app-team-attendance",
  templateUrl: "./team-attendance.page.html",
  styleUrls: ["./team-attendance.page.scss"],
})
export class TeamAttendancePage implements OnInit {
  public calendarComponentTypeTeam = APP_CONSTANTS.DATA_TYPE_TEAM;
  statusList: any[];
  attendanceList: any[];
  attendanceCopy: any[];
  userList: any[];
  attendanceFilter: { min: any; max: any };
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  public COLOR_CODES = { 1: "Pending", 2: "Approved", 3: "Rejected" };

  isMaxMonth: boolean;
  isMinMonth: boolean;
  attendFilterForm: FormGroup;
  public attendanceStatusTeamData = {};
  userDetails: any;
  screenType: string;
  body: any;
  defaultFilterValue: {
    User?: number;
  };
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  public isLeavesListReadyToRender = false;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    public authGuardService: AuthGuardService,
    private apiService: ApiService,
    private coreService: CoreService,
    private attendaceService: AttendanceService,
    private networkService: NetworkService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.statusList = ["pending", "approved", "rejected"];
    this.body = {};
    this.attendanceFilter = {
      min: this.coreService.getMomentNow(),
      max: this.coreService.getMomentNow(),
    };
    this.attendanceFilter.min.set({
      year: 2000,
      month: 0,
      date: 1,
    });
    this.isMaxMonth = true;

    this.attendFilterForm = this.fb.group({
      User: [""],
    });

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }

    this.isLeavesListReadyToRender = false;
    const date: any = this.coreService.getMomentNow();

    this.body = {
      FromDate: date,
      ToDate: this.coreService.getMomentNow(),
    };
    this.isMaxMonth = true;
    this.defaultFilterValue = {
      User: 0,
    };

    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAtendanceFromLocal();
    } else {
      this.getTeamAttendance();
    }
    this.attendFilterForm.patchValue(this.defaultFilterValue);
  }

  handleTeamCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    this.cdr.detectChanges();
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAtendanceFromLocal();
    } else {
      this.getTeamAttendance();
      if (!payload.sameMonth) { this.getAttendanceStatusCalendar(); }
    }
  }

  getAttendanceStatusCalendar() {
    this.attendanceStatusTeamData = {};
    this.apiService
      .getAttendanceStatusCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          Filters: this.body.Filters,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_TEAM
      )
      .subscribe((res: any) => {
        this.attendanceStatusTeamData = {};
        if (res.L1) {
          [...res.L1].map((o) => {
            const date = this.coreService.getMomentFormatDate(
              this.yearFormatted,
              o.Date
            );
            this.attendanceStatusTeamData = {
              ...this.attendanceStatusTeamData,
              [date]: {
                AbsentCount: o.AbsentCount,
                LeaveCount: o.LeaveCount,
                PresentCount: o.PresentCount,
              },
            };
            return true;
          });
        }
        if (res.L2) {
          [...res.L2].map((o) => {
            const date = this.coreService.getMomentFormatDate(
              this.yearFormatted,
              o.Date
            );
            if (this.attendanceStatusTeamData[date]) {
              this.attendanceStatusTeamData[date].AbsentCount += o.AbsentCount;
              this.attendanceStatusTeamData[date].LeaveCount += o.LeaveCount;
              this.attendanceStatusTeamData[date].PresentCount += o.PresentCount;
            } else {
              this.attendanceStatusTeamData = {
                ...this.attendanceStatusTeamData,
                [date]: {
                  AbsentCount: o.AbsentCount,
                  LeaveCount: o.LeaveCount,
                  PresentCount: o.PresentCount,
                },
              };
            }
            return true;
          });
        }
        if (res.L3) {
          [...res.L3].map((o) => {
            const date = this.coreService.getMomentFormatDate(
              this.yearFormatted,
              o.Date
            );

            if (this.attendanceStatusTeamData[date]) {
              this.attendanceStatusTeamData[date].AbsentCount += o.AbsentCount;
              this.attendanceStatusTeamData[date].LeaveCount += o.LeaveCount;
              this.attendanceStatusTeamData[date].PresentCount += o.PresentCount;
            } else {
              this.attendanceStatusTeamData = {
                ...this.attendanceStatusTeamData,
                [date]: {
                  AbsentCount: o.AbsentCount,
                  LeaveCount: o.LeaveCount,
                  PresentCount: o.PresentCount,
                },
              };
            }
            return true;
          });
        }
        if (res.length > 0) {
          [...res].map((o) => {
            const date = this.coreService.getMomentFormatDate(
              this.yearFormatted,
              o.Date
            );

            if (this.attendanceStatusTeamData[date]) {
              this.attendanceStatusTeamData[date].AbsentCount += o.AbsentCount;
              this.attendanceStatusTeamData[date].LeaveCount += o.LeaveCount;
              this.attendanceStatusTeamData[date].PresentCount += o.PresentCount;
            } else {
              this.attendanceStatusTeamData = {
                ...this.attendanceStatusTeamData,
                [date]: {
                  AbsentCount: o.AbsentCount,
                  LeaveCount: o.LeaveCount,
                  PresentCount: o.PresentCount,
                },
              };
            }
            return true;
          });
        }
      });
  }

  async handleFilter() {
    const modalRef = await this.modalCtrl.create({
      component: AttendanceFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        attendanceFilterForm: this.attendFilterForm,
        defaultValues: this.defaultFilterValue,
      },
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          console.log(res.data);
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          if (!this.body.CalendarFromDate || !this.body.CalendarToDate) {
            this.body.CalendarFromDate = this.coreService
              .getMomentNow()
              .set("date", 1)
              .format(this.yearFormatted);
            this.body.CalendarToDate = this.coreService.getMomentNow().format(this.yearFormatted);
          }
          this.body.Filters = { EmployeeId: res.data.User };
          this.body.User = res.data.User;
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAtendanceFromLocal();
          } else {
            this.getTeamAttendance();
            this.getAttendanceStatusCalendar();
          }
          let list = JSON.parse(JSON.stringify(this.attendanceCopy));
          list = this.dataAdjustment(
            JSON.parse(JSON.stringify(this.attendanceCopy))
          );
          this.groutAtendByDat(list);
        } else {
          this.body = {
            FromDate: this.coreService.getMomentObject(this.body.FromDate),
            ToDate: this.coreService.getMomentObject(this.body.ToDate),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAtendanceFromLocal();
          } else {
            this.getTeamAttendance();
            this.getAttendanceStatusCalendar();
          }
        }
      }
    });
  }

  groutAtendByDat(list?: any) {
    this.attendanceList = list.reduce((result: any, currentValue: any) => {
      (result[currentValue["AttendanceDate"]] =
        result[currentValue["AttendanceDate"]] || []).push(currentValue);
      return result;
    }, {});
    this.attendanceList = Object.keys(this.attendanceList).map((date) => {
      return {
        date,
        data: this.attendanceList[date],
        Collapsed: true,
      };
    });
  }

  nextMonthAttend() {
    const fromdate: any = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    fromdate.add(1, "M");
    fromdate.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });
    const todate = this.coreService
      .getMomentByDate(this.body.ToDate.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      });
    todate.add(1, "M");
    this.body.Status = "all";
    this.body.User = 0;

    if (this.attendanceFilter.max.unix() > fromdate.unix()) {
      fromdate.set({ date: 1 });
      if (fromdate.month() - this.body.FromDate.month() !== 1) {
        fromdate.subtract(1, "M");
      }
      this.body.FromDate = this.coreService.getMomentByDate(
        fromdate.toISOString()
      );
      todate.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(todate.toISOString());

      this.attendFilterForm.patchValue({
        User: this.body.User,
      });

      this.isMinMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAtendanceFromLocal();
      } else {
        this.getTeamAttendance();
      }
    } else {
      const max = this.attendanceFilter.max;

      if (
        this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) !==
        max.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
      ) {
        this.body.ToDate = max;

        this.body.FromDate = this.coreService.getMomentNow();
        this.body.FromDate.set({ date: 1 });

        this.attendFilterForm.patchValue({
          User: this.body.User,
        });
        this.isMaxMonth = true;

        if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Offline
        ) {
          this.getAtendanceFromLocal();
        } else {
          this.getTeamAttendance();
        }
      }
    }
  }

  prevMonthAttend() {
    const fromdate: any = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    fromdate.subtract(1, "M");
    fromdate.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });
    const todate = this.coreService
      .getMomentByDate(this.body.ToDate.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      });
    todate.subtract(1, "M");
    this.body.Status = "all";
    this.body.User = 0;

    if (this.attendanceFilter.min.unix() <= fromdate.unix()) {
      fromdate.set({ date: 1 });
      if (this.body.FromDate.month() === fromdate.month()) {
        fromdate.subtract(1, "M");
      }

      this.body.FromDate = this.coreService.getMomentByDate(
        fromdate.toISOString()
      );
      todate.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(todate.toISOString());

      this.attendFilterForm.patchValue({
        User: this.body.User,
      });

      this.isMaxMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAtendanceFromLocal();
      } else {
        this.getTeamAttendance();
      }
    } else {
      this.isMinMonth = true;
    }
  }

  getTeamAttendance(e?: any, StopLoader?: boolean) {
    this.apiService
      .getMyAttendance({
        FromDate: this.body.FromDate.format(
          APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
        ),
        ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        StopLoader,
      })
      .subscribe(
        (res: any) => {
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
          if (res && res.MyTeamAttendance && res.MyTeamAttendance.length) {
            this.userList = res.MyTeamAttendance.map((ele) => ele.EmployeeId)
              .map((ele, i, final) => final.indexOf(ele) === i && i)
              .filter((ele) => res.MyTeamAttendance[ele])
              .map((e) => res.MyTeamAttendance[e]);
            // this.userList = this.userList.map(
            //   ({ EmployeeId, EmployeeName }) => ({
            //     EmployeeId,
            //     EmployeeName,
            //   })
            // );
            this.attendanceCopy = res.MyTeamAttendance.map((o) => ({
              ...o,
              IsSyncOnline: true,
              IsMyAttendance: 1,
            }));
            const list = this.dataAdjustment(
              JSON.parse(JSON.stringify(this.attendanceCopy))
            );

            this.groutAtendByDat(list);

            this.isLeavesListReadyToRender = true;
            if (this.platform.is("cordova")) {
              const myAttendance = [];
              this.attendanceCopy.forEach((ele: any) => {
                const obj = JSON.parse(JSON.stringify(ele));
                obj.CorrectionRequest =
                  obj.CorrectionRequest && obj.CorrectionRequest.length
                    ? JSON.stringify(obj.CorrectionRequest)
                    : "";
                obj.SpentHours = obj.SpentHours
                  ? JSON.stringify(obj.SpentHours)
                  : "";
                myAttendance.push(obj);
              });
              this.attendaceService.storeAttendanceForOffline(myAttendance);
            }

            if (history.state.data) {
              const notificationData = history.state["data"];
              const date = this.coreService.getMomentByDate(
                notificationData.date
              );
              setTimeout(() => {
                if (
                  date.isSame(this.body.FromData, "month") &&
                  date.isSame(this.body.FromData, "year")
                ) {
                  const list = [];
                  this.attendanceList.forEach(({ data }) => list.push(...data));
                  const request = list.filter(
                    ({ CorrectionRequest }) =>
                      CorrectionRequest &&
                      CorrectionRequest.some(
                        (ele) => ele.EmpAttnRegularizeId === notificationData.id
                      )
                  )[0];
                  this.navigate(request, "team-correction");
                } else {
                  this.notificationRequest(notificationData);
                }
                history.state.data = undefined;
              }, 500);
            }
          } else {
            this.attendanceList = [];
            this.attendanceCopy = [];
          }
          this.isLeavesListReadyToRender = true;
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          this.attendanceList = [];
          this.attendanceCopy = [];
        }
      );
  }

  async navigate(item, type) {
    if (item) {
      const item_info = Object.assign({}, item);
      const date = this.coreService.getMomentObject(item.AttendanceDate);
      item_info.date = date;
      if (
        item.CorrectionRequest &&
        !item.CorrectionRequest.filter(({ DayType }) => DayType === "Full Day")
          .length
      ) {
        const mod = await this.modalCtrl.create({
          component: CorrectionApproveRejectComponent,
          cssClass: "corr-page",
          componentProps: {
            correctionInfo: item_info,
          },
          animated: true,
          swipeToClose: true,
        });
        mod.present();
        mod.onDidDismiss().then((res) => {
          if (res && res.data) {
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getAtendanceFromLocal();
            } else {
              this.getTeamAttendance();
            }
          }
        });
      }
    }
  }

  getAtendanceFromLocal(e?: any) {
    const params = JSON.parse(JSON.stringify(this.body));
    params.FromDate = this.coreService
      .getMomentByDate(params.FromDate)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    params.ToDate = this.coreService
      .getMomentByDate(params.ToDate)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    params.IsMyAttendance = 1;

    this.attendaceService
      .getData(params)
      .then((res: AttendanceModel[]) => {
        res.forEach((ele: any) => {
          ele.CorrectionRequest =
            ele.CorrectionRequest && ele.CorrectionRequest.length
              ? JSON.parse(ele.CorrectionRequest)
              : "";
          ele.SpentHours = ele.SpentHours ? JSON.parse(ele.SpentHours) : "";
        });

        this.attendanceCopy = res;
        const list = this.dataAdjustment(
          JSON.parse(JSON.stringify(this.attendanceCopy))
        );
        this.groutAtendByDat(list);
        this.isLeavesListReadyToRender = true;
      })
      .catch((err) => {
        this.coreService.displayToast(
          "You are offline and no data found for user locally",
          "warning"
        );
        this.isLeavesListReadyToRender = true;
      })
      .finally(() => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      });
  }

  dataAdjustment(list) {
    list.forEach((element) => {
      if (element.CorrectionRequest && element.CorrectionRequest.length) {
        element.CorrectionRequest.forEach((request) => {
          if (
            request.DayType === "First Half" &&
            element.InTime &&
            element.InTime !== "-"
          ) {
            element.InTimeStatus = this.COLOR_CODES[request.StatusId];
          } else if (
            request.DayType === "Second Half" &&
            element.OutTime &&
            element.OutTime !== "-"
          ) {
            element.OutTimeStatus = this.COLOR_CODES[request.StatusId];
          } else if (
            request.DayType === "Full Day" &&
            element.InTime &&
            element.InTime !== "-" &&
            element.OutTime &&
            element.OutTime !== "-"
          ) {
            element.InTimeStatus = element.OutTimeStatus = this.COLOR_CODES[
              request.StatusId
            ];
          }

          if (request.InTime) {
            request.InTime = this.coreService.getTimeByLocalZone(
              request.InTime,
              APP_CONSTANTS.EXACT_TIME_24_FORMAT
            );
          }

          if (request.OutTime) {
            request.OutTime = this.coreService.getTimeByLocalZone(
              request.OutTime,
              APP_CONSTANTS.EXACT_TIME_24_FORMAT
            );
          }
        });
      }

      if (element.InTime !== "-") {
        element.InTime = this.coreService.getTimeByLocalZone(
          element.InTime,
          APP_CONSTANTS.EXACT_TIME_24_FORMAT
        );
      }
      if (element.OutTime !== "-") {
        element.OutTime = this.coreService.getTimeByLocalZone(
          element.OutTime,
          APP_CONSTANTS.EXACT_TIME_24_FORMAT
        );
      }

      if (element.InTime !== "-" && element.OutTime !== "-") {
        const SpentHours = this.coreService.getTimeDifference(
          element.InTime,
          element.OutTime
        );
        element.SpentHours = SpentHours;
      }
    });

    if (this.body.User && this.body.User.length !== 0) {
      list = this.attendanceCopy.filter(
        (ele) => this.body.User.indexOf(ele.EmployeeId) > -1
      );
    }
    if (this.body.Status && this.body.Status !== "all") {
      list.forEach(({ CorrectionRequest }, i) => {
        if (CorrectionRequest && CorrectionRequest.length) {
          list[i].CorrectionRequest = list[i].CorrectionRequest.filter(
            ({ StatusId }) => this.STATUS_CODES[this.body.Status] === StatusId
          );
        }
      });
      list = list.filter(
        ({ CorrectionRequest }) => CorrectionRequest && CorrectionRequest.length
      );
    }
    return list;
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAtendanceFromLocal(e);
    } else {
      this.getTeamAttendance(e, true);
      this.getAttendanceStatusCalendar();
    }
  }

  generatePdf() {
    let EmployeeName = "";
    if (this.body.User) {
      const index = this.attendanceList.findIndex(
        (ele) => ele.EmployeeId === this.body.User
      );
      EmployeeName = index >= 0 ? this.attendanceList[index].EmployeeName : "";
    }
    let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Team Attendance Report</h3>
      </div>
      <div>
      <table class="info-table">
          <tr>
            <td>Employee: ${
      this.body.User && EmployeeName ? EmployeeName : "All"
      }</td>
            <td>From Date: ${this.body.FromDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Employee</th>
              <th>Attendance Date</th>
              <th>In Time</th>
              <th>Out Time</th>
              <th>Total (Hrs)</th>
              <th>Status</th>
              <th>Shift</th>
            </tr>
          </thead>
          <tbody>`;
    this.attendanceList.forEach((item) => {
      item.data.forEach((element) => {
        const AttendanceDate = this.coreService
          .getMomentByDate(element.AttendanceDate)
          .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
        const InTime =
          element.InTime && element.InTime !== "-"
            ? this.coreService.getMomentByTime(
              element.InTime,
              APP_CONSTANTS.TIME_12_FORMAT
            )
            : "";
        const OutTime =
          element.OutTime && element.OutTime !== "-"
            ? this.coreService.getMomentByTime(
              element.OutTime,
              APP_CONSTANTS.TIME_12_FORMAT
            )
            : "";
        const row = `
                      <tr>
                        <td>${element.Pin}</td>
                        <td>${element.EmployeeName}</td>
                        <td>${AttendanceDate}</td>
                        <td>${InTime}</td>
                        <td>${OutTime}</td>
                        <td>${
          element.SpentHours ? element.SpentHours.label : "-"
          }</td>
                        <td>${element.Status}</td>
                        <td>${element.Shift}</td>
                      </tr>`;
        html += row;
      });
    });
    const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                      <p>P-Present, A-Absent, FHA-First Half Absent, SHA-Second Half Absent, H-Holiday, W-Weekly Off , HDA-Half Day Absent, L-Leave, FHL-First Half Leave, SHL-Second Half Leave, OD-On Duty, FHOD-First Half On Duty, SHOD-Second Half On Duty, AC-Attendance Correction, FHAC-First Half Attendance Correction, SHAC-Second Half Attendance Correction,
                      FHP-First Half Present, SHP-Second Half Present, EO-Early Out, LA- Late Arrival
                      </p>
                      <p>Report Generated By: ${this.userDetails.FirstName} ${
      this.userDetails.LastName
      } : ${this.coreService.getMomentNow("DD MMMM yyyy")}</p>
                    </div>
                  </div>
                </body>
              </html>`;
    html += endOfHtml;
    this.pdfGenerator
      .fromData(html, {
        documentSize: "A4",
        fileName: `team-attendance_report${this.coreService.getMomentNow(
          "DDMMYYYY_hhmmss"
        )}.pdf`,
        type: "share",
      })
      .then((res) => {
        console.log("res =>", res);
      })
      .catch((err) => {
        console.log("error =>", err);
      });
  }

  notificationRequest(data: { id: number; date: string }) {
    const date = this.coreService
      .getMomentObject(data.date)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    this.apiService
      .getMyAttendance({
        FromDate: date,
        ToDate: date,
      })
      .subscribe((res: any) => {
        if (res && res.MyTeamAttendance && res.MyTeamAttendance.length) {
          const result = res.MyTeamAttendance.filter(
            ({ CorrectionRequest }) =>
              CorrectionRequest &&
              CorrectionRequest.some(
                (ele) => ele.EmpAttnRegularizeId === data.id
              )
          );
          const item = this.dataAdjustment(result);
          this.navigate(item[0], "team-correction");
        }
      });
    history.state.data = undefined;
  }
  ionViewDidLeave() {
    this.isLeavesListReadyToRender = true;
    this.attendanceList = [];
    this.attendanceCopy = [];
    this.userList = [];
  }
}
