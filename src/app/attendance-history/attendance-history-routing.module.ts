import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendanceHistoryPage } from './attendance-history.page';

const routes: Routes = [
  {
    path: '',
    component: AttendanceHistoryPage,
    children: [
      {
        path: '',
        redirectTo: 'my-attendance',
        pathMatch: 'full'
      },
      {
        path: 'my-attendance',
        children: [
          {
            path: '',
            loadChildren: () => import('./my-attendance/my-attendance.module').then(m => m.MyAttendancePageModule)
          },
        ]
      },
      {
        path: 'team-attendance',
        children: [
          {
            path: '',
            loadChildren: () => import('./team-attendance/team-attendance.module').then(m => m.TeamAttendancePageModule)
          }
        ]
      },
    ]
  },
  {
    path: 'correction-request',
    loadChildren: () => import('./correction-request/correction-request.module').then( m => m.CorrectionRequestPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceHistoryPageRoutingModule { }
