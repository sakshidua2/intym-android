import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorrectionRequestPage } from './correction-request.page';

describe('CorrectionRequestPage', () => {
  let component: CorrectionRequestPage;
  let fixture: ComponentFixture<CorrectionRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrectionRequestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorrectionRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
