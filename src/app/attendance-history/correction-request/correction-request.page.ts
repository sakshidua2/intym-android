import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CoreService } from "src/app/services/core.service";
import { ModalController } from "@ionic/angular";
import { ApiService } from "src/app/services/api.service";
import { forkJoin } from "rxjs";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";

@Component({
  selector: "app-correction-request",
  templateUrl: "./correction-request.page.html",
  styleUrls: ["./correction-request.page.scss"],
})
export class CorrectionRequestPage implements OnInit {
  correctionReqForm: FormGroup;
  isSubmitted: boolean;
  correctItem: any;
  type: string;
  AttnRegularizeTypeId: { in: number; out: number };
  AttnRegularizeTypes: any[];
  inTimeComment: {
    commentFor: number;
    commentForLabel: string;
    comment: string;
    supervisorComment: string;
  };
  outTimeComment: {
    commentFor: number;
    commentForLabel: string;
    comment: string;
    supervisorComment: string;
  };
  public APPLIED_STATUS_ID = 1;
  public APPROVED_STATUS_ID = 2;
  public REJECTED_STATUS_ID = 3;
  public CANCELLED_STATUS_ID = 4;

  public requestIds = [];
  public blockInTime = false;
  public blockOutTime = false;
  public inTimeCorrected: any;
  public outTimeCorrected: any;
  public inTimeStatus = this.APPLIED_STATUS_ID;
  public outTimeStatus = this.APPLIED_STATUS_ID;
  public requestComments = [];
  public showCorrectionRequestForm = true;

  constructor(
    private coreService: CoreService,
    public modalCtrl: ModalController,
    private apiService: ApiService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.correctionReqForm = new FormGroup({
      in_time: new FormControl(""),
      out_time: new FormControl(""),
      comment: new FormControl("", [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
    });

    if (this.correctItem.InTime === "-") {
      this.correctionReqForm.get("in_time").disable();
    }

    if (this.correctItem.OutTime === "-") {
      this.correctionReqForm.get("out_time").disable();
    }
  }

  ionViewDidEnter() {
    const { CorrectionRequest } = this.correctItem;
    const requestIds = [];
    const requestComments = [];
    const blockTimeStatus = [
      this.APPLIED_STATUS_ID,
      this.APPROVED_STATUS_ID,
      this.REJECTED_STATUS_ID,
      this.CANCELLED_STATUS_ID,
    ];
    if (CorrectionRequest && CorrectionRequest.length) {
      for (let index = 0; index < CorrectionRequest.length; index++) {
        const request = CorrectionRequest[index];
        if (request.DayType === "First Half") {
          requestIds.push({
            dayType: 1,
            dayTypeLabel: "Log In",
            id: request.EmpAttnRegularizeId,
          });
          this.inTimeComment = {
            commentFor: 1,
            commentForLabel: "Log In Comments",
            comment: request.UserComments,
            supervisorComment: request.SupervisorComments,
          };
          this.blockInTime = blockTimeStatus.indexOf(request.StatusId) >= 0;
          this.inTimeCorrected = request.InTime;
          this.correctItem.InAttnRegularizeCode = request.AttnRegularizeCode;
          this.inTimeStatus = request.StatusId;
          this.correctionReqForm.removeControl("in_time");
        } else if (request.DayType === "Second Half") {
          this.correctionReqForm.removeControl("out_time");
          requestIds.push({
            dayType: 2,
            dayTypeLabel: "Log Out",
            id: request.EmpAttnRegularizeId,
          });
          this.outTimeComment = {
            commentFor: 2,
            commentForLabel: "Log Out Comments",
            comment: request.UserComments,
            supervisorComment: request.SupervisorComments,
          };
          this.correctItem.OutAttnRegularizeCode = request.AttnRegularizeCode;
          this.blockOutTime = blockTimeStatus.indexOf(request.StatusId) >= 0;
          this.outTimeCorrected = request.OutTime;
          this.outTimeStatus = request.StatusId;
        }
      }
      this.requestIds = requestIds;
      this.requestComments = requestComments.sort(
        (a, b) => a.commentFor - b.commentFor
      );
      this.showCorrectionRequestForm = CorrectionRequest.length !== 2;
      this.correctionReqForm.patchValue({
        in_time: this.inTimeCorrected,
        out_time: this.outTimeCorrected,
      });
    }
    this.RegularizationTypes();
  }

  sendReq() {
    this.isSubmitted = true;
    if (this.correctionReqForm.valid) {
      const requestDate: any = this.coreService
        .getMomentByDate(this.correctItem.AttendanceDate)
        .set({
          hour: 0,
          minute: 0,
          second: 0,
          millisecond: 0,
        });
      const toDay: any = this.coreService.getMomentNow();
      toDay.set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      });
      if (requestDate.unix() !== toDay.unix()) {
        const { in_time, out_time, comment } = this.correctionReqForm.value;
        const payloads = [];
        if (in_time) {
          payloads.push({
            InTime: this.generateDateForCorrection(
              this.correctItem.AttendanceDate,
              in_time
            ),
            OutTime: "",
            AttnRegularizeTypeId: this.AttnRegularizeTypeId.in,
            CreatedDate: this.coreService.getMomentNow(),
            DayType: 1,
            UserComments: comment,
          });
        }
        if (out_time) {
          payloads.push({
            InTime: "",
            OutTime: this.generateDateForCorrection(
              this.correctItem.AttendanceDate,
              out_time
            ),
            AttnRegularizeTypeId: this.AttnRegularizeTypeId.out,
            CreatedDate: this.coreService.getMomentNow(),
            DayType: 2,
            UserComments: comment,
          });
        }
        if (payloads.length) {
          this.requestForCorrection(payloads);
        } else {
          this.coreService.displayToast(
            "Please In/Out Time to make request",
            "danger"
          );
        }
      } else {
        this.coreService.displayToast(
          "Request can not be applied on the same day",
          "danger"
        );
      }
    }
  }
  RegularizationTypes() {
    const regularization = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.REGULARIZATION_DETAILS)
    );
    if (regularization) {
      this.AttnRegularizeTypes = [...regularization["data"]];
      this.AttnRegularizeTypeId = {
        in: this.AttnRegularizeTypes[0].AttnRegularizeTypeId,
        out: this.AttnRegularizeTypes[0].AttnRegularizeTypeId,
      };
    } else {
      this.addRegularization();
    }
  }

  addRegularization() {
    this.apiService.getRegularizationTypes().subscribe((res: any) => {
      if (res) {
        const AttnRegularizeTypes = {
          data: res,
          date: this.coreService.getMomentNow().unix(),
        };
        localStorage.setItem(
          APP_CONSTANTS.REGULARIZATION_DETAILS,
          JSON.stringify(AttnRegularizeTypes)
        );
        this.AttnRegularizeTypes = [...res];
        this.AttnRegularizeTypeId = {
          in: this.AttnRegularizeTypes[0].AttnRegularizeTypeId,
          out: this.AttnRegularizeTypes[0].AttnRegularizeTypeId,
        };
      }
    });
  }

  timeAdjust(event, type) {
    this.correctionReqForm.get("comment").setValidators(Validators.required);
    this.correctionReqForm.get("comment").updateValueAndValidity();

    const val = event.detail.value;
    if (
      this.correctionReqForm.value.out_time &&
      this.correctionReqForm.value.in_time >
      this.correctionReqForm.value.out_time
    ) {
      this.correctionReqForm
        .get("out_time")
        .setValue(this.correctItem.out_time);
      this.coreService.displayToast(
        "Out time should higher then in time",
        "danger"
      );
    }
  }

  get correctionCntrl() {
    return this.correctionReqForm.controls;
  }
  navigate() {
    this.correctionReqForm.reset();
    this.modalCtrl.dismiss();
  }

  generateDateForCorrection(date, correction) {
    const dt: any = this.coreService.getMomentByDate(date);
    const correctionDateTime: any = this.coreService.getMomentObject(
      correction
    );
    const correctionTime = correctionDateTime.format(
      APP_CONSTANTS.EXACT_TIME_24_FORMAT
    );
    const correctionTimeArr = correctionTime.split(":");
    return dt.set({
      hour: correctionTimeArr[0],
      minute: correctionTimeArr[1],
    });
  }

  requestForCorrection(payloads) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      if (payloads.length === 1) {
        this.apiService.applyAttendanceCorrection(payloads[0]).subscribe(
          (res: any) => {
            if (res) {
              this.coreService.displayToast(res.Message, "success");
            }
            this.modalCtrl.dismiss(true);
          },
          (err) => {
            const message =
              err &&
                err.error &&
                (err.error["Status"] ||
                  err.error["ErrorMessage"] ||
                  err.error["Message"])
                ? err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"]
                : "Something went wrong!";

            this.coreService.displayToast(message, "danger");
          }
        );
      } else if (payloads.length === 2) {
        forkJoin([
          this.apiService.applyAttendanceCorrection(payloads[0]),
          this.apiService.applyAttendanceCorrection(payloads[1]),
        ]).subscribe(
          ([res1, res2]) => {
            if (res1) {
              this.coreService.displayToast(
                "Successfully submitted",
                "success"
              );
            }
            this.modalCtrl.dismiss(true);
          },
          (err) => {
            const msg =
              err && err.error && err.error.Message
                ? err.error.Message
                : "Something went wrong!";
            this.coreService.displayToast(msg, "danger");
          }
        );
      }
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }
}
