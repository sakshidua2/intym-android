import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorrectionRequestPage } from './correction-request.page';

const routes: Routes = [
  {
    path: '',
    component: CorrectionRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorrectionRequestPageRoutingModule {}
