import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: "app-attendance-filter",
  templateUrl: "./attendance-filter.component.html",
  styleUrls: ["./attendance-filter.component.scss"],
})
export class AttendanceFilterComponent implements OnInit {
  @Input() attendanceFilterForm: FormGroup;
  userList: any[] = [];
  @Input() defaultValues: any;
  isSubmitted: boolean;
  constructor(
    public modalCtrl: ModalController,
    public coreService: CoreService,
    public apiService: ApiService
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.getEmployeeList();
  }

  get fControls() {
    return this.attendanceFilterForm.controls;
  }

  getEmployeeList() {
    this.apiService.getEmployeeList().subscribe((res: any) => {
      if (res.L1) {
        this.userList = this.userList.concat(res.L1);
      }
      if (res.L2) {
        this.userList = this.userList.concat(res.L2);
      }
      if (res.L3) {
        this.userList = this.userList.concat(res.L3);
      }
    });
  }

  dismissModal() {
    this.modalCtrl.dismiss(undefined, "close");
  }

  onSubmit() {
    if (this.attendanceFilterForm.valid) {
      const data = this.attendanceFilterForm.getRawValue();
      const canEmit = Object.values(data).filter((ele) => ele);
      if (canEmit && canEmit.length) {
        this.modalCtrl.dismiss(data, "apply");
        return;
      }
      this.modalCtrl.dismiss(undefined, "apply");
    }
  }

  resetValue() {
    this.attendanceFilterForm.reset();
    this.attendanceFilterForm.patchValue(this.defaultValues);
    this.onSubmit();
  }
}
