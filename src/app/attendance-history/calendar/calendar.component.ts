import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { CalendarComponent } from "ionic2-calendar";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
})
export class CustomCalendarComponent implements OnInit {
  static COMPONENT_TYPE_MY = APP_CONSTANTS.DATA_TYPE_MY;
  static COMPONENT_TYPE_TEAM = APP_CONSTANTS.DATA_TYPE_TEAM;
  @ViewChild(CalendarComponent, null) myCalendar: CalendarComponent;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  body: any;
  today: string;
  selectedDay: string;
  monthNumber: number;
  selectedMonth = -1;

  @Input() componentType = CustomCalendarComponent.COMPONENT_TYPE_TEAM;
  @Input() attendanceStatusData = {};
  @Input() attendanceStatusTeamData = {};
  @Input() getMyCountActionCallback: (payload: any) => void;
  @Input() getTeamCountActionCallback: (payload: any) => void;

  get componentTypeMy() {
    return CustomCalendarComponent.COMPONENT_TYPE_MY;
  }
  get componentTypeTeam() {
    return CustomCalendarComponent.COMPONENT_TYPE_TEAM;
  }

  constructor(
    private apiService: ApiService,
    private coreService: CoreService
  ) { }

  viewTitle;
  public calendar = {
    mode: "month",
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewDay: (date: Date) => {
        return date.getDate();
      },
    },
  };

  ngOnInit() {
    this.body = {
      FromDate: this.coreService
        .getMomentNow()
        .set("date", 1)
        .format(this.yearFormatted),
      ToDate: this.coreService.getMomentNow().format(this.yearFormatted),
    };
    this.monthNumber = this.coreService.getMomentObject().month();
    this.selectedDay = this.today = this.coreService.getMomentNow(
      this.yearFormatted
    );
  }

  prevMonth() {
    this.myCalendar.slidePrev();
  }

  nextMonth() {
    this.myCalendar.slideNext();
  }

  selectToday() {
    this.calendar.currentDate = new Date();
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onCurrentDateChanged(ev) {
    const date = this.coreService.getMomentObject(ev);
    const startTime = date.format(this.yearFormatted);
    const endTime = date.format(this.yearFormatted);
    this.selectedDay = date.format(this.yearFormatted);
    if (date.month() !== this.monthNumber) {
      this.body.FromDate = date.startOf("month").format(this.yearFormatted);
      this.body.ToDate = date.endOf("month").format(this.yearFormatted);
    } else {
      this.body = {
        FromDate: this.coreService
          .getMomentNow()
          .set("date", 1)
          .format(this.yearFormatted),
        ToDate: this.coreService.getMomentNow().format(this.yearFormatted),
      };
    }
    let sameMonth = true;
    if (date.month() !== this.selectedMonth) {
      sameMonth = false;
      this.selectedMonth = date.month();
    }
    if (this.componentType === this.componentTypeMy) {
      this.getMyCountActionCallback({ startTime, endTime, body: this.body, sameMonth });
    } else {
      this.getTeamCountActionCallback({ startTime, endTime, body: this.body, sameMonth });
    }
  }

  handleExport() {
    this.apiService.createReport({ ...this.body, ReportId: 1 }).subscribe((res: any) => {
      this.coreService.displayToast(LABEL_CONSTANTS.EXPORT_SUCCESS, "success");
    });
  }

  isVisible(date) {
    const today = this.coreService.getMomentObject();
    const calendarDate = this.coreService.getMomentObject(date);
    if (calendarDate.format() > today.format()) {
      return false;
    }
    return true;
  }

  getMyClass(date) {
    let cls = '';
    if (date === this.selectedDay) {
      cls = 'selected';
    }

    if (date === this.today) {
      cls += ' today-class';
    }

    return cls;
  }

  getMyPTagClass(date) {
    let cls = '';
    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'P') {
      cls += ' approved';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'HDA') {
      cls += ' hda';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'NP') {
      cls += ' nopunch';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'L') {
      cls += ' leave';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'H') {
      cls += ' disabled';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'A') {
      cls += ' reject';
    }

    return cls;
  }

  getClass(date) {
    let cls = '';
    if (date === this.selectedDay) {
      cls = 'selected';
    }

    if (date === this.today) {
      cls += ' today-class';
    }

    if (!this.isVisible(date)) {
      return cls;
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'P') {
      cls += ' approved';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'HDA') {
      cls += ' hda';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'NP') {
      cls += ' nopunch';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'L') {
      cls += ' leave';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'H') {
      cls += ' disabled';
    }

    if (this.attendanceStatusData && this.attendanceStatusData[date] === 'A') {
      cls += ' reject';
    }

    return cls;
  }
}
