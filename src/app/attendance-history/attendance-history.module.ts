import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendanceHistoryPageRoutingModule } from './attendance-history-routing.module';

import { AttendanceHistoryPage } from './attendance-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AttendanceHistoryPageRoutingModule
  ],
  declarations: [AttendanceHistoryPage]
})
export class AttendanceHistoryPageModule { }
