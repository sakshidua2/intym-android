import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyAttendancePageRoutingModule } from './my-attendance-routing.module';

import { MyAttendancePage } from './my-attendance.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { CustomCalendarComponent } from '../calendar/calendar.component';
import { NgCalendarModule } from 'ionic2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule,
    ReactiveFormsModule,
    MyAttendancePageRoutingModule,
    SharedModule
  ],
  declarations: [MyAttendancePage, CustomCalendarComponent]
})
export class MyAttendancePageModule { }
