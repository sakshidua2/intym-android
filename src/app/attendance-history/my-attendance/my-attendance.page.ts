import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ModalController, Platform } from "@ionic/angular";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CorrectionRequestPage } from "../correction-request/correction-request.page";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { ApiService } from "src/app/services/api.service";
import { CoreService } from "src/app/services/core.service";
import { forkJoin } from "rxjs";
import { LeaveFilterComponent } from "src/app/leaves/leave-filter/leave-filter.component";
import { AttendanceService } from "src/app/services/db/attendance.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { AttendanceModel } from "src/app/local-db-models/attendace";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";

import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";

@Component({
  selector: "app-my-attendance",
  templateUrl: "./my-attendance.page.html",
  styleUrls: ["./my-attendance.page.scss"],
})
export class MyAttendancePage implements OnInit {
  public calendarComponentTypeMy = APP_CONSTANTS.DATA_TYPE_MY;
  attendanceList: any[];
  attendanceListCopy: any[];
  statusList: string[];
  attendFilterForm: FormGroup;
  public STATUS_CODES = { pending: 1, approved: 2, rejected: 3, cancelled: 4 };
  public COLOR_CODES = { 1: "Pending", 2: "Approved", 3: "Rejected" };
  userDetails: any;
  body: any;
  screenType: string;
  defaultFilterValue: any;
  firstLoad = false;
  public attendanceStatusData = {};
  attendanceFilter: { min: any; max: any };
  isMaxMonth: boolean;
  isMinMonth: boolean;
  isSupervisor: boolean;
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  public isLeavesListReadyToRender = false;
  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private apiService: ApiService,
    private coreService: CoreService,
    private attendaceService: AttendanceService,
    private networkService: NetworkService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (
      this.userDetails &&
      ["Admin", "Supervisor"].includes(this.userDetails.Role)
    ) {
      this.isSupervisor = true;
    }
    this.statusList = ["pending", "approved", "rejected"];
    this.body = {};
    this.attendanceFilter = {
      min: this.coreService.getMomentNow(),
      max: this.coreService.getMomentNow(),
    };

    this.attendanceFilter.min.set({
      year: 2000,
      month: 0,
      date: 1,
    });
    this.attendFilterForm = this.fb.group({
      FromDate: [""],
      ToDate: [""],
      Status: ["all"],
    });
    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  ionViewDidEnter() {
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }

    this.isLeavesListReadyToRender = false;
    const date: any = this.coreService.getMomentNow();
    this.body = {
      FromDate: date,
      ToDate: this.coreService.getMomentNow(),
    };

    this.isMaxMonth = true;
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
      Status: "all",
    };
    this.attendFilterForm.patchValue(this.defaultFilterValue);
  }

  handleMyCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAtendanceFromLocal();
    } else {
      if (this.firstLoad && payload.sameMonth) {
        this.getAtendance();
      } else {
        this.firstLoad = true;
      }
      if (!payload.sameMonth) { this.getAttendanceStatusCalendar(); }
    }
  }

  getAttendanceStatusCalendar() {
    this.attendanceStatusData = {};
    this.apiService
      .getAttendanceStatusCalendar(
        {
          FromDate: this.body.CalendarFromDate,
          ToDate: this.body.CalendarToDate,
          StopLoader: false,
        },
        APP_CONSTANTS.DATA_TYPE_MY
      )
      .subscribe((res: any) => {
        Object.keys(res).map((val, key) => {
          const date = this.coreService.getMomentFormatDate(
            this.yearFormatted,
            val
          );
          this.attendanceStatusData = {
            ...this.attendanceStatusData,
            [date]: res[val],
          };
          return true;
        });
      });
  }

  async handleFilter() {
    const minDate: any = this.coreService.getMomentNow();
    const min = minDate
      .startOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const maxDate: any = this.coreService.getMomentNow();
    const max = maxDate
      .endOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const modalRef = await this.modalCtrl.create({
      component: LeaveFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        leaveFilterForm: this.attendFilterForm,
        defaultValues: this.defaultFilterValue,
        minMaxDates: { min, max },
        isVisitsPage: true,
      },
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          const FromDate = this.body.FromDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          const ToDate = this.body.ToDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          if (
            (res.data.FromDate &&
              FromDate.unix() !== res.data.FromDate.unix()) ||
            (res.data.ToDate && ToDate.unix() !== res.data.ToDate.unix())
          ) {
            Object.assign(this.body, res.data);
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getAtendanceFromLocal();
            } else {
              this.getAtendance();
            }
            return;
          }
          this.body = res.data;
          let list = JSON.parse(JSON.stringify(this.attendanceListCopy));
          if (res.data.Status && res.data.Status !== "all") {
            list.forEach(({ CorrectionRequest }, i) => {
              if (CorrectionRequest && CorrectionRequest.length) {
                list[i].CorrectionRequest = list[i].CorrectionRequest.filter(
                  ({ StatusId }) =>
                    this.STATUS_CODES[res.data.Status] === StatusId
                );
              }
            });
            list = list.filter(
              ({ CorrectionRequest }) =>
                CorrectionRequest && CorrectionRequest.length
            );
          }
          this.attendanceList = [...list];
        } else {
          const date: any = this.coreService.getMomentNow();

          this.body = {
            FromDate: date.date(1),
            ToDate: this.coreService.getMomentNow(),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getAtendanceFromLocal();
          } else {
            this.getAtendance();
          }
        }
      }
    });
  }

  getAtendance(e?: any, StopLoader: boolean = false) {
    const data = {
      FromDate: this.body.FromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
    };

    forkJoin(
      this.apiService.getMyAttendance({ ...data, StopLoader }),
      this.apiService.getAttendanceLog({ ...data, StopLoader }, APP_CONSTANTS.DATA_TYPE_MY)
    ).subscribe(
      (res: any) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        const MyAttendance = res[0].MyAttendance.map((o) => ({
          ...o,
          IsMyAttendance: 0,
        }));
        const AttendanceLog = res[1];
        const max = this.attendanceFilter.max;

        if (
          this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) ===
          max.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
        ) {
          this.isMaxMonth = true;
        } else {
          this.isMaxMonth = false;
        }
        const min = this.attendanceFilter.min;
        if (this.body.FromDate.isSame(min)) {
          this.isMinMonth = true;
        } else {
          this.isMinMonth = false;
        }
        const result = this.processRawData(MyAttendance, AttendanceLog);

        this.attendanceList = JSON.parse(JSON.stringify(result));
        this.attendanceListCopy = JSON.parse(JSON.stringify(result));

        if (this.body.Status && this.body.Status !== "all") {
          this.attendanceList.forEach(({ CorrectionRequest }, i) => {
            if (CorrectionRequest && CorrectionRequest.length) {
              this.attendanceList[i].CorrectionRequest = this.attendanceList[
                i
              ].CorrectionRequest.filter(
                ({ StatusId }) =>
                  this.STATUS_CODES[this.body.Status] === StatusId
              );
            }
          });
          this.attendanceList = this.attendanceList.filter(
            ({ CorrectionRequest }) =>
              CorrectionRequest && CorrectionRequest.length
          );
        }

        if (history.state.data) {
          const notificationData = history.state["data"];

          const date = this.coreService.getMomentObject(notificationData.date);
          setTimeout(() => {
            if (
              date.isSame(this.body.FromData, "month") &&
              date.isSame(this.body.FromData, "year")
            ) {
              const request = this.attendanceList.filter(
                ({ CorrectionRequest }) =>
                  CorrectionRequest &&
                  CorrectionRequest.some(
                    (ele) => ele.EmpAttnRegularizeId === notificationData.id
                  )
              );
              this.navigate(request[0], "my-correction");
            } else {
              this.notificationRequest(notificationData);
            }
            history.state.data = undefined;
          }, 500);
        } else {
          if (this.attendanceList[0].InTime !== '-' || this.attendanceList[0].OutTime !== '-') {
            this.navigate(this.attendanceList[0], "my-correction");
          } else {
            this.coreService.displayToast("No record found.", "danger");
          }
        }
        this.isLeavesListReadyToRender = true;

        if (this.platform.is("cordova")) {
          const myAttendance = [];
          this.attendanceListCopy.forEach((ele: any) => {
            const obj = JSON.parse(JSON.stringify(ele));
            obj.SpentHours = obj.SpentHours
              ? JSON.stringify(obj.SpentHours)
              : "";
            obj.CorrectionRequest =
              obj.CorrectionRequest && obj.CorrectionRequest.length
                ? JSON.stringify(obj.CorrectionRequest)
                : "";
            myAttendance.push(obj);
          });
          this.attendaceService.storeAttendanceForOffline(myAttendance);
        }
      },
      (err) => {
        this.isLeavesListReadyToRender = true;
        const message =
          err &&
            err.error &&
            (err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"])
            ? err.error["Status"] ||
            err.error["ErrorMessage"] ||
            err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  notificationRequest(data: { id: number; date: string }) {
    const body = {
      FromDate: this.coreService
        .getMomentObject(data.date)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: this.coreService
        .getMomentObject(data.date)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
    };
    forkJoin(
      this.apiService.getMyAttendance(body),
      this.apiService.getAttendanceLog(body, APP_CONSTANTS.DATA_TYPE_MY)
    ).subscribe((res: any) => {
      let MyAttendance = res[0].map((o) => ({
        ...o,
        IsMyAttendance: 0,
      }));
      MyAttendance = MyAttendance.filter(
        ({ CorrectionRequest }) =>
          CorrectionRequest &&
          CorrectionRequest.some((ele) => ele.EmpAttnRegularizeId === data.id)
      );
      const AttendanceLog = res[1].MyAttendance;
      if (
        MyAttendance &&
        MyAttendance.length &&
        AttendanceLog &&
        AttendanceLog.length
      ) {
        const result = this.processRawData(
          [...MyAttendance],
          [...AttendanceLog]
        );
        this.navigate(result[0], "my-correction");
      }
    });
    history.state.data = undefined;
  }

  processRawData(MyAttendance: any[], AttendanceLog: any[]) {
    MyAttendance.forEach((element, i) => {
      if (element.CorrectionRequest && element.CorrectionRequest.length) {
        element.CorrectionRequest.forEach((request) => {
          if (request.DayType === "First Half") {
            element.InTimeStatus = this.COLOR_CODES[request.StatusId];
          } else if (request.DayType === "Second Half") {
            element.OutTimeStatus = this.COLOR_CODES[request.StatusId];
          } else if (request.DayType === "Full Day") {
            element.InTimeStatus = element.OutTimeStatus = this.COLOR_CODES[
              request.StatusId
            ];
          }
          if (request.InTime) {
            request.InTime = this.coreService.getTimeByLocalZone(
              request.InTime,
              APP_CONSTANTS.EXACT_TIME_24_FORMAT
            );
          }

          if (request.OutTime) {
            request.OutTime = this.coreService.getTimeByLocalZone(
              request.OutTime,
              APP_CONSTANTS.EXACT_TIME_24_FORMAT
            );
          }
        });
      }
      const logs = AttendanceLog.filter(
        (ele) =>
          this.coreService
            .getMomentObject(ele.TimeStamp)
            .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) ===
          this.coreService
            .getMomentByDate(element.AttendanceDate)
            .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
      );
      if (logs) {
        let isTodayLogs = false;
        if (i === MyAttendance.length - 1) {
          isTodayLogs = true;
        }
        let today: any = this.coreService.getMomentNow();
        today = today.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
        logs.sort(
          (a, b) =>
            this.coreService.getMomentByDate(b.TimeStamp).unix() -
            this.coreService.getMomentByDate(a.TimeStamp).unix()
        );

        logs.forEach((log) => {
          if (log.AttendanceStatus === 1) {
            element.InTimeType = log.DeviceSN;
            if (
              isTodayLogs &&
              today ===
              this.coreService
                .getMomentByDate(log.TimeStamp)
                .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
            ) {
              element.InTime = this.coreService
                .getMomentByDate(log.TimeStamp)
                .format(APP_CONSTANTS.EXACT_LOCAL_TIME_HH_MM_SS);
            }
          } else if (log.AttendanceStatus === 2) {
            element.OutTimeType = log.DeviceSN;
            if (
              isTodayLogs &&
              today ===
              this.coreService
                .getMomentByDate(log.TimeStamp)
                .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
            ) {
              element.OutTime = this.coreService
                .getMomentByDate(log.TimeStamp)
                .format(APP_CONSTANTS.EXACT_LOCAL_TIME_HH_MM_SS);
            }
          }
        });
      }
    });

    MyAttendance.forEach((ele: any, i) => {
      if (
        this.coreService
          .getMomentByDate(ele.AttendanceDate)
          .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) !==
        this.coreService
          .getMomentObject()
          .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
      ) {
        if (ele.InTime !== "-") {
          ele.InTime = this.coreService.getTimeByLocalZone(
            ele.InTime,
            APP_CONSTANTS.EXACT_TIME_24_FORMAT
          );
        }
        if (ele.OutTime !== "-") {
          ele.OutTime = this.coreService.getTimeByLocalZone(
            ele.OutTime,
            APP_CONSTANTS.EXACT_TIME_24_FORMAT
          );
        }
      }

      if (ele.InTime !== "-" && ele.OutTime !== "-") {
        ele.SpentHours = this.coreService.getTimeDifference(
          ele.InTime,
          ele.OutTime
        );
      }
    });

    return MyAttendance;
  }
  nextMonthAttend() {
    const fromdate: any = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    fromdate.add(1, "M");
    fromdate.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });
    const todate = this.coreService
      .getMomentByDate(this.body.ToDate.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      });
    todate.add(1, "M");
    this.body.Status = "all";

    if (this.attendanceFilter.max.unix() > fromdate.unix()) {
      fromdate.set({ date: 1 });
      if (fromdate.month() - this.body.FromDate.month() !== 1) {
        fromdate.subtract(1, "M");
      }
      this.body.FromDate = this.coreService.getMomentByDate(
        fromdate.toISOString()
      );
      todate.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(todate.toISOString());

      this.attendFilterForm.patchValue({
        FromDate: this.body.FromDate.format(
          APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
        ),
        ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        User: 0,
        Status: this.body.Status,
      });

      this.isMinMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAtendanceFromLocal();
      } else {
        this.getAtendance();
      }
    } else {
      const max = this.attendanceFilter.max;

      if (
        this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) !==
        max.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
      ) {
        this.body.ToDate = max;

        this.body.FromDate = this.coreService.getMomentNow();
        this.body.FromDate.set({ date: 1 });

        this.attendFilterForm.patchValue({
          FromDate: this.body.FromDate.format(
            APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
          ),
          ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
          User: 0,
          Status: this.body.Status,
        });
        this.isMaxMonth = true;

        if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Offline
        ) {
          this.getAtendanceFromLocal();
        } else {
          this.getAtendance();
        }
      }
    }
  }

  prevMonthAttend() {
    const fromdate: any = this.coreService.getMomentByDate(
      this.body.FromDate.toISOString()
    );
    fromdate.subtract(1, "M");
    fromdate.set({
      date: this.attendanceFilter.max.date() + 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });

    const todate = this.coreService
      .getMomentByDate(this.body.ToDate.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      });
    todate.subtract(1, "M");

    if (this.attendanceFilter.min.unix() <= fromdate.unix()) {
      fromdate.set({ date: 1 });
      if (this.body.FromDate.month() === fromdate.month()) {
        fromdate.subtract(1, "M");
      }

      this.body.FromDate = this.coreService.getMomentByDate(
        fromdate.toISOString()
      );
      todate.endOf("month");
      this.body.ToDate = this.coreService.getMomentByDate(todate.toISOString());

      this.body.Status = "all";
      this.attendFilterForm.patchValue({
        FromDate: this.body.FromDate.format(
          APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
        ),
        ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        User: 0,
        Status: this.body.Status,
      });

      this.isMaxMonth = false;
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Offline
      ) {
        this.getAtendanceFromLocal();
      } else {
        this.getAtendance();
      }
    } else {
      this.isMinMonth = true;
    }
  }

  async navigate(item, type) {
    if (item) {
      const item_info = Object.assign({}, item);
      if (
        item_info.CorrectionRequest &&
        item_info.CorrectionRequest.filter(
          ({ DayType }) => DayType === "Full Day"
        ).length
      ) {
        this.coreService.displayToast(
          "Something went wrong! Please contact Administration",
          "warning"
        );
      } else {
        const mod = await this.modalCtrl.create({
          component: CorrectionRequestPage,
          cssClass: "corr-page",
          componentProps: {
            type,
            correctItem: item_info,
          },
          animated: true,
          swipeToClose: true,
        });
        mod.present();
        mod.onDidDismiss().then((res) => {
          if (res && res.data) {
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getAtendanceFromLocal();
            } else {
              this.getAtendance();
            }
          }
        });
      }
    }
  }

  getAtendanceFromLocal(e?: any) {
    const params = JSON.parse(JSON.stringify(this.body));
    params.FromDate = this.coreService
      .getMomentByDate(params.FromDate)
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    params.ToDate = this.coreService
      .getMomentByDate(params.ToDate)
      .add(1, "day")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    params.IsMyAttendance = 0;
    this.attendaceService
      .getData(params)
      .then((res: AttendanceModel[]) => {
        res.forEach((ele: any) => {
          ele.CorrectionRequest =
            ele.CorrectionRequest && ele.CorrectionRequest.length
              ? JSON.parse(ele.CorrectionRequest)
              : "";
          ele.SpentHours = ele.SpentHours ? JSON.parse(ele.SpentHours) : "";
        });

        this.attendanceList = res;
        this.attendanceListCopy = res;
        this.isLeavesListReadyToRender = true;
      })
      .catch((err) => {
        this.coreService.displayToast(
          "You are offline and no data found for user locally",
          "warning"
        );
        this.isLeavesListReadyToRender = true;
      })
      .finally(() => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
      });
  }

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getAtendanceFromLocal(e);
    } else {
      this.getAtendance(e, true);
      this.getAttendanceStatusCalendar();
    }
  }

  generatePdf() {
    if (this.isSupervisor) {
      let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Daily Attendance Report</h3>
      </div>
      <div>
      <table class="info-table">
          <tr>
            <td>Employee: ${this.userDetails.FirstName} ${
        this.userDetails.LastName
        }</td>
            <td>From Date: ${this.body.FromDate.format(
          APP_CONSTANTS.DATE_FORMAT
        )}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.format(
          APP_CONSTANTS.DATE_FORMAT
        )}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Employee</th>
              <th>Attendance Date</th>
              <th>In Time</th>
              <th>Out Time</th>
              <th>Total (Hrs)</th>
              <th>Status</th>
              <th>Shift</th>
            </tr>
          </thead>
          <tbody>`;
      this.attendanceList.forEach((element) => {
        const AttendanceDate = this.coreService
          .getMomentByDate(element.AttendanceDate)
          .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);

        const InTime =
          element.InTime && element.InTime !== "-"
            ? this.coreService.getMomentByTime(
              element.InTime,
              APP_CONSTANTS.TIME_12_FORMAT
            )
            : "";
        const OutTime =
          element.OutTime && element.OutTime !== "-"
            ? this.coreService.getMomentByTime(
              element.OutTime,
              APP_CONSTANTS.TIME_12_FORMAT
            )
            : "";
        const row = `
                <tr>
                  <td>${element.Pin}</td>
                  <td>${element.EmployeeName}</td>
                  <td>${AttendanceDate}</td>
                  <td>${InTime}</td>
                  <td>${OutTime}</td>
                  <td>${
          element.SpentHours ? element.SpentHours.label : "-"
          }</td>
                  <td>${element.Status}</td>
                  <td>${element.Shift}</td>
                </tr>`;
        html += row;
      });
      const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                      <p>P-Present, A-Absent, FHA-First Half Absent, SHA-Second Half Absent, H-Holiday, W-Weekly Off , HDA-Half Day Absent, L-Leave, FHL-First Half Leave, SHL-Second Half Leave, OD-On Duty, FHOD-First Half On Duty, SHOD-Second Half On Duty, AC-Attendance Correction, FHAC-First Half Attendance Correction, SHAC-Second Half Attendance Correction,
                      FHP-First Half Present, SHP-Second Half Present, EO-Early Out, LA- Late Arrival
                      </p>
                      <p>Report Generated By: ${this.userDetails.FirstName} ${
        this.userDetails.LastName
        } : ${this.coreService.getMomentNow("DD MMMM yyyy")}</p>
                    </div>
                  </div>
                </body>
              </html>`;
      html += endOfHtml;
      this.pdfGenerator
        .fromData(html, {
          documentSize: "A4",
          fileName: `my-attendance_report${this.coreService.getMomentNow(
            "DDMMYYYY_hhmmss"
          )}.pdf`,
          type: "share",
        })
        .then((res) => {
          console.log("res =>", res);
        })
        .catch((err) => {
          console.log("error =>", err);
        });
    }
  }

  ionViewDidLeave() {
    this.isLeavesListReadyToRender = false;
    this.attendanceList = [];
    this.attendanceListCopy = [];
  }
}
