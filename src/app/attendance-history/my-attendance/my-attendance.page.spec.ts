import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyAttendancePage } from './my-attendance.page';

describe('MyAttendancePage', () => {
  let component: MyAttendancePage;
  let fixture: ComponentFixture<MyAttendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAttendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
