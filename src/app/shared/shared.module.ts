import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import { DateTimeFormatPipe } from '../pipes/date-pipe';
import { TimeFormatPipe } from '../pipes/time-pipe';



@NgModule({
  declarations: [DateTimeFormatPipe, TimeFormatPipe],
  imports: [
    CommonModule,
  ],
  providers: [DatePipe],
  exports: [
    DateTimeFormatPipe,
    TimeFormatPipe,
    DatePipe
  ]
})
export class SharedModule { }
