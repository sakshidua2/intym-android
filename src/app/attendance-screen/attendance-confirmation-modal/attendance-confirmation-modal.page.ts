import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: "app-attendance-confirmation-modal",
  templateUrl: "./attendance-confirmation-modal.page.html",
  styleUrls: ["./attendance-confirmation-modal.page.scss"],
})
export class AttendanceConfirmationModalPage implements OnInit {
  @Input() data: any;
  CustomerVisitForm: FormGroup;
  @Input() activeCustomer: any;
  constructor(private modalCtrl: ModalController, private fb: FormBuilder, private coreService: CoreService) {}

  ngOnInit() {
    if (this.data.isCustomer) {
      this.CustomerVisitForm = this.fb.group({
        CustomerName: ["", [Validators.required, Validators.minLength(3), this.coreService.onlySpacesNotAllowed]],
        CustomerAddress: ["", [Validators.required, Validators.minLength(6), this.coreService.onlySpacesNotAllowed]],
        ImageData: [""],
      });
    }
  }

  get CustomerFormCntrl() {
    if (this.CustomerVisitForm) {
      return this.CustomerVisitForm.controls;
    }
  }

  handelCustomerFromSubmit() {
    if (this.CustomerVisitForm.valid) {
      const data = {
        ...this.CustomerVisitForm.value,
        type: 3,
      };
      this.modalCtrl.dismiss(data);
    }
  }

  handelCheckOut() {
    const data = {
      ...this.activeCustomer,
      type: 4,
    };
    this.modalCtrl.dismiss(data);
  }
  handelCancel() {
    this.modalCtrl.dismiss(false);
  }

  handlePunch() {
    this.modalCtrl.dismiss(true);
  }
}
