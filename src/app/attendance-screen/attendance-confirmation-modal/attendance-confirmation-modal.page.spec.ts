import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttendanceConfirmationModalPage } from './attendance-confirmation-modal.page';

describe('AttendanceConfirmationModalPage', () => {
  let component: AttendanceConfirmationModalPage;
  let fixture: ComponentFixture<AttendanceConfirmationModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceConfirmationModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AttendanceConfirmationModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
