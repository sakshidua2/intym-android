import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendanceConfirmationModalPage } from './attendance-confirmation-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AttendanceConfirmationModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceConfirmationModalPageRoutingModule {}
