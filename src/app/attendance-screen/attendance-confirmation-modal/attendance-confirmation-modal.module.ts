import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AttendanceConfirmationModalPageRoutingModule } from "./attendance-confirmation-modal-routing.module";

import { AttendanceConfirmationModalPage } from "./attendance-confirmation-modal.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttendanceConfirmationModalPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [AttendanceConfirmationModalPage],
})
export class AttendanceConfirmationModalPageModule {}
