import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/services/api.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { CoreService } from "src/app/services/core.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";

@Component({
  selector: "app-qr",
  templateUrl: "./qr.page.html",
  styleUrls: ["./qr.page.scss"],
})
export class QrPage implements OnInit {
  public ExpiryTime = 0;
  public attendanceStatusList = [];
  public attendanceStatus = null;
  public punchInDetails = null;
  public punchOutDetails = null;
  public punchStatus = 0;
  constructor(
    private router: Router,
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.initializeViewData();
  }

  generateCode() {
    const queryParams = {
      ExpiryTime: this.ExpiryTime,
      AttendanceStatusId: 0,
      AttendanceStatusText: "Scan Code",
    };
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      queryParams.AttendanceStatusId = this.attendanceStatus.AttendanceStatusId;
      queryParams.AttendanceStatusText = this.attendanceStatus.Description;
    }
    console.log(queryParams);

    this.router.navigate(["/attendance-screen/qr/generated"], { queryParams });
  }

  getQRDetails() {
    const qrDetailsStr = localStorage.getItem(APP_CONSTANTS.QR_CODE_DATA_KEY);
    if (qrDetailsStr) {
      try {
        const qrDetails = JSON.parse(qrDetailsStr);
        const data = qrDetails.data;
        const expireTimeStamp = qrDetails.timestamp;
        if (
          data &&
          expireTimeStamp > this.coreService.getMomentObject().unix()
        ) {
          this.ExpiryTime = data.ExpiryTime;
        } else {
          this.requestQRDetails();
        }
      } catch (error) {
        this.requestQRDetails();
      }
    } else {
      this.requestQRDetails();
    }
  }

  requestQRDetails() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.getQRDetails().subscribe((res: any) => {
        this.ExpiryTime = res.ExpiryTime;
        const expire = this.coreService
          .getMomentObject()
          .add(APP_CONSTANTS.QR_LOCAL_CACHE_EXPIRE_TIME_SPAN, "day")
          .unix();
        const payloadLocal = {
          data: { ...res },
          timestamp: expire,
        };
        localStorage.setItem(
          APP_CONSTANTS.QR_CODE_DATA_KEY,
          JSON.stringify(payloadLocal)
        );
      });
    } else {
      this.ExpiryTime = APP_CONSTANTS.QR_EXPIRE_DEFAULT;
    }
  }

  initializeViewData() {
    this.getAttendanceStatusList();
    this.getQRDetails();
  }

  getAttendanceStatusList() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.getAttendanceStatusList().subscribe(
        (res: any) => {
          this.attendanceStatusList = res;
          this.attendanceStatusList.forEach(
            (ele: { AttendanceStatusId: string; Description: string }) => {
              if (ele.Description.includes("Attendance")) {
                ele.Description = ele.Description.replace("Attendance", "Log");
              }
            }
          );
          this.getAttendanceLog();
        },
        (err) => {
          console.log("Err => ", err);
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getAttendanceLog() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const now = this.coreService.getMomentNow();
      const payload = {
        FromDate: now,
        ToDate: now,
      };
      this.apiService.getAttendanceLog(payload, APP_CONSTANTS.DATA_TYPE_MY).subscribe(
        (res: any) => {
          const filteredUserPunchData = this.filterUserPunchData(res);
          this.punchInDetails = filteredUserPunchData.punchedIn;
          this.punchOutDetails = filteredUserPunchData.punchedOut;
          this.punchStatus = filteredUserPunchData.punchStatus;
          switch (filteredUserPunchData.punchStatus) {
            case 0:
              this.attendanceStatus = this.attendanceStatusList[0];
              break;
            case 1:
              this.attendanceStatus = JSON.parse(
                JSON.stringify(this.attendanceStatusList[1])
              );
              this.attendanceStatus.Description = "Scanned";
              break;
            case 2:
              this.attendanceStatus = JSON.parse(
                JSON.stringify(this.attendanceStatusList[1])
              );
              this.attendanceStatus.Description = "Scanned";
              break;
          }
        },
        (err) => {
          console.log("Err => ", err);
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  filterUserPunchData(res) {
    let punchedIn = null;
    let punchedOut = null;
    let punchStatus = 0;
    const userDetailsStr = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    if (userDetailsStr) {
      try {
        const userDetails: any = JSON.parse(userDetailsStr);
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[0] &&
          this.attendanceStatusList[0].AttendanceStatusId
        ) {
          punchedIn = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o, i) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[0].AttendanceStatusId &&
                i === res.length - 1
            );
        }
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[1] &&
          this.attendanceStatusList[1].AttendanceStatusId
        ) {
          punchedOut = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[1].AttendanceStatusId
            );
        }
        if (punchedIn && punchedOut) {
          punchStatus = 2;
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );
          punchedOut["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedOut.TimeStamp
          );
        } else if (punchedIn) {
          punchStatus = 1;
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );
          punchedOut = null;
        } else {
          punchedIn = null;
          punchedOut = null;
        }
      } catch (error) { }
    }
    return { punchStatus, punchedIn, punchedOut };
  }
}
