import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

import { IonicModule } from '@ionic/angular';

import { GeneratedPageRoutingModule } from './generated-routing.module';

import { GeneratedPage } from './generated.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneratedPageRoutingModule,
    NgxQRCodeModule,
    SharedModule
  ],
  declarations: [GeneratedPage]
})
export class GeneratedPageModule {}
