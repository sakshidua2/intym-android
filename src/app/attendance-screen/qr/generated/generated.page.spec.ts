import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeneratedPage } from './generated.page';

describe('GeneratedPage', () => {
  let component: GeneratedPage;
  let fixture: ComponentFixture<GeneratedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneratedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
