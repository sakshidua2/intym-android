import { Component, OnInit } from "@angular/core";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import CryptoJS from "crypto-js";
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
import { CoreService } from "src/app/services/core.service";

@Component({
  selector: "app-generated",
  templateUrl: "./generated.page.html",
  styleUrls: ["./generated.page.scss"],
})
export class GeneratedPage implements OnInit {
  public attendanceTypeLabel = "";
  public attendanceTime;
  public employeeName = "";
  public employeeNumber = "";
  public employeePin = "";
  public elementType = "url";
  public qrPayload = "";
  private expireTimeout = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private coreService: CoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events.subscribe((res: any) => {
      if (res instanceof NavigationEnd) {
        if (this.expireTimeout) {
          clearTimeout(this.expireTimeout);
        }
      }
    });
  }

  ionViewDidEnter() {
    this.activatedRoute.queryParams.subscribe((params) => {
      const userDetailsStr = localStorage.getItem(
        APP_CONSTANTS.USER_DETAILS_KEY
      );
      if (userDetailsStr) {
        try {
          const userDetails = JSON.parse(userDetailsStr);
          this.employeeName = `${userDetails.FirstName} ${userDetails.LastName}`;
          this.employeeNumber = userDetails.EmployeeId;
          this.employeePin = userDetails.Pin;
          this.attendanceTime = this.coreService.getMomentNow();
          const expireTime = parseInt(params.ExpiryTime);
          const expireDateTime: any = this.coreService.getMomentNow();
          expireDateTime.add(expireTime, "second");
          this.attendanceTypeLabel = params.AttendanceStatusText.replace(
            "Attendance",
            "Log"
          );
          const payloadStr = `${this.employeeNumber}_${userDetails.FirstName}-${
            userDetails.LastName
          }_${expireDateTime.format(APP_CONSTANTS.DATE_FORMAT_QR_CODE)}`;
          const key = CryptoJS.enc.Base64.parse(
            APP_CONSTANTS.QR_ENCRYPTION_SECRET_KEY
          );
          const encrypted = CryptoJS.AES.encrypt(payloadStr, key, {
            iv: key,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
          });
          const encryptedStr = CryptoJS.enc.Base64.stringify(
            encrypted.ciphertext
          );
          this.qrPayload = encryptedStr;
          this.expireTimeout = setTimeout(() => {
            this.handleQRExpire();
          }, expireTime * 1000);
        } catch (error) {}
      }
    });
  }

  handleQRExpire() {
    clearTimeout(this.expireTimeout);
    this.qrPayload = "";
    this.router.navigate(["/attendance-screen/qr"]);
  }

  handleClick() {
    this.handleQRExpire();
  }

  ionViewDidLeave() {
    clearTimeout(this.expireTimeout);
  }
}
