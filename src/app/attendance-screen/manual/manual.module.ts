import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ManualPageRoutingModule } from "./manual-routing.module";

import { ManualPage } from "./manual.page";
import { CameraPreview } from "@ionic-native/camera-preview/ngx";
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManualPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ManualPage],
  providers: [CameraPreview],
})
export class ManualPageModule {}
