import { Component, OnInit } from "@angular/core";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions,
} from "@ionic-native/camera-preview/ngx";
import { CoreService } from "src/app/services/core.service";
import { Router } from "@angular/router";
import { ModalController, Platform } from "@ionic/angular";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { AttendanceConfirmationModalPage } from "../attendance-confirmation-modal/attendance-confirmation-modal.page";
import { ApiService } from "src/app/services/api.service";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { AttendanceSuccessPage } from "../attendance-success/attendance-success.page";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { NOTIFY_OBSERVABLE } from "src/app/config/notify-observable.constants";

@Component({
  selector: "app-manual",
  templateUrl: "./manual.page.html",
  styleUrls: ["./manual.page.scss"],
})
export class ManualPage implements OnInit {
  public isFrontCamera = true;
  public cameraPreviewCompleted = false;
  public userDetails: any;
  attendanceStatusList: any[];
  public attendanceStatus = null;
  public punchInDetails = null;
  public checkInDetails = null;
  public checkOutDetails = null;
  public punchOutDetails = null;
  public punchStatus = 0;
  public capturedImage = "";
  isCheckedIn: boolean;
  isAttendanceAction: boolean;
  actionType: any;
  customerData: any = null;
  btnsDisabel: {
    login: boolean;
    logout: boolean;
  };
  constructor(
    private cameraPreview: CameraPreview,
    private coreService: CoreService,
    private router: Router,
    private modalCtrl: ModalController,
    private apiService: ApiService,
    private geolocation: Geolocation,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private networkService: NetworkService,
    private platform: Platform
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.isCheckedIn = false;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.getAttendanceStatusList();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
    this.btnsDisabel = {
      login: false,
      logout: false,
    };
    this.initCamera();
    this.isAttendanceAction = false;
    this.checkGPSPermission();
    try {
      this.userDetails = JSON.parse(
        localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
      );
    } catch (error) {
      this.userDetails = null;
    }
    this.initNotifyObservables();
  }

  initCamera() {
    this.capturedImage = "";
    let cameraYpos = 0;
    if (this.platform.is("ios")) {
      cameraYpos = 92;
    } else {
      cameraYpos = 112;
    }
    const cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: cameraYpos,
      width: window.screen.width,
      height: (window.screen.height / 3) * 2,
      camera: "front",
      tapPhoto: true,
      previewDrag: true,
      toBack: true,
      alpha: 1,
    };
    this.cameraPreview.startCamera(cameraPreviewOpts).then(
      (res) => {
        console.log("Result => ", res);
        this.cameraPreviewCompleted = true;
      },
      (err) => {
        console.log("err => ", err);
        this.cameraPreviewCompleted = true;
      }
    );
  }

  // handleToggleCamera() {
  //   this.cameraPreview.switchCamera();
  // }

  confirmAttendance(type, isCustomer) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const payload = {
        title: `${isCustomer ? "Customer " : ""}${type === "attendance-in" ? "Check In" : "Check Out"
          } ${isCustomer ? "" : "Confirmation"}`,
        type,
        isCustomer,
        typeLabel: type === "attendance-in" ? "Check In" : "Check Out",
        time: this.coreService.getMomentNow(APP_CONSTANTS.TIME_FORMAT_HH_MM_A),
      };
      this.showConfirmationModal(payload);
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  async showConfirmationModal(data) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const modal = await this.modalCtrl.create({
        component: AttendanceConfirmationModalPage,
        cssClass: `custom-filter-modal ${this.checkInDetails ? "check-out-popup" : "check-in-popup"
          }`,
        swipeToClose: true,
        componentProps: {
          data,
          activeCustomer: this.checkInDetails ? this.checkInDetails : "",
        },
        mode: "md",
      });
      await modal.present();
      modal.onDidDismiss().then((res) => {
        if (res.data) {
          const data = {
            CustomerName: res.data.CustomerName,
            CustomerAddress: res.data.CustomerAddress,
          };
          this.handlePunch(res.data.type, data);
        }
      });
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  handlePunch(type, customerData?) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      if (this.userDetails) {
        switch (type) {
          case 1:
            this.btnsDisabel.login = true;
            this.furtherProcess(type);
            break;
          case 2:
            this.btnsDisabel.logout = true;
            if (this.checkInDetails) {
              const data = {
                CustomerName: this.checkInDetails.CustomerName,
                CustomerAddress: this.checkInDetails.CustomerAddress,
              };
              this.furtherProcess(4, data);
            } else {
              this.furtherProcess(type);
            }
            break;
          case 3:
            this.furtherProcess(type, customerData);
            break;
          case 4:
            this.furtherProcess(type, customerData);
            break;
        }
      } else {
        this.coreService.displayToast("User details not found!", "danger");
      }
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  furtherProcess(type, customerData?) {
    // this.coreService.showLoader();
    this.isAttendanceAction = true;
    this.actionType = type;
    this.customerData = customerData ? customerData : null;
    this.checkGPSPermission();
  }

  logAttendance() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      let payload: any = {};
      const pictureOpts: CameraPreviewPictureOptions = {
        width: 1280,
        height: 1280,
        quality: 85,
      };
      this.geolocation
        .getCurrentPosition()
        .then((res: any) => {
          if (res) {
            payload.Latitude = res.coords.latitude;
            payload.Longitude = res.coords.longitude;
            this.cameraPreview.takePicture(pictureOpts).then((imageData) => {
              const Base64Image = "data:image/jpeg;base64," + imageData;
              this.capturedImage = Base64Image;
              const details = {
                TimeStamp: this.coreService.getMomentNow(),
                AttendanceStatus: this.actionType,
                EmployeeId: this.userDetails.EmployeeId,
                Base64Image,
                CustomerName: this.customerData
                  ? this.customerData.CustomerName
                  : "",
                CustomerAddress: this.customerData
                  ? this.customerData.CustomerAddress
                  : "",
              };
              Object.assign(payload, details);
              // console.log({ payload });
              // this.coreService.hideLoader();
              this.apiService
                .logUserAttendance(payload)
                .subscribe((res: any) => {
                  this.capturedImage = "";
                  if (res) {
                    if (this.actionType === 4 && this.btnsDisabel.logout) {
                      // this.checkInDetails = null;
                      payload.CustomerName = "";
                      payload.CustomerAddress = "";
                      payload.AttendanceStatus = 2;
                      this.callLogout(payload);
                    } else {
                      this.showSucess(payload);
                      this.getAttendanceLog();

                      if (this.actionType === 1) {
                        this.btnsDisabel.login = false;
                      }
                      if (this.actionType === 2) {
                        this.btnsDisabel.logout = false;
                      }
                    }
                    // this.coreService.displayToast(res.Status, "success");
                  } else {
                    if (this.actionType === 1) {
                      this.btnsDisabel.login = false;
                    }
                    if (this.actionType === 2) {
                      this.btnsDisabel.logout = false;
                    }
                  }
                });
            });
          }
        })
        .catch((err) => {
          this.capturedImage = "";
          if (err) {
            this.coreService.displayToast("Please allow location!", "danger");
            if (this.actionType === 1) {
              this.btnsDisabel.login = false;
            }
            if (this.actionType === 2) {
              this.btnsDisabel.logout = false;
            }
          }
        });
    } else {
      this.capturedImage = "";
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  callLogout(payload) {
    this.apiService.logUserAttendance(payload).subscribe((res: any) => {
      if (res) {
        this.showSucess(payload);
        this.getAttendanceLog();
        this.btnsDisabel.logout = false;
      } else {
        this.btnsDisabel.logout = false;
      }
    });
  }

  async showSucess(data) {
    let modelData = { ...data };
    switch (data.AttendanceStatus) {
      case 1:
        modelData["title"] = "Log In";
        break;
      case 2:
        modelData["title"] = "Log Out";
        break;
      case 3:
        modelData["title"] = "Check In";
        break;
      case 4:
        modelData["title"] = "Check Out";
        break;
    }
    const sucessRef = await this.modalCtrl.create({
      component: AttendanceSuccessPage,
      componentProps: { data: modelData },
      cssClass: "successPage",
    });
    await sucessRef.present();
    sucessRef.onDidDismiss().then((res) => {
      this.getAttendanceStatusList();
    });
  }

  getAttendanceStatusList() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.getAttendanceStatusList().subscribe(
        (res: any) => {
          this.attendanceStatusList = res;
          this.getAttendanceLog();
        },
        (err) => {
          console.log("Err => ", err);
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getAttendanceLog() {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const now = this.coreService.getMomentNow();
      const payload = {
        FromDate: now,
        ToDate: now,
      };
      this.apiService.getAttendanceLog(payload, APP_CONSTANTS.DATA_TYPE_MY).subscribe(
        (res: any) => {
          const filteredUserPunchData = this.filterUserPunchData(res);
          this.punchInDetails = filteredUserPunchData.punchedIn;
          this.punchOutDetails = filteredUserPunchData.punchedOut;
          this.checkInDetails = filteredUserPunchData.checkIn;
          this.checkOutDetails = filteredUserPunchData.checkOut;
          this.punchStatus = filteredUserPunchData.punchStatus;
          switch (filteredUserPunchData.punchStatus) {
            case 0:
              this.attendanceStatus = this.attendanceStatusList[0];
              break;
            case 1:
              this.attendanceStatus = this.attendanceStatusList[1];
              break;
            case 2:
              this.attendanceStatus = this.attendanceStatusList[2];
              break;
            case 3:
              this.attendanceStatus = this.attendanceStatusList[3];
              break;
          }
        },
        (err) => {
          console.log("Err => ", err);
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  filterUserPunchData(res) {
    let punchedIn = null;
    let punchedOut = null;
    let checkIn = null;
    let checkOut = null;
    let punchStatus = 0;
    const userDetailsStr = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    if (userDetailsStr) {
      try {
        const userDetails: any = JSON.parse(userDetailsStr);
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[0] &&
          this.attendanceStatusList[0].AttendanceStatusId
        ) {
          punchedIn = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o, i) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[0].AttendanceStatusId &&
                i === res.length - 1
            );
        }
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[1] &&
          this.attendanceStatusList[1].AttendanceStatusId
        ) {
          punchedOut = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[1].AttendanceStatusId
            );
        }
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[2] &&
          this.attendanceStatusList[2].AttendanceStatusId
        ) {
          checkIn = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[2].AttendanceStatusId
            );
        }
        if (
          userDetails.EmployeeId &&
          this.attendanceStatusList[3] &&
          this.attendanceStatusList[3].AttendanceStatusId
        ) {
          checkOut = res
            .sort((a, b) => b.EmpAttnTransactionId - a.EmpAttnTransactionId)
            .find(
              (o) =>
                o.EmployeeId === userDetails.EmployeeId &&
                o.AttendanceStatus ===
                this.attendanceStatusList[3].AttendanceStatusId
            );
        }
        if (punchedIn && punchedOut) {
          punchStatus = 2;
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );
          punchedOut["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedOut.TimeStamp
          );
        } else if (punchedIn && checkIn && checkOut) {
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );

          checkIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            checkIn.TimeStamp
          );
          if (
            this.coreService.getMomentByDate(checkIn.TimeStamp).unix() <
            this.coreService.getMomentByDate(checkOut.TimeStamp).unix()
          ) {
            checkIn = null;
            punchStatus = 1;
            checkOut["TimeStampFormated"] = this.coreService.getMomentByDate(
              checkOut.TimeStamp
            );
          } else {
            punchStatus = 3;
            checkOut = null;
          }

          punchedOut = null;
        } else if (punchedIn && checkIn && !checkOut) {
          punchStatus = 3;
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );

          checkIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            checkIn.TimeStamp
          );

          checkOut = null;

          punchedOut = null;
        } else if (punchedIn && !checkIn) {
          punchStatus = 1;
          punchedIn["TimeStampFormated"] = this.coreService.getMomentByDate(
            punchedIn.TimeStamp
          );

          checkIn = null;
          checkOut = null;

          punchedOut = null;
        } else {
          punchedIn = null;
          punchedOut = null;
          checkOut = null;
          checkIn = null;
        }
      } catch (error) { }
    }
    // console.log(punchStatus, punchedIn, punchedOut, checkIn, checkOut);

    return { punchStatus, punchedIn, punchedOut, checkIn, checkOut };
  }

  ionViewDidLeave() {
    this.handleStopCamera();
  }
  checkGPSPermission() {
    if (this.platform.is("ios")) {
      console.log("ios platform");
      //this.askToTurnOnGPS();
      this.getLocationCoordinates();
    } else {
      console.log("android platform");
      this.androidPermissions
        .checkPermission(
          this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
        )
        .then(
          (result) => {
            if (result.hasPermission) {
              //If having permission show 'Turn On GPS' dialogue
              this.askToTurnOnGPS();
            } else {
              //If not having permission ask for permission
              this.requestGPSPermission();
            }
          },
          (err) => {
            console.log(err);
            if (this.actionType === 1) {
              this.btnsDisabel.login = false;
            }
            if (this.actionType === 2) {
              this.btnsDisabel.logout = false;
            }
          }
        );
    }
  }
  askToTurnOnGPS() {
    this.locationAccuracy
      .canRequest()
      .then((res) => {
        if (res) {
          this.locationAccuracy
            .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            .then(
              () => {
                // When GPS Turned ON call method to get Accurate location coordinates
                // this.getLocationCoordinates()
                if (this.isAttendanceAction) {
                  this.logAttendance();
                }
              },
              (error) => {
                this.askToTurnOnGPS();
              }
              // alert('Error requesting location permissions ' + JSON.stringify(error))
            );
        }
      })
      .catch((err) => {
        if (this.actionType === 1) {
          this.btnsDisabel.login = false;
        }
        if (this.actionType === 2) {
          this.btnsDisabel.logout = false;
        }
        console.log("gps is not available in this device");
      });
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions
          .requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
          )
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            (error) => {
              this.requestGPSPermission();
              //Show alert if user click on 'No Thanks'
              // alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }
  getLocationCoordinates() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        console.log({ resp });
        if (this.isAttendanceAction) {
          this.logAttendance();
        }
      })
      .catch((error) => {
        console.error("Error getting location", error);
      });
  }
  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  initNotifyObservables() {
    this.coreService.notifyObservable$.subscribe((res: any) => {
      if (res.option === NOTIFY_OBSERVABLE.STOP_CAMERA) {
        this.handleStopCamera();
      } else if (res.option === NOTIFY_OBSERVABLE.START_CAMERA) {
        this.initCamera();
      }
    });
  }

  handleStopCamera() {
    this.capturedImage = "";
    this.cameraPreviewCompleted = false;
    try {
      this.cameraPreview
        .stopCamera()
        .then(() => console.log("Camera stop"))
        .catch((err) => console.log("err => ", err));
    } catch (error) { }
  }
}
