import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AttendanceScreenPage } from "./attendance-screen.page";

const routes: Routes = [
  {
    path: "",
    component: AttendanceScreenPage,
    children: [
      {
        path: "",
        redirectTo: "qr",
        pathMatch: "full",
      },
      {
        path: "qr",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("./qr/qr.module").then((m) => m.QrPageModule),
          },
        ],
      },
      {
        path: "manual",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("./manual/manual.module").then((m) => m.ManualPageModule),
          },
        ],
      },
    ],
  },
  {
    path: "attendance-confirmation-modal",
    loadChildren: () =>
      import(
        "./attendance-confirmation-modal/attendance-confirmation-modal.module"
      ).then((m) => m.AttendanceConfirmationModalPageModule),
  },
  {
    path: "attendance-success",
    loadChildren: () =>
      import("./attendance-success/attendance-success.module").then(
        (m) => m.AttendanceSuccessPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceScreenPageRoutingModule {}
