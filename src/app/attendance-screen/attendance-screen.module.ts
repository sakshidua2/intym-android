import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendanceScreenPageRoutingModule } from './attendance-screen-routing.module';

import { AttendanceScreenPage } from './attendance-screen.page';
import { CameraPreview } from '@ionic-native/camera-preview/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AttendanceScreenPageRoutingModule
  ],
  declarations: [AttendanceScreenPage],
  providers: [
    CameraPreview
  ]
})
export class AttendanceScreenPageModule { }
