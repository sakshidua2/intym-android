import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NavController } from "@ionic/angular";
import { APP_CONSTANTS } from "../config/app.constants";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-attendance-screen",
  templateUrl: "./attendance-screen.page.html",
  styleUrls: ["./attendance-screen.page.scss"],
})
export class AttendanceScreenPage implements OnInit {
  public isSupervisor: boolean = false;
  public showAttendanceOptions = true;
  public allowRemoteLogin = false;
  notificationCount: string | number;
  constructor(
    private router: Router,
    private coreService: CoreService,
    private navCntrl: NavController,
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    const userDetailsStr = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    if (userDetailsStr) {
      try {
        const userDetails = JSON.parse(userDetailsStr);
        if (userDetails && ["Admin", "Supervisor"].includes(userDetails.Role)) {
          this.isSupervisor = true;
        } else {
          this.isSupervisor = false;
        }
        this.allowRemoteLogin = userDetails.AllowRemoteLogin;
      } catch (error) {
        this.allowRemoteLogin = false;
      }
    } else {
      this.allowRemoteLogin = false;
    }

    console.log(this.allowRemoteLogin);
    console.log(this.showAttendanceOptions);
    // this.showAttendanceOptions = true;

    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
    if (this.router.url.endsWith("/manual")) {
      this.coreService.notifyOther({
        option: NOTIFY_OBSERVABLE.START_CAMERA,
        value: true,
      });
    }
    console.log(this.router.url);
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  cameraClick() {
    let attendance = this;
    const attendanceLog = localStorage.getItem(APP_CONSTANTS.ATTENDANCE_LOG);
    const token = localStorage.getItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
    const userDetail = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    const auth_logged_in = localStorage.getItem(APP_CONSTANTS.AUTH_LOGGED_IN);
    const option = {
      ATTENDANCE_LOG: attendanceLog,
      TOKEN: token,
      USER_DETAIL: userDetail,
      AUTH_LOGGED_IN: auth_logged_in,
    };
    window["plugins"].newActivityPlugin.new_activity(
      option,
      function (res) {
        console.log("Response Data : " + res);
        if (res === "qr") {
          attendance.navCntrl.navigateForward("/attendance-screen");
        } else if (res === "dashboard") {
          attendance.navCntrl.navigateForward("");
        }
      },
      function (err) {
        console.log("Response Data : " + err);
      }
    );
  }

  ionViewWillLeave() {
    this.coreService.notifyOther({
      option: NOTIFY_OBSERVABLE.STOP_CAMERA,
      value: true,
    });
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
