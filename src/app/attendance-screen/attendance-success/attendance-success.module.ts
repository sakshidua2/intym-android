import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendanceSuccessPageRoutingModule } from './attendance-success-routing.module';

import { AttendanceSuccessPage } from './attendance-success.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttendanceSuccessPageRoutingModule,
    SharedModule
  ],
  declarations: [AttendanceSuccessPage]
})
export class AttendanceSuccessPageModule {}
