import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendanceSuccessPage } from './attendance-success.page';

const routes: Routes = [
  {
    path: '',
    component: AttendanceSuccessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceSuccessPageRoutingModule {}
