import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { APP_CONSTANTS } from 'src/app/config/app.constants';

@Component({
  selector: "app-attendance-success",
  templateUrl: "./attendance-success.page.html",
  styleUrls: ["./attendance-success.page.scss"],
})
export class AttendanceSuccessPage implements OnInit {
  @Input() data: any;
  isDismissed: boolean;
  public timeFormat = APP_CONSTANTS.TIME_12_FORMAT;
  constructor(
    private modelCntrl: ModalController
  ) {}

  ngOnInit() {
    this.isDismissed = false;
    setTimeout(() => {
      if (!this.isDismissed) {
        this.navigate();
      }
    }, 5000);
  }

  navigate() {
    this.isDismissed = true;
    this.modelCntrl.dismiss();
  }
}
