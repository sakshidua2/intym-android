import { Component, OnInit } from '@angular/core';
import { APP_CONSTANTS } from "../../config/app.constants";
import { ModalController } from "@ionic/angular";
import { Router } from "@angular/router";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { CoreService } from "../../services/core.service";
import { NOTIFY_OBSERVABLE } from "../../config/notify-observable.constants";
import { SettingsPage } from "../../settings/settings.page";
import { LABEL_CONSTANTS } from "../../config/labels.constants";
import tt from '@tomtom-international/web-sdk-maps';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  public isSupervisor: boolean = false;
  notificationCount: string | number = 0;
  map: any;
  constructor(
    private modalController: ModalController,
    private coreService: CoreService,
    private router: Router,
    private geolocation: Geolocation,
  ) { }

  ngOnInit() {

  }

  ionViewDidLeave() {
    this.map = undefined;
  }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    let mapPage = this;
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });

    this.geolocation
      .getCurrentPosition()
      .then((res: any) => {
        if (res) {
          console.log("res", res);
          this.map = tt.map({
            key: APP_CONSTANTS.TOM_TOM_MAP_KEY,
            container: 'map',
            style: APP_CONSTANTS.TOM_TOM_MAP_STYLE,
            center: [res.coords.longitude, res.coords.latitude],
            zoom: 3,
          });
          if (history.state.data) {
            for (const empDetail of history.state.data) {
              const popup = new tt.Popup({
                offset: {
                  top: [0, 0],
                  bottom: [0, -70],
                  'bottom-right': [0, -70],
                  'bottom-left': [0, -70],
                  left: [25, -35],
                  right: [-25, -35]
                },
                className: 'leave-list-item',
                maxWidth: '90%',
              }).setHTML(`<ion-card class="cust-visit-card"><ion-card-content class="leaves-card-content">
                <ion-row class="wrapper-row custom-wrapper">
                  <ion-col class="wrapper-col left-col" size="12">
                    <span class="label">Employee: </span>
                    <span class="value">${empDetail.EmployeeName}</span>
                  </ion-col>
                  <ion-col class="wrapper-col left-col" size="12">
                    <span class="label">CheckInTime: </span>
                    <span class="value">${this.coreService.getMomentFormatDate(APP_CONSTANTS.DATE_TIME_FORMATER, empDetail.CheckInTime)}</span>
                  </ion-col>
                  ${empDetail.CheckOutTime ? `<ion-col class="wrapper-col left-col" size="12">
                    <span class="label">CheckOutTime: </span>
                    <span class="value">${this.coreService.getMomentFormatDate(APP_CONSTANTS.DATE_TIME_FORMATER, empDetail.CheckOutTime)}</span>
                  </ion-col>` : ''}
                  <ion-col class="wrapper-col left-col" size="12">
                    <span class="label">Customer: </span>
                    <span class="value">${empDetail.CustomerName}</span>
                  </ion-col>
                  <ion-col class="wrapper-col left-col" size="12">
                    <span class="label">Customer Address: </span>
                    <span class="value">${empDetail.CustomerAddress}</span>
                  </ion-col>
                </ion-row></ion-card-content>
                </ion-card>`);
              new tt.Marker()
                .setLngLat([empDetail.Longitude, empDetail.Latitude])
                .addTo(this.map)
                .setPopup(popup);
              console.log(empDetail);
            }
          }
        }
      })
      .catch((err) => {
        console.log('Err', err);
        mapPage.coreService.displayToast(
          LABEL_CONSTANTS.ENABLE_LOCATION,
          "warning"
        );
      });
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }

}
