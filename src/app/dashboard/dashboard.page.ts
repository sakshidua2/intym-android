import { Component, OnInit, ChangeDetectorRef, NgZone } from "@angular/core";
import { NavController, Platform, ModalController } from "@ionic/angular";
import { ApiService } from "../services/api.service";
import { CoreService } from "../services/core.service";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { LABEL_CONSTANTS } from "../config/labels.constants";
import { APP_CONSTANTS } from "../config/app.constants";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";
import { Router } from "@angular/router";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"],
})
export class DashboardPage implements OnInit {
  public isSupervisor: boolean = false;
  public isShowTable: boolean = false;
  public IsMapViewEnabled: boolean = false;
  body: any;
  quickActions = [];
  dashboard = this;
  timeLine = [];
  attendanceLoaded = false;
  presentData = [];
  inMeetingsData = [];
  doneMeetingsData = [];
  tableType: string;
  overView: any = null;
  customerVisit = {
    DoneMeetingCount: 0,
    InMeetingCount: 0,
    PresentCount: 0,
  };
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  screenType: string;
  notificationCount: string | number = 0;
  constructor(
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService,
    private platform: Platform,
    private screenOrientation: ScreenOrientation,
    private cdr: ChangeDetectorRef,
    private _zone: NgZone,
    private router: Router,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.quickActions = [
      {
        title: "Leaves",
        link: "/leaves/my-leaves",
        icon: "assets/images/menuleaveapplication.svg",
        isSvg: true,
      },
      {
        title: "Punch",
        link: "/attendance-screen",
        icon: "assets/images/face-detection.svg",
        isSvg: true,
      },
      {
        title: "Statistics",
        link: "/home/my-dashboard",
        icon: "assets/images/marketing.svg",
        isSvg: true,
      },
      {
        title: "Attendances",
        link: "/attendance-history/my-attendance",
        icon: "assets/images/menuattendancehistory.svg",
        isSvg: true,
      },
      {
        title: "Visits",
        link: "/customer-visit-history/my-visit",
        icon: "assets/images/userhistory.svg",
        isSvg: true,
      },
    ];

    if (this.platform.is("cordova")) {
      this.screenOrientation.onChange().subscribe((res: any) => {
        if (res.type === "orientationchange") {
          this.screenType = this.screenOrientation.type.split("-")[0];
          this.cdr.detectChanges();
        }
      });
    }
  }

  showTable(type) {
    this.isShowTable = true;
    this.tableType = type;
    if (type === "present") {
      this.getHomeAttendanceDetails();
    }
    if (type === "in-meetings") {
      this.getHomeInMeetingDetails();
    }
    if (type === "done-meetings") {
      this.getHomeDoneMeetingDetails();
    }
  }

  showMap(data) {
    // const EmpDetail = [];
    // for (const d of data) {
    //   EmpDetail.push({ detail: d });
    // }
    this.navCntrl.navigateForward("/dashboard/map", {
      state: { data },
    });
  }

  hideTable() {
    this.isShowTable = false;
    this.tableType = "";
  }

  ionViewDidEnter() {
    console.log("ionviewdid enter dashboard");
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.IsMapViewEnabled = user.IsMapViewEnabled;
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
    this.body = {
      FromDate: this.coreService.getMomentNow(),
      ToDate: this.coreService.getMomentNow(),
    };
    this.screenType = "";
    if (this.platform.is("cordova")) {
      this.screenType = this.screenOrientation.type.split("-")[0];
    }
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.getAttedndance();
      this.getCustomerVisitCount();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getHomeAttendanceDetails() {
    this.apiService.getHomeAttendanceDetails().subscribe((res: any) => {
      this.presentData = res.data.L1.concat(res.data.L2, res.data.L3);
    });
  }

  getHomeInMeetingDetails() {
    this.apiService.getHomeInMeetingDetails().subscribe((res: any) => {
      this.inMeetingsData = res.data.L1.concat(res.data.L2, res.data.L3);
    });
  }

  getHomeDoneMeetingDetails() {
    this.apiService.getHomeDoneMeetingDetails().subscribe((res: any) => {
      this.doneMeetingsData = res.data.L1.concat(res.data.L2, res.data.L3);
      const user: any = JSON.parse(
        localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
      );
      if (user && user.IsSalesVisitVisible && res.data.EmployeeSalesVisits) {
        this.doneMeetingsData = this.doneMeetingsData.concat(res.data.EmployeeSalesVisits);
      }
    });
  }

  getCustomerVisitCount() {
    this.customerVisit = {
      DoneMeetingCount: 0,
      InMeetingCount: 0,
      PresentCount: 0,
    };
    this.apiService.getHomeCustomerVisitCount().subscribe((res: any) => {
      if (res.data.L1) {
        this.customerVisit["DoneMeetingCount"] += res.data.L1.DoneMeetingCount;
        this.customerVisit["InMeetingCount"] += res.data.L1.InMeetingCount;
        this.customerVisit["PresentCount"] += res.data.L1.PresentCount;
      }
      if (res.data.L2) {
        this.customerVisit["DoneMeetingCount"] += res.data.L2.DoneMeetingCount;
        this.customerVisit["InMeetingCount"] += res.data.L2.InMeetingCount;
        this.customerVisit["PresentCount"] += res.data.L2.PresentCount;
      }
      if (res.data.L3) {
        this.customerVisit["DoneMeetingCount"] += res.data.L3.DoneMeetingCount;
        this.customerVisit["InMeetingCount"] += res.data.L3.InMeetingCount;
        this.customerVisit["PresentCount"] += res.data.L3.PresentCount;
      }
      const user: any = JSON.parse(
        localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
      );
      if (user && user.IsSalesVisitVisible) {
        this.apiService
          .getSalesVisitCalendar(
            {
              FromDate: this.body.FromDate,
              ToDate: this.body.ToDate,
            },
            APP_CONSTANTS.DATA_TYPE_TEAM
          )
          .subscribe((res: any) => {
            [...res].map((o) => {
              const date = this.coreService.getMomentFormatDate(
                this.yearFormatted,
                o.Date
              );
              if (date === this.coreService.getMomentNow().format(this.yearFormatted)) {
                this.customerVisit['DoneMeetingCount'] += o.Total;
              }
              return true;
            });
          });
      }
    });
  }

  getAttedndance() {
    this.attendanceLoaded = false;
    const date = this.coreService.getMomentNow(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    const request = {
      FromDate: date,
      ToDate: date,
    };

    this.apiService
      .getAttendanceLog(request, APP_CONSTANTS.DATA_TYPE_MY)
      .subscribe((res: any) => {
        this.timeLine = [...res];
        this.timeLine.forEach((ele: any, i) => {
          //
          if (!ele.AttendanceStatus) {
            ele.AttendanceStatus = 1;
            ele.DeviceSN = "biometric_" + ele.DeviceSN;
          }
          //

          switch (ele.AttendanceStatus) {
            case 1:
              ele["message"] =
                i === this.timeLine.length - 1
                  ? "Logged In"
                  : ele.DeviceSN.includes("biometric_")
                  ? " Biometric(BYSK182560023) scanned"
                  : ele.DeviceSN.includes("scanner_")
                  ? "QR Scanned"
                  : "Unknown Device";

              break;
            case 2:
              ele["message"] =
                ele.DeviceSN === "Mobile" ? "Logged Out" : "QR Scanned";
              break;
            case 3:
              ele[
                "message"
              ] = `Checked In At ${ele.CustomerName} (${ele.CustomerAddress})`;
              break;
            case 4:
              ele[
                "message"
              ] = `Checked Out At ${ele.CustomerName} (${ele.CustomerAddress})`;
              break;
          }
        });
        if (this.timeLine.length) {
          this.timelineOverView(
            this.timeLine[this.timeLine.length - 1],
            this.timeLine[0].AttendanceStatus === 2 ? this.timeLine[0] : ""
          );
        } else {
          this.overView = null;
        }
        this.attendanceLoaded = true;
      });
  }

  timelineOverView(inData, lastData) {
    this.overView = {
      checkIntime: this.coreService.getMomentByDate(inData.TimeStamp),
      avgTime: this.timeDifference(
        inData.TimeStamp,
        lastData ? lastData.TimeStamp : ""
      ),
      status: lastData.AttendanceStatus === 2 ? "Logged Out" : "In Office",
      class: lastData.AttendanceStatus === 2 ? "" : "success",
    };
  }

  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  timeDifference(inTime, outTime) {
    const date1 = this.coreService.getMomentByDate(inTime);
    const date2 = outTime
      ? this.coreService.getMomentByDate(outTime)
      : this.coreService.getMomentObject();
    let diff;
    if (date2 > date1) {
      diff = date2.diff(date1);
    } else {
      diff = date1.diff(date2);
    }
    let minutes = Math.floor(diff / 60000);

    return `${
      Math.floor(minutes / 60) < 10
        ? "0" + Math.floor(minutes / 60)
        : Math.floor(minutes / 60)
    }h ${minutes % 60 < 10 ? "0" + (minutes % 60) : minutes % 60}m`;
  }

  handlePunchIn(link) {
    this.navigate({ link });
  }
  navigate(item) {
    let dashboard = this;
    if (item.link === "/attendance-screen") {
      const attendanceLog = localStorage.getItem(APP_CONSTANTS.ATTENDANCE_LOG);
      const token = localStorage.getItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
      const userDetail = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
      const auth_logged_in = localStorage.getItem(APP_CONSTANTS.AUTH_LOGGED_IN);
      const option = {
        ATTENDANCE_LOG: attendanceLog,
        TOKEN: token,
        USER_DETAIL: userDetail,
        AUTH_LOGGED_IN: auth_logged_in,
      };
      window["plugins"].newActivityPlugin.new_activity(
        option,
        function (res) {
          if (res === "qr") {
            dashboard.navCntrl.navigateForward("/attendance-screen");
          } else if (res === "dashboard") {
            const attendanceLog = localStorage.getItem(
              APP_CONSTANTS.ATTENDANCE_LOG
            );

            if (
              dashboard.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Online
            ) {
              dashboard._zone.run(() => dashboard.getAttedndance());
            } else {
              dashboard.coreService.displayToast(
                LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
                "warning"
              );
            }
          }
        },
        function (err) {}
      );
    } else if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline) {
      const message = "You are offline! Please check internet connection.";
      this.coreService.displayToast(message, "warning");
    } else {
      this.navCntrl.navigateForward(item.link);
    }
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
