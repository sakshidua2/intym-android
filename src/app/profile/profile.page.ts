import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import {
  NavController,
  ActionSheetController,
  Platform,
  AlertController,
} from "@ionic/angular";
import { ApiService } from "../services/api.service";
import { CoreService } from "../services/core.service";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { UserProfileService } from "../services/db/user-profile.service";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { AuthGuardService } from "../services/auth-guard.service";
import { APP_CONSTANTS } from "../config/app.constants";
import { UserProfileModel } from "../local-db-models/user-profile";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { SafeUrl } from "@angular/platform-browser";
import { LABEL_CONSTANTS } from "../config/labels.constants";
import { Router } from "@angular/router";
import { ModalController } from "@ionic/angular";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  isSupervisor: boolean;
  userDetailsFrom: FormGroup;
  ProfilePic: SafeUrl;
  UpdatedProfilePic: any;
  profilepicUpdated: boolean;
  apiUserDetails: any;
  isAnyvalueChanged = false;
  public defaultProfileImg = "assets/images/user-profile.svg";
  public showProfilePicPlaceHolder = true;
  valueChanged = {
    FirstName: false,
    LastName: false,
    PhoneNo: false,
  };
  notificationCount: string | number = 0;
  constructor(
    private navCntrl: NavController,
    private apiService: ApiService,
    private coreService: CoreService,
    private actionSheetController: ActionSheetController,
    private platform: Platform,
    private camera: Camera,
    private router: Router,
    private userProfileService: UserProfileService,
    private networkService: NetworkService,
    private authService: AuthGuardService,
    private alertCtrl: AlertController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.userDetailsFrom = new FormGroup({
      EmployeeId: new FormControl({ value: "", disabled: true }, [
        Validators.required,
      ]),
      FirstName: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
        this.coreService.onlySpacesNotAllowed,
      ]),
      LastName: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
        this.coreService.onlySpacesNotAllowed,
      ]),
      PhoneNo: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        this.coreService.onlySpacesNotAllowed,
      ]),
      MailId: new FormControl({ value: "", disabled: true }, [
        Validators.required,
        this.coreService.onlySpacesNotAllowed,
      ]),
    });

    this.userDetailsFrom.valueChanges.subscribe((res: any) => {
      if (this.apiUserDetails) {
        Object.keys(this.valueChanged).forEach((ele: any) => {
          if (res[ele] !== this.apiUserDetails[ele]) {
            this.valueChanged[ele] = true;
            this.isAnyvalueChanged = true;
          } else {
            this.valueChanged[ele] = false;
          }
        });

        if (
          !this.valueChanged.FirstName &&
          !this.valueChanged.LastName &&
          !this.valueChanged.PhoneNo
        ) {
          this.isAnyvalueChanged = this.profilepicUpdated;
        }
      }
    });
  }

  ionViewDidEnter() {
    const userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (userDetails && ["Admin", "Supervisor"].includes(userDetails.Role)) {
      this.isSupervisor = true;
    }
    this.profilepicUpdated = false;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getUserDetailsFromLocal();
    } else {
      this.getUserDetails();
    }
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }
  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }

  openImagePage() {
    this.authService.setProfilePic = this.ProfilePic;
    this.navCntrl.navigateForward("/profile/display-image");
  }

  handleLoad() {
    this.showProfilePicPlaceHolder = false;
  }

  getUserDetails() {
    this.apiService.getUserDetails().subscribe((res: any) => {
      this.userDetailsFrom.patchValue(res);
      this.apiUserDetails = res;
      if (res.ProfilePic && res.ProfilePic !== "") {
        this.ProfilePic = this.coreService.sanitizeSecurityTrustUrl(
          res.ProfilePic
        );
        this.profilepicUpdated = false;
      } else {
        this.ProfilePic = this.defaultProfileImg;
        this.profilepicUpdated = true;
        this.isAnyvalueChanged = true;
      }
      this.setAuthgardValuesOfUser(res);

      if (this.platform.is("cordova")) {
        this.userProfileService.storeProfileForOffline(res);
      }
    });
  }

  get userDetailCntrls() {
    return this.userDetailsFrom.controls;
  }

  cancelFn() {
    this.navCntrl.navigateRoot("/dashboard");
  }

  async handleChangeProfile() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select image source",
      buttons: [
        {
          text: "Load from Library",
          icon: "image-outline",
          handler: () => {
            this.platform.is("cordova")
              ? this.chooseImage(true)
              : console.log("cordova is not availbale");
          },
        },
        {
          text: "Camera",
          icon: "camera-outline",
          handler: () => {
            this.platform.is("cordova")
              ? this.chooseImage(false)
              : console.log("cordova is not availbale");
          },
        },
        {
          text: "Cancel",
          icon: "close",
          role: "cancel",
          handler: () => { },
        },
      ],
    });

    await actionSheet.present();
  }

  chooseImage(album: boolean) {
    const options: CameraOptions = {
      quality: 49,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true,
    };

    if (album) {
      options["sourceType"] = this.camera.PictureSourceType.SAVEDPHOTOALBUM;
    }

    this.camera.getPicture(options).then(
      (imageData) => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        this.UpdatedProfilePic = base64Image;
        this.ProfilePic = this.coreService.sanitizeSecurityTrustUrl(
          base64Image
        );
        this.profilepicUpdated = true;
        this.isAnyvalueChanged = true;
      },
      (err) => {
        console.log("Error while getting camera picture : ", err);
      }
    );
  }

  update() {
    if (
      this.userDetailsFrom.valid &&
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      let request = {};
      let offlineCacheData: any = {};
      Object.assign(request, this.userDetailsFrom.getRawValue());
      Object.assign(offlineCacheData, this.userDetailsFrom.getRawValue());

      offlineCacheData["ProfilePic"] = this.ProfilePic;
      if (this.profilepicUpdated) {
        request["ProfilePic"] = this.UpdatedProfilePic;
      }

      this.apiService.updateUserDetails(request, "yes").subscribe(
        (res: any) => {
          this.coreService.displayToast(res.Status, "success");
          if (!this.profilepicUpdated) {
            request["ProfilePic"] = this.ProfilePic;
          }
          this.setAuthgardValuesOfUser(request);
          this.coreService.notifyOther({
            option: NOTIFY_OBSERVABLE.PROFILE_CHANGE,
            value: true,
          });
          this.valueChanged = {
            FirstName: false,
            LastName: false,
            PhoneNo: false,
          };
          this.isAnyvalueChanged = false;
          this.profilepicUpdated = false;
          this.apiUserDetails = { ...this.apiUserDetails, ...request };
        },
        (err) => {
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
          this.getUserDetails();
        }
      );
      if (this.platform.is("cordova")) {
        this.userProfileService
          .updateProfilePageDate(offlineCacheData)
          .then(() => {
            this.handleProfileSuccessReflection(offlineCacheData);
          });
      }
    } else if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  getUserDetailsFromLocal() {
    this.userProfileService
      .getFirstRecord()
      .then((res: UserProfileModel) => {
        this.userDetailsFrom.patchValue(res);
        this.apiUserDetails = res;
        if (res.ProfilePic) {
          this.ProfilePic = this.coreService.sanitizeSecurityTrustUrl(
            res.ProfilePic
          );
        } else {
          this.ProfilePic = this.defaultProfileImg;
        }
      })
      .catch((err) => {
        this.coreService.displayToast(
          "You are offline and no data found for user locally",
          "warning"
        );
      });
  }

  setAuthgardValuesOfUser(res) {
    let fullName = res.FirstName ? res.FirstName : "";
    fullName += res.LastName ? " " + res.LastName : "";
    this.authService.setMenuTitle = fullName;
    this.authService.setMenuInfo = res.EmployeeId;
    this.authService.setProfilePic = res.ProfilePic;
    localStorage.setItem(APP_CONSTANTS.USER_DETAILS_KEY, JSON.stringify(res));
  }

  handleProfileSuccessReflection(paylaod) {
    let fullName = paylaod.FirstName ? paylaod.FirstName : "";
    fullName += paylaod.LastName ? ` ${paylaod.LastName}` : paylaod.LastName;
    this.authService.setMenuTitle = fullName;
    this.authService.setMenuInfo = paylaod.EmployeeId;
    if (this.profilepicUpdated) {
      this.authService.setProfilePic = paylaod.ProfilePic;
    }
    this.profilepicUpdated = false;
    this.apiUserDetails = { ...this.apiUserDetails, ...paylaod };

    localStorage.setItem(
      APP_CONSTANTS.USER_DETAILS_KEY,
      JSON.stringify(this.apiUserDetails)
    );
    this.coreService.notifyOther({
      option: NOTIFY_OBSERVABLE.PROFILE_CHANGE,
      value: true,
    });
  }

  async handleLogout() {
    const logoutAlert = await this.alertCtrl.create({
      header: "Sign Out!",
      message: "Are you sure you want to sign out?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Yes",
          handler: () => {
            this.authService.handleLogoutProcess();
          },
        },
      ],
    });

    await logoutAlert.present();
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
