import { Component, OnInit } from "@angular/core";
import { AuthGuardService } from "src/app/services/auth-guard.service";

@Component({
  selector: "app-display-image",
  templateUrl: "./display-image.page.html",
  styleUrls: ["./display-image.page.scss"],
})
export class DisplayImagePage implements OnInit {
  displayImage: any;
  constructor(private authService: AuthGuardService) { }

  ngOnInit() {
    this.displayImage = this.authService.getProfilePic;
  }
}
