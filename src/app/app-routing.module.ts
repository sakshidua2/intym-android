import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "./services/auth-guard.service";
import { NoAuthGuardService } from "./services/no-auth-guard.service";
import { AppComponent } from "./app.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full",
  },
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginPageModule),
    canActivate: [NoAuthGuardService],
  },
  {
    path: "attendance-screen",
    loadChildren: () =>
      import("./attendance-screen/attendance-screen.module").then(
        (m) => m.AttendanceScreenPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "attendance-history",
    loadChildren: () =>
      import("./attendance-history/attendance-history.module").then(
        (m) => m.AttendanceHistoryPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "profile",
    loadChildren: () =>
      import("./profile/profile.module").then((m) => m.ProfilePageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: "change-password",
    loadChildren: () =>
      import("./change-password/change-password.module").then(
        (m) => m.ChangePasswordPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "leaves",
    loadChildren: () =>
      import("./leaves/leaves.module").then((m) => m.LeavesPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: "forgot-password",
    loadChildren: () =>
      import("./forgot-password/forgot-password.module").then(
        (m) => m.ForgotPasswordPageModule
      ),
    canActivate: [NoAuthGuardService],
  },
  {
    path: "static-content",
    loadChildren: () =>
      import("./static-content/static-content.module").then(
        (m) => m.StaticContentPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "notification-list",
    loadChildren: () =>
      import("./notification-list/notification-list.module").then(
        (m) => m.NotificationListPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: "settings",
    loadChildren: () =>
      import("./settings/settings.module").then((m) => m.SettingsPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: "customer-visit-history",
    loadChildren: () =>
      import("./customer-visit-history/customer-visit-history.module").then(
        (m) => m.CustomerVisitHistoryPageModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: "sales-visit",
    loadChildren: () =>
      import("./sales-visit/sales-visit.module").then(
        (m) => m.SalesVisitPageModule
      ),
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
