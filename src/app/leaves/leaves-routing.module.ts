import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeavesPage } from './leaves.page';

const routes: Routes = [
  {
    path: '',
    component: LeavesPage,
    children: [
      {
        path: '',
        redirectTo: 'my-leaves',
        pathMatch: 'full'
      },
      {
        path: 'my-leaves',
        children: [
          {
            path: '',
            loadChildren: () => import('./my-leaves/my-leaves.module').then(m => m.MyLeavesPageModule)
          }
        ]
      },
      {
        path: 'team-leaves',
        children: [
          {
            path: '',
            loadChildren: () => import('./team-leaves/team-leaves.module').then(m => m.TeamLeavesPageModule)
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeavesPageRoutingModule { }
