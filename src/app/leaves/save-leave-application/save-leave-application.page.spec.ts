import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaveLeaveApplicationPage } from './save-leave-application.page';

describe('SaveLeaveApplicationPage', () => {
  let component: SaveLeaveApplicationPage;
  let fixture: ComponentFixture<SaveLeaveApplicationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveLeaveApplicationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaveLeaveApplicationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
