import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveLeaveApplicationPageRoutingModule } from './save-leave-application-routing.module';

import { SaveLeaveApplicationPage } from './save-leave-application.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SaveLeaveApplicationPageRoutingModule
  ],
  declarations: [SaveLeaveApplicationPage]
})
export class SaveLeaveApplicationPageModule {}
