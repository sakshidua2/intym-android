import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CoreService } from "src/app/services/core.service";
import { ModalController } from "@ionic/angular";
import { ApiService } from "src/app/services/api.service";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";

@Component({
  selector: "app-save-leave-application",
  templateUrl: "./save-leave-application.page.html",
  styleUrls: ["./save-leave-application.page.scss"],
})
export class SaveLeaveApplicationPage implements OnInit {
  @Input() leaveInfo: any;
  @Input() leaveBalance: any;
  @Input() selectedDate: any;
  public startDate = null;
  leaveApplication: FormGroup;
  isSubmitted: boolean;
  fromMinDate: any;
  fromMaxDate: any;
  toMinDate: any;
  toMaxDate: any;
  leaveTypes: any[];
  leaveDayTypes: any[];
  NoofDays: number;
  isSameDates: boolean;
  constructor(
    private fb: FormBuilder,
    public modalCtrl: ModalController,
    private apiService: ApiService,
    private coreService: CoreService,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    const now: any = this.coreService.getMomentNow();
    this.startDate = now;
    this.isSubmitted = false;
    this.NoofDays = 1;
    const minDate: any = this.coreService.getMomentNow();
    const maxDate: any = this.coreService.getMomentNow();
    maxDate.add(1, "year");
    minDate.subtract(1, "year")
    this.fromMinDate = this.toMinDate = minDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    this.fromMaxDate = this.toMaxDate = maxDate.format(
      APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
    );
    this.leaveApplication = this.fb.group({
      FromDate: this.selectedDate || [now.toISOString(), Validators.compose([Validators.required])],
      ToDate: this.selectedDate || [now.toISOString(), Validators.compose([Validators.required])],
      Reason: [
        "",
        Validators.compose([
          Validators.required,
          this.coreService.onlySpacesNotAllowed,
          Validators.maxLength(500),
        ]),
      ],
      FromDayType: [, Validators.compose([Validators.required])],
      ToDayType: [
        { value: "", disabled: true },
        Validators.compose([Validators.required]),
      ],
      LeaveType: ["", Validators.compose([Validators.required])],
    });
    this.dateAdjust("from");
    if (this.leaveInfo) {
      const FromDate = this.leaveInfo.FromDate.split("/");
      this.leaveInfo.FromDate = `${FromDate[1]}/${FromDate[0]}/${FromDate[2]}`;
      const ToDate = this.leaveInfo.ToDate.split("/");
      this.leaveInfo.ToDate = `${ToDate[1]}/${ToDate[0]}/${ToDate[2]}`;
      this.leaveApplication.patchValue(this.leaveInfo);
      this.NoofDays = Number(this.leaveInfo.LeaveCount);
    }
    this.isSameDates = true;
  }

  ionViewDidEnter() {
    this.getLeaveTypes();
    this.getLeaveDayTypes();
  }

  setTotalLeaves() {
    if (
      this.coreService
        .getMomentObject(this.fControls.FromDate.value)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) ===
      this.coreService
        .getMomentObject(this.fControls.ToDate.value)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
    ) {
      if (this.fControls.FromDayType.value !== 0) {
        this.NoofDays = 0.5;
      } else {
        this.NoofDays = 1;
      }
      return;
    }
    if (
      this.fControls.FromDayType.value !== 0 &&
      this.fControls.ToDayType.value !== 0
    ) {
      this.NoofDays = this.dateDifference(
        this.fControls.FromDate.value,
        this.fControls.ToDate.value
      );
      this.NoofDays = this.NoofDays === 0 ? 0.5 : this.NoofDays;
    } else {
      if (this.NoofDays === parseInt(this.NoofDays.toString(), 10)) {
        this.NoofDays = this.dateDifference(
          this.fControls.FromDate.value,
          this.fControls.ToDate.value
        );
        if (
          this.fControls.FromDayType.value !== 0 ||
          this.fControls.ToDayType.value !== 0
        ) {
          this.NoofDays += 0.5;
        } else {
          this.NoofDays += 1;
        }
      } else {
        if (
          this.fControls.FromDayType.value !== 0 ||
          this.fControls.ToDayType.value !== 0
        ) {
          this.NoofDays =
            this.dateDifference(
              this.fControls.FromDate.value,
              this.fControls.ToDate.value
            ) + 0.5;
        } else {
          this.NoofDays =
            this.dateDifference(
              this.fControls.FromDate.value,
              this.fControls.ToDate.value
            ) + 1;
        }
      }
    }
    return;
  }

  getLeaveTypes() {
    const localLeaveTypes = localStorage.getItem(
      APP_CONSTANTS.LEAVE_TYPES_LOCAL_STR_KEY
    );
    if (localLeaveTypes) {
      try {
        const typesPayload = JSON.parse(
          localStorage.getItem(APP_CONSTANTS.LEAVE_TYPES_LOCAL_STR_KEY)
        );
        const now: any = this.coreService.getMomentNow();
        if (
          typesPayload &&
          typesPayload.timestamp &&
          typesPayload.timestamp > now.unix()
        ) {
          this.leaveTypes = typesPayload.types;
          setTimeout(() => {
            this.fControls.LeaveType.setValue(typesPayload.types[0].LeaveId);
          }, 0);
        } else if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Online
        ) {
          this.getLeaveTypesApiCall();
        } else if (typesPayload) {
          this.leaveTypes = typesPayload.types;
          setTimeout(() => {
            this.fControls.LeaveType.setValue(typesPayload.types[0].LeaveId);
          }, 0);
        } else {
          this.coreService.displayToast(
            LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
            "warning"
          );
          this.dismiss();
        }
      } catch (err) {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
          "warning"
        );
        this.dismiss();
      }
    } else if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.getLeaveTypesApiCall();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
        "warning"
      );
      this.dismiss();
    }
  }

  getLeaveTypesApiCall() {
    this.apiService.getLeaveTypes().subscribe(
      (res: any) => {
        if (res && res.length) {
          this.leaveTypes = res;
          setTimeout(() => {
            this.fControls.LeaveType.setValue(res[0].LeaveId);
          }, 0);
          const now: any = this.coreService.getMomentNow();
          now.add(APP_CONSTANTS.LEAVE_TYPES_CACHE_EXPIRE_TIME_SPAN, "day");
          const payloadLocal = { types: [...res], timestamp: now.unix() };
          localStorage.setItem(
            APP_CONSTANTS.LEAVE_TYPES_LOCAL_STR_KEY,
            JSON.stringify(payloadLocal)
          );
        } else {
          this.leaveTypes = [];
        }
      },
      (err) => {
        this.leaveTypes = [];
        const message =
          err &&
            err.error &&
            (err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"])
            ? err.error["Status"] ||
            err.error["ErrorMessage"] ||
            err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  getLeaveDayTypes() {
    const localDayTypes = localStorage.getItem(
      APP_CONSTANTS.LEAVE_DAY_TYPES_LOCAL_STR_KEY
    );
    if (localDayTypes) {
      try {
        const typesPayload = JSON.parse(
          localStorage.getItem(APP_CONSTANTS.LEAVE_DAY_TYPES_LOCAL_STR_KEY)
        );
        const now: any = this.coreService.getMomentNow();
        if (
          typesPayload &&
          typesPayload.timestamp &&
          typesPayload.timestamp > now.unix()
        ) {
          this.leaveDayTypes = typesPayload.types;
          setTimeout(() => {
            this.fControls.ToDayType.setValue(typesPayload.types[0].DayId);
            this.fControls.FromDayType.setValue(typesPayload.types[0].DayId);
          }, 0);
        } else if (
          this.networkService.getCurrentNetworkStatus() ===
          ConnectionStatus.Online
        ) {
          this.getDayTypesApiCall();
        } else if (typesPayload) {
          this.leaveDayTypes = typesPayload.types;
          setTimeout(() => {
            this.fControls.ToDayType.setValue(typesPayload.types[0].DayId);
            this.fControls.FromDayType.setValue(typesPayload.types[0].DayId);
          }, 0);
        } else {
          this.coreService.displayToast(
            LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
            "warning"
          );
          this.dismiss();
        }
      } catch (err) {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
          "warning"
        );
        this.dismiss();
      }
    } else if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.getDayTypesApiCall();
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_INSUFF_DATA,
        "warning"
      );
      this.dismiss();
    }
  }

  getDayTypesApiCall() {
    this.apiService.getLeaveDayTypes().subscribe(
      (res: any) => {
        if (res && res.length) {
          this.leaveDayTypes = res;
          setTimeout(() => {
            this.fControls.ToDayType.setValue(res[0].DayId);
            this.fControls.FromDayType.setValue(res[0].DayId);
          }, 0);
          const now: any = this.coreService.getMomentNow();
          now.add(APP_CONSTANTS.LEAVE_DAY_TYPES_CACHE_EXPIRE_TIME_SPAN, "day");
          const payloadLocal = { types: [...res], timestamp: now.unix() };
          localStorage.setItem(
            APP_CONSTANTS.LEAVE_DAY_TYPES_LOCAL_STR_KEY,
            JSON.stringify(payloadLocal)
          );
        } else {
          this.leaveDayTypes = [];
        }
      },
      (err) => {
        this.leaveDayTypes = [];
        const message =
          err &&
            err.error &&
            (err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"])
            ? err.error["Status"] ||
            err.error["ErrorMessage"] ||
            err.error["Message"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  dateAdjust(type: string) {
    if (type === "from") {
      if (
        this.coreService.getMomentObject(this.fControls.FromDate.value).unix() >
        this.coreService.getMomentObject(this.fControls.ToDate.value).unix()
      ) {
        this.fControls.ToDate.setValue(
          this.coreService
            .getMomentObject(this.fControls.FromDate.value)
            .toISOString()
        );
      }
    } else {
      if (
        this.coreService.getMomentObject(this.fControls.ToDate.value).unix() <
        this.coreService.getMomentObject(this.fControls.FromDate.value).unix()
      ) {
        this.fControls.FromDate.setValue(
          this.coreService
            .getMomentObject(this.fControls.ToDate.value)
            .toISOString()
        );
      }
    }
    if (
      this.coreService
        .getMomentObject(this.fControls.FromDate.value)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD) !==
      this.coreService
        .getMomentObject(this.fControls.ToDate.value)
        .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD)
    ) {
      this.setTotalLeaves();
    } else {
      if (this.fControls.FromDayType.value !== 0) {
        this.NoofDays = 0.5;
      } else {
        this.NoofDays = 1;
      }
    }

    if (this.NoofDays > 1) {
      this.fControls.ToDayType.enable();
    } else {
      this.fControls.ToDayType.disable();
    }
  }

  dateDifference(date1: any, date2: any) {
    const dt1ISO = this.coreService.getMomentObject(date1).toISOString();
    const dt2ISO = this.coreService.getMomentObject(date2).toISOString();
    const dt1 = this.coreService.getMomentByDate(dt1ISO);
    const dt2 = this.coreService.getMomentByDate(dt2ISO);
    return Math.floor(dt2.diff(dt1) / (1000 * 60 * 60 * 24));
  }

  get fControls() {
    return this.leaveApplication.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.leaveApplication.valid) {
      const data: any = this.leaveApplication.getRawValue();
      data.FromDate = data.FromDate
        ? data.FromDate
        : data.ToDate
          ? data.ToDate
          : "";
      data.ToDate = data.ToDate
        ? data.ToDate
        : data.FromDate
          ? data.FromDate
          : "";
      data.NoofDays = this.NoofDays;

      if (data.FromDate) {
        data.FromDate = this.coreService
          .getMomentObject(data.FromDate)
          .toISOString();
      }
      if (data.ToDate) {
        data.ToDate = this.coreService
          .getMomentObject(data.ToDate)
          .toISOString();
      }

      data.LeaveId =
        this.leaveInfo && this.leaveInfo.EmpLeaveId
          ? this.leaveInfo.EmpLeaveId
          : data.LeaveType;
      delete data.LeaveType;
      if (!this.leaveInfo) {
        const now: any = this.coreService.getMomentNow();
        data.CreatedDate = now.toISOString();
      }

      if (this.leaveBalance >= data.NoofDays) {
        if (this.leaveInfo) {
          this.updateLeave(data);
        } else {
          this.applyLeave(data);
        }
      } else {
        this.coreService.displayToast(
          "Didn't have enough leave balance.",
          "warning"
        );
      }
    }
  }

  dismiss(role?) {
    role
      ? this.modalCtrl.dismiss(undefined, "leave-success")
      : this.modalCtrl.dismiss();
  }

  private updateLeave(data: any) {
    this.apiService.updateLeave(data).subscribe(
      (res: any) => {
        this.coreService.displayToast(res, "success");
        this.dismiss(true);
      },
      (err) => {
        const message =
          err && err.error
            ? err.error["ErrorMessage"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  private applyLeave(data: any) {
    this.apiService.applyLeave(data, "yes").subscribe(
      (res: any) => {
        this.coreService.displayToast(res.Status, "success");
        this.dismiss(true);
      },
      (err) => {
        const message =
          err && err.error
            ? err.error["ErrorMessage"]
            : "Something went wrong! please try later.";
        this.coreService.displayToast(message, "danger");
      }
    );
  }

  getFullName(firstName, lastName) {
    let fullName = firstName ? `${firstName}` : "";
    fullName += lastName ? ` ${lastName}` : "";
    return fullName;
  }
}
