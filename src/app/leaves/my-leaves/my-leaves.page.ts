import { Component, OnInit } from "@angular/core";
import { LeaveListItemBlockComponent } from "src/app/components/leave-list-item-block/leave-list-item-block.component";
import { ApiService } from "src/app/services/api.service";
import { LeaveFilterComponent } from "../leave-filter/leave-filter.component";
import { ModalController, Platform } from "@ionic/angular";
import { FormBuilder, FormGroup } from "@angular/forms";
import { SaveLeaveApplicationPage } from "../save-leave-application/save-leave-application.page";
import { CoreService } from "src/app/services/core.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LeavesService } from "src/app/services/db/leaves.service";
import { LeavesModel } from "src/app/local-db-models/leaves";
import { LeavesBalanceService } from "src/app/services/db/leaves-balance.service";
import { LeavesBalanceModel } from "src/app/local-db-models/leaves-balance";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";
@Component({
  selector: "app-my-leaves",
  templateUrl: "./my-leaves.page.html",
  styleUrls: ["./my-leaves.page.scss"],
})
export class MyLeavesPage implements OnInit {
  public leaveComponentTypeMy = LeaveListItemBlockComponent.COMPONENT_TYPE_MY;
  public calendarComponentTypeMy = APP_CONSTANTS.DATA_TYPE_MY;
  public leaves = [];
  leavesCopy = [];
  modalRef: any;
  body: any;
  leaveBalance: any;
  leaveFilterForm: FormGroup;
  public leaveStatusData: any;
  defaultFilterValue: { FromDate: string; ToDate: string; Status?: string };
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public yearFormatted = APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD;
  isSupervisor: boolean;
  public isLeavesListReadyToRender = false;
  constructor(
    private apiService: ApiService,
    public modalController: ModalController,
    private coreService: CoreService,
    private fb: FormBuilder,
    private networkService: NetworkService,
    private leavesService: LeavesService,
    private leavesBalanceService: LeavesBalanceService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform
  ) { }

  ngOnInit() {
    this.body = {};
    this.leaveFilterForm = this.fb.group({
      FromDate: [""],
      ToDate: [""],
      Status: [""],
    });
  }

  ionViewDidEnter() {
    this.isLeavesListReadyToRender = false;
    this.isSupervisor = false;
    const user = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    }
    const fromDate: any = this.coreService.getMomentNow();
    const toDate: any = this.coreService.getMomentNow();
    this.body = {
      FromDate: fromDate,
      ToDate: toDate,
      EmpLeaveId: 0,
    };
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
      Status: "all",
    };
    this.leaveFilterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getLeavesBalanceFromLocal();
      this.getLeavesFromLocal();
    } else {
      this.getMyLeaveBalance();
      this.getMyLeaves();
    }
  }

  handleMyCountActionCallback = async (payload: any) => {
    this.body.FromDate = this.coreService.getMomentObject(payload.startTime);
    this.body.ToDate = this.coreService.getMomentObject(payload.endTime);
    this.body.CalendarFromDate = payload.body.FromDate;
    this.body.CalendarToDate = payload.body.ToDate;
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getLeavesBalanceFromLocal();
      this.getLeavesFromLocal();
    } else {
      this.getMyLeaveBalance();
      if (!payload.sameMonth) { this.getLeaveCalendar(); } else {
        this.getMyLeaves();
      }
    }
  }

  getLeaveCalendar(e?: any) {
    this.leaveStatusData = {};
    this.apiService.getLeaveCalendar({
      FromDate: this.body.CalendarFromDate,
      ToDate: this.body.CalendarToDate,
      StopLoader: false,
    }).subscribe(async (res: any) => {
      await res.map((val, key) => {
        const date = this.coreService.getMomentFormatDate(
          this.yearFormatted,
          val.Date
        );
        this.leaveStatusData = {
          ...this.leaveStatusData,
          [date]: val,
        };
        return true;
      });
      this.getMyLeaves(e);
    });
  }

  generatePdf() {
    if (this.isSupervisor) {
      const userDetails = JSON.parse(
        localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
      );
      let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Employee Leave Report</h3>
      </div>
      <div>
        <table class="info-table">
          <tr>
            <td>Employee: ${userDetails.FirstName + userDetails.LastName}</td>
            <td>From Date: ${this.body.FromDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Employee</th>
              <th>From Date</th>
              <th>To Date</th>
              <th>Type</th>
              <th>No of Days</th>
              <th>Last Modified Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
    `;
      this.leaves.forEach((element) => {
        const FromDate = this.coreService
          .getMomentByDate(element.FromDate)
          .format(APP_CONSTANTS.DATE_FORMAT);
        const ToDate = this.coreService
          .getMomentByDate(element.ToDate)
          .format(APP_CONSTANTS.DATE_FORMAT);
        const row = `
                  <tr>
                  <td>${element.EmployeeName}</td>
                  <td>${FromDate}</td>
                  <td>${ToDate}</td>
                  <td>${element.LeaveType}</td>
                  <td>${element.LeaveCount}</td>
                  <td>${[2, 3].includes(element.StatusId)
            ? element.SupervisorName
            : ""
          }</td>
                  <td>-</td>
                  <td>${element.StatusName}</td>
                </tr>`;
        html += row;
      });
      const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                     <p>Report Generated By: ${userDetails.FirstName + userDetails.LastName
        } : ${this.coreService.getMomentNow("dd MMMM yyyy")}</p>
                  </div>
                </body>
              </html>`;
      html += endOfHtml;
      this.pdfGenerator
        .fromData(html, {
          documentSize: "A4",
          fileName: `my-leaves_report${this.coreService.getMomentNow(
            "ddMMyyyy_hhmmss"
          )}.pdf`,
          type: "share",
        })
        .then((res) => {
          console.log("res =>", res);
        })
        .catch((err) => {
          console.log({ err });
        });
    }
  }
  getMyLeaves(e?: any, StopLoader?: boolean) {
    const EmpLeaveId = this.leaveStatusData && this.leaveStatusData[this.body.ToDate.format(this.yearFormatted)]
      ? this.leaveStatusData[this.body.ToDate.format(this.yearFormatted)].EmpLeaveId
      : 0;
    if (EmpLeaveId !== 0) {
      this.apiService
        .getLeaveDataById(EmpLeaveId)
        .subscribe(
          (res: any) => {
            const IsMyLeave = true;
            const myLeaves = [{ ...res, IsMyLeave }];
            this.leaves = myLeaves;
            this.leavesCopy = myLeaves;
            this.leaves = this.sortLeaves(this.leaves);
            if (e && e.target && e.target.complete) {
              e.target.complete();
            }
            this.isLeavesListReadyToRender = true;
            if (this.platform.is("cordova")) {
              this.leavesService.storeLeavesForOffline(
                this.leavesCopy,
                IsMyLeave
              );
            }
          },
          (err) => {
            this.isLeavesListReadyToRender = true;
            const message =
              err &&
                err.error &&
                (err.error["Status"] ||
                  err.error["ErrorMessage"] ||
                  err.error["Message"])
                ? err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"]
                : "Something went wrong! please try later.";
            this.coreService.displayToast(message, "danger");
          }
        );
    } else {
      this.leaves = [];
      if (e && e.target && e.target.complete) {
        e.target.complete();
      }
    }
  }

  getMyLeaveBalance() {
    this.apiService.getMyLeaveBalance().subscribe((res: any) => {
      this.leaveBalance = res;
      if (this.platform.is("cordova")) {
        this.leavesBalanceService.storeLeavesForOffline(res);
      }
    });
  }

  getLeavesFromLocal(e?: any, StopLoader?: boolean) {
    // this.coreService.showLoader();
    const params = {
      EmpLeaveId: this.body.EmpLeaveId,
      IsMyLeave: true,
      StopLoader,
    };
    this.leavesService
      .getDataByEmpLeaveId(params)
      .then((res: LeavesModel[]) => {
        this.leaves = res;
        this.leavesCopy = res;
        if (this.body.Status && this.body.Status !== "all") {
          this.leaves = this.filterLeaves(this.leaves);
        }
        this.leaves = this.sortLeaves(this.leaves);
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
      })
      .catch((err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_NO_LOCAL_DATA,
          "warning"
        );
      });
  }

  getLeavesBalanceFromLocal() {
    this.leavesBalanceService
      .getData()
      .then((res: LeavesBalanceModel[]) => {
        this.leaveBalance = res;
      })
      .catch((err) => {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_NO_LOCAL_DATA,
          "warning"
        );
      });
  }

  handleMyLeaveActionCallback = async (payload: any) => {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const modalRef = await this.modalController.create({
        component: SaveLeaveApplicationPage,
        cssClass: "custom-filter-modal",
        componentProps: { leaveInfo: payload.type },
        mode: "md",
      });
      modalRef.present();
      modalRef.onDidDismiss().then((res: any) => {
        console.log(res);
      });
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  };

  handleCancelLeaveCallback = async (payload: any) => {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.updateLeave([payload]).subscribe(
        (res: any) => {
          if (res && res.Status) {
            this.coreService.displayToast(
              "Leave cancelled successfully",
              "success"
            );
          } else {
            this.coreService.displayToast("Something went wrong!", "danger");
          }
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getLeavesFromLocal();
          } else {
            this.getMyLeaves();
          }
        },
        (err) => {
          const message =
            err && err.error
              ? err.error["Status"] || err.error["ErrorMessage"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  };

  doRefresh(e) {
    this.body.CalendarFromDate = this.body.FromDate.startOf("month").format(this.yearFormatted);
    this.body.CalendarToDate = this.body.ToDate.endOf("month").format(this.yearFormatted);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getLeavesFromLocal(e);
    } else {
      this.getLeaveCalendar(e);
      this.getMyLeaves(e, true);
    }
  }

  async handleFilter() {
    const minDate: any = this.coreService.getMomentNow();
    const min = minDate
      .startOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const maxDate: any = this.coreService.getMomentNow();
    const max = maxDate
      .endOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const modalRef = await this.modalController.create({
      component: LeaveFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        isTeamLeaves: true,
        leaveFilterForm: this.leaveFilterForm,
        defaultValues: this.defaultFilterValue,
        minMaxDates: { min, max },
      },
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          const FromDate = this.body.FromDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          const ToDate = this.body.ToDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          if (
            (res.data.FromDate &&
              FromDate.unix() !== res.data.FromDate.unix()) ||
            (res.data.ToDate && ToDate.unix() !== res.data.ToDate.unix())
          ) {
            this.body = res.data;
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getLeavesFromLocal();
            } else {
              this.getMyLeaves();
            }
            return;
          }
          this.body = res.data;
          let list = JSON.parse(JSON.stringify(this.leavesCopy));
          if (res.data.User) {
            list = list.filter((ele) => ele.EmployeeId === this.body.User);
          }
          if (res.data.Status && res.data.Status !== "all") {
            list = list.filter(
              (ele) => ele.StatusName.toLowerCase() === res.data.Status
            );
          }
          this.leaves = list.sort(
            (a, b) =>
              this.coreService.getMomentByDate(b.AppliedDate).unix() -
              this.coreService.getMomentByDate(a.AppliedDate).unix()
          );
        } else {
          const fromDate: any = this.coreService.getMomentNow();
          const toDate: any = this.coreService.getMomentNow();
          this.body = {
            FromDate: fromDate.date(1),
            ToDate: toDate.add(1, "month").endOf("month"),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getLeavesFromLocal();
          } else {
            this.getMyLeaves();
          }
        }
      }
    });
  }

  async handleAdd() {
    if (
      this.networkService.getCurrentNetworkStatus() ===
      ConnectionStatus.Online &&
      this.leaveBalance[0].Balance !== 0
    ) {
      const modalRef = await this.modalController.create({
        component: SaveLeaveApplicationPage,
        cssClass: "custom-filter-modal",
        mode: "md",
        componentProps: { leaveBalance: this.leaveBalance[0].Balance },
      });
      modalRef.present();
      modalRef.onDidDismiss().then((res) => {
        if (res && res.role && res.role !== "backdrop") {
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getLeavesBalanceFromLocal();
            this.getLeavesFromLocal();
          } else {
            this.getMyLeaveBalance();
            this.getMyLeaves();
          }
        }
      });
    } else if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    } else if (this.leaveBalance && this.leaveBalance[0].Balance === 0) {
      this.coreService.displayToast(
        "Didn't have enough leave balance.",
        "warning"
      );
    }
  }

  filterLeaves(leaves) {
    return leaves.filter(
      (ele) => ele.StatusName.toLowerCase() === this.body.Status
    );
  }

  sortLeaves(leaves) {
    return leaves.sort(
      (a, b) =>
        this.coreService.getMomentByDate(b.AppliedDate).unix() -
        this.coreService.getMomentByDate(a.AppliedDate).unix()
    );
  }
}
