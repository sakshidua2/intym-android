import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyLeavesPageRoutingModule } from './my-leaves-routing.module';
import { CustomCalendarComponent } from '../calendar/calendar.component';
import { MyLeavesPage } from './my-leaves.page';
import { LeaveListItemBlockComponent } from 'src/app/components/leave-list-item-block/leave-list-item-block.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgCalendarModule } from 'ionic2-calendar';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    MyLeavesPageRoutingModule,
    SharedModule,
    NgCalendarModule,
  ],
  declarations: [MyLeavesPage, LeaveListItemBlockComponent, CustomCalendarComponent],
})
export class MyLeavesPageModule { }
