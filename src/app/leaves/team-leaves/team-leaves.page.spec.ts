import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeamLeavesPage } from './team-leaves.page';

describe('TeamLeavesPage', () => {
  let component: TeamLeavesPage;
  let fixture: ComponentFixture<TeamLeavesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamLeavesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TeamLeavesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
