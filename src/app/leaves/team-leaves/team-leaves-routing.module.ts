import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamLeavesPage } from './team-leaves.page';

const routes: Routes = [
  {
    path: '',
    component: TeamLeavesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamLeavesPageRoutingModule {}
