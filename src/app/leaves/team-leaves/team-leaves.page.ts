import { Component, OnInit } from "@angular/core";
import { LeaveListItemBlockComponent } from "src/app/components/leave-list-item-block/leave-list-item-block.component";
import { ApiService } from "src/app/services/api.service";
import { ModalController, Platform } from "@ionic/angular";
import { LeaveFilterComponent } from "../leave-filter/leave-filter.component";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CoreService } from "src/app/services/core.service";
import {
  NetworkService,
  ConnectionStatus,
} from "src/app/services/network.service";
import { LeavesService } from "src/app/services/db/leaves.service";
import { LeavesModel } from "src/app/local-db-models/leaves";
import { APP_CONSTANTS } from "src/app/config/app.constants";
import { LABEL_CONSTANTS } from "src/app/config/labels.constants";
import { PDFGenerator } from "@ionic-native/pdf-generator/ngx";

@Component({
  selector: "app-team-leaves",
  templateUrl: "./team-leaves.page.html",
  styleUrls: ["./team-leaves.page.scss"],
})
export class TeamLeavesPage implements OnInit {
  public leaveComponentTypeTeam =
    LeaveListItemBlockComponent.COMPONENT_TYPE_TEAM;
  public leaves = [];
  leavesCopy = [];
  leaveFilterForm: FormGroup;
  defaultFilterValue: {
    FromDate: string;
    ToDate: string;
    Status?: string;
    User?: number;
  };
  body: any;
  userList: any;
  public dateFormat = APP_CONSTANTS.DATE_FORMAT;
  public isLeavesListReadyToRender = false;
  constructor(
    private apiService: ApiService,
    private modalController: ModalController,
    private fb: FormBuilder,
    private coreService: CoreService,
    private networkService: NetworkService,
    private leavesService: LeavesService,
    private pdfGenerator: PDFGenerator,
    private platform: Platform
  ) { }

  ngOnInit() {
    this.body = {};
    this.leaveFilterForm = this.fb.group({
      FromDate: [""],
      ToDate: [""],
      Status: ["all"],
      User: [""],
    });
  }

  ionViewDidEnter() {
    this.isLeavesListReadyToRender = false;
    const fromDate: any = this.coreService.getMomentNow();
    const toDate: any = this.coreService.getMomentNow();
    this.body = {
      FromDate: fromDate.date(1),
      ToDate: toDate.add(1, "month").endOf("month"),
    };
    this.defaultFilterValue = {
      FromDate: this.body.FromDate.toISOString(),
      ToDate: this.body.ToDate.toISOString(),
      Status: "all",
      User: 0,
    };
    this.leaveFilterForm.patchValue(this.defaultFilterValue);
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getLeavesFromLocal();
    } else {
      this.getMyLeaves();
    }
  }

  getMyLeaves(e?: any, StopLoader?: boolean) {
    this.apiService
      .getLeaves({
        FromDate: this.body.FromDate.format(
          APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
        ),
        ToDate: this.body.ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
        StopLoader,
      }, APP_CONSTANTS.DATA_TYPE_TEAM)
      .subscribe(
        (res: any) => {
          const teamLeaves = res.L1.concat(res.L2, res.L3);
          this.userList = teamLeaves.map((ele) => ele.EmployeeId)
            .map((ele, i, final) => final.indexOf(ele) === i && i)
            .filter((ele) => teamLeaves[ele])
            .map((e) => teamLeaves[e]);
          this.userList = this.userList.map(({ EmployeeId, EmployeeName }) => ({
            EmployeeId,
            EmployeeName,
          }));
          const IsMyLeave = false;
          const myTeamLeaves = teamLeaves.map((o) => ({
            ...o,
            IsMyLeave,
          }));
          this.leaves = myTeamLeaves;
          this.leavesCopy = myTeamLeaves;
          if (this.body.User && this.body.User !== 0) {
            this.leaves = this.filterLeavesByUser(this.leaves);
          }
          if (this.body.Status && this.body.Status !== "all") {
            this.leaves = this.filterLeaves(this.leaves);
          }
          this.leaves = this.sortLeaves(this.leaves);
          if (e && e.target && e.target.complete) {
            e.target.complete();
          }
          this.isLeavesListReadyToRender = true;
          if (history.state.data) {
            const notificationData = history.state["data"];
            const date = this.coreService.getMomentObject(
              notificationData.date
            );
            this.body.FromDate = this.body.ToDate = date;
            this.leaveFilterForm.patchValue({ FromDate: date.toISOString(), ToDate: date.toISOString() });
            this.getMyLeaves();
            history.state.data = undefined;
          }
          if (this.platform.is("cordova")) {
            this.leavesService.storeLeavesForOffline(
              this.leavesCopy,
              IsMyLeave
            );
          }
        },
        (err) => {
          this.isLeavesListReadyToRender = true;
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
  }

  getLeavesFromLocal(e?: any, StopLoader?: boolean) {
    // this.coreService.showLoader();
    const ToDate = this.coreService
      .getMomentObject(this.body.ToDate)
      .add(1, "day");
    const params = {
      FromDate: this.body.FromDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      ToDate: ToDate.format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD),
      IsMyLeave: false,
      StopLoader,
    };
    this.leavesService
      .getData(params)
      .then((res: LeavesModel[]) => {
        this.userList = res
          .map((ele) => ele.EmployeeId)
          .map((ele, i, final) => final.indexOf(ele) === i && i)
          .filter((ele) => res[ele])
          .map((e) => res[e]);
        this.userList = this.userList.map(({ EmployeeId, EmployeeName }) => ({
          EmployeeId,
          EmployeeName,
        }));
        this.leaves = res;
        this.leavesCopy = res;
        if (this.body.User && this.body.User !== 0) {
          this.leaves = this.filterLeavesByUser(this.leaves);
        }
        if (this.body.Status && this.body.Status !== "all") {
          this.leaves = this.filterLeaves(this.leaves);
        }
        this.leaves = this.sortLeaves(this.leaves);
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
      })
      .catch((err) => {
        if (e && e.target && e.target.complete) {
          e.target.complete();
        }
        this.isLeavesListReadyToRender = true;
        // this.coreService.hideLoader();
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_NO_LOCAL_DATA,
          "warning"
        );
      });
  }

  handleTeamLeaveActionCallback = (payload: any) => {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.updateLeave([payload]).subscribe(
        (res: any) => {
          this.getMyLeaves();
          if (res && res.Status) {
            this.coreService.displayToast(
              `Leave ${payload.StatusId === 2 ? "approved" : "rejected"
              } successfully`,
              "success"
            );
          } else {
            this.coreService.displayToast("Something went wrong!", "dange");
          }
        },
        (err) => {
          this.coreService.displayToast("Something went wrong!", "danger");
        }
      );
    } else {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  };

  doRefresh(e) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.getLeavesFromLocal(e);
    } else {
      this.getMyLeaves(e, true);
    }
  }

  async handleFilter() {
    const minDate: any = this.coreService.getMomentNow();
    const min = minDate
      .startOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const maxDate: any = this.coreService.getMomentNow();
    const max = maxDate
      .endOf("year")
      .format(APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD);
    const modalRef = await this.modalController.create({
      component: LeaveFilterComponent,
      cssClass: "custom-filter-modal leave-filter-modal",
      componentProps: {
        isTeamLeaves: true,
        leaveFilterForm: this.leaveFilterForm,
        defaultValues: this.defaultFilterValue,
        minMaxDates: { min, max },
      },
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      if (res.role && !["close", "backdrop"].includes(res.role)) {
        if (res && res.data) {
          Object.keys(res.data).forEach((key) => {
            if (!res.data[key]) {
              delete res.data[key];
            }
          });
          const FromDate = this.body.FromDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          const ToDate = this.body.ToDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0,
          });
          if (
            (res.data.FromDate &&
              FromDate.unix() !== res.data.FromDate.unix()) ||
            (res.data.ToDate && ToDate.unix() !== res.data.ToDate.unix())
          ) {
            this.body = res.data;
            if (
              this.networkService.getCurrentNetworkStatus() ===
              ConnectionStatus.Offline
            ) {
              this.getLeavesFromLocal();
            } else {
              this.getMyLeaves();
            }
            return;
          }
          this.body = res.data;
          let list = JSON.parse(JSON.stringify(this.leavesCopy));
          if (res.data.User && res.data.User !== 0) {
            list = list.filter((ele) => this.body.User.indexOf(ele.EmployeeId) > -1);
          }
          if (res.data.Status && res.data.Status !== "all") {
            list = list.filter(
              (ele) => ele.StatusName.toLowerCase() === res.data.Status
            );
          }
          this.leaves = list.sort(
            (a, b) =>
              this.coreService.getMomentByDate(b.AppliedDate).unix() -
              this.coreService.getMomentByDate(a.AppliedDate).unix()
          );
        } else {
          const fromDate: any = this.coreService.getMomentNow();
          const toDate: any = this.coreService.getMomentNow();
          this.body = {
            FromDate: fromDate.date(1),
            ToDate: toDate.add(1, "month").endOf("month"),
          };
          if (
            this.networkService.getCurrentNetworkStatus() ===
            ConnectionStatus.Offline
          ) {
            this.getLeavesFromLocal();
          } else {
            this.getMyLeaves();
          }
        }
      }
    });
  }

  generatePdf() {
    let EmployeeName = "";
    if (this.body.User) {
      const index = this.leaves.findIndex(
        (ele) => ele.EmployeeId === this.body.User
      );
      EmployeeName = index >= 0 ? this.leaves[index].EmployeeName : "";
    }
    let html = `
    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      .title-content {
        text-align: center;
      }
      .title-content h1 {
        margin: 5px 0;
        font-size: 20px;
      }
      .title-content h3 {
        margin: 5px 0;
        font-size: 16px;
      }
      table {
        border-collapse: collapse;
      }
      table,
      th,
      td {
        border: 1px solid #000;
      }
      .info-table {
        margin: 10px auto;
        width: 50%;
      }
      .data-tbl {
        margin: 10px auto;
        width: 95%;
      }
    </style>
  </head>
  <body>
    <div>
      <div class="title-content">
        <h1>Government of India</h1>
        <h3>Team Leaves Report</h3>
      </div>
      <div>
      <table class="info-table">
          <tr>
            <td>Employee: ${this.body.User && EmployeeName ? EmployeeName : "All"
      }</td>
            <td>From Date: ${this.body.FromDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
          </tr>
          <tr>
            <td>To Date: ${this.body.ToDate.format(
        APP_CONSTANTS.DATE_FORMAT
      )}</td>
            <td>Department: All</td>
          </tr>
          <tr>
            <td>Designation: All</td>
            <td>Location: All</td>
          </tr>
          <tr>
            <td>Status: ${this.body.Status || "all"}</td>
            <td>Type: All</td>
          </tr>
          <tr>
            <td>Employee Type: All</td>
            <td></td>
          </tr>
        </table>
      </div>
      <div>
        <table class="data-tbl">
          <thead>
            <tr>
              <th>Employee</th>
              <th>From Date</th>
              <th>To Date</th>
              <th>Type</th>
              <th>No of Days</th>
              <th>Authorized By</th>
              <th>Last Modified Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>`;
    this.leaves.forEach((element) => {
      const FromDate = this.coreService
        .getMomentByDate(element.FromDate)
        .format(APP_CONSTANTS.DATE_FORMAT);
      const ToDate = this.coreService
        .getMomentByDate(element.ToDate)
        .format(APP_CONSTANTS.DATE_FORMAT);
      const row = `
                <tr>
                  <td>${element.EmployeeName}</td>
                  <td>${FromDate}</td>
                  <td>${ToDate}</td>
                  <td>${element.LeaveType}</td>
                  <td>${element.LeaveCount}</td>
                  <td>${[2, 3].includes(element.StatusId)
          ? element.SupervisorName
          : ""
        }</td>
                  <td>-</td>
                  <td>${element.StatusName}</td>
                </tr>`;
      html += row;
    });
    const userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    const endOfHtml = `</tbody>
                      </table>
                    </div>
                    <div>
                     <p>Report Generated By: ${userDetails.FirstName + userDetails.LastName
      } : ${this.coreService.getMomentNow("dd MMMM yyyy")}</p>
                  </div>
                </body>
              </html>`;
    html += endOfHtml;
    this.pdfGenerator
      .fromData(html, {
        documentSize: "A4",
        fileName: `team-leaves_report${this.coreService.getMomentNow(
          "ddMMyyyy_hhmmss"
        )}.pdf`,
        type: "share",
      })
      .then((res) => {
        console.log("res =>", res);
      })
      .catch((err) => {
        console.log("error =>", err);
      });
  }

  filterLeavesByUser(leaves) {
    return leaves.filter((ele) => this.body.User.indexOf(ele.EmployeeId) > -1);
  }

  filterLeaves(leaves) {
    return leaves.filter(
      (ele) => ele.StatusName.toLowerCase() === this.body.Status
    );
  }

  sortLeaves(leaves) {
    return leaves.sort(
      (a, b) =>
        this.coreService.getMomentByDate(b.AppliedDate).unix() -
        this.coreService.getMomentByDate(a.AppliedDate).unix()
    );
  }
}
