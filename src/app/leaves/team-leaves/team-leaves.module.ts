import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamLeavesPageRoutingModule } from './team-leaves-routing.module';

import { TeamLeavesPage } from './team-leaves.page';
import { LeaveListItemBlockComponent } from 'src/app/components/leave-list-item-block/leave-list-item-block.component';
import { LeaveFilterComponent } from '../leave-filter/leave-filter.component';
import { SaveLeaveApplicationPageModule } from '../save-leave-application/save-leave-application.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SaveLeaveApplicationPageModule,
    TeamLeavesPageRoutingModule,
    SharedModule
  ],
  declarations: [TeamLeavesPage, LeaveListItemBlockComponent, LeaveFilterComponent ],
  providers: [DatePipe]
})
export class TeamLeavesPageModule { }
