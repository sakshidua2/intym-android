import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { SaveLeaveApplicationPage } from "./save-leave-application/save-leave-application.page";
import { SettingsPage } from "../settings/settings.page";
import { APP_CONSTANTS } from "../config/app.constants";
import { Router } from "@angular/router";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";

@Component({
  selector: "app-leaves",
  templateUrl: "./leaves.page.html",
  styleUrls: ["./leaves.page.scss"],
})
export class LeavesPage implements OnInit {
  public isSupervisor: boolean;
  public showViewNow: boolean;
  notificationCount: string | number = 0;
  constructor(private modalController: ModalController, private router: Router, private coreService: CoreService) { }

  ngOnInit() { }

  ionViewDidEnter() {
    const user: any = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    if (user && ["Admin", "Supervisor"].includes(user.Role)) {
      this.isSupervisor = true;
    } else {
      this.isSupervisor = false;
    }
    this.showViewNow = true;
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }
  showNotifications() {
    this.router.navigate(["/notification-list"]);
  }
  async handleAdd() {
    const modalRef = await this.modalController.create({
      component: SaveLeaveApplicationPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
