import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { CoreService } from 'src/app/services/core.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: "app-leave-filter",
  templateUrl: "./leave-filter.component.html",
  styleUrls: ["./leave-filter.component.scss"],
})
export class LeaveFilterComponent implements OnInit {
  @Input() isTeamLeaves: boolean;
  @Input() leaveFilterForm: FormGroup;
  @Input() defaultValues: any;
  @Input() minMaxDates: { min: any; max: any };
  @Input() isVisitsPage: boolean;
  userList: any[] = [];
  isSubmitted: boolean;
  fromMinDate: string;
  fromMaxDate: string;
  toMinDate: string;
  toMaxDate: string;
  StatusList: string[];
  constructor(
    public modalCtrl: ModalController,
    public coreService: CoreService,
    public apiService: ApiService,
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.fromMaxDate = this.toMaxDate = this.minMaxDates.max;
    this.fromMinDate = this.toMinDate = this.minMaxDates.min;
  }

  ionViewDidEnter() {
    this.StatusList = [
      "all",
      this.isVisitsPage ? "pending" : "applied",
      "approved",
      "rejected",
    ];
    if (!this.isVisitsPage) {
      this.StatusList.push("cancelled");
    }
    this.getEmployeeList();
  }

  getEmployeeList() {
    this.apiService.getEmployeeList().subscribe((res: any) => {
      if (res.L1) {
        this.userList = this.userList.concat(res.L1);
      }
      if (res.L2) {
        this.userList = this.userList.concat(res.L2);
      }
      if (res.L3) {
        this.userList = this.userList.concat(res.L3);
      }
      console.log(this.userList);
    });
  }

  dateAdjust(type: string) {
    if (type === "from") {
      if (
        this.coreService.getMomentObject(this.fControls.FromDate.value).unix() >
        this.coreService.getMomentObject(this.fControls.ToDate.value).unix()
      ) {
        this.fControls.ToDate.setValue(this.coreService.getMomentObject(this.fControls.FromDate.value).toISOString());
      }
    } else {
      if (
        this.coreService.getMomentObject(this.fControls.ToDate.value).unix() <
        this.coreService.getMomentObject(this.fControls.FromDate.value).unix()
      ) {
        this.fControls.FromDate.setValue(this.coreService.getMomentObject(this.fControls.ToDate.value).toISOString());
      }
    }
  }

  get fControls() {
    return this.leaveFilterForm.controls;
  }

  dismissModal() {
    this.modalCtrl.dismiss(undefined, "close");
  }

  onSubmit() {
    if (this.leaveFilterForm.valid) {
      const data = this.leaveFilterForm.getRawValue();
      if (data.FromDate) {
        data.FromDate = this.coreService.getMomentObject(data.FromDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
      }
      if (data.ToDate) {
        data.ToDate = this.coreService.getMomentObject(data.ToDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
      }
      const canEmit = Object.values(data).filter((ele) => ele);
      if (canEmit && canEmit.length) {
        this.modalCtrl.dismiss(data, "apply");
        return;
      }
      this.modalCtrl.dismiss(undefined, "apply");
    }
  }

  resetValue() {
    this.leaveFilterForm.reset();
    this.leaveFilterForm.patchValue(this.defaultValues);
    this.onSubmit();
  }
}
