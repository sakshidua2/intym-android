import { Injectable } from '@angular/core';
import { LocalDbService } from '../local-db.service';
import { LOCAL_DB_CONSTANTS } from 'src/app/config/local-db.constants';
import { LeavesBalanceModel } from 'src/app/local-db-models/leaves-balance';

@Injectable({
  providedIn: 'root'
})
export class LeavesBalanceService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_LEAVES_BALANCE;
  constructor(
    private localDbService: LocalDbService
  ) { }

  insertData(leaveBalance: LeavesBalanceModel) {
    const values = [
      leaveBalance.LeaveTypeId,
      leaveBalance.LeaveName,
      leaveBalance.Balance,
    ];
    return this.localDbService.db.executeSql(this.generateInsertSql(), values)
      .then(() => {
        console.log('Leave balance data added successfully');
      }).catch((err) => {
        console.error('Error while adding leave balance data', err);
      });
  }

  getData() {
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db.executeSql(`SELECT * FROM ${this.tbl}`, [])
            .then((res: any) => {
              const items: LeavesBalanceModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            }).catch((err) => {
              console.error('Error while fetching data =>', err);
              resolve([]);
            });
        }
      });
    });
  }

  deleteAllData() {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl}`;
      tx.executeSql(sql, [], (localTx, res) => {
        console.log('deleting leave data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting leave data => ', err);
      });
    }).then(() => {
      console.log('Transaction deleting leave data successfully');
    }).catch((err) => {
      console.error('Error while transaction deleting leave data => ', err);
    });
  }

  private generateInsertSql() {
    return `INSERT INTO ${this.tbl} (LeaveTypeId, LeaveName, Balance) VALUES (?, ?, ?)`;
  }

  insertBatch(leavesBalance: LeavesBalanceModel[]) {
    const insertSql = this.generateInsertSql();
    const batch = [];
    const leavesCount = leavesBalance.length;
    for (let index = 0; index < leavesCount; index++) {
      const leaveBalance = leavesBalance[index];
      const values = [
        leaveBalance.LeaveTypeId,
        leaveBalance.LeaveName,
        leaveBalance.Balance,
      ];
      batch.push([insertSql, values]);
    }
    this.localDbService.db.sqlBatch(batch)
      .then((res) => {
        console.log('Leaves balance records are added successfully. ');
      })
      .catch((err) => {
        console.log('Something went wrong while adding leaves balance records => ', err);
      });
  }

  storeLeavesForOffline(leavesBalance) {
    this.deleteAllData()
      .then(() => {
        console.log('Deleted previous records now start adding new.');
        this.insertBatch(leavesBalance);
      })
      .catch((err) => {
        console.error('Error while deleting previous records => ', err);
      });
  }
}
