import { Injectable } from "@angular/core";
import { LocalDbService } from "../local-db.service";
import { LOCAL_DB_CONSTANTS } from "src/app/config/local-db.constants";
import { AttendanceLogModel } from 'src/app/local-db-models/attendace_log';

@Injectable({
  providedIn: "root",
})
export class AttendanceLogService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_ATTENDANCE_LOG;
  constructor(
    private localDbService: LocalDbService
  ) { }

  insertData(attendanceLog: AttendanceLogModel) {
    const values = [
      attendanceLog.EmpAttnTransactionId,
      attendanceLog.EmployeeId,
      attendanceLog.EmployeeName,
      attendanceLog.TimeStamp,
      attendanceLog.AttendanceStatus,
      attendanceLog.DeviceSN,
      attendanceLog.ImageURL,
      attendanceLog.Latitude,
      attendanceLog.Longitude,
      attendanceLog.CustomerName,
      attendanceLog.CustomerAddress,
      attendanceLog.ApprovalStatus,
      attendanceLog.IsMyAttendanceLog
    ];
    return this.localDbService.db
      .executeSql(this.generateInsertSql(), values)
      .then(() => {
        console.log("Attendance log data added successfully");
      })
      .catch((err) => {
        console.error("Error while adding leave data", err);
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(`SELECT * FROM ${this.tbl} ORDER BY EmpAttnTransactionId desc`, [])
            .then((res: any) => {
              const items: AttendanceLogModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }

  getData(payload) {
    const { FromDate, ToDate, IsMyAttendanceLog } = payload;
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            WHERE TimeStamp BETWEEN ? AND ? AND IsMyAttendanceLog = ?
            ORDER BY EmpAttnTransactionId desc
          `, [FromDate, ToDate, IsMyAttendanceLog])
            .then((res: any) => {
              const items: AttendanceLogModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }

  deleteAllData() {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl}`;
      tx.executeSql(sql, [], (localTx, res) => {
        console.log('deleting attendance log data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting attendance log data => ', err);
      });
    });
  }

  deleteDataByMyAttendanceLogStatus(IsMyAttendanceLog) {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl} WHERE IsMyAttendanceLog = ?`;
      tx.executeSql(sql, [IsMyAttendanceLog], (localTx, res) => {
        console.log('deleting attendance log data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting attendance data => ', err);
      });
    });
  }

  private generateInsertSql() {
    return `INSERT INTO ${this.tbl} (
      EmpAttnTransactionId,
      EmployeeId,
      EmployeeName,
      TimeStamp,
      AttendanceStatus,
      DeviceSN,
      ImageURL,
      Latitude,
      Longitude,
      CustomerName,
      CustomerAddress,
      ApprovalStatus,
      IsMyAttendanceLog
    ) VALUES(?, ?, ?, DATETIME(?), ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
  }

  insertBatch(attendanceLogs: AttendanceLogModel[]) {
    const insertSql = this.generateInsertSql();
    const batch = [];
    const leavesCount = attendanceLogs.length;
    for (let index = 0; index < leavesCount; index++) {
      const attendanceLog = attendanceLogs[index];
      const values = [
        attendanceLog.EmpAttnTransactionId,
        attendanceLog.EmployeeId,
        attendanceLog.EmployeeName,
        attendanceLog.TimeStamp,
        attendanceLog.AttendanceStatus,
        attendanceLog.DeviceSN,
        attendanceLog.ImageURL,
        attendanceLog.Latitude,
        attendanceLog.Longitude,
        attendanceLog.CustomerName,
        attendanceLog.CustomerAddress,
        attendanceLog.ApprovalStatus,
        attendanceLog.IsMyAttendanceLog
      ];
      batch.push([insertSql, values]);
    }
    this.localDbService.db
      .sqlBatch(batch)
      .then((res) => {
        console.log("Attendance log records are added successfully.");
      })
      .catch((err) => {
        console.log(
          "Something went wrong while adding leaves records => ",
          err
        );
      });
  }

  storeAttendanceLogForOffline(attendanceLogs, IsMyAttendanceLog) {
    this.deleteDataByMyAttendanceLogStatus(IsMyAttendanceLog)
      .then(() => {
        console.log('Deleted previous records now start adding new.');
        this.insertBatch(attendanceLogs);
      })
      .catch((err) => {
        console.error("Error while deleting previous records => ", err);
      });
  }
}
