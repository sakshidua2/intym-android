import { Injectable } from "@angular/core";
import { LocalDbService } from "../local-db.service";
import { LOCAL_DB_CONSTANTS } from "src/app/config/local-db.constants";
import { LeavesModel } from "src/app/local-db-models/leaves";
import { CoreService } from "../core.service";

@Injectable({
  providedIn: "root",
})
export class LeavesService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_LEAVES;
  constructor(
    private localDbService: LocalDbService,
    private coreService: CoreService
  ) { }

  insertData(leave: LeavesModel) {
    const values = [
      leave.EmpLeaveId,
      leave.EmployeeId,
      leave.EmployeeName,
      leave.Username,
      leave.FromDate,
      leave.ToDate,
      leave.LeaveTypeId,
      leave.LeaveType,
      leave.StatusId,
      leave.StatusName,
      leave.Comments,
      leave.Reason,
      leave.AuthorizedBy,
      leave.SupervisorName,
      leave.IsDeleted,
      leave.LeaveCount,
      leave.AppliedDate,
      leave.IsMyLeave
    ];
    return this.localDbService.db
      .executeSql(this.generateInsertSql(), values)
      .then(() => {
        console.log("Leave data added successfully");
      })
      .catch((err) => {
        console.error("Error while adding leave data", err);
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            ORDER BY AppliedDate desc
          `, [])
            .then((res: any) => {
              const items: LeavesModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }

  getDataByEmpLeaveId(payload) {
    const { EmpLeaveId, IsMyLeave } = payload;
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            WHERE EmpLeaveId = ?
            AND IsMyLeave = ?
            ORDER BY AppliedDate desc
          `, [EmpLeaveId, IsMyLeave])
            .then((res: any) => {
              const items: LeavesModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }

  getData(payload) {
    const { FromDate, ToDate, IsMyLeave } = payload;
    return new Promise(resolve => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            WHERE FromDate BETWEEN ? AND ? AND ToDate BETWEEN ? AND ?
            AND IsMyLeave = ?
            ORDER BY AppliedDate desc
          `, [FromDate, ToDate, FromDate, ToDate, IsMyLeave])
            .then((res: any) => {
              const items: LeavesModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }

  deleteAllData() {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl}`;
      tx.executeSql(sql, [], (localTx, res) => {
        console.log('deleting leave data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting leave data => ', err);
      });
    });
  }

  deleteDataByMyLeaveStatus(IsMyLeave) {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl} WHERE IsMyLeave = ?`;
      tx.executeSql(sql, [IsMyLeave], (localTx, res) => {
        console.log('deleting leave data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting leave data => ', err);
      });
    });
  }

  private generateInsertSql() {
    return `INSERT INTO ${this.tbl} (
      EmpLeaveId,
      EmployeeId,
      EmployeeName,
      Username,
      FromDate,
      ToDate,
      LeaveTypeId,
      LeaveType,
      StatusId,
      StatusName,
      Comments,
      Reason,
      AuthorizedBy,
      SupervisorName,
      IsDeleted,
      LeaveCount,
      AppliedDate,
      IsMyLeave
    ) VALUES(?, ?, ?, ?, DATETIME(?), DATETIME(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, DATETIME(?), ?)`;
  }

  insertBatch(leaves: LeavesModel[]) {
    const insertSql = this.generateInsertSql();
    const batch = [];
    const leavesCount = leaves.length;
    for (let index = 0; index < leavesCount; index++) {
      const leave = leaves[index];
      const values = [
        leave.EmpLeaveId,
        leave.EmployeeId,
        leave.EmployeeName,
        leave.Username,
        leave.FromDate,
        leave.ToDate,
        leave.LeaveTypeId,
        leave.LeaveType,
        leave.StatusId,
        leave.StatusName,
        leave.Comments,
        leave.Reason,
        leave.AuthorizedBy,
        leave.SupervisorName,
        leave.IsDeleted,
        leave.LeaveCount,
        leave.AppliedDate,
        leave.IsMyLeave
      ];
      batch.push([insertSql, values]);
    }
    this.localDbService.db
      .sqlBatch(batch)
      .then((res) => {
        console.log("Leaves records are added successfully.");
      })
      .catch((err) => {
        console.log(
          "Something went wrong while adding leaves records => ",
          err
        );
      });
  }

  storeLeavesForOffline(leaves, IsMyLeave) {
    this.deleteDataByMyLeaveStatus(IsMyLeave)
      .then(() => {
        console.log('Deleted previous records now start adding new.');
        this.insertBatch(leaves);
      })
      .catch((err) => {
        console.error("Error while deleting previous records => ", err);
      });
  }
}
