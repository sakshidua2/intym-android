import { Injectable } from "@angular/core";
import { LOCAL_DB_CONSTANTS } from "src/app/config/local-db.constants";
import { LocalDbService } from "../local-db.service";
import { CoreService } from "../core.service";
import { AttendanceModel } from "src/app/local-db-models/attendace";

@Injectable({
  providedIn: "root",
})
export class AttendanceService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_ATTENDANCE;
  constructor(
    private localDbService: LocalDbService,
    private coreService: CoreService
  ) {}

  insertData(attendance: AttendanceModel) {
    const values = [
      attendance.AttendanceDate,
      attendance.CorrectionRequest,
      attendance.Department,
      attendance.EmployeeId,
      attendance.EmployeeName,
      attendance.InTime,
      attendance.InTimeStatus,
      attendance.OutTimeStatus,
      attendance.OutTime,
      attendance.Pin,
      attendance.Shift,
      attendance.Status,
      attendance.SpentHours,
      attendance.IsMyAttendance,
    ];
    return this.localDbService.db
      .executeSql(this.generateInsertSql(), values)
      .then(() => {
        console.log("Attendance data added successfully");
      })
      .catch((err) => {
        console.error("Error while adding Attendance data", err);
      });
  }

  private generateInsertSql() {
    return `INSERT INTO ${this.tbl} (
      AttendanceDate,
      CorrectionRequest,
      Department,
      EmployeeId,
      EmployeeName,
      InTime,
      InTimeStatus,
      OutTimeStatus,
      OutTime,
      Pin,
      Shift,
      Status,
      SpentHours,
      IsMyAttendance
    ) VALUES(DATETIME(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)`;
  }

  storeAttendanceForOffline(attendance, isOnline = true) {
    this.deleteDataByAttendanceType(attendance[0].IsMyAttendance)
      .then(() => {
        console.log("Deleted previous records now start adding new.");
        this.insertBatch(attendance, isOnline);
      })
      .catch((err) => {
        console.error("Error while deleting previous records => ", err);
      });
  }

  deleteAllData() {
    return this.localDbService.db
      .transaction((tx) => {
        const sql = `DELETE FROM ${this.tbl}`;
        tx.executeSql(
          sql,
          [],
          (localTx, res) => {
            console.log("deleting attendance data successfully");
          },
          (localTx, err) => {
            console.error("Error while deleting attendance data => ", err);
          }
        );
      })
      .then(() => {
        console.log("Transaction deleting attendance data successfully");
      })
      .catch((err) => {
        console.error(
          "Error while transaction deleting attendance data => ",
          err
        );
      });
  }

  deleteDataByAttendanceType(IsMyAttendance) {
    return this.localDbService.db
      .transaction((tx) => {
        const sql = `DELETE FROM ${this.tbl} WHERE IsMyAttendance = ?`;
        tx.executeSql(
          sql,
          [IsMyAttendance],
          (localTx, res) => {
            console.log("deleting attendance data successfully");
          },
          (localTx, err) => {
            console.error("Error while deleting attendance data => ", err);
          }
        );
      })
      .then(() => {
        console.log("Transaction deleting attendance data successfully");
      })
      .catch((err) => {
        console.error(
          "Error while transaction deleting attendance data => ",
          err
        );
      });
  }

  insertBatch(attendance: AttendanceModel[], isOnline = true) {
    const insertSql = this.generateInsertSql();
    const batch = [];
    const attendanceCount = attendance.length;
    for (let index = 0; index < attendanceCount; index++) {
      const data = attendance[index];
      const values = [
        data.AttendanceDate,
        data.CorrectionRequest,
        data.Department,
        data.EmployeeId,
        data.EmployeeName,
        data.InTime,
        data.InTimeStatus ? data.InTimeStatus : "",
        data.OutTimeStatus ? data.OutTimeStatus : "",
        data.OutTime,
        data.Pin,
        data.Shift,
        data.Status,
        data.SpentHours ? data.SpentHours : "",
        data.IsMyAttendance,
      ];
      batch.push([insertSql, values]);
    }
    this.localDbService.db
      .sqlBatch(batch)
      .then((res) => {
        console.log("Attendance records are added successfully.");
      })
      .catch((err) => {
        console.log(
          "Something went wrong while adding attendance records => ",
          err
        );
      });
  }

  getData(payload) {
    const { FromDate, ToDate, IsMyAttendance } = payload;
    return new Promise((resolve) => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            WHERE AttendanceDate BETWEEN ? AND ?
            AND IsMyAttendance = ?
            ORDER BY AttendanceDate asc
          `,
              [FromDate, ToDate, IsMyAttendance]
            )
            .then((res: any) => {
              console.log("offline attrndance service res==>", res);

              const items: AttendanceModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }
}
