import { Injectable } from '@angular/core';
import { LocalDbService } from '../local-db.service';
import { LOCAL_DB_CONSTANTS } from 'src/app/config/local-db.constants';
import { UserProfileModel } from 'src/app/local-db-models/user-profile';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_USER_PROFILE;
  constructor(
    private localDbService: LocalDbService
  ) { }

  getFirstRecord() {
    return new Promise((resolve) => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db.executeSql(`SELECT * FROM ${this.tbl} LIMIT 1`, [])
            .then(res => {
              const totalRecords = res.rows.length;
              let user = null;
              if (totalRecords > 0) {
                user = res.rows.item(0);
              }
              resolve(user);
            }).catch(() => {
              resolve(null);
            });
        } else {
          resolve(null);
        }
      });
    });
  }

  insertData(profile: UserProfileModel) {
    const values = [
      profile.EmployeeId,
      profile.Pin,
      profile.UserName,
      profile.FirstName,
      profile.LastName,
      profile.MailId,
      profile.Role,
      profile.Shift,
      profile.Gender,
      profile.Location,
      profile.Department,
      profile.Designation,
      profile.EmployeeType,
      profile.JoinDate,
      profile.Address,
      profile.City,
      profile.Country,
      profile.Zipcode,
      profile.PhoneNo,
      profile.ProfilePic,
    ];
    return this.localDbService.db.executeSql(`INSERT INTO ${this.tbl} (
      EmployeeId,
      Pin,
      UserName,
      FirstName,
      LastName,
      MailId,
      Role,
      Shift,
      Gender,
      Location,
      Department,
      Designation,
      EmployeeType,
      JoinDate,
      Address,
      City,
      Country,
      Zipcode,
      PhoneNo,
      ProfilePic
    ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, values)
      .then(() => {
        console.log('Profile data added successfully');
      }).catch((err) => {
        console.error('Error while adding Profile data', err);
      });
  }

  updateData(profile: UserProfileModel, id: number) {
    const values = [
      profile.UserName,
      profile.FirstName,
      profile.LastName,
      profile.MailId,
      profile.Role,
      profile.Shift,
      profile.Gender,
      profile.Location,
      profile.Department,
      profile.Designation,
      profile.EmployeeType,
      profile.JoinDate,
      profile.Address,
      profile.City,
      profile.Country,
      profile.Zipcode,
      profile.PhoneNo,
      profile.ProfilePic,
      id
    ];
    return this.localDbService.db.executeSql(`UPDATE ${this.tbl} SET
      UserName = ?,
      FirstName = ?,
      LastName = ?,
      MailId = ?,
      Role = ?,
      Shift = ?,
      Gender = ?,
      Location = ?,
      Department = ?,
      Designation = ?,
      EmployeeType = ?,
      JoinDate = ?,
      Address = ?,
      City = ?,
      Country = ?,
      Zipcode = ?,
      PhoneNo = ?,
      ProfilePic = ?
      WHERE EmployeeId = ?`, values)
      .then((res) => {
        console.log('Profile data updated successfully');
      }).catch((err) => {
        console.error('Error while updating Profile data => ', err);
      });
  }

  deleteData() {
    this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${this.tbl}`;
      tx.executeSql(sql, [], (localTx, res) => {
        console.log('deleting profile data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting profile data => ', err);
      });
    }).then(() => {
      console.log('Transaction deleting profile data successfully');
    }).catch((err) => {
      console.error('Error while transaction deleting profile data => ', err);
    });
  }

  isProfileDataAvailable(id: number) {
    return new Promise((resolve) => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db.executeSql(`SELECT * FROM ${this.tbl} WHERE EmployeeId = ${id}`, [])
            .then(res => {
              const totalRecords = res.rows.length;
              if (totalRecords === 1) {
                resolve(true);
              } else {
                resolve(false);
              }
            }).catch(() => {
              resolve(false);
            });
        } else {
          resolve(false);
        }
      });
    });
  }

  storeProfileForOffline(payload: UserProfileModel) {
    const { EmployeeId } = payload;
    this.isProfileDataAvailable(EmployeeId)
      .then((res: boolean) => {
        if (res) {
          this.updateData(payload, EmployeeId);
        } else {
          this.insertData(payload);
        }
      });
  }

  updateProfilePageDate(profile: UserProfileModel) {
    const values = [
      profile.FirstName,
      profile.LastName,
      profile.MailId,
      profile.PhoneNo,
      profile.ProfilePic,
      profile.EmployeeId
    ];
    return this.localDbService.db.executeSql(`UPDATE ${this.tbl} SET
      FirstName = ?,
      LastName = ?,
      MailId = ?,
      PhoneNo = ?,
      ProfilePic = ?
      WHERE EmployeeId = ?`, values)
      .then((res) => {
        console.log('Profile data updated successfully');
      }).catch((err) => {
        console.error('Error while updating Profile data => ', err);
      });
  }
}
