import { Injectable } from '@angular/core';
import { LocalDbService } from '../local-db.service';
import { LOCAL_DB_CONSTANTS } from 'src/app/config/local-db.constants';
import { CachedRequestModel } from 'src/app/local-db-models/cached-request';
import { CoreService } from '../core.service';

@Injectable({
  providedIn: 'root'
})
export class CachedRequestService {
  constructor(
    private coreService: CoreService,
    private localDbService: LocalDbService
  ) { }

  getData() {
    return new Promise((resolve) => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db.executeSql(`SELECT * FROM ${LOCAL_DB_CONSTANTS.TBL_CACHED_REQUESTS}`, [])
            .then(res => {
              const items: CachedRequestModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            }).catch(() => {
              resolve([]);
            });
        } else {
          resolve([]);
        }
      });
    });
  }

  insertData(
    url: string,
    type: string,
    data: string,
    headers: string,
    dependentLocalRecordId: number = 0,
    dependentPayload: string = null
  ) {
    this.coreService.displayToast('Your data is stored locally because you seem to be offline.', 'warning');
    const values = [url, type, data, headers, this.coreService.getMomentObject().unix(), dependentLocalRecordId, dependentPayload];
    return this.localDbService.db.executeSql(`INSERT INTO ${LOCAL_DB_CONSTANTS.TBL_CACHED_REQUESTS} (url, type, data, headers, time, dependentLocalRecordId, dependentPayload) VALUES(?, ?, ?, ?, ?, ?, ?)`, values)
      .then(() => {
        console.log('cached request added successfully');
      }).catch(() => {
        console.error('Error while adding cached request data');
      });
  }

  deleteData() {
    this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${LOCAL_DB_CONSTANTS.TBL_CACHED_REQUESTS}`;
      tx.executeSql(sql, [], (localTx, res) => {
        console.log('res.rowsAffected => ', res.rowsAffected);
        console.log('deleting cached requests data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting cached requests data => ', err);
      });
    }).then(() => {
      console.log('Transaction deleting cached requests data successfully');
    }).catch((err) => {
      console.error('Error while transaction deleting cached requests data => ', err);
    });
  }

  deleteDataById(id) {
    return this.localDbService.db.transaction((tx) => {
      const sql = `DELETE FROM ${LOCAL_DB_CONSTANTS.TBL_CACHED_REQUESTS} WHERE id = ?`;
      tx.executeSql(sql, [id], (localTx, res) => {
        console.log('res.rowsAffected => ', res.rowsAffected);
        console.log('deleting cached requests data successfully');
      }, (localTx, err) => {
        console.error('Error while deleting cached requests data => ', err);
      });
    });
  }
}
