import { Injectable } from "@angular/core";
import { LocalDbService } from "../local-db.service";
import { LOCAL_DB_CONSTANTS } from "src/app/config/local-db.constants";
import { HolidaysModel } from "src/app/local-db-models/holidays_list";

@Injectable({
  providedIn: "root",
})
export class HolidaysListService {
  private tbl = LOCAL_DB_CONSTANTS.TBL_HOLIDAYS_LIST;

  constructor(private localDbService: LocalDbService) {}

  storeHolidaysForOffline(holidays) {
    this.deleteAllData()
      .then(() => {
        console.log("Deleted previous records now start adding new.");
        this.insertBatch(holidays);
      })
      .catch((err) => {
        console.error("Error while deleting previous records => ", err);
      });
  }

  deleteAllData() {
    return this.localDbService.db
      .transaction((tx) => {
        const sql = `DELETE FROM ${this.tbl}`;
        tx.executeSql(
          sql,
          [],
          (localTx, res) => {
            console.log("deleting holidays data successfully");
          },
          (localTx, err) => {
            console.error("Error while deleting attendance data => ", err);
          }
        );
      })
      .then(() => {
        console.log("Transaction deleting holidays data successfully");
      })
      .catch((err) => {
        console.error(
          "Error while transaction deleting attendance data => ",
          err
        );
      });
  }

  insertBatch(holidaysList: HolidaysModel[]) {
    const insertSql = this.generateInsertSql();
    const batch = [];
    const attendanceCount = holidaysList.length;
    for (let index = 0; index < attendanceCount; index++) {
      const data = holidaysList[index];
      const values = [
        data.DayValue,
        data.HolidayDate,
        data.HolidayDesc,
        data.HolidayId,
        data.LocationName,
        data.Optional,
      ];
      batch.push([insertSql, values]);
    }
    this.localDbService.db
      .sqlBatch(batch)
      .then((res) => {
        console.log("holidays records are added successfully.");
      })
      .catch((err) => {
        console.log(
          "Something went wrong while adding holidays records => ",
          err
        );
      });
  }

  private generateInsertSql() {
    return `INSERT INTO ${this.tbl} (
      DayValue,
      HolidayDate,
      HolidayDesc,
      HolidayId,
      LocationName,
      Optional
    ) VALUES(?, ?, ?, ?, ?, ?)`;
  }

  getData() {
    return new Promise((resolve) => {
      this.localDbService.dbState().subscribe((ready) => {
        if (ready) {
          this.localDbService.db
            .executeSql(
              `
            SELECT * FROM ${this.tbl}
            ORDER BY HolidayDate asc
          `,
              []
            )
            .then((res: any) => {
              // console.log("offline holidays service res==>", res);

              const items: HolidaysModel[] = [];
              if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                  items.push(res.rows.item(i));
                }
              }
              resolve(items);
            })
            .catch((err) => {
              console.error("Error while fetching data =>", err);
              resolve([]);
            });
        }
      });
    });
  }
}
