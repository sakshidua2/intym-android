import { Injectable } from "@angular/core";
import { SQLiteObject, SQLite } from "@ionic-native/sqlite/ngx";
import { BehaviorSubject } from "rxjs";
import { Platform } from "@ionic/angular";
import { LOCAL_DB_CONSTANTS } from "../config/local-db.constants";

@Injectable({
  providedIn: "root",
})
export class LocalDbService {
  private storage: SQLiteObject;
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor(private platform: Platform, private sqlite: SQLite) {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        this.sqlite
          .create({
            name: LOCAL_DB_CONSTANTS.DB_NAME,
            location: "default",
          })
          .then((db: SQLiteObject) => {
            this.createTable(db);
            this.storage = db;
            this.isDbReady.next(true);
          })
          .catch((err: any) => {
            console.log(err);
          });
      } else {
        console.info("Cordova not available");
      }
    });
  }

  createTable(db) {
    const createTblQry = [
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_CACHED_REQUESTS} (id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT, type TEXT, data TEXT, headers TEXT, time NUMBER, dependentLocalRecordId NUMBER DEFAULT 0, dependentPayload TEXT DEFAULT NULL)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_USER_PROFILE} (EmployeeId INTEGER PRIMARY KEY, Pin TEXT, UserName TEXT, FirstName TEXT, LastName TEXT, MailId TEXT, Role TEXT, Shift TEXT, Gender TEXT, Location TEXT, Department TEXT, Designation TEXT, EmployeeType TEXT, JoinDate TEXT, Address TEXT, City TEXT, Country TEXT, Zipcode TEXT, PhoneNo TEXT, ProfilePic TEXT)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_LEAVES} (Id INTEGER PRIMARY KEY AUTOINCREMENT, EmployeeId TEXT, EmpLeaveId INTEGER, EmployeeName TEXT, Username TEXT, FromDate TEXT, ToDate TEXT, LeaveTypeId NUMBER, LeaveType TEXT, StatusId NUMBER, StatusName TEXT, Comments TEXT, Reason TEXT, AuthorizedBy NUMBER, SupervisorName TEXT, IsDeleted BOOLEAN, LeaveCount TEXT, AppliedDate TEXT, IsMyLeave BOOLEAN DEFAULT TRUE)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_LEAVES_BALANCE} (id INTEGER PRIMARY KEY AUTOINCREMENT, LeaveTypeId INTEGER, LeaveName STRING, Balance DOUBLE)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_ATTENDANCE} (id INTEGER PRIMARY KEY AUTOINCREMENT, AttendanceDate TEXT, CorrectionRequest TEXT, Department TEXT, EmployeeId NUMBER, EmployeeName TEXT, InTime TEXT, InTimeStatus TEXT ,OutTimeStatus TEXT , OutTime TEXT, Pin TEXT, Shift TEXT, Status TEXT, SpentHours TEXT, IsMyAttendance NUMBER DEFAULT 0)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_ATTENDANCE_LOG} (id INTEGER PRIMARY KEY AUTOINCREMENT, EmpAttnTransactionId INTEGER, EmployeeId INTEGER, EmployeeName TEXT, TimeStamp TEXT, AttendanceStatus INTEGER, DeviceSN TEXT, ImageURL TEXT, Latitude NUMBER, Longitude NUMBER, CustomerName TEXT, CustomerAddress TEXT, ApprovalStatus INTEGER, IsMyAttendanceLog BOOLEAN DEFAULT TRUE)`,
      `CREATE TABLE IF NOT EXISTS ${LOCAL_DB_CONSTANTS.TBL_HOLIDAYS_LIST} (Id INTEGER PRIMARY KEY AUTOINCREMENT, DayValue TEXT, HolidayDate TEXT, HolidayDesc TEXT, HolidayId INTEGER, LocationName TEXT, Optional BOOLEAN DEFAULT FALSE)`,
    ];
    const executeSqls = createTblQry.map((qry) => {
      return db.executeSql(qry, []);
    });
    const promise = Promise.all(executeSqls);
    promise
      .then(() => {
        console.log("Tables created successfully!");
      })
      .catch((err) => {
        console.error("Error while creating tables => ", err);
      });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }

  get db() {
    return this.storage;
  }
}
