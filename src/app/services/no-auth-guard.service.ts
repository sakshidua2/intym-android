import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { APP_CONSTANTS } from '../config/app.constants';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuardService implements CanActivate {
  private isLoggedIn = false;
  constructor(
    private router: Router
  ) { }

  canActivate(): boolean {
    this.setIsLoggedIn = localStorage.getItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY) === 'true';

    if (this.getIsLoggedIn) {
      this.router.navigate(['/dashboard'], { replaceUrl: true });
    }
    return !this.getIsLoggedIn;
  }

  get getIsLoggedIn() {
    return this.isLoggedIn;
  }

  set setIsLoggedIn(flag) {
    this.isLoggedIn = flag;
  }
}
