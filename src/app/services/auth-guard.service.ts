import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { APP_CONSTANTS } from "../config/app.constants";
import { CoreService } from "./core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { SafeUrl } from "@angular/platform-browser";
import { Platform } from "@ionic/angular";
import { UserProfileService } from "./db/user-profile.service";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";

@Injectable({
  providedIn: "root",
})
export class AuthGuardService implements CanActivate {
  private isLoggedIn = false;
  private menuTitle = "InTym";
  private menuInfo = "InTym";
  private profilePic: SafeUrl = "assets/icon/favicon.png";
  constructor(
    private router: Router,
    private coreService: CoreService,
    private platform: Platform,
    private userProfileService: UserProfileService,
    private splashScreen: SplashScreen
  ) {}

  canActivate(): boolean {
    this.setIsLoggedIn =
      localStorage.getItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY) === "true";
    this.setMenuTitleFn();

    if (!this.getIsLoggedIn) {
      this.router.navigate(["/login"], { replaceUrl: true });
    }
    this.splashScreen.hide();
    return this.getIsLoggedIn;
  }

  get getIsLoggedIn() {
    return this.isLoggedIn;
  }

  set setIsLoggedIn(flag) {
    this.isLoggedIn = flag;
  }

  get getMenuTitle() {
    return this.menuTitle;
  }

  get getMenuInfo() {
    return this.menuInfo;
  }

  get getProfilePic() {
    return this.profilePic;
  }

  set setMenuTitle(name) {
    this.menuTitle = name;
  }

  set setMenuInfo(name) {
    this.menuInfo = name;
  }

  set setProfilePic(data) {
    if (typeof data === "string") {
      this.profilePic = this.coreService.sanitizeSecurityTrustUrl(data);
    } else {
      this.profilePic = data;
    }
  }

  private setMenuTitleFn() {
    if (this.isLoggedIn) {
      const loggedInUserDetailsStr = localStorage.getItem(
        APP_CONSTANTS.USER_DETAILS_KEY
      );
      if (loggedInUserDetailsStr) {
        const loggedInUserDetails = JSON.parse(loggedInUserDetailsStr);
        this.menuTitle = loggedInUserDetails.FirstName
          ? loggedInUserDetails.FirstName
          : "";
        this.menuTitle += loggedInUserDetails.LastName
          ? " " + loggedInUserDetails.LastName
          : "";
        this.menuInfo = loggedInUserDetails.Pin;
        this.setProfilePic = loggedInUserDetails.ProfilePic;
        this.coreService.notifyOther({
          option: NOTIFY_OBSERVABLE.PROFILE_CHANGE,
          value: true,
        });
      } else {
        this.menuTitle = "InTym";
        this.menuInfo = "InTym";
        this.profilePic = "assets/icon/favicon.png";
      }
    } else {
      this.menuTitle = "InTym";
      this.menuInfo = "InTym";
      this.profilePic = "assets/icon/favicon.png";
    }
  }

  handleLogoutProcess(showMessage: boolean = false) {
    localStorage.removeItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
    localStorage.removeItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY);
    localStorage.removeItem(APP_CONSTANTS.AUTH_DATA_KEY);
    localStorage.removeItem(APP_CONSTANTS.USER_DETAILS_KEY);
    this.setIsLoggedIn = false;
    this.router.navigate(["/login"], { replaceUrl: true });
    if (showMessage) {
      this.coreService.displayToast("Sign Out Success!", "success");
    }
    if (this.platform.is("cordova")) {
      this.userProfileService.deleteData();
    }
  }
}
