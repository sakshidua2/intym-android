import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError, EMPTY } from "rxjs";
import { map, catchError, finalize, tap } from "rxjs/operators";
import { APP_CONSTANTS } from "../config/app.constants";
import { CoreService } from "./core.service";
import { NetworkService, ConnectionStatus } from "./network.service";
import { CachedRequestService } from "./db/cached-request.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { LoadingController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class HttpInterceptorService implements HttpInterceptor {
  private _pendingRequests = 0;
  private isHttpErrorOccured = 0;
  isLoading: any;
  constructor(
    private coreService: CoreService,
    private networkService: NetworkService,
    private cachedRequestService: CachedRequestService,
    private loadingController: LoadingController
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = localStorage.getItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
    if (token && !req.url.includes('api.tomtom.com')) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    }
    if (!req.headers.has("Content-Type")) {
      req = req.clone({
        setHeaders: {
          "Content-Type": "application/json",
        },
      });
    }
    req = req.clone({
      headers: req.headers.set("Accept", "application/json"),
    });

    // START Checking for offline storage for api requests
    if (
      this.networkService.getCurrentNetworkStatus() ===
      ConnectionStatus.Offline &&
      req.headers.has(APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_OFFLINE) &&
      req.headers.get(APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_OFFLINE) === "yes"
    ) {
      const headerDependentLocalRecordId = req.headers.has(
        APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_REC_ID
      )
        ? parseInt(
          req.headers.get(
            APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_REC_ID
          )
        )
        : 0;
      const headerDependentPayload = req.headers.has(
        APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_PAYLOAD
      )
        ? req.headers.get(
          APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_PAYLOAD
        )
        : null;
      req.headers.delete(
        APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_REC_ID
      );
      req.headers.delete(
        APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_PAYLOAD
      );
      const { body, method, urlWithParams } = req;
      const headersKeys = req.headers.keys();
      const headersKeysLength = headersKeys.length;
      const headers = {};
      for (let i = 0; i < headersKeysLength; i++) {
        const key = headersKeys[i];
        headers[key] = req.headers.get(key);
      }
      const headersStr = JSON.stringify(headers);
      const bodyStr = JSON.stringify(body);
      this.cachedRequestService.insertData(
        urlWithParams,
        method,
        bodyStr,
        headersStr,
        headerDependentLocalRecordId,
        headerDependentPayload
      );
      return EMPTY;
    }
    // END Checking for offline storage for api requests

    req.headers.delete(APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_REC_ID);
    req.headers.delete(
      APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_DEPENDENT_PAYLOAD
    );

    this._pendingRequests++;
    let StopLoader = false;
    let isHttpErrorOccured = false;

    if (
      req.headers.has(APP_CONSTANTS.HTTP_LOADER) &&
      req.headers.get(APP_CONSTANTS.HTTP_LOADER) === "true"
    ) {
      StopLoader = true;
    }
    if ((req.body && req.body.StopLoader) || StopLoader) {
      this._pendingRequests--;
    }
    if (req.body && (req.body.StopLoader || !req.body.StopLoader)) {
      delete req.body.StopLoader;
    }
    if (this._pendingRequests === 1) {
      this.showLoader();
    }
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) { }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        // this.coreService.hideLoader();
        if (error.status === 401) {
          if (error.error.success === false) { } else {
            if (!this.isHttpErrorOccured) {
              this.coreService.notifyOther({
                option: NOTIFY_OBSERVABLE.LOGOUT_ACTION,
                value: { action: "token-expired", showMessage: false },
              });
            }

            this.isHttpErrorOccured++;
          }
        }
        return throwError(error);
      }),
      finalize(() => {
        this._pendingRequests--;
        if (this._pendingRequests <= 0) {
          this._pendingRequests = 0;
          this.isHttpErrorOccured = 0;
          this.hideLoader();
        }
        this.hideLoader();
      })
    );
  }
  showLoader(loadingId: string = 'ifOfLoading', loadingMessage: string = 'Loading...') {
    this.isLoading = true;
    this.loadingController.create({
      id: loadingId
    }).then((res) => {
      res.present().then(() => {
        if (!this.isLoading) {
          res.dismiss();
        }
      });
      res.onDidDismiss().then((dis) => {
        this.isLoading = false;
      });
    });
  }

  hideLoader(loadingId: string = 'ifOfLoading') {
    if (this.isLoading) {
      this.isLoading = false;
      this.loadingController.getTop().then(a => {
        if (a)
          a.dismiss(loadingId).then(() => console.log('loading dismissed'));
      });
    }
  }
}
