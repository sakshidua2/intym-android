import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { APP_CONSTANTS } from "../config/app.constants";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) { }

  get apiEndpoint() {
    return APP_CONSTANTS.API_URL;
  }

  getCacheRequestOfflineHeader(cacheRequestOffline) {
    return {
      headers: {
        [APP_CONSTANTS.HEADER_KEY_CACHE_REQUEST_OFFLINE]: cacheRequestOffline,
      },
    };
  }
  login(data) {
    return this.http.post(`${this.apiEndpoint}/Login`, data);
  }

  getUserDetails(options?: HttpHeaders) {
    return this.http.get(`${this.apiEndpoint}/GetUserDetails`, { ...options });
  }

  getLeaves(data, dataType, cacheRequestOffline = "no") {
    const options = this.getCacheRequestOfflineHeader(cacheRequestOffline);
    return this.http.post(
      `${this.apiEndpoint}/GetAllUserLeaveRequests/${dataType}`,
      data,
      options
    );
  }

  getMyLeaveBalance() {
    return this.http.get(`${this.apiEndpoint}/GetLeaveBalance`);
  }
  changePassword(data) {
    return this.http.post(`${this.apiEndpoint}/ChangePassword`, data);
  }

  getHolidayList(headers?: any) {
    return this.http.get(`${this.apiEndpoint}/GetHolidayList`, headers);
  }

  forgotPassword(data) {
    return this.http.post(`${this.apiEndpoint}/ForgotPassword`, data);
  }
  getDashboardData() {
    return this.http.get(`${this.apiEndpoint}/GetDashboardData`);
  }

  getStaticContent(uri) {
    return this.http.get(`${this.apiEndpoint}/${uri}`);
  }

  getStaticPageContent() {
    return this.http.get(`${this.apiEndpoint}/GetStaticPageContent`);
  }

  getPendingUserLeaveRequests(data: any) {
    return this.http.post(
      `${this.apiEndpoint}/GetPendingUserLeaveRequests`,
      data
    );
  }

  updateUserDetails(data: any, cacheRequestOffline = "no") {
    const options = this.getCacheRequestOfflineHeader(cacheRequestOffline);
    return this.http.post(
      `${this.apiEndpoint}/UpdateUserDetails`,
      data,
      options
    );
  }

  getLeaveTypes() {
    return this.http.get(APP_CONSTANTS.API_URL + "/GetLeaveTypes");
  }

  getLeaveDayTypes() {
    return this.http.get(APP_CONSTANTS.API_URL + "/GetLeaveDayTypes");
  }

  applyLeave(data: any, cacheRequestOffline = "no") {
    const options = this.getCacheRequestOfflineHeader(cacheRequestOffline);
    return this.http.post(APP_CONSTANTS.API_URL + "/ApplyLeave", data, options);
  }

  updateLeave(data: any, cacheRequestOffline = "no", optionalHeaders = null) {
    const options = this.getCacheRequestOfflineHeader(cacheRequestOffline);
    if (optionalHeaders) {
      options.headers = { ...options.headers, ...optionalHeaders };
    }
    return this.http.post(`${this.apiEndpoint}/UpdateLeave`, data, options);
  }

  getMyAttendance(data: any) {
    return this.http.post(`${this.apiEndpoint}/GetAttendanceSummary`, data);
  }

  getAttendanceGraph(data: any) {
    return this.http.post(`${this.apiEndpoint}/AttendanceSummaryGraph`, data);
  }

  getLeaveStatusCounter(data: any) {
    return this.http.post(`${this.apiEndpoint}/LeaveStatusCounter`, data);
  }

  getAttendanceCorrectionCounter(data: any) {
    return this.http.post(
      `${this.apiEndpoint}/AttendanceCorrectionStatusCounter`,
      data
    );
  }
  getWorkHoursGraph(data) {
    return this.http.post(
      `${this.apiEndpoint}/MyAttendanceWorkHoursGraph`,
      data
    );
  }

  getQRDetails() {
    return this.http.get(`${this.apiEndpoint}/GetQRDetails`);
  }

  getUpcomingLeaves(data: any) {
    return this.http.post(`${this.apiEndpoint}/UpcomingLeaves`, data);
  }

  getAttendanceStatusList() {
    return this.http.get(`${this.apiEndpoint}/GetAttendanceStatusList`);
  }

  getAttendanceLog(data, dataType) {
    return this.http.post(`${this.apiEndpoint}/GetAttendanceLog/${dataType}`, data);
  }

  applyAttendanceCorrection(data) {
    return this.http.post(
      `${this.apiEndpoint}/ApplyAttendanceCorrection`,
      data
    );
  }

  getRegularizationTypes() {
    return this.http.get(`${this.apiEndpoint}/GetRegularizationTypes`);
  }

  updateAttendanceCorre(data) {
    return this.http.post(
      `${this.apiEndpoint}/UpdateAttendanceCorrection`,
      data
    );
  }

  logUserAttendance(data) {
    return this.http.post(`${this.apiEndpoint}/LogUserAttendance`, data);
  }

  getRequistionStatusList() {
    return this.http.get(`${this.apiEndpoint}/GetRequistionStatusList`);
  }

  updateAttendanceStatus(data) {
    return this.http.post(`${this.apiEndpoint}/UpdateAttendance`, data);
  }

  getNotifications(params: { take: string; skip: string }, headers?: any) {
    return this.http.get(`${this.apiEndpoint}/notification`, {
      params,
      headers,
    });
    // return this.http.get(`${this.apiEndpoint}/notification?skip=${params.skip};take=${params.take}`);
  }

  markAsRead(notificationIdList: string[]) {
    return this.http.post(`${this.apiEndpoint}/notification/markread`, notificationIdList);
  }

  togglePushNotification(data: string) {
    return this.http.get(
      `${this.apiEndpoint}/notification/enablePushNotification`,
      { params: { enablePushNotification: data } }
    );
  }

  getUnreadNotificationCount() {
    return this.http.get(`${this.apiEndpoint}/notification/countunread`);
  }

  setNotificationToken(data: { fcmToken: string }) {
    return this.http.post(`${this.apiEndpoint}/notification/addfcmtoken`, {}, { params: data });
  }

  getPushNotificationStatus() {
    return this.http.get(
      `${this.apiEndpoint}/notification/getPushNotificationStatus`
    );
  }

  getHomeCustomerVisitCount() {
    return this.http.get(`${this.apiEndpoint}/HomeCustomerVisitCounter/`);
  }

  getHomeAttendanceDetails() {
    return this.http.get(`${this.apiEndpoint}/HomeAttendanceDetails`);
  }

  getHomeInMeetingDetails() {
    return this.http.get(`${this.apiEndpoint}/HomeInMeetingDetails`);
  }

  getHomeDoneMeetingDetails() {
    return this.http.get(`${this.apiEndpoint}/HomeDoneMeetingDetails`);
  }

  getCustomerVisitCalendar(data, hierarchyLevel) {
    return this.http.post(
      `${this.apiEndpoint}/CustomerVisitCalendar/${hierarchyLevel}`,
      data
    );
  }

  getAttendanceStatusCalendar(data, hierarchyLevel) {
    return this.http.post(
      `${this.apiEndpoint}/AttendanceStatusCalendar/${hierarchyLevel}`,
      data
    );
  }

  getLeaveCalendar(data) {
    return this.http.post(
      `${this.apiEndpoint}/GetLeaveCalendar`,
      data
    );
  }

  editHierarchyLevel(data) {
    return this.http.post(`${this.apiEndpoint}/EditHierarchyLevel`, data);
  }

  getEmployeeList() {
    return this.http.get(`${this.apiEndpoint}/GetEmployeeList`);
  }

  getProjects() {
    return this.http.get(`${this.apiEndpoint}/GetProjects`);
  }

  getRegions() {
    return this.http.get(`${this.apiEndpoint}/GetRegions`);
  }

  getLeaveDataById(EmpLeaveId) {
    return this.http.get(`${this.apiEndpoint}/GetLeaveDataById/${EmpLeaveId}`);
  }

  createReport(data) {
    return this.http.post(`${this.apiEndpoint}/CreateReport`, data);
  }

  downloadReportFromUrl(url) {
    return this.http.get(url);
  }

  getCustomerVisitStatisticsCounter(data, dataType) {
    return this.http.post(`${this.apiEndpoint}/CustomerVisitStatisticsCounter/${dataType}`, data);
  }

  getProductList() {
    return this.http.get(`${this.apiEndpoint}/products`);
  }

  getSalesVisitCalendar(data, dataType) {
    return this.http.post(`${this.apiEndpoint}/counter/${dataType}`, data);
  }

  getSalesVisits(data, dataType) {
    return this.http.post(`${this.apiEndpoint}/statistics/${dataType}`, data);
  }

  deleteSalesVisit(id) {
    return this.http.post(`${this.apiEndpoint}/DeleteSalesVisit/${id}`, {});
  }

  createSalesVisit(data) {
    return this.http.post(`${this.apiEndpoint}/CreateSalesVisit/`, data);
  }

  updateSalesVisit(data) {
    return this.http.post(`${this.apiEndpoint}/UpdateSalesVisit/`, data);
  }

  getAddressByLatLong(latlong) {
    return this.http.get(`https://api.tomtom.com/search/2/reverseGeocode/${latlong}.json?key=${APP_CONSTANTS.TOM_TOM_MAP_KEY}`);
  }

  getSalesvisitCustomerDataByContect(ContactNumber: string) {
    return this.http.post(`${this.apiEndpoint}/SalesVisitCustomerDataByContact/${ContactNumber}`, {});
  }
}
