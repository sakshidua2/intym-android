import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { CachedRequestService } from './db/cached-request.service';
import { CachedRequestModel } from '../local-db-models/cached-request';
import { HttpClient } from '@angular/common/http';
import { CoreService } from './core.service';

@Injectable({
  providedIn: 'root'
})
export class OfflineManagerService {

  constructor(
    private http: HttpClient,
    private coreService: CoreService,
    private cachedRequestService: CachedRequestService
  ) { }

  checkForEvents() {
    this.cachedRequestService.getData()
      .then((requests: CachedRequestModel[]) => {
        if (requests && requests.length > 0) {
          this.sendRequestsViaWebWorker(requests);
          // this.sendRequests(requests).subscribe(() => {
          //   this.coreService.displayToast('Cached data synced successfully!', 'success');
          //   this.cachedRequestService.deleteData();
          // });
        } else {
          console.log('No cached requests to trigger and sync');
        }
      });
  }

  // sendRequests(requests: CachedRequestModel[]) {
  //   const responses = [];
  //   for (const request of requests) {
  //     let payload = request.data;
  //     const headersStr = request.headers;
  //     const headers = {};
  //     try {
  //       payload = JSON.parse(payload);
  //       const headersObj = JSON.parse(headersStr);
  //       const headersKeys = Object.keys(headersObj);
  //       const headersKeysLength = headersKeys.length;
  //       for (let i = 0; i < headersKeysLength; i++) {
  //         const key = headersKeys[i];
  //         headers[key] = headersObj[key];
  //       }
  //     } catch (error) { }
  //     const res = this.http.request(request.type, request.url, { body: payload, headers });
  //     responses.push(res);
  //   }
  //   return forkJoin(responses);
  // }

  sendRequestsViaWebWorker(requests) {
    const totalRequests = requests.length;
    let currentReq = 0;
    let workerInstance = new Worker('../../assets/web-workers/cache-request-manager.js');
    workerInstance.onmessage = (event) => {
      if (event.data && event.data.status) {
        // success api call
        const { response } = event.data;
        console.log('response => ', response);
      } else {
        // fail api call
      }
      this.cachedRequestService.deleteDataById(requests[currentReq].id);
      currentReq++;
      if (currentReq < totalRequests) {
        workerInstance.postMessage(this.prepareRequestObj(requests[currentReq]));
      } else {
        currentReq = 0;
        workerInstance.terminate();
        workerInstance = undefined;
        this.coreService.displayToast('Cached data synced successfully!', 'success');
      }
    };
    workerInstance.postMessage(this.prepareRequestObj(requests[currentReq]));
  }

  prepareRequestObj(rawReq) {
    return { ...rawReq, headers: JSON.parse(rawReq.headers) };
  }
}
