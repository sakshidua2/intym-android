import { Injectable } from "@angular/core";
import { ToastController, LoadingController } from "@ionic/angular";
import { Subject } from "rxjs/internal/Subject";
import { DomSanitizer } from "@angular/platform-browser";
import { FormControl } from "@angular/forms";
import * as moment from "moment";
import CryptoJS from "crypto-js";

import { APP_CONSTANTS } from "../config/app.constants";

@Injectable({
  providedIn: "root",
})
export class CoreService {
  private notify = new Subject<{ option: string; value: any }>();
  notifyObservable$ = this.notify.asObservable();

  correctionItem: any;
  private loading: any;
  notificationCount: string | number = 0;

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private domSanitizer: DomSanitizer
  ) { }

  async displayToast(message: string, type: string, options: any = {}) {
    const opts = {
      message,
      duration: 10000,
      position: "bottom",
      cssClass: `app-toast-danger`,
      buttons: [
        {
          text: "X",
          role: "cancel",
        },
      ],
    };
    const toast = await this.toastCtrl.create({ ...opts, ...options });
    toast.present();
    return toast;
  }

  async showLoader() {
    this.loading = true;
    return await this.loadingCtrl.create({}).then((a) => {
      a.present().then(() => {
        if (!this.loading) {
          a.dismiss();
        }
      });
    });
  }

  async hideLoader() {
    if (this.loading) {
      this.loading = false;
      await this.loadingCtrl.dismiss().catch((err) => {
        console.log(err);
      });
    }
  }

  notifyOther(data: { option: string; value: any }) {
    if (data) {
      this.notify.next(data);
    }
  }

  sanitizeSecurityTrustUrl(rawUrl) {
    return this.domSanitizer.bypassSecurityTrustUrl(rawUrl);
  }

  onlySpacesNotAllowed(control: FormControl) {
    if (control.value && control.value.trim() === "") {
      return {
        onlySpaces: true,
      };
    }
    return null;
  }

  addressPatternValidation(control: FormControl) {
    const pattern = /^[a-zA-Z0-9 ,./-]*$/;
    console.log(control, pattern.test(control.value));
    if (!pattern.test(control.value)) {
      return {
        addressPattern: true,
      };
    }
    return null;
  }

  getMomentNow(format?: string): any {
    if (format) {
      return moment().format(format);
    }
    return moment();
  }

  getMomentByDate(date: string) {
    if (date.search(/[zZ]/g) < 0) {
      date += "Z";
    }
    return moment(date);
  }

  getMomentObject(date?: string) {
    if (date) {
      return moment(date);
    }
    return moment();
  }

  getMomentFormatDate(format?: string, date?: string) {
    if (date) {
      return moment(date).format(format);
    }
    return moment().format(format);
  }

  getMonthNumberFromMonthName(monthName?: string) {
    return moment().month(monthName).format('M');
  }

  getMomentByTime(time: string, format?: string) {
    const timeArr = time.split(":");
    const hours = timeArr[0] ? parseInt(timeArr[0]) : 0;
    const mins = timeArr[1] ? parseInt(timeArr[1]) : 0;
    if (format) {
      const date = moment().hour(hours).minute(mins);
      return date.format(format);
    }
    return moment().hour(hours).minute(mins);
  }

  getTimeByLocalZone(time: string, format?: string) {
    const timeArr = time.split(":");
    const hours = timeArr[0] ? parseInt(timeArr[0]) : 0;
    const mins = timeArr[1] ? parseInt(timeArr[1]) : 0;
    const date = moment().hour(hours).minute(mins);
    const zonediff = date.utcOffset();
    date.add({
      hours: Math.floor(zonediff / 60),
      minutes: Math.floor(zonediff % 60),
    });
    if (format) {
      return date.format(format);
    }
    return date;
  }

  getTimeDifference(inTime: any, outTime: any) {
    const InTime: any = this.getMomentByTime(inTime);
    const OutTime: any = this.getMomentByTime(outTime);
    const SpentHours: any =
      InTime.unix() > OutTime.unix()
        ? InTime.diff(OutTime)
        : OutTime.diff(InTime);
    const d: any = moment.duration(SpentHours);
    return {
      hours: d.asHours(),
      minutes: d.asMinutes() % 60,
      label: `${Math.floor(d.asHours())}h ${Math.floor(d.asMinutes() % 60)}min`,
    };
  }

  getMomentDatesRange(fromDate, toDate) {
    const dates = [];
    const date = moment(fromDate);
    while (date.unix() <= toDate.unix()) {
      dates.push(date.format(APP_CONSTANTS.DATE_FORMATER));
      date.add(1, "day");
    }
    return dates;
  }

  calculateMomentDateDiff(from, to) {
    const InTime = this.getMomentByDate(from);
    const OutTime = this.getMomentByDate(to);
    let diff;
    const SpentHours: any =
      InTime.unix() > OutTime.unix()
        ? InTime.diff(OutTime)
        : OutTime.diff(InTime);
    const d: any = moment.duration(SpentHours);
    return {
      hours: Math.floor(d.asHours()),
      minutes: d.asMinutes() % 60,
      label: `${Math.floor(d.asHours())}h ${Math.floor(d.asMinutes() % 60)}min`,
    };
  }

  convertMomentToLocalTimezone(dt: moment.Moment) {
    const date = moment(dt);
    const zonediff = date.utcOffset();
    date.add({
      hours: Math.floor(zonediff / 60),
      minutes: Math.floor(zonediff % 60),
    });
    return date;
  }

  decryptData(data, key) {
    let bytes = CryptoJS.AES.decrypt(data, key);
    let keys = bytes.toString(CryptoJS.enc.Utf8);
    return JSON.parse(keys);
  }

  encryptData(data: any, key: string) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), key).toString();
  }
}
