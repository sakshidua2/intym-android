import { Component, OnInit, ViewChild } from "@angular/core";
import { ApiService } from "../services/api.service";
import { IonInfiniteScroll, ModalController } from "@ionic/angular";
import { APP_CONSTANTS } from "../config/app.constants";
import {
  NOTIFICATION_MESSAGE,
  NOTIFICATION_EMPLOYEE_REDIRECT,
  NOTIFICATION_SUPERVISOR_REDIRECT,
  NOTIFICATION_LABLES,
} from "../config/notifications.constants";
import { CoreService } from "../services/core.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { Router } from "@angular/router";
import { ConnectionStatus, NetworkService } from "../services/network.service";
import { LABEL_CONSTANTS } from "../config/labels.constants";
import { SettingsPage } from "../settings/settings.page";

@Component({
  selector: "app-notification-list",
  templateUrl: "./notification-list.page.html",
  styleUrls: ["./notification-list.page.scss"],
})
export class NotificationListPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  notificationsList: any[];
  currentLength: number;
  limit: number = 20;
  isSupervisor: boolean;
  isNotificationListReadyToRender: boolean;
  isToggled: boolean;
  constructor(
    private apiService: ApiService,
    private coreService: CoreService,
    private navCntrl: Router,
    private networkService: NetworkService,
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.isNotificationListReadyToRender = false;
    const userDetails = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY)
    );
    this.isSupervisor = false;
    if (userDetails && ["Admin", "Supervisor"].includes(userDetails.Role)) {
      this.isSupervisor = true;
    }
    this.currentLength = 0;
    this.limit = 20;
    this.notificationsList = [];
    this.getPushNotificationStatus();
    this.getNotifications();
  }

  getNotifications(e?: any, isRefresh?: boolean) {
    if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const headers = { [APP_CONSTANTS.HTTP_LOADER]: e ? "true" : "false" };
      this.apiService
        .getNotifications(
          { skip: `${this.currentLength}`, take: `${this.limit}` },
          headers
        )
        .subscribe(
          (res: any) => {
            this.isNotificationListReadyToRender = true;
            if (res && res.length) {
              res.forEach((element) => {
                console.log("element", element);
                if (!element.Read) {
                  const date = this.coreService
                    .getMomentObject(element.ActionDate)
                    .format(APP_CONSTANTS.DATE_FORMAT_NOTIFICATION);
                  const difference: any = this.coreService.getMomentByDate(
                    element.CreatedDate
                  );
                  let Message = "";
                  let redirectionLink = "";
                  if (element.Status === "Created") {
                    Message = element.NotificationType === "LeaveRequest"
                      ? NOTIFICATION_LABLES.LEAVE_REQUEST_RECEIVED :
                      element.NotificationType === "Report" ? NOTIFICATION_LABLES.REPORT_REQUEST_RECEIVED
                        : NOTIFICATION_LABLES.REQUEST_RECEIVED;
                    Message = Message.replace(
                      "##type##",
                      `<strong>${NOTIFICATION_MESSAGE[element.NotificationType]
                      }</strong>`
                    );
                    Message = Message.replace(
                      "##user##",
                      `<strong>${element.FirstName + " " + element.LastName
                      }</strong>`
                    );
                    Message = Message.replace(
                      "##cust##",
                      element.CustomerName !== null ?
                        `from <strong>
                      ${element.CustomerName}
                      </strong>` : ``
                    )
                    redirectionLink =
                      NOTIFICATION_SUPERVISOR_REDIRECT[element.NotificationType];
                  } else {
                    Message =
                      element.NotificationType === "CustomerVisit"
                        ? NOTIFICATION_LABLES.VISIT_REQUEST_PROCESSED
                        : NOTIFICATION_LABLES.OTHER_REQUEST_PROCESSED;
                    Message = Message.replace(
                      "##type##",
                      `<strong>${NOTIFICATION_MESSAGE[element.NotificationType]
                      }</strong>`
                    );
                    Message = Message.replace(
                      "##status##",
                      `<strong>${element.Status}</strong>`
                    );

                    Message = Message.replace(
                      "##user##",
                      `<strong>${element.NotificationType !== "CustomerVisit"
                        ? element.FirstName + " " + element.LastName
                        : element.CustomerName
                      }</strong>`
                    );

                    Message = Message.replace(
                      "##cust##",
                      `<strong>
                      ${element.CustomerName}
                      </strong>`
                    )
                    redirectionLink =
                      NOTIFICATION_EMPLOYEE_REDIRECT[element.NotificationType];
                  }
                  Message = Message.replace(
                    "##date##",
                    `<strong>${date}</strong>`
                  );

                  if (element.NotificationType === "CustomerVisit") {
                    Message = Message.replace(
                      "##address##",
                      `<strong>${element.CustomerAddress}</strong>`
                    );
                  }
                  element.Message = Message;
                  element.Redirection = redirectionLink;
                  let Time = difference.fromNow();
                  if (Time === "a day ago") {
                    Time = "1 day ago";
                  }
                  element.Time = Time;
                }
              });
              this.notificationsList = [...this.notificationsList, ...res];
              if (e && e.target) {
                e.target.complete();
              }
            } else if (e && e.target) {
              if (isRefresh) {
                e.target.complete();
                return;
              }
              e.target.disabled = true;
            }
          },
          (err) => {
            this.isNotificationListReadyToRender = true;
            if (e && e.target && e.target.complete) {
              e.target.complete();
            }
          }
        );
    } else {
      this.isNotificationListReadyToRender = true;
      if (e && e.target && e.target.complete) {
        e.target.complete();
      }
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }

  loadData(e) {
    this.currentLength = this.notificationsList.length;
    this.getNotifications(e);
  }

  markReadAll() {
    const notificationsIdList = [];
    this.notificationsList.map(notification => {
      notificationsIdList.push(notification.Id);
      return true;
    });
    this.apiService.markAsRead(notificationsIdList).subscribe((res) => {
      this.currentLength = 0;
      this.limit = 20;
      this.notificationsList = [];
      this.getNotifications();
      this.coreService.notifyOther({
        option: NOTIFY_OBSERVABLE.GET_NOTIFICATION_COUNT,
        value: true,
      });
    });
  }

  hangleNavigation(info: any) {
    if (info.Read) {
      this.handleRedirection(info, false);
    } else {
      this.apiService.markAsRead([info.Id]).subscribe((res) => {
        this.handleRedirection(info, true);
      });
    }
  }

  handleRedirection(info: any, count: boolean) {
    if (count) {
      this.coreService.notifyOther({
        option: NOTIFY_OBSERVABLE.GET_NOTIFICATION_COUNT,
        value: true,
      });
    }
    if (info.ActionType) {
      window.open(info.ActionType);
    } else {
      this.navCntrl.navigate([info.Redirection], {
        state: {
          data: {
            id: info.RequestId,
            date: info.ActionDate,
            Status: info.Status.toLowerCase(),
          },
        },
      });
    }
  }

  enableDisableNotifications(event: any) {
    this.isToggled = event;
    this.apiService
      .togglePushNotification(this.isToggled.toString())
      .subscribe((res: any) => {
        localStorage.setItem(
          APP_CONSTANTS.PUSH_NOTIFICATIONS_STATUS,
          this.isToggled.toString()
        );
      });
  }

  getPushNotificationStatus() {
    this.apiService.getPushNotificationStatus().subscribe((res: any) => {
      if (res) {
        this.isToggled = res.Status === "True";
      }
    });
  }
  doRefresh(e) {
    this.limit = 20;
    this.currentLength = 0;
    this.isNotificationListReadyToRender = false;
    this.notificationsList = [];
    this.getNotifications(e, true);
  }

  async showSettings() {
    const modalRef = await this.modalController.create({
      component: SettingsPage,
      cssClass: "custom-filter-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }
}
