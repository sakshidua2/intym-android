import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CoreService } from "../services/core.service";
import { ApiService } from "../services/api.service";
import { APP_CONSTANTS } from "../config/app.constants";
import { ModalController } from "@ionic/angular";
import { ForgotPasswordPage } from "../forgot-password/forgot-password.page";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public showPassword = false;
  isRemember: boolean;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private coreService: CoreService,
    private apiService: ApiService,
    private mdCtrl: ModalController,
    private networkService: NetworkService
  ) {}

  ngOnInit() {
    this.isRemember = true;
    this.loginForm = this.fb.group({
      username: [
        "",
        [Validators.required, this.coreService.onlySpacesNotAllowed],
      ],
      password: [
        "",
        [Validators.required, this.coreService.onlySpacesNotAllowed],
      ],
    });
  }

  ionViewDidEnter() {
    const rememberdData = localStorage.getItem(
      APP_CONSTANTS.REMEMBER_ME_DETAILS
    );

    if (rememberdData) {
      const data = JSON.parse(
        this.coreService.decryptData(
          rememberdData,
          APP_CONSTANTS.REMEMBER_ME_DETAILS_KEY
        )
      );

      this.isRemember = data.isRemember;
      this.loginForm.patchValue(data.userDetails);
    }
  }
  get usernameCtrl() {
    return this.loginForm.get("username");
  }
  get passwordCtrl() {
    return this.loginForm.get("password");
  }

  handleSubmit() {
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;
      const payload = {
        Username: username,
        Password: password,
      };
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Online
      ) {
        this.apiService.login(payload).subscribe(
          (res: any) => {
            const { token } = res;
            localStorage.setItem(APP_CONSTANTS.AUTH_TOKEN_KEY, token);
            localStorage.setItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY, "true");
            localStorage.setItem(APP_CONSTANTS.AUTH_LOGGED_IN, "true");
            localStorage.setItem(
              APP_CONSTANTS.AUTH_DATA_KEY,
              JSON.stringify(res)
            );
            if (this.isRemember) {
              const rememberDetails = {
                userDetails: this.loginForm.value,
                isRemember: this.isRemember,
              };

              const encrypted = this.coreService.encryptData(
                JSON.stringify(rememberDetails),
                APP_CONSTANTS.REMEMBER_ME_DETAILS_KEY
              );

              localStorage.setItem(
                APP_CONSTANTS.REMEMBER_ME_DETAILS,
                encrypted
              );
            } else {
              const rememberdData = localStorage.getItem(
                APP_CONSTANTS.REMEMBER_ME_DETAILS
              );

              if (rememberdData) {
                localStorage.removeItem(APP_CONSTANTS.REMEMBER_ME_DETAILS);
              }
            }

            this.coreService.notifyOther({
              option: NOTIFY_OBSERVABLE.GET_NOTIFICATION_COUNT,
              value: true,
            });
            this.coreService.notifyOther({
              option: NOTIFY_OBSERVABLE.ADD_FCM_TOKEN,
              value: true,
            });
            this.apiService.getUserDetails().subscribe(
              (resUser: any) => {
                // resUser["AllowRemoteLogin"] = true;  // just for testing manual attendance in/out
                localStorage.setItem(
                  APP_CONSTANTS.USER_DETAILS_KEY,
                  JSON.stringify(resUser)
                );
                const date = this.coreService.getMomentNow(
                  APP_CONSTANTS.DATE_FORMAT_YYYY_MM_DD
                );
                const request = {
                  FromDate: date,
                  ToDate: date,
                };
                this.apiService
                  .getAttendanceLog(request, APP_CONSTANTS.DATA_TYPE_MY)
                  .subscribe(
                    (res: any) => {
                      localStorage.setItem(
                        APP_CONSTANTS.ATTENDANCE_LOG,
                        JSON.stringify(res)
                      );

                      this.router.navigate(["/dashboard"], {
                        replaceUrl: true,
                      });

                      this.coreService.notifyOther({
                        option: NOTIFY_OBSERVABLE.LOCALSTORAGE_UPDATED,
                        value: null,
                      });
                    },
                    (err) => {
                      const message =
                        err &&
                        err.error &&
                        (err.error["Status"] ||
                          err.error["ErrorMessage"] ||
                          err.error["Message"])
                          ? err.error["Status"] ||
                            err.error["ErrorMessage"] ||
                            err.error["Message"]
                          : "Something went wrong! please try later.";
                      this.coreService.displayToast(message, "danger");
                    }
                  );
              },
              (err) => {
                const message =
                  err &&
                  err.error &&
                  (err.error["Status"] ||
                    err.error["ErrorMessage"] ||
                    err.error["Message"])
                    ? err.error["Status"] ||
                      err.error["ErrorMessage"] ||
                      err.error["Message"]
                    : "Something went wrong! please try later.";
                this.coreService.displayToast(message, "danger");
              }
            );
          },
          (err) => {
            const message =
              err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
                ? err.error["Status"] ||
                  err.error["ErrorMessage"] ||
                  err.error["Message"]
                : "Wrong username or password";

            this.coreService.displayToast(message, "danger");
          }
        );
      } else {
        const message = "You are offline! Please check internet connection.";
        this.coreService.displayToast(message, "warning");
      }
    }
  }

  async handleForgotPassword() {
    const modalRef = await this.mdCtrl.create({
      component: ForgotPasswordPage,
      cssClass: "forgot-modal",
      mode: "md",
    });
    modalRef.present();
    modalRef.onDidDismiss().then((res: any) => {
      console.log(res);
    });
  }

  handelIsRemember() {
    this.isRemember = !this.isRemember;
  }

  ionViewDidLeave() {
    this.loginForm.reset();
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }
}
