import { Component, ViewChild, OnInit } from "@angular/core";

import {
  Platform,
  MenuController,
  AlertController,
  IonRouterOutlet,
  IonContent,
  NavController,
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";
import { AuthGuardService } from "./services/auth-guard.service";
import { CoreService } from "./services/core.service";
import { APP_CONSTANTS } from "./config/app.constants";
import { NetworkService, ConnectionStatus } from "./services/network.service";
import { OfflineManagerService } from "./services/offline-manager.service";
import { NOTIFY_OBSERVABLE } from "./config/notify-observable.constants";
import { SafeUrl } from "@angular/platform-browser";
import { ApiService } from "./services/api.service";
import { AppVersion } from "@ionic-native/app-version/ngx";
import { FirebaseX } from "@ionic-native/firebase-x/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  @ViewChild(IonContent) ionContent: IonContent;
  public menuTitle: string;
  public menuInfo: string;
  public profilePic: SafeUrl;
  public defaultProfileImg = "assets/images/user-profile.svg";
  public showProfilePicPlaceHolder = true;
  public IsSalesVisitVisible = false;
  public allowRemoteLogin = false;
  public appCurrentVersion = "";
  notificationCount: string | number;
  isLoggedIn: boolean;
  constructor(
    private platform: Platform,
    private navCntrl: NavController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private menuCtrl: MenuController,
    private alertCtrl: AlertController,
    public authGuardService: AuthGuardService,
    private coreService: CoreService,
    private networkService: NetworkService,
    private offlineManagerService: OfflineManagerService,
    private apiService: ApiService,
    private appVersion: AppVersion,
    private firebaseX: FirebaseX
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.getMenuDetails();
    this.initNotifyObservables();
    this.getAppVersion();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.statusBar.styleLightContent();
      } else if (this.platform.is("ios")) {
        this.statusBar.styleDefault();
        this.statusBar.overlaysWebView(false);
      } else {
        this.statusBar.styleDefault();
      }
      // this.splashScreen.hide();
      this.subscribeBackButton();
      this.checkCachedRequests();
      this.checkAllowremort();
      this.isLoggedIn =
        localStorage.getItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY) === "true";
      if (this.isLoggedIn) {
        this.getNotificationsCount();
      }
      if (this.platform.is("cordova")) {
        this.initializeFirebase();
      }
    });
  }

  checkRegularization() {
    const regularization = JSON.parse(
      localStorage.getItem(APP_CONSTANTS.REGULARIZATION_DETAILS)
    );
    if (
      (!regularization ||
        (regularization.data &&
          regularization.date < this.coreService.getMomentObject().unix())) &&
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      this.apiService.getRegularizationTypes().subscribe((res: any) => {
        if (res) {
          const AttnRegularizeTypes = {
            data: res,
            date: this.coreService.getMomentObject().unix(),
          };
          localStorage.setItem(
            APP_CONSTANTS.REGULARIZATION_DETAILS,
            JSON.stringify(AttnRegularizeTypes)
          );
        }
      });
    }
  }

  checkAllowremort() {
    const userDetailsStr = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
    if (userDetailsStr) {
      try {
        const userDetails = JSON.parse(userDetailsStr);
        if (!["Admin", "Supervisor"].includes(userDetails.Role)) {
          this.allowRemoteLogin = userDetails.AllowRemoteLogin;
        } else if (["Admin", "Supervisor"].includes(userDetails.Role)) {
          this.allowRemoteLogin = true;
        }
        console.log(userDetails, userDetails.IsSalesVisitVisible, userDetails.IsMapViewEnabled);
        if (userDetails.IsSalesVisitVisible) {
          console.log("true");
          this.IsSalesVisitVisible = true;
        } else {
          console.log("false");
          this.IsSalesVisitVisible = false;
        }
      } catch (error) {
        this.allowRemoteLogin = false;
      }
    } else {
      this.allowRemoteLogin = false;
    }
  }
  handleNavigation(page) {
    let appcomponent = this;
    this.closeMenu();
    if (page === "attendance-screen") {
      const attendanceLog = localStorage.getItem(APP_CONSTANTS.ATTENDANCE_LOG);
      const token = localStorage.getItem(APP_CONSTANTS.AUTH_TOKEN_KEY);
      const userDetail = localStorage.getItem(APP_CONSTANTS.USER_DETAILS_KEY);
      const auth_logged_in = localStorage.getItem(APP_CONSTANTS.AUTH_LOGGED_IN);
      const option = {
        ATTENDANCE_LOG: attendanceLog,
        TOKEN: token,
        USER_DETAIL: userDetail,
        AUTH_LOGGED_IN: auth_logged_in,
      };
      window["plugins"].newActivityPlugin.new_activity(
        option,
        function (res) {
          if (res === "qr") {
            appcomponent.navCntrl.navigateForward("/attendance-screen");
          } else if (res === "dashboard") {
            appcomponent.navCntrl.navigateForward("");
          }
        },
        function (err) {}
      );
    } else if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline) {
      const message = "You are offline! Please check internet connection.";
      this.coreService.displayToast(message, "warning");
    } else {
      this.router.navigate([`/${page}`]);
    }
  }

  handleStaticPage(page, pageId) {
    this.closeMenu();
    if (pageId !== "eula") {
      this.router.navigate([`/${page}`], { queryParams: { pageId } });
    } else {
      this.router.navigate([`/${page}/${pageId}`]);
    }
  }

  subscribeBackButton() {
    this.platform.backButton.subscribeWithPriority(0, async () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url === "/login") {
        navigator["app"].exitApp();
      } else {
        const exitAlert = await this.alertCtrl.create({
          header: "Exit app!",
          message: "Are you sure you want to exit?",
          buttons: [
            {
              text: "no",
              role: "cancel",
              cssClass: "secondary",
            },
            {
              text: "Yes",
              handler: () => {
                navigator["app"].exitApp();
              },
            },
          ],
        });
        await exitAlert.present();
      }
    });
  }

  checkCachedRequests() {
    this.networkService
      .onNetworkChange()
      .subscribe((status: ConnectionStatus) => {
        if (status === ConnectionStatus.Online) {
          this.offlineManagerService.checkForEvents();
          this.getNotificationsCount();
        }
      });
  }

  closeMenu() {
    this.menuCtrl.close();
    this.ionContent.scrollToTop();
  }

  getMenuDetails() {
    this.menuTitle = this.authGuardService.getMenuTitle;
    this.menuInfo = this.authGuardService.getMenuInfo;
    this.profilePic = this.authGuardService.getProfilePic;
  }

  handleLoad() {
    this.showProfilePicPlaceHolder = false;
  }

  initNotifyObservables() {
    this.coreService.notifyObservable$.subscribe((res: any) => {
      if (res.option === NOTIFY_OBSERVABLE.PROFILE_CHANGE && res.value) {
        this.getMenuDetails();
      } else if (res.option === NOTIFY_OBSERVABLE.LOGOUT_ACTION) {
        if (res.value.action === "token-expired") {
          const rememberdData = localStorage.getItem(
            APP_CONSTANTS.REMEMBER_ME_DETAILS
          );
          if (rememberdData && this.authGuardService.getIsLoggedIn) {
            const data = JSON.parse(
              this.coreService.decryptData(
                rememberdData,
                APP_CONSTANTS.REMEMBER_ME_DETAILS_KEY
              )
            );

            const payload = {
              Username: data.userDetails["username"],
              Password: data.userDetails["password"],
            };
            this.apiService.login(payload).subscribe((res: any) => {
              const { token } = res;
              localStorage.setItem(APP_CONSTANTS.AUTH_TOKEN_KEY, token);
              localStorage.setItem(APP_CONSTANTS.AUTH_LOGIN_FLAG_KEY, "true");
              localStorage.setItem(
                APP_CONSTANTS.AUTH_DATA_KEY,
                JSON.stringify(res)
              );
              this.router.navigate(["/dashboard"], { replaceUrl: true });
            });
          } else {
            const showMessage = res.showMessage ? res.showMessage : false;
            this.authGuardService.handleLogoutProcess(showMessage);
          }
        } else {
          const showMessage = res.showMessage ? res.showMessage : false;
          this.authGuardService.handleLogoutProcess(showMessage);
        }
      } else if (res.option === NOTIFY_OBSERVABLE.LOCALSTORAGE_UPDATED) {
        this.checkAllowremort();
        this.checkRegularization();
      } else if (res.option === NOTIFY_OBSERVABLE.GET_NOTIFICATION_COUNT) {
        this.getNotificationsCount();
      } else if (res.option === NOTIFY_OBSERVABLE.ADD_FCM_TOKEN) {
        const token = localStorage.getItem(APP_CONSTANTS.FIREBASE_TOKEN);
        this.addFirebaseToken(token);
      }
    });
  }

  getAppVersion() {
    this.appVersion
      .getVersionNumber()
      .then((version) => {
        this.appCurrentVersion = `v ${version}`;
      })
      .catch((err) => {
        this.appCurrentVersion = `v ${APP_CONSTANTS.VERSION}`;
      });
  }

  initializeFirebase() {
    this.firebaseX
      .getToken()
      .then((token) => {
        localStorage.setItem(APP_CONSTANTS.FIREBASE_TOKEN, token);
        if (this.isLoggedIn) {
          this.addFirebaseToken(token);
        }
      }) // save the token server-side and use it to push notifications to this device
      .catch((error) => console.error("Error getting token", error));
    this.firebaseX.getCurrentUser().then((token) => {});
    this.firebaseX.onMessageReceived().subscribe(
      (message) => {
        if (message && message.messageType === "notification") {
          if (message.tap) {
            this.router.navigate(["/notification-list"]);
          }
        }
      },
      (err) => {
        this.coreService.displayToast(err, "error");
      }
    );
    this.firebaseX.onTokenRefresh().subscribe((token: string) => {
      localStorage.setItem(APP_CONSTANTS.FIREBASE_TOKEN, token);
      if (this.isLoggedIn) {
        this.addFirebaseToken(token);
      }
    });
  }

  addFirebaseToken(token: string) {
    this.apiService
      .setNotificationToken({ fcmToken: token })
      .subscribe((res: any) => {});
  }

  getNotificationsCount() {
    this.apiService.getUnreadNotificationCount().subscribe(
      (res: any) => {
        if (res) {
          this.coreService.notificationCount = this.notificationCount =
            res.Message > 99 ? `99+` : res.Message;
          this.coreService.notifyOther({
            option: NOTIFY_OBSERVABLE.NOTIFICATION_COUNT,
            value: this.notificationCount,
          });
        }
      },
      () => {
        this.coreService.notificationCount = this.notificationCount = 0;
      }
    );
  }
}
