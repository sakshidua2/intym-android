import { Component, OnInit } from '@angular/core';
import { CoreService } from "../services/core.service";
import { ApiService } from "../services/api.service";
import { NOTIFY_OBSERVABLE } from "../config/notify-observable.constants";
import { APP_CONSTANTS } from "../config/app.constants";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ModalController } from "@ionic/angular";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { LABEL_CONSTANTS } from "../config/labels.constants";
import { Router } from "@angular/router";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  public settingsForm: FormGroup;
  isSubmitted: boolean;
  notificationCount: string | number = 0;
  constructor(
    private fb: FormBuilder,
    public modalCtrl: ModalController,
    private networkService: NetworkService,
    private coreService: CoreService,
    private apiService: ApiService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.isSubmitted = false;
    const userDetail = JSON.parse(localStorage.getItem(
      APP_CONSTANTS.USER_DETAILS_KEY
    ));
    const levelArr = userDetail.HierarchyLevel ? userDetail.HierarchyLevel.split(',') : ['L1'];
    this.settingsForm = this.fb.group(
      {
        level1: [levelArr.indexOf('L1') > -1 ? true : false],
        level2: [levelArr.indexOf('L2') > -1 ? true : false],
        level3: [levelArr.indexOf('L3') > -1 ? true : false],
      },
    );
  }

  ionViewDidEnter() {
    this.notificationCount = this.coreService.notificationCount;
    this.coreService.notifyObservable$.subscribe((res) => {
      if (res.option === NOTIFY_OBSERVABLE.NOTIFICATION_COUNT) {
        if (res.value) {
          this.notificationCount = res.value;
        }
      }
    });
  }

  showNotifications() {
    this.router.navigate(['/notification-list']);
  }

  get level1Ctrl() {
    return this.settingsForm.get('level1');
  }
  get level2Ctrl() {
    return this.settingsForm.get('level2');
  }
  get level3Ctrl() {
    return this.settingsForm.get('level3');
  }

  handleSubmit() {
    this.isSubmitted = true;
    if (this.settingsForm.valid) {
      if (
        this.networkService.getCurrentNetworkStatus() ===
        ConnectionStatus.Online
      ) {
        this.isSubmitted = false;
        const levels = [];
        if (this.level1Ctrl.value) {
          levels.push('L1');
        }
        if (this.level2Ctrl.value) {
          levels.push('L2');
        }
        if (this.level3Ctrl.value) {
          levels.push('L3');
        }
        this.apiService
          .editHierarchyLevel({ HierarchyLevel: levels.join(',') })
          .subscribe((res: any) => {
            const { token } = res;
            localStorage.setItem(APP_CONSTANTS.AUTH_TOKEN_KEY, token);
            localStorage.setItem(
              APP_CONSTANTS.AUTH_DATA_KEY,
              JSON.stringify(res)
            );
            this.apiService.getUserDetails().subscribe(
              (resUser: any) => {
                localStorage.setItem(
                  APP_CONSTANTS.USER_DETAILS_KEY,
                  JSON.stringify(resUser)
                );
                this.coreService.displayToast(
                  "Settings Saved",
                  "success"
                );
                this.dismiss(true);
              },
              (err) => {
                const message =
                  err && err.error
                    ? err.error["ErrorMessage"]
                    : "Something went wrong! please try later.";
                this.coreService.displayToast(message, "danger");
              });
          },
            (err) => {
              const message =
                err && err.error
                  ? err.error["ErrorMessage"]
                  : "Something went wrong! please try later.";
              this.coreService.displayToast(message, "danger");
            })
      } else {
        this.coreService.displayToast(
          LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
          'warning'
        );
      }
    }
  }
  dismiss(role?) {
    role
      ? this.modalCtrl.dismiss(undefined, "settings-success")
      : this.modalCtrl.dismiss();
  }
}

