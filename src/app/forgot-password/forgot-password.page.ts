import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CoreService } from "../services/core.service";
import { ModalController } from "@ionic/angular";
import { NetworkService, ConnectionStatus } from "../services/network.service";
import { LABEL_CONSTANTS } from "../config/labels.constants";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.page.html",
  styleUrls: ["./forgot-password.page.scss"],
})
export class ForgotPasswordPage implements OnInit {
  public forgotForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private coreService: CoreService,
    private modalCntrl: ModalController,
    private networkService: NetworkService
  ) { }

  ngOnInit() {
    this.forgotForm = this.fb.group({
      email: ["", [Validators.required, this.emailValidator]],
    });
  }

  get emailCtrl() {
    return this.forgotForm.get("email");
  }

  emailValidator(control) {
    // tslint:disable-next-line: max-line-length
    const reg = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9]{2,}(?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
    if (typeof control.value !== "undefined" && reg.test(control.value)) {
      return null;
    } else {
      return { invalidEmailAddress: true };
    }
  }

  handleSubmit() {
    if (
      this.forgotForm.valid &&
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Online
    ) {
      const { email } = this.forgotForm.value;
      const payload = {
        MailId: email,
      };

      this.apiService.forgotPassword(payload).subscribe(
        (res: any) => {
          this.coreService.displayToast(res.Status, "success");
          this.forgotForm.reset();
          this.modalCntrl.dismiss();
        },
        (err) => {
          const message =
            err &&
              err.error &&
              (err.error["Status"] ||
                err.error["ErrorMessage"] ||
                err.error["Message"])
              ? err.error["Status"] ||
              err.error["ErrorMessage"] ||
              err.error["Message"]
              : "Something went wrong! please try later.";
          this.coreService.displayToast(message, "danger");
        }
      );
    } else if (
      this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline
    ) {
      this.coreService.displayToast(
        LABEL_CONSTANTS.OFFLINE_CHECK_CONNECTION,
        "warning"
      );
    }
  }
}
