package com.sybica.falconwings;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.media.Image.Plane;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Trace;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.provider.Settings;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.honeywell.hbt.intym.R;
import com.sybica.falconwings.util.AsyncTaskCompleteListener;
import com.sybica.falconwings.util.Constant;
import com.sybica.falconwings.util.GPSTracker;
import com.sybica.falconwings.util.NetworkRequest;
import com.sybica.falconwings.util.SharedPrefrence;
import com.sybica.fw_lib.Detection;
import com.sybica.fw_lib.Recorgnize;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;



import org.json.JSONException;
import org.json.JSONObject;


public abstract class CameraActivity extends AppCompatActivity implements OnImageAvailableListener, Camera.PreviewCallback, AsyncTaskCompleteListener {

  public static final int PERMISSIONS_REQUEST = 1;
  protected int previewWidth = 0;
  protected int previewHeight = 0;
  private boolean debug = false;
  private Handler handler;
  private HandlerThread handlerThread;
  private boolean useCamera2API;
  private boolean isProcessingFrame = false;
  private byte[][] yuvBytes = new byte[3][];
  private int[] rgbBytes = null;
  private int yRowStride;
  private Runnable postInferenceCallback;
  private Runnable imageConverter;

  private static final String KEY_USE_FACING = "use_facing";
  private Integer useFacing = null;

  private String cameraId = null;
  boolean state;
  Detection face_detector;
  TextView allow_necessary_permision;
  protected Integer getCameraFacing() {
    return useFacing;
  }
  public final String[] permission=new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CAMERA};
  SharedPrefrence sf;
  NetworkRequest request;
  TextView checkin_toast;
  String currentAddress="";


  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(null);

    sf=new SharedPrefrence(this);
    face_detector = new Recorgnize();
    Intent intent = getIntent();
    useFacing = intent.getIntExtra(KEY_USE_FACING, CameraCharacteristics.LENS_FACING_FRONT);
    //useFacing = intent.getIntExtra(KEY_USE_FACING, CameraCharacteristics.LENS_FACING_BACK);

    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    setContentView(R.layout.activity_new);
    Toolbar toolbar = findViewById(R.id.toolbar);
    allow_necessary_permision = findViewById(R.id.allow_necessary_permision);
    checkin_toast = findViewById(R.id.checkin_toast);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if(checkPermission(permission)){
        setUp();
    }

  }

  public boolean checkPermission(String[] permissions) {

      boolean status = true;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

      ArrayList<String> requestpermiosn = new ArrayList<>();

      for (String s : permissions) {

        if (!hasPermission(s)) {
          requestpermiosn.add(s);
        }

      }
      if (requestpermiosn.size() > 0) {
        String[] stockArr = new String[requestpermiosn.size()];
        stockArr = requestpermiosn.toArray(stockArr);
          status=false;
        requestPermissions(stockArr, PERMISSIONS_REQUEST);
      } else {

        status=true;
      }
    } else {


      status=true;

    }

    return status;
  }

    private void gettingCurrentAddressFromLatLong() {
        String latitude = sf.getSharedPrefrenceString(Constant.LATITUDE);
        String longitude = sf.getSharedPrefrenceString(Constant.LONGITUDE);
        if(Constant.CURRENT_LOCATION_MODE=="google"){
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
                if(addresses!=null && addresses.size()>0) {
                    currentAddress = addresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(Constant.CURRENT_LOCATION_MODE=="tomtom"){
            HashMap<String,String> map = new HashMap<>();
            map.put(Constant.KEY,Constant.API_KEY);
            String mode = Constant.GET_LOCATION_MODE+"/"+latitude+","+longitude+".json";
            request.getRequest(mode,this,map,this,Constant.TOMTOM_URL);
        }

    }

  public void setUp() {
      allow_necessary_permision.setVisibility(View.GONE);
      checkin_toast.setVisibility(View.VISIBLE);
      request = new NetworkRequest();

      GPSTracker gps = new GPSTracker(CameraActivity.this);
      if(gps.canGetLocation()){

          double latitude = gps.getLatitude();
          double longitude = gps.getLongitude();
          SharedPrefrence sf = new SharedPrefrence(this);
          sf.setSharedPrefrenceString(Constant.LATITUDE, String.valueOf(latitude));
          sf.setSharedPrefrenceString(Constant.LONGITUDE, String.valueOf(longitude));
          gettingCurrentAddressFromLatLong();
      }else{
          gps.showSettingsAlert(sf,this);
      }

      String auth_loggedin = sf.getSharedPrefrenceString(Constant.AUTH_LOGGED_IN);
      if(auth_loggedin!=null && auth_loggedin.equals("false")){
          setFragment();
      }else {
//          HashMap<String,String> map = new HashMap<>();
//          request.getRequest(Constant.GET_USER,this,map,this,Constant.BASE_URL);
          JSONObject object = new JSONObject();
          try {
              object.put(Constant.EMPID, sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
              object.put(Constant.ORGANISATION,Constant.ORGANIZATION);
              object.put(Constant.DOWNLOAD_LAT,sf.getSharedPrefrenceString(Constant.LATITUDE));
              object.put(Constant.DOWNLOAD_Long,sf.getSharedPrefrenceString(Constant.LONGITUDE));
              object.put(Constant.DEVICE_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
              object.put(Constant.TIMEZONE, TimeZone.getDefault().getID());
              request.postRequest("",this,object,this,Constant.URL,true);
          }catch (JSONException ex){

          }

      }
  }


  private void onSwitchCamClick() {

    switchCamera();

  }

  public void switchCamera() {

    Intent intent = getIntent();

    if (useFacing == CameraCharacteristics.LENS_FACING_FRONT) {
      useFacing = CameraCharacteristics.LENS_FACING_BACK;
    } else {
      useFacing = CameraCharacteristics.LENS_FACING_FRONT;
    }

    intent.putExtra(KEY_USE_FACING, useFacing);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

    restartWith(intent);

  }

  private void restartWith(Intent intent) {
    finish();
    overridePendingTransition(0, 0);
    startActivity(intent);
    overridePendingTransition(0, 0);
  }

  protected int[] getRgbBytes() {
    imageConverter.run();
    return rgbBytes;
  }

  protected int getLuminanceStride() {
    return yRowStride;
  }

  protected byte[] getLuminance() {
    return yuvBytes[0];
  }

  /** Callback for android.hardware.Camera API */
  @Override
  public void onPreviewFrame(final byte[] bytes, final Camera camera) {
    if (isProcessingFrame) {
      return;
    }

    try {
      // Initialize the storage bitmaps once when the resolution is known.
      if (rgbBytes == null) {
        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        previewHeight = previewSize.height;
        previewWidth = previewSize.width;
        //rgbBytes = new int[previewWidth * previewHeight];
        //onPreviewSizeChosen(new Size(previewSize.width, previewSize.height), 90);
          rgbBytes = new int[previewWidth * previewHeight];
          int rotation = 90;
          if (useFacing == CameraCharacteristics.LENS_FACING_FRONT) {
              rotation = 270;
          }
          onPreviewSizeChosen(new Size(previewSize.width, previewSize.height), rotation);
      }
    } catch (final Exception e) {
      return;
    }

    isProcessingFrame = true;
    yuvBytes[0] = bytes;
    yRowStride = previewWidth;

    imageConverter = face_detector.intializeImageConverter(bytes,previewWidth,previewHeight,rgbBytes);

    postInferenceCallback =
        new Runnable() {
          @Override
          public void run() {
            camera.addCallbackBuffer(bytes);
            isProcessingFrame = false;
          }
        };
    processImage();
  }

  /** Callback for Camera2 API */
  @Override
  public void onImageAvailable(final ImageReader reader) {
    // We need wait until we have some size from onPreviewSizeChosen
    if (previewWidth == 0 || previewHeight == 0) {
      return;
    }
    if (rgbBytes == null) {
      rgbBytes = new int[previewWidth * previewHeight];
    }
    try {
      final Image image = reader.acquireLatestImage();

      if (image == null) {
        return;
      }

      if (isProcessingFrame) {
        image.close();
        return;
      }
      isProcessingFrame = true;
      Trace.beginSection("imageAvailable");
      final Plane[] planes = image.getPlanes();
      fillBytes(planes, yuvBytes);
      yRowStride = planes[0].getRowStride();
      final int uvRowStride = planes[1].getRowStride();
      final int uvPixelStride = planes[1].getPixelStride();

      imageConverter = face_detector.intializeImageConverter1(yuvBytes,previewWidth,previewHeight,rgbBytes,yRowStride,uvRowStride,uvPixelStride);
      postInferenceCallback =
          new Runnable() {
            @Override
            public void run() {
              image.close();
              isProcessingFrame = false;
            }
          };

      processImage();
    } catch (final Exception e) {
      Trace.endSection();
      return;
    }
    Trace.endSection();
  }

  @Override
  public synchronized void onStart() {
    super.onStart();
  }

  @Override
  public synchronized void onResume() {
    super.onResume();

    handlerThread = new HandlerThread("inference");
    handlerThread.start();
    handler = new Handler(handlerThread.getLooper());
  }

  @Override
  public synchronized void onPause() {

    handlerThread.quitSafely();
    try {
      handlerThread.join();
      handlerThread = null;
      handler = null;
    } catch (final InterruptedException e) {
    }

    super.onPause();
  }

  @Override
  public synchronized void onStop() {
    super.onStop();
  }

  @Override
  public synchronized void onDestroy() {
    super.onDestroy();
  }

  protected synchronized void runInBackground(final Runnable r) {
    if (handler != null) {
      handler.post(r);
    }
  }



  private static boolean allPermissionsGranted(final int[] grantResults) {
    for (int result : grantResults) {
      if (result != PackageManager.PERMISSION_GRANTED) {
        return false;
      }
    }
    return true;
  }

  @TargetApi(Build.VERSION_CODES.M)
  public boolean hasPermission(String permission) {
    return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
            checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
  }


  // Returns true if the device supports the required hardware level, or better.
  private boolean isHardwareLevelSupported(
      CameraCharacteristics characteristics, int requiredLevel) {
    int deviceLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
    if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
      return requiredLevel == deviceLevel;
    }
    // deviceLevel is not LEGACY, can use numerical sort
    return requiredLevel <= deviceLevel;
  }

   private String chooseCamera() {

        final CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        try {
            for (final String cameraId : manager.getCameraIdList()) {
                final CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                final StreamConfigurationMap map =
                        characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                if (map == null) {
                    continue;
                }
                final Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);

                if (useFacing != null &&
                        facing != null &&
                        !facing.equals(useFacing)
                ) {
                    continue;
                }

                useCamera2API = (facing == CameraCharacteristics.LENS_FACING_EXTERNAL)
                                || isHardwareLevelSupported(
                                characteristics, CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL);

                return cameraId;
            }
        } catch (CameraAccessException e) {

        }

        return null;
    }


 protected void setFragment() {

        this.cameraId = chooseCamera();

        Fragment fragment;
        if (useCamera2API) {
            CameraConnectionFragment camera2Fragment =
                    CameraConnectionFragment.newInstance(
                            new CameraConnectionFragment.ConnectionCallback() {
                                @Override
                                public void onPreviewSizeChosen(final Size size, final int rotation) {
                                    previewHeight = size.getHeight();
                                    previewWidth = size.getWidth();
                                    CameraActivity.this.onPreviewSizeChosen(size, rotation);
                                }
                            },
                            this,
                            getLayoutId(),
                            getDesiredPreviewFrameSize());

            camera2Fragment.setCamera(cameraId);
            fragment = camera2Fragment;

        } else {

          int facing = (useFacing == CameraCharacteristics.LENS_FACING_BACK) ?
                          Camera.CameraInfo.CAMERA_FACING_BACK :
                          Camera.CameraInfo.CAMERA_FACING_FRONT;
            LegacyCameraConnectionFragment frag = new LegacyCameraConnectionFragment(this,
                    getLayoutId(),
                    getDesiredPreviewFrameSize(), facing);
            fragment = frag;

        }

        getFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

  protected void fillBytes(final Plane[] planes, final byte[][] yuvBytes) {
    // Because of the variable row stride it's not possible to know in
    // advance the actual necessary dimensions of the yuv planes.
    for (int i = 0; i < planes.length; ++i) {
      final ByteBuffer buffer = planes[i].getBuffer();
      if (yuvBytes[i] == null) {
        yuvBytes[i] = new byte[buffer.capacity()];
      }
      buffer.get(yuvBytes[i]);
    }
  }

  public boolean isDebug() {
    return debug;
  }

  protected void readyForNextImage() {
    if (postInferenceCallback != null) {
      postInferenceCallback.run();
    }
  }

  protected int getScreenOrientation() {
    switch (getWindowManager().getDefaultDisplay().getRotation()) {
      case Surface.ROTATION_270:
        return 270;
      case Surface.ROTATION_180:
        return 180;
      case Surface.ROTATION_90:
        return 90;
      default:
        return 0;
    }
  }


  protected abstract void processImage();

  protected abstract void onPreviewSizeChosen(final Size size, final int rotation);

  protected abstract int getLayoutId();

  protected abstract Size getDesiredPreviewFrameSize();
}



