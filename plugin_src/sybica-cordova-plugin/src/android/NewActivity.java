package com.sybica.falconwings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetector;

import android.graphics.RectF;
import android.hardware.camera2.CameraCharacteristics;
import android.location.Address;
import android.location.Geocoder;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.androidnetworking.error.ANError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.honeywell.hbt.intym.R;
import com.sybica.falconwings.customview.OverlayView;
import com.sybica.falconwings.db.ActivityModal;
import com.sybica.falconwings.db.DatabaseHandler;
import com.sybica.falconwings.db.FaceData;
import com.sybica.falconwings.networkmodal.GetUserModal;
import com.sybica.falconwings.networkmodal.attendanceLog.AttendanceLogData;
import com.sybica.falconwings.networkmodal.currentlocation.CurrentLocation;
import com.sybica.falconwings.util.AsyncTaskCompleteListener;
import com.sybica.falconwings.util.Constant;
import com.sybica.falconwings.util.MyApplication;
import com.sybica.falconwings.util.Network;
import com.sybica.falconwings.util.NetworkRequest;
import com.sybica.falconwings.util.SharedPrefrence;
import com.sybica.fw_lib.ImageUtils;
import com.sybica.fw_lib.MultiBoxTracker;
import com.sybica.fw_lib.Recorgnize;
import com.sybica.fw_lib.SimilarityClassifier;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NewActivity extends CameraActivity implements ImageReader.OnImageAvailableListener, AsyncTaskCompleteListener {
    private static final int TF_OD_API_INPUT_SIZE = 112;
    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);
    private static final boolean SAVE_PREVIEW_BITMAP = false;
    OverlayView trackingOverlay;
    private Integer sensorOrientation;
    public static SimilarityClassifier detector;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;

    private boolean computingDetection = false;
    public boolean addPending = false;
    private long timestamp = 0;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;
    private MultiBoxTracker tracker;

    private FaceDetector faceDetector;

    private Bitmap portraitBmp = null;
    private Bitmap faceBmp = null;

    private ImageView fabEdit;
    private ImageView fab_home;
    DatabaseHandler db;
    TextView register;
    LinearLayout bottombutton;
    SharedPrefrence sf;
    TextView log_in;
    TextView log_out;
    TextView check_in;
    TextView check_out;
    AppCompatTextView tvtime1;
    AppCompatTextView tvtime2;
    Integer status=-1;
    LinearLayout botom_layout;
    LinearLayout checkin_layout;
    TextView checkin_toast;
    TextView submit;
    TextView cancel;
    EditText ed_name;
    EditText ed_address;
    Integer check_status;
    boolean recorgnition_flag=true;
    String encoded="";
    Integer punchStatus = 0;
    private String punchTime;
    TabLayout tabLayout;
    List<AttendanceLogData> myAttendance;
    boolean faceRecorgnize=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this);
        sf = new SharedPrefrence(this);
        request = new NetworkRequest();
        initView();
        disableButton();
        settingCurrentDayAndTime();

        if(checkPermission(permission)){
            getLocalUserData();
        }

        clickListener();

        faceDetector = face_detector.startFaceDetection();
    }



    @Override
    public void onBackPressed() {
        sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.BACK_RESULT_CODE);
        sf.setSharedPrefrenceString(Constant.EDIT_FR,"false");
        Intent intent = new Intent();
        setResult(Constant.BACK_RESULT_CODE, intent);
        finish();
        super.onBackPressed();
        
    }

    private void initView() {
        register = findViewById(R.id.register);
        fabEdit = findViewById(R.id.fab_edit);
        fab_home = findViewById(R.id.fab_home);
        bottombutton=findViewById(R.id.bottombutton);
        log_in=findViewById(R.id.log_in);
        log_out=findViewById(R.id.log_out);
        check_in=findViewById(R.id.check_in);
        check_out=findViewById(R.id.check_out);
        tvtime1=findViewById(R.id.time1);
        tvtime2 = findViewById(R.id.time2);
        checkin_layout=findViewById(R.id.checkin_layout);
        botom_layout=findViewById(R.id.botom_layout);
        checkin_toast=findViewById(R.id.checkin_toast);
        submit=findViewById(R.id.submit);
        cancel=findViewById(R.id.cancel);
        ed_name=findViewById(R.id.ed_name);
        ed_address = findViewById(R.id.ed_address);
        tabLayout=findViewById(R.id.tabLayout);
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.white));
        tabLayout.setSelectedTabIndicatorHeight(7);
        TextView manualtab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
        tabLayout.getTabAt(0).setCustomView(manualtab);
        TextView qrtab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
        tabLayout.getTabAt(1).setCustomView(qrtab);
    }

    private void settingCurrentDayAndTime() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.SUNDAY:
                tvtime1.setText("Today, Sunday");
                break;
            case Calendar.MONDAY:
                tvtime1.setText("Today, Monday");
                break;
            case Calendar.TUESDAY:
                tvtime1.setText("Today, Tuesday");
                break;
            case Calendar.WEDNESDAY:
                tvtime1.setText("Today, WEDNESDAY");
                break;
            case Calendar.THURSDAY:
                tvtime1.setText("Today, THURSDAY");
                break;
            case Calendar.FRIDAY:
                tvtime1.setText("Today, FRIDAY");
                break;
            case Calendar.SATURDAY:
                tvtime1.setText("Today, SATURDAY");
                break;
        }
        Date currentTime = Calendar.getInstance().getTime();
        String formatedtime = Constant.formatedDateTime(currentTime);
        tvtime2.setText(formatedtime);
    }

    public void getLocalUserData() {
        List<FaceData> allfacedata = db.getAllFaceData(sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
        if(allfacedata!=null && allfacedata.size()>0){
            if(sf.getSharedPrefrenceString(Constant.EDIT_FR).equals("true")){
                register.setText("Re-register");
                registerLayoutVisible();
            }else {
                getAttendanceLog();
                loginLayoutVisible();
            }
        }else{
            register.setText("Register");
            registerLayoutVisible();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST:

                if (grantResults.length > 0) {

                    for (int i = 0; i <= grantResults.length - 1; i++) {

                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            state = true;
                        } else {

                            state = false;
                            break;
                        }
                    }

                    if (state) {
                        setUp();
                        getLocalUserData();
                    } else {
                        allow_necessary_permision.setVisibility(View.VISIBLE);
                        checkin_toast.setVisibility(View.GONE);
                        allow_necessary_permision.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                checkPermission(permission);
                            }
                        });
                        //Toast.makeText(this,"Allow neccessary permission",Toast.LENGTH_SHORT).show();
                    }


                }
                break;
        }
    }
    
    private void setAttendanceLogStatus(){
            if(myAttendance!=null && myAttendance.size()>0) {
                punchTime = myAttendance.get(0).getTimeStamp();
                if (myAttendance.get(0).getAttendanceStatus() == Constant.LOGIN) {
                    punchStatus = 1;
                } else if (myAttendance.get(0).getAttendanceStatus() == Constant.LOGOUT) {
                    punchStatus = 2;
                } else if (myAttendance.get(0).getAttendanceStatus() == Constant.CHECKIN) {
                    sf.setSharedPrefrenceString(Constant.CUSTOMER_NAME,myAttendance.get(0).getCustomerName());
                    sf.setSharedPrefrenceString(Constant.CUSTOMER_ADDRESS,myAttendance.get(0).getCustomerAddress());
                    punchStatus = 3;
                } else if (myAttendance.get(0).getAttendanceStatus() == Constant.CHECKOUT) {
                    sf.setSharedPrefrenceString(Constant.CUSTOMER_NAME,"");
                    sf.setSharedPrefrenceString(Constant.CUSTOMER_ADDRESS,"");
                    punchStatus = 1;
                }
            }
            punchStatus();
    }

    private void getAttendanceLog() {
        Log.e("getattendancelog",Constant.BASE_URL);
        List<ActivityModal> activitydata = db.getAllActivityData();
        if(activitydata!=null && activitydata.size()>0){
            String attendancelog = sf.getSharedPrefrenceString(Constant.ATTENDANCE_LOG);
            myAttendance = new Gson().fromJson(attendancelog, new TypeToken<List<AttendanceLogData>>(){}.getType());
            setAttendanceLogStatus();
        }else{
            if (!Network.isConnected(this)) {
                String attendancelog = sf.getSharedPrefrenceString(Constant.ATTENDANCE_LOG);
                myAttendance = new Gson().fromJson(attendancelog, new TypeToken<List<AttendanceLogData>>(){}.getType());
                setAttendanceLogStatus();
            }else {
                Date currentTime = Calendar.getInstance().getTime();
                String currentTimeStamp = Constant.formatDate(currentTime);
                JSONObject object = new JSONObject();
                try {
                    object.put("FromDate", currentTimeStamp);
                    object.put("ToDate", currentTimeStamp);
                    request.postRequestArrayResponse(Constant.GET_ATTENDANCE_LOG, this, object, this, Constant.BASE_URL, true);
                } catch (JSONException ex) {

                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    public void registerLayoutVisible(){
        register.setVisibility(View.VISIBLE);
        fabEdit.setVisibility(View.GONE);
        bottombutton.setVisibility(View.GONE);
    }

    @SuppressLint("RestrictedApi")
    public void loginLayoutVisible(){
        register.setVisibility(View.GONE);
        fabEdit.setVisibility(View.VISIBLE);
        bottombutton.setVisibility(View.VISIBLE);
    }

    private void punchStatus(){
        if(punchStatus==1){
            log_in.setEnabled(false);
            log_out.setEnabled(true);
            check_in.setEnabled(true);
            check_out.setEnabled(false);
            log_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            log_out.setBackgroundColor(getResources().getColor(R.color.red));
            check_in.setBackgroundColor(getResources().getColor(R.color.red));
            check_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            tvtime1.setText("Log In");
            if(punchTime!=null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date parsetime = format.parse(punchTime);
                    String punchdatetime = Constant.formatedDateTimeZone(parsetime);
                    tvtime2.setText(punchdatetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }else if(punchStatus==2){
            log_in.setEnabled(false);
            log_out.setEnabled(false);
            check_in.setEnabled(false);
            check_out.setEnabled(false);
            log_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            log_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            check_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            check_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            tvtime1.setText("Logout");
            if(punchTime!=null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date parsetime = format.parse(punchTime);
                    String punchdatetime = Constant.formatedDateTimeZone(parsetime);
                    tvtime2.setText(punchdatetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else if(punchStatus==3){
            log_in.setEnabled(false);
            log_out.setEnabled(true);
            check_in.setEnabled(false);
            check_out.setEnabled(true);
            log_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            log_out.setBackgroundColor(getResources().getColor(R.color.red));
            check_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            check_out.setBackgroundColor(getResources().getColor(R.color.red));
            tvtime1.setText("Check In");
            if(punchTime!=null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date parsetime = format.parse(punchTime);
                    String punchdatetime = Constant.formatedDateTimeZone(parsetime);
                    tvtime2.setText(punchdatetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else {
            log_in.setEnabled(true);
            log_out.setEnabled(false);
            check_in.setEnabled(false);
            check_out.setEnabled(false);
            log_in.setBackgroundColor(getResources().getColor(R.color.red));
            log_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            check_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            check_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
            settingCurrentDayAndTime();
        }
    }

    private void clickListener() {
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddClick();
            }
        });

        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sf.setSharedPrefrenceString(Constant.EDIT_FR,"true");
                registerLayoutVisible();
                register.setText("Re-register");
                settingCurrentDayAndTime();
                
            }
        });

        fab_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.BACK_RESULT_CODE);
                sf.setSharedPrefrenceString(Constant.EDIT_FR,"false");
//                Intent intent = new Intent();
                setResult(Constant.BACK_RESULT_CODE);
                finish();
            }
        });

        log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableButton();
                encoded = getBase64();
                status = Constant.LOGIN;
                check_status = Constant.LOGIN;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(!faceRecorgnize){
                            Toast.makeText(NewActivity.this,Constant.ALLIGN_FACE_TOAST,Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 2000);
            }
        });

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutConfirmationAlert();

            }
        });

        check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableButton();
                settingCurrentAddress();
                botom_layout.setVisibility(View.GONE);
                checkin_layout.setVisibility(View.VISIBLE);
                checkin_toast.setVisibility(View.VISIBLE);
                checkin_toast.setText("Customer Check-In");
                check_status = Constant.CHECKIN;

            }
        });

        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableButton();
                botom_layout.setVisibility(View.GONE);
                checkin_layout.setVisibility(View.VISIBLE);
                checkin_toast.setVisibility(View.VISIBLE);
                checkin_toast.setText("Customer Check-Out");
                String customername = sf.getSharedPrefrenceString(Constant.CUSTOMER_NAME);
                String customeraddress = sf.getSharedPrefrenceString(Constant.CUSTOMER_ADDRESS);
                if(customername!=null && !customername.equals("")){
                    ed_name.setEnabled(false);
                    ed_name.setText(customername);
                }
                if(customeraddress!=null && !customeraddress.equals("")){
                    ed_address.setEnabled(false);
                    ed_address.setText(customeraddress);
                }
                check_status = Constant.CHECKOUT;
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if(tab.getPosition()==1){
                    sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.QR_RESULT_CODE);
                    sf.setSharedPrefrenceString(Constant.EDIT_FR,"false");
                    Intent intent = new Intent();
                    setResult(Constant.QR_RESULT_CODE, intent);
                    finish();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = ed_name.getText().toString();
                String address = ed_address.getText().toString();
                if(name.isEmpty()){
                    ed_name.setError("Name is required");
                }else if(address.isEmpty()){
                    ed_address.setError("Address is required");
                }else {
                    submit.setEnabled(false);
                    submit.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
                    cancel.setEnabled(false);
                    cancel.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
                    encoded = getBase64();
                    status = check_status;
                    currentAddress=ed_address.getText().toString();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(!faceRecorgnize){
                                Toast.makeText(NewActivity.this,Constant.ALLIGN_FACE_TOAST,Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 2000);
                    
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                punchStatus();
                botom_layout.setVisibility(View.VISIBLE);
                checkin_layout.setVisibility(View.GONE);
                checkin_toast.setVisibility(View.GONE);

            }
        });
    }

    private void showLogoutConfirmationAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to logout?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                disableButton();
                encoded = getBase64();
                status = Constant.LOGOUT;
                check_status = Constant.LOGOUT;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(!faceRecorgnize){
                            Toast.makeText(NewActivity.this,Constant.ALLIGN_FACE_TOAST,Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 2000);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void settingCurrentAddress() {
       ed_address.setText(currentAddress);
    }

    private void onAddClick() {
        addPending = true;
    }

    private void disableButton(){
        log_in.setEnabled(false);
        log_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
        log_out.setEnabled(false);
        log_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
        check_in.setEnabled(false);
        check_in.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
        check_out.setEnabled(false);
        check_out.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
    }

    public String getBase64(){
        Bitmap resizebitmap = ImageUtils.getResizedBitmap(croppedBitmap, 200);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizebitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    @Override
    protected void processImage() {
        ++timestamp;
        final long currTimestamp = timestamp;
        trackingOverlay.postInvalidate();

        // No mutex needed as this method is not reentrant.
        if (computingDetection) {
            readyForNextImage();
            return;
        }
        computingDetection = true;

        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

        readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
        // For examining the actual TF input.
        if (SAVE_PREVIEW_BITMAP) {
            ImageUtils.saveBitmap(croppedBitmap);
        }

        InputImage image = InputImage.fromBitmap(croppedBitmap, 0);
        faceDetector
                .process(image)
                .addOnSuccessListener(new OnSuccessListener<List<Face>>() {
                    @Override
                    public void onSuccess(List<Face> faces) {
                        if (faces.size() == 0) {
                            updateResults(currTimestamp, new LinkedList<>());
                            return;
                        }
                        runInBackground(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        onFacesDetected(currTimestamp, faces, addPending);
                                        addPending=false;
                                    }
                                });
                    }

                });
    }

    @SuppressLint("RestrictedApi")
    private void onFacesDetected(long currTimestamp, List<Face> faces, boolean add) {
        final List<SimilarityClassifier.Recognition> mappedRecognitions = new LinkedList<SimilarityClassifier.Recognition>();
        int sourceW = rgbFrameBitmap.getWidth();
        int sourceH = rgbFrameBitmap.getHeight();
        int targetW = portraitBmp.getWidth();
        int targetH = portraitBmp.getHeight();
        Matrix transform = createTransform(
                sourceW,
                sourceH,
                targetW,
                targetH,
                sensorOrientation);
        final Canvas cv = new Canvas(portraitBmp);

        cv.drawBitmap(rgbFrameBitmap, transform, null);

        final Canvas cvFace = new Canvas(faceBmp);

        for (Face face : faces) {
            final RectF boundingBox = new RectF(face.getBoundingBox());
            final boolean goodConfidence = true; //face.get;
            if (boundingBox != null && goodConfidence) {

                cropToFrameTransform.mapRect(boundingBox);

                RectF faceBB = new RectF(boundingBox);
                transform.mapRect(faceBB);

                float sx = ((float) TF_OD_API_INPUT_SIZE) / faceBB.width();
                float sy = ((float) TF_OD_API_INPUT_SIZE) / faceBB.height();
                Matrix matrix = new Matrix();
                matrix.postTranslate(-faceBB.left, -faceBB.top);
                matrix.postScale(sx, sy);

                cvFace.drawBitmap(portraitBmp, matrix, null);

                String label = "";
                float confidence = -1f;
                Integer color = Color.BLUE;
                float[][] extra = null;
                Bitmap crop = null;


                try {
                    crop = Bitmap.createBitmap(portraitBmp,
                            (int) faceBB.left,
                            (int) faceBB.top,
                            (int) faceBB.width(),
                            (int) faceBB.height());
                }catch (IllegalArgumentException e){

                }

                final long startTime = SystemClock.uptimeMillis();
                final float[][] embedding = detector.recognizeImage(faceBmp, add);
                HashMap<String, SimilarityClassifier.Recognition> registered = getRecorgnizeDataFromLocal();
                SimilarityClassifier.Recognition rec = detector.findingNearestImage(registered);
                if (addPending) {
                    rec.setExtra(embedding);
                }
                final int numDetectionsOutput = 1;
                final ArrayList<SimilarityClassifier.Recognition> resultsAux = new ArrayList<>(numDetectionsOutput);
                resultsAux.add(rec);
//        lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                if (resultsAux.size() > 0) {
                    SimilarityClassifier.Recognition result = resultsAux.get(0);
                    extra = result.getExtra();
                    boolean checkconfidence = face_detector.checkConfidenceLevel(result, Double.parseDouble(sf.getSharedPrefrenceString(Constant.CONFIDENCE_LEVEL)));
                    if (checkconfidence) {
                        if(status!=-1) {

                            faceRecorgnize=true;

                            Date currentTime = Calendar.getInstance().getTime();
                            String ftime = Constant.formatTime(currentTime);
                            String name = ed_name.getText().toString();
                            String address = ed_address.getText().toString();
                            if (!Network.isConnected(NewActivity.this)) {
                                savedataToLocal(name,address);
                            }else {
                                JSONObject object = new JSONObject();
                                try {
                                    object.put(Constant.TIMESTAMP, Constant.getTimeStamp(currentTime));
                                    object.put(Constant.AttendanceStatus,status);
                                    object.put(Constant.EMPLOYEEID,sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
                                    if(recorgnition_flag)
                                        object.put(Constant.BASE64IMAGE,"data:image/jpeg;base64,/9j/"+encoded);
                                    else
                                        object.put(Constant.BASE64IMAGE,"");
                                    object.put(Constant.LATITUDE,Double.parseDouble(sf.getSharedPrefrenceString(Constant.LATITUDE)));
                                    object.put(Constant.LONGITUDE,Double.parseDouble(sf.getSharedPrefrenceString(Constant.LONGITUDE)));
                                    if(status.equals(Constant.CHECKIN) || status.equals(Constant.CHECKOUT)){
                                        object.put(Constant.CUSTOMER_NAME,name);
                                        object.put(Constant.CUSTOMER_ADDRESS,address);
                                    }
                                    object.put(Constant.ISOFFLINEENTRY,true);
                                    object.put(Constant.ELAPSE_TIME,String.valueOf(SystemClock.elapsedRealtime()));
                                    object.put(Constant.SYNC_TIME,Constant.getTimeStamp(currentTime));
                                    AttendanceLogData data = new AttendanceLogData();
                                    data.setAttendanceStatus(status);
                                    data.setTimeStamp(Constant.getTimeStamp(currentTime));
                                    data.setCustomerName(name);
                                    data.setCustomerAddress(address);
                                    if(myAttendance!=null) {
                                        myAttendance.add(data);

                                        Collections.reverse(myAttendance);

                                        sf.setSharedPrefrenceString(Constant.ATTENDANCE_LOG, new Gson().toJson(myAttendance));
                                    }
                                    request.postRequest(Constant.UPDATE_ATTENDENCE,this,object,this,Constant.BASE_URL,true);
                                    status=-1;
                                }catch (JSONException ex){

                                    Log.e("exception",ex.toString());
                                }
                            }

                        }

//                        confidence = result.getDistance();
//                        label = result.getTitle();
                        if (result.getId().equals("0")) {
                            color = Color.GREEN;
                        }
                        else {
                            color = Color.RED;
                        }
                    }

                }

                if (getCameraFacing() == CameraCharacteristics.LENS_FACING_FRONT) {

                    // camera is frontal so the image is flipped horizontally
                    // flips horizontally
                    Matrix flip = new Matrix();
                    if (sensorOrientation == 90 || sensorOrientation == 270) {
                        flip.postScale(1, -1, previewWidth / 2.0f, previewHeight / 2.0f);
                    }
                    else {
                        flip.postScale(-1, 1, previewWidth / 2.0f, previewHeight / 2.0f);
                    }
                    //flip.postScale(1, -1, targetW / 2.0f, targetH / 2.0f);
                    flip.mapRect(boundingBox);

                }

                final SimilarityClassifier.Recognition result = new SimilarityClassifier.Recognition(
                        "0", label, confidence, boundingBox);

                result.setColor(color);
                result.setLocation(boundingBox);
                result.setExtra(extra);
                result.setCrop(crop);
                mappedRecognitions.add(result);

            }

        }
        updateResults(currTimestamp, mappedRecognitions);

    }

    private Matrix createTransform(
            final int srcWidth,
            final int srcHeight,
            final int dstWidth,
            final int dstHeight,
            final int applyRotation) {

        Matrix matrix = new Matrix();
        if (applyRotation != 0) {
            if (applyRotation % 90 != 0) {
            }

            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f);

            // Rotate around origin.
            matrix.postRotate(applyRotation);
        }

//        // Account for the already applied rotation, if any, and then determine how
//        // much scaling is needed for each axis.
//        final boolean transpose = (Math.abs(applyRotation) + 90) % 180 == 0;
//        final int inWidth = transpose ? srcHeight : srcWidth;
//        final int inHeight = transpose ? srcWidth : srcHeight;

        if (applyRotation != 0) {

            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f);
        }

        return matrix;

    }

    private HashMap<String, SimilarityClassifier.Recognition> getRecorgnizeDataFromLocal() {
        String employeeid = sf.getSharedPrefrenceString(Constant.EMPLOYEEID);
        List<FaceData> allfacedata = db.getAllFaceData(sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
        HashMap<String, SimilarityClassifier.Recognition> registered = new HashMap<>();
        if(allfacedata!=null && allfacedata.size()>0) {
            registered.put(employeeid, allfacedata.get(0).getRec());
        }

        return registered;
    }

    private void updateResults(long currTimestamp, final List<SimilarityClassifier.Recognition> mappedRecognitions) {

        tracker.trackResults(mappedRecognitions, currTimestamp);
        trackingOverlay.postInvalidate();
        computingDetection = false;
        //adding = false;


        if (mappedRecognitions.size() > 0) {
            SimilarityClassifier.Recognition rec = mappedRecognitions.get(0);
            if (rec.getExtra() != null && rec.getCrop()!=null) {
                showAddFaceDialog(rec);
            }

        }

    }

    private void showAddFaceDialog(SimilarityClassifier.Recognition rec) {
        MyApplication app = (MyApplication) getApplicationContext();
        app.setData(rec);
        Intent in = new Intent(this, AddFaceActivity.class);
        in.putExtra(Constant.CUSTOMER_ADDRESS,currentAddress);
        startActivity(in);
        finish();

    }

    private void savedataToLocal(String name, String address) {
        Date currentTime = Calendar.getInstance().getTime();
        String formatedtime = Constant.getTimeStamp(currentTime);
        String ftime = Constant.formatTime(currentTime);
        SharedPrefrence sf = new SharedPrefrence(this);
        String employeeid = sf.getSharedPrefrenceString(Constant.EMPLOYEEID);
        String latitude = sf.getSharedPrefrenceString(Constant.LATITUDE);
        String longitude = sf.getSharedPrefrenceString(Constant.LONGITUDE);
        ActivityModal activity = new ActivityModal();
        activity.setEmpid(employeeid);
        activity.setLatitude(latitude);
        activity.setLongitude(longitude);
        activity.setImage_blob(encoded);
        activity.setTimestamp(formatedtime);
        activity.setType(status.toString());
        if(status.equals(Constant.CHECKIN) || status.equals(Constant.CHECKOUT)){
            activity.setName(name);
            activity.setAddress(address);
        }
        db.addActivityData(activity);
        AttendanceLogData data = new AttendanceLogData();
        data.setAttendanceStatus(status);
        data.setTimeStamp(formatedtime);
        data.setCustomerName(name);
        data.setCustomerAddress(address);
        
        myAttendance.add(data);
        
        Collections.reverse(myAttendance);

        sf.setSharedPrefrenceString(Constant.ATTENDANCE_LOG,new Gson().toJson(myAttendance));
        
        Intent in = new Intent(NewActivity.this, LoggedTimeActivity.class);
        in.putExtra(Constant.STATUS,status);
        in.putExtra(Constant.CUSTOMER_ADDRESS,currentAddress);
        in.putExtra(Constant.LOGGED_TIME,ftime);
        startActivity(in);
        finish();
    }

    @Override
    protected void onPreviewSizeChosen(Size size, int rotation) {
        sensorOrientation = rotation - getScreenOrientation();
        Recorgnize.PreviewSizeObject ob = face_detector.onPreviewSizeChosen(this, size, rotation, sensorOrientation);
        tracker = ob.tracker;
        detector=ob.detector;
        previewWidth=ob.previewWidth;
        previewHeight=ob.previewHeight;
        rgbFrameBitmap=ob.rgbFrameBitmap;
        croppedBitmap=ob.croppedBitmap;
        portraitBmp=ob.portraitBmp;
        faceBmp=ob.faceBmp;
        frameToCropTransform=ob.frameToCropTransform;
        cropToFrameTransform=ob.cropToFrameTransform;
        trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);
        trackingOverlay.addCallback(
                new OverlayView.DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);
                        if (isDebug()) {
                            tracker.drawDebug(canvas);
                        }
                    }
                });

        tracker.setFrameConfiguration(previewWidth, previewHeight, sensorOrientation);
    }

    @Override
    protected int getLayoutId() {
         return R.layout.tfe_od_camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    public void onTaskCompleted(JSONObject response, String mode,JSONArray responseArray) {
        if(mode.equals(Constant.UPDATE_ATTENDENCE)){
            encoded="";
            Date currentTime = Calendar.getInstance().getTime();
            String ftime = Constant.formatTime(currentTime);
            Intent in = new Intent(NewActivity.this, LoggedTimeActivity.class);
            in.putExtra(Constant.STATUS,check_status);
            in.putExtra(Constant.CUSTOMER_ADDRESS,currentAddress);
            in.putExtra(Constant.LOGGED_TIME,ftime);
            startActivity(in);
            finish();
        }else if(mode.equals(Constant.GET_USER)) {

            GetUserModal userdata = new Gson().fromJson(response.toString(), GetUserModal.class);
            String confidenceLevel = userdata.getfRConfidenceThreshold();
            sf.setSharedPrefrenceString(Constant.CONFIDENCE_LEVEL,confidenceLevel);
            sf.setSharedPrefrenceString(Constant.EMPLOYEEID, String.valueOf(userdata.getEmployeeId()));

            JSONObject object = new JSONObject();
            try {
                object.put(Constant.EMPID, sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
                object.put(Constant.ORGANISATION,Constant.ORGANIZATION);
                object.put(Constant.DOWNLOAD_LAT,sf.getSharedPrefrenceString(Constant.LATITUDE));
                object.put(Constant.DOWNLOAD_Long,sf.getSharedPrefrenceString(Constant.LONGITUDE));
                object.put(Constant.DEVICE_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                object.put(Constant.TIMEZONE, TimeZone.getDefault().getID());
                request.postRequest("",this,object,this,Constant.URL,true);
            }catch (JSONException ex){

            }

        }else if(mode.equals("")){
            try {
                String download_url = response.getString("downloadUrl");
                String download_modal = sf.getSharedPrefrenceString(Constant.DOWNLOAD_MODAL);
                if(download_modal!=null && !download_modal.equals("false"))
                request.DownloadModalFile(download_url,getFilesDir().getPath(),Constant.MODAL_FILE_NAME,this,Constant.DOWNLOAD,this);
                else{
                    sf.setSharedPrefrenceString(Constant.DOWNLOAD_MODAL,"false");
                    setFragment();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(mode.equals(Constant.DOWNLOAD)){
            sf.setSharedPrefrenceString(Constant.AUTH_LOGGED_IN,"false");
            sf.setSharedPrefrenceString(Constant.DOWNLOAD_MODAL,"false");
            setFragment();
            getLocalUserData();
        }else if(mode.equals(Constant.GET_ATTENDANCE_LOG)){
            Type attendnceTypeList = new TypeToken<ArrayList<AttendanceLogData>>(){}.getType();
            myAttendance = new Gson().fromJson(responseArray.toString(),attendnceTypeList);
            sf.setSharedPrefrenceString(Constant.ATTENDANCE_LOG,new Gson().toJson(myAttendance));
           setAttendanceLogStatus();
        }else if(mode.equals(Constant.GET_LOCATION_MODE)){
            CurrentLocation location = new Gson().fromJson(response.toString(),CurrentLocation.class);
            currentAddress = location.getAddresses().get(0).getAddress().getFreeformAddress();
        }
    }

    @Override
    public void onTaskFailed(ANError error, String mode) {
        if(error.getErrorBody()!=null) {
            try {
                JSONObject jsonObj = new JSONObject(error.getErrorBody());
                String msg = jsonObj.getString("ErrorMessage");

                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            }catch (JSONException ex){

            }
        }else{

            Toast.makeText(this, error.getErrorDetail(), Toast.LENGTH_SHORT).show();
        }

        if(mode.equals(Constant.UPDATE_ATTENDENCE)){
            punchStatus();
        }
    }
}











