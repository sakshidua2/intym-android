package com.sybica.falconwings;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.honeywell.hbt.intym.R;
import com.sybica.falconwings.networkmodal.currentlocation.CurrentLocation;
import com.sybica.falconwings.util.AsyncTaskCompleteListener;
import com.sybica.falconwings.util.Constant;
import com.sybica.falconwings.util.NetworkRequest;
import com.sybica.falconwings.util.SharedPrefrence;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class LoggedTimeActivity extends AppCompatActivity {
    Integer status;
    String logged_time;
    TextView tv_logged_time;
    TextView logtime;
    TextView latlong;
    ImageView back;
    public static final int LOGGED_TIME_OUT = 1500;
    String edit_fr="";
    String latitude="";
    String longitude="";
    String currentaddress = "";
    SharedPrefrence sf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_time);

        sf = new SharedPrefrence(this);
        edit_fr =  sf.getSharedPrefrenceString(Constant.EDIT_FR);
        latitude = sf.getSharedPrefrenceString(Constant.LATITUDE);
        longitude = sf.getSharedPrefrenceString(Constant.LONGITUDE);

        Intent in = getIntent();
        if(in!=null){
            status = in.getIntExtra(Constant.STATUS,-1);
            logged_time = in.getStringExtra(Constant.LOGGED_TIME);
            currentaddress = in.getStringExtra(Constant.CUSTOMER_ADDRESS);
        }

        initView();
        settingValue();
        clickListener();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.NEW_ACTIVITY_RESULT_CODE);
                    setResult(RESULT_OK);
                    finish();
                }
            }, LOGGED_TIME_OUT);

    }

    private void clickListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.NEW_ACTIVITY_RESULT_CODE);
                setResult(RESULT_OK);
                finish();
            }
        });
    }


    private void settingValue() {
        logtime.setText(logged_time);
        settingCurrentAddress();

        if(status==Constant.LOGIN){
            tv_logged_time.setText("Attendance-In has been logged successfully");
        }else if(status==Constant.LOGOUT){
            tv_logged_time.setText("Attendance-Out has been logged successfully");
        }else if(status==Constant.CHECKIN){
            tv_logged_time.setText("Check-In has been logged successfully");
        }else if(status==Constant.CHECKOUT){
            tv_logged_time.setText("Check-Out has been logged successfully");
        }else if(status==Constant.REGISTER){
            if(edit_fr.equals("true")){
                tv_logged_time.setText("Employee successfully edit for FR "+logged_time);
            }else {
                tv_logged_time.setText("Employee successfully register for FR "+logged_time);
            }

        }
    }

    private void settingCurrentAddress() {
      if(currentaddress!=null && currentaddress!=""){
          latlong.setText(currentaddress);
      }else {
          latlong.setText(latitude+" , "+longitude);
      }


    }

    private void initView() {
        tv_logged_time=findViewById(R.id.tv_logged_time);
        logtime=findViewById(R.id.logtime);
        latlong=findViewById(R.id.latlong);
        back=findViewById(R.id.back);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.NEW_ACTIVITY_RESULT_CODE);
        setResult(RESULT_OK);
        finish();
    }
}
