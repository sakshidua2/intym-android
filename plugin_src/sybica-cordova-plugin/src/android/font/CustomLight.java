package com.sybica.falconwings.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CustomLight extends AppCompatTextView {
    Context context;
    public CustomLight(Context context) {
        super(context);
        this.context=context;
        init();
    }

    public CustomLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init();
    }

    public CustomLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        init();
    }

    @Override
    public void setText(CharSequence textt, BufferType type) {
        CharSequence text = textt;
        if (text.length() > 0) {
            text = String.valueOf(text.charAt(0)).toUpperCase() + text.subSequence(1, text.length());
        }
        super.setText(text, type);
    }

    private void init() {
        String otfName = "fonts/HoneywellSansWeb-Light.ttf";
        Typeface font = Typeface.createFromAsset(context.getAssets(), otfName);
        this.setTypeface(font);
    }
}

