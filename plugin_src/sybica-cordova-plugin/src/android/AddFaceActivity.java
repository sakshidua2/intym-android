package com.sybica.falconwings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.honeywell.hbt.intym.R;
import com.sybica.falconwings.db.DatabaseHandler;
import com.sybica.falconwings.db.FaceData;
import com.sybica.falconwings.util.Constant;
import com.sybica.falconwings.util.MyApplication;
import com.sybica.falconwings.util.SharedPrefrence;
import com.sybica.fw_lib.SimilarityClassifier;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddFaceActivity extends AppCompatActivity {
    ImageView image;
    EditText employeeId;
    TextView submit;
    TextView retake;
    SimilarityClassifier.Recognition rec;
    SharedPrefrence sf;
    DatabaseHandler db;
    String latitude;
    String longitude;
    String empid;
    String currentaddress="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_face);
        initview();
        sf = new SharedPrefrence(AddFaceActivity.this);
        db = new DatabaseHandler(this);
        empid = sf.getSharedPrefrenceString(Constant.EMPLOYEEID);
        latitude = sf.getSharedPrefrenceString(Constant.LATITUDE);
        longitude = sf.getSharedPrefrenceString(Constant.LONGITUDE);
        if(empid!=null && !empid.isEmpty()){
            employeeId.setText(empid);
        }
        MyApplication app = (MyApplication) getApplicationContext();
        rec = app.getData();
        image.setImageBitmap(rec.getCrop());
        Intent in = getIntent();
        if(in!=null){
            currentaddress = in.getStringExtra(Constant.CUSTOMER_ADDRESS);
        }

        clickListener();

    }

    private void clickListener() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    disableButton();
                    Date currentTime = Calendar.getInstance().getTime();
                    String formatedtime = Constant.formatedDateTime(currentTime);
                    String ftime = Constant.formatTime(currentTime);
                    List<FaceData> allfacedata = db.getAllFaceData(sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
                    if(allfacedata!=null && allfacedata.size()>0 && allfacedata.get(0).getId().equals(empid)){
                        FaceData data = new FaceData();
                        data.setId(empid);
                        data.setRec(rec);
                        data.setLastRecorgnizeTime(formatedtime);
                        db.updateFaceData(data);
                    }else{
                        FaceData data = new FaceData();
                        data.setId(empid);
                        data.setRec(rec);
                        data.setLastRecorgnizeTime(formatedtime);
                        db.addFaceData(data);
                    }

                    sf.setSharedPrefrenceString(Constant.EDIT_FR,"false");

                Intent in = new Intent(AddFaceActivity.this, LoggedTimeActivity.class);
                in.putExtra(Constant.LOGGED_TIME,ftime);
                in.putExtra(Constant.CUSTOMER_ADDRESS,currentaddress);
                in.putExtra(Constant.STATUS,Constant.REGISTER);
                startActivity(in);
                finish();
                }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.NEW_ACTIVITY_RESULT_CODE);
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void disableButton(){
        submit.setEnabled(false);
        submit.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
        retake.setEnabled(false);
        retake.setBackgroundColor(getResources().getColor(R.color.tfe_semi_transparent));
    }
    private void initview() {
        image = findViewById(R.id.image);
        employeeId=findViewById(R.id.employeeId);
        submit=findViewById(R.id.submit);
        retake=findViewById(R.id.retake);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sf.setSharedPrefrenceInteger(Constant.RESULT_CODE,Constant.NEW_ACTIVITY_RESULT_CODE);
        setResult(RESULT_OK);
        finish();
    }
}











