package com.sybica.falconwings.db;

import androidx.annotation.Keep;

import com.sybica.fw_lib.SimilarityClassifier;

@Keep
public class FaceData {
    public String id;
    public SimilarityClassifier.Recognition rec;
    public String lastRecorgnizeTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SimilarityClassifier.Recognition getRec() {
        return rec;
    }

    public void setRec(SimilarityClassifier.Recognition rec) {
        this.rec = rec;
    }

    public String getLastRecorgnizeTime() {
        return lastRecorgnizeTime;
    }

    public void setLastRecorgnizeTime(String lastRecorgnizeTime) {
        this.lastRecorgnizeTime = lastRecorgnizeTime;
    }

}
