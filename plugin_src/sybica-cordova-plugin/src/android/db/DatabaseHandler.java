package com.sybica.falconwings.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.sybica.fw_lib.SimilarityClassifier;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "faceData";
    private static final String EMPLOYEE_TABLE_NAME = "empTable";
    private static final String ACTIVITY_TABLE_NAME = "activityTable";
    //user columns
    private static final String KEY_ID = "id";
    private static final String KEY_EMP_ID = "empid";
    private static final String KEY_RECORGITION = "recorgnition";
    private static final String KEY_LAST_RECOGNIZE_TIME = "lastRecorgnizeTime";

    //activity columns
    private static final String KEY_ACTIVITY_ID = "activity_id";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_IMAGE_BLOB ="image_blob";
    private static final String KEY_TIME_STAMP = "time_stamp";
    private static final String KEY_TYPE = "type";
    private static final String KEY_NAME = "name";
    private static final String KEY_ADDRESS = "address";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + EMPLOYEE_TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_EMP_ID + " TEXT,"
                + KEY_RECORGITION + " TEXT," + KEY_LAST_RECOGNIZE_TIME + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_ACTIVITY_TABLE = "CREATE TABLE " + ACTIVITY_TABLE_NAME + "("
                + KEY_ACTIVITY_ID + " INTEGER PRIMARY KEY," + KEY_EMP_ID + " TEXT,"
                + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT," + KEY_IMAGE_BLOB + " TEXT," + KEY_TIME_STAMP
                + " TEXT," + KEY_TYPE + " TEXT," + KEY_NAME + " TEXT," + KEY_ADDRESS + " TEXT" + ")";

        db.execSQL(CREATE_ACTIVITY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + EMPLOYEE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ACTIVITY_TABLE_NAME);

        onCreate(db);

    }

    public void addFaceData(FaceData facedata) {
        Gson gson = new Gson();
        String jsonstring = gson.toJson(facedata.getRec());
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EMP_ID, facedata.getId());
        values.put(KEY_RECORGITION, jsonstring);
        values.put(KEY_LAST_RECOGNIZE_TIME,facedata.getLastRecorgnizeTime());

        db.insert(EMPLOYEE_TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public int updateFaceData(FaceData facedata) {
        Gson gson = new Gson();
        String jsonstring = gson.toJson(facedata.getRec());
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EMP_ID, facedata.getId());
        values.put(KEY_RECORGITION, jsonstring);
        values.put(KEY_LAST_RECOGNIZE_TIME,facedata.getLastRecorgnizeTime());

        // updating row
        return db.update(EMPLOYEE_TABLE_NAME, values, KEY_EMP_ID + " = ?",
                new String[] { String.valueOf(facedata.getId()) });
    }

    public List<FaceData> getAllFaceData(String empid) {
        List<FaceData> contactList = new ArrayList<FaceData>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + EMPLOYEE_TABLE_NAME + " WHERE " + KEY_EMP_ID + "=" + empid;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                FaceData contact = new FaceData();
                contact.setId(cursor.getString(1));
                contact.setRec(new Gson().fromJson(cursor.getString(2), SimilarityClassifier.Recognition.class));
                contact.setLastRecorgnizeTime(cursor.getString(3));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public void addActivityData(ActivityModal activity) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EMP_ID, activity.getEmpid());
        values.put(KEY_LATITUDE,activity.getLatitude());
        values.put(KEY_LONGITUDE,activity.getLongitude());
        values.put(KEY_IMAGE_BLOB,activity.getImage_blob());
        values.put(KEY_TIME_STAMP,activity.getTimestamp());
        values.put(KEY_TYPE,activity.getType());
        values.put(KEY_NAME,activity.getName());
        values.put(KEY_ADDRESS,activity.getAddress());

        db.insert(ACTIVITY_TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public List<ActivityModal> getAllActivityData() {
        List<ActivityModal> contactList = new ArrayList<ActivityModal>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ACTIVITY_TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ActivityModal contact = new ActivityModal();
                contact.setId(cursor.getString(0));
                contact.setEmpid(cursor.getString(1));
                contact.setLatitude(cursor.getString(2));
                contact.setLongitude(cursor.getString(3));
                contact.setImage_blob(cursor.getString(4));
                contact.setTimestamp(cursor.getString(5));
                contact.setType(cursor.getString(6));
                contact.setName(cursor.getString(7));
                contact.setAddress(cursor.getString(8));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public void deleteActivity(String activity_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String where="activity_id=?";
        db.delete(ACTIVITY_TABLE_NAME, where, new String[]{activity_id}) ;
    }


    public void deleteFaceData(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ EMPLOYEE_TABLE_NAME);
        db.execSQL("delete from "+ ACTIVITY_TABLE_NAME);
    }
}




