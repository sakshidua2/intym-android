package com.sybica.falconwings;

import android.content.Context;
import android.content.Intent;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaArgs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import com.google.gson.Gson;
import com.sybica.falconwings.db.ActivityModal;
import com.sybica.falconwings.db.DatabaseHandler;
import com.sybica.falconwings.networkmodal.GetUserModal;
import com.sybica.falconwings.util.Constant;
import com.sybica.falconwings.util.SharedPrefrence;

import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * This class echoes a string called from JavaScript.
 */
public class SybicaCordovaPlugin extends CordovaPlugin {
    CallbackContext callbackContext;
    String token;
    String atendancelog;
    String userDetail;
    String auth_logged_in;
    SharedPrefrence sf;

   public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
         this.callbackContext=callbackContext;
         sf=new SharedPrefrence(this.cordova.getActivity());
        JSONObject object = args.getJSONObject(0);
        atendancelog =  object.getString("ATTENDANCE_LOG");
        userDetail = object.getString("USER_DETAIL");
        token = object.getString("TOKEN");
        auth_logged_in = object.getString("AUTH_LOGGED_IN");
        sf.setSharedPrefrenceString(Constant.TOKEN,token);
        if(sf.getSharedPrefrenceString(Constant.ATTENDANCE_LOG).equals("")){
            sf.setSharedPrefrenceString(Constant.ATTENDANCE_LOG,atendancelog);
        }
        if(sf.getSharedPrefrenceString(Constant.AUTH_LOGGED_IN).equals(""))
        sf.setSharedPrefrenceString(Constant.AUTH_LOGGED_IN,auth_logged_in);
        GetUserModal usermodal = new Gson().fromJson(userDetail,GetUserModal.class);
        sf.setSharedPrefrenceString(Constant.CONFIDENCE_LEVEL,usermodal.getfRConfidenceThreshold());
        sf.setSharedPrefrenceString(Constant.EMPLOYEEID, String.valueOf(usermodal.getEmployeeId()));
        Context context = cordova.getActivity().getApplicationContext();
        if(action.equals("new_activity")) {
            this.openNewActivity(context);
            return true;
        }
        return false;
    }

     private void openNewActivity(Context context) {
        Intent intent = new Intent(context, NewActivity.class);
        this.cordova.startActivityForResult(this,intent,Constant.REQUEST_CODE);
    }

public void onActivityResult(int requestCode, int resultCode, Intent intent) {
       Log.e("TAG", "onActivityResult: " + requestCode + " " + resultCode);
       if(callbackContext!=null) {
           if (sf.getSharedPrefrenceInteger(Constant.RESULT_CODE)==Constant.QR_RESULT_CODE)
               callbackContext.success("qr");
           else if (sf.getSharedPrefrenceInteger(Constant.RESULT_CODE)==Constant.BACK_RESULT_CODE)
               callbackContext.success("dashboard");
           else if (sf.getSharedPrefrenceInteger(Constant.RESULT_CODE)==Constant.NEW_ACTIVITY_RESULT_CODE)
               openNewActivity(cordova.getActivity().getApplicationContext());

       }
    }
}

