package com.sybica.falconwings.util;

import com.androidnetworking.error.ANError;

import org.json.JSONArray;
import org.json.JSONObject;

public interface AsyncTaskCompleteListener {
     void onTaskCompleted(JSONObject response, String mode, JSONArray responseArray);
     void onTaskFailed(ANError error,String mode);
}


