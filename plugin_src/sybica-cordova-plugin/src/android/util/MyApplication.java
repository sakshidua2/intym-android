package com.sybica.falconwings.util;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.androidnetworking.AndroidNetworking;
import com.sybica.fw_lib.SimilarityClassifier;

public class MyApplication extends Application {
    public SimilarityClassifier.Recognition data = null;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
        Reciever reciever = new Reciever();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(reciever,filter);
    }

    public SimilarityClassifier.Recognition getData() {
        return data;
    }

    public void setData(SimilarityClassifier.Recognition data) {
        this.data = data;
    }
}


