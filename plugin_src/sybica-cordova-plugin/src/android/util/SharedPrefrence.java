package com.sybica.falconwings.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefrence {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name


    public SharedPrefrence(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("pref_name", PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setSharedPrefrenceString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void setSharedPrefrenceInteger(String key,Integer value){
        editor.putInt(key,value);
        editor.apply();
    }

    public Integer getSharedPrefrenceInteger(String key){
        return pref.getInt(key,-1);
    }

    public void setSharedPrefrenceBoolean(String key,boolean value){
        editor.putBoolean(key,value);
        editor.apply();
    }

    public String getSharedPrefrenceString(String key) {
        return pref.getString(key, "");
    }

    public boolean getSharedPrefrenceBoolean(String key){
        return pref.getBoolean(key,false);
    }

    public HashMap<String,String> getAllSharedprefrnceObject(){
        return (HashMap<String, String>) pref.getAll();
    }

}

