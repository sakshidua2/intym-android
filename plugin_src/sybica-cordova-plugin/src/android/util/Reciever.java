package com.sybica.falconwings.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;


import com.androidnetworking.error.ANError;
import com.sybica.falconwings.db.ActivityModal;
import com.sybica.falconwings.db.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Reciever extends BroadcastReceiver implements AsyncTaskCompleteListener {
    SharedPrefrence sf;
    DatabaseHandler db;
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("receiver","receiver");

        this.context=context;
        if (Network.isConnected(context)) {
             db = new DatabaseHandler(context);
            sf = new SharedPrefrence(context);
            boolean recognition_status = true;
            List<ActivityModal> activitydata = db.getAllActivityData();
            if(activitydata!=null && activitydata.size()>0){
                for(int i=0;i<activitydata.size();i++){
                        sendDataToSerever(activitydata.get(i),recognition_status);
                }
            }
        }

    }

    private void sendDataToSerever(ActivityModal activityModal, boolean recognition_status) {
        Date currentTime = Calendar.getInstance().getTime();
        JSONObject object = new JSONObject();
        try {
            object.put(Constant.TIMESTAMP, activityModal.getTimestamp());
            object.put(Constant.AttendanceStatus,Integer.parseInt(activityModal.getType()));
            object.put(Constant.EMPLOYEEID,sf.getSharedPrefrenceString(Constant.EMPLOYEEID));
            if(recognition_status)
                object.put(Constant.BASE64IMAGE,"data:image/jpeg;base64,/9j/"+activityModal.getImage_blob());
            else
                object.put(Constant.BASE64IMAGE,"");

            object.put(Constant.LATITUDE,Double.parseDouble(sf.getSharedPrefrenceString(Constant.LATITUDE)));
            object.put(Constant.LONGITUDE,Double.parseDouble(sf.getSharedPrefrenceString(Constant.LONGITUDE)));
            if(Integer.parseInt(activityModal.getType()) == Constant.CHECKIN || Integer.parseInt(activityModal.getType()) ==Constant.CHECKOUT){
                object.put(Constant.CUSTOMER_NAME,activityModal.getName());
                object.put(Constant.CUSTOMER_ADDRESS,activityModal.getAddress());
            }
            object.put(Constant.ISOFFLINEENTRY,true);
            object.put(Constant.ELAPSE_TIME,String.valueOf(SystemClock.elapsedRealtime()));
            object.put(Constant.SYNC_TIME,Constant.getTimeStamp(currentTime));

            NetworkRequest request = new NetworkRequest();
            request.postRequest(Constant.UPDATE_ATTENDENCE,context,object,this,Constant.BASE_URL,false);
            db.deleteActivity(activityModal.getId());
        }catch (JSONException ex){

            Log.e("exception",ex.toString());
        }
    }


    @Override
    public void onTaskCompleted(JSONObject response, String mode, JSONArray responseArray) {

    }

    @Override
    public void onTaskFailed(ANError error, String mode) {


    }
}
