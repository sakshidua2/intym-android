package com.sybica.falconwings.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.WindowManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Constant {
    public static String CONFIDENCE_LEVEL = "confidenceLevel";
    public static Integer LOGIN = 1;
    public static Integer LOGOUT = 2;
    public static Integer CHECKIN = 3;
    public static Integer CHECKOUT = 4;
    public static Integer REGISTER = 5;
    public static String EMPLOYEEID = "EmployeeId";
    public static String VECTOR_IMAGE = "vector_image";
    public static String CROP_IMAGE = "bitmapimage";
    public static String EDIT_FR = "false";
    public static String LATITUDE = "Latitude";
    public static String LONGITUDE = "Longitude";
    public static String DOWNLOAD_MODAL = "download_modal";
    public static String MODAL_FILE_NAME = "mobile_kingfisher_v01.tflite";
    public static String URL = "https://us-central1-sybica-80359.cloudfunctions.net/getDownloadUrlDev";
    public static String LOGGED_TIME = "loggedtime";
    public static String STATUS = "status";
    public static String CUSTOMER_NAME= "CustomerName";
    public static String CUSTOMER_ADDRESS  = "CustomerAddress";
    public static String ORGANIZATION = "honeywell";
    public static String BASE_URL = "https://intymapimmgmtapp.azurewebsites.net/api/";
    public static String TOKEN = "token";
    public static String ATTENDANCE_LOG = "attendance-log";
    public static String AUTH_LOGGED_IN = "auth_logged_in";
public static String AttendanceStatus = "AttendanceStatus";
    public static String TIMESTAMP = "TimeStamp";
    public static String BASE64IMAGE = "Base64Image";
    public static String ISOFFLINEENTRY = "IsOfflineEntry";
    public static String ELAPSE_TIME = "ElapsedTimeSinceBoot";
    public static String SYNC_TIME = "SyncTime";
    public static String RESULT_CODE = "result_code";
    public static Integer BACK_RESULT_CODE = 103;
    public static Integer QR_RESULT_CODE = 102;
    public static Integer REQUEST_CODE = 101;
    public static String EMPID = "empid";
    public static String ORGANISATION = "organization";
    public static String DOWNLOAD_LAT = "download_location_lat";
    public static String DOWNLOAD_Long = "download_location_long";
    public static String DEVICE_ID = "device_id";
    public static String TIMEZONE = "timezone";
    //public static String CURRENT_LOCATION_MODE = "google";
    public static String CURRENT_LOCATION_MODE = "tomtom";
    //mode
    public static String GET_USER = "GetUserDetails";
    public static String UPDATE_ATTENDENCE = "LogUserAttendance";
    public static String DOWNLOAD = "download";
    public static String GET_ATTENDANCE_LOG = "GetAttendanceLog/myData";
    public static String ALLIGN_FACE_TOAST = "Please align your face properly or check lighting conditions";
    public static String GET_LOCATION_MODE = "reverseGeocode";
    public static String KEY = "key";
    public static Integer NEW_ACTIVITY_RESULT_CODE = 104;
    public static String API_KEY = "OzEBhPGGVe78b2knHDBdtbLwzYpMEzEI";
    public static String TOMTOM_URL = "https://api.tomtom.com/search/2/";



    public static String formatedDateTime(Date currentTime){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy, hh:mm a");
        String formatedtime = format.format(currentTime);
        return formatedtime;
    }

    public static String formatedDateTimeZone(Date currentTime){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy, hh:mm a");
        format.setTimeZone(TimeZone.getDefault());
        String formatedtime = format.format(currentTime);
        return formatedtime;
    }

    public static String formatTime(Date currentTime){
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        String ftime = format.format(currentTime);
        return ftime;
    }

    public static String formatDate(Date currentTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String ftime = format.format(currentTime);
        return ftime;
    }

    public static String getTimeStamp(Date currentTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String ftime = format.format(currentTime);
        return ftime;

    }

}




