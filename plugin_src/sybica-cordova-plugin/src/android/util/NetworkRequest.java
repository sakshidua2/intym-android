package com.sybica.falconwings.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.honeywell.hbt.intym.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class NetworkRequest {
    public void getRequest(String mode, Context context, HashMap<String,String> map, AsyncTaskCompleteListener listener, String baseUrl){
        if (!Network.isConnected(context)) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPrefrence sf = new SharedPrefrence(context);
        ProgressDialog progressdialog = initProgressDialog(context);
        progressdialog.show();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        ANRequest.GetRequestBuilder networkRequest = AndroidNetworking.get(baseUrl + mode);
        if(map.size()>0){
            for (Map.Entry<String,String> entry : map.entrySet()){
                networkRequest.addQueryParameter(entry.getKey(), entry.getValue());
            }
        }
        networkRequest.addHeaders("Content-Type", "application/json");
        networkRequest.setOkHttpClient(client);
        networkRequest.setPriority(Priority.MEDIUM);
        networkRequest.build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resposne",response.toString());
               progressdialog.dismiss();
                listener.onTaskCompleted(response,Constant.GET_LOCATION_MODE,new JSONArray());
            }

            @Override
            public void onError(ANError anError) {
                Log.e("error",anError.toString());
                progressdialog.dismiss();
                listener.onTaskFailed(anError,Constant.GET_LOCATION_MODE);
            }
        });


    }

    public void postRequest(String mode, Context context,JSONObject jsonobject,AsyncTaskCompleteListener listener,String baseUrl,boolean showProgress){
        Log.e("jsonobject",jsonobject.toString());
        if (!Network.isConnected(context)) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPrefrence sf = new SharedPrefrence(context);
        ProgressDialog progressdialog = initProgressDialog(context);
        if(showProgress)
        progressdialog.show();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + sf.getSharedPrefrenceString(Constant.TOKEN))
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        ANRequest.PostRequestBuilder networkRequest = AndroidNetworking.post(baseUrl + mode);
        networkRequest.addJSONObjectBody(jsonobject);
        networkRequest.addHeaders("Content-Type", "application/json");
        networkRequest.setOkHttpClient(client);
        networkRequest.setPriority(Priority.MEDIUM);
        networkRequest.build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resposne",response.toString());
                if(showProgress)
                progressdialog.dismiss();
                listener.onTaskCompleted(response,mode,new JSONArray());
            }

            @Override
            public void onError(ANError anError) {
                Log.e("error",anError.getErrorBody());
                if(showProgress)
                progressdialog.dismiss();
                listener.onTaskFailed(anError,mode);
            }
        });
    }

    public void postRequestArrayResponse(String mode, Context context,JSONObject jsonobject,AsyncTaskCompleteListener listener,String baseUrl,boolean showProgress){
        if (!Network.isConnected(context)) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPrefrence sf = new SharedPrefrence(context);
        ProgressDialog progressdialog = initProgressDialog(context);
        if(showProgress)
            progressdialog.show();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + sf.getSharedPrefrenceString(Constant.TOKEN))
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        ANRequest.PostRequestBuilder networkRequest = AndroidNetworking.post(baseUrl + mode);
        networkRequest.addJSONObjectBody(jsonobject);
        networkRequest.addHeaders("Content-Type", "application/json");
        networkRequest.setOkHttpClient(client);
        networkRequest.setPriority(Priority.MEDIUM);
        networkRequest.build().getAsJSONArray(new JSONArrayRequestListener() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("resposne",response.toString());
                if(showProgress)
                    progressdialog.dismiss();
                listener.onTaskCompleted(new JSONObject(),mode,response);
            }

            @Override
            public void onError(ANError anError) {
                Log.e("error",anError.getErrorBody());
                if(showProgress)
                    progressdialog.dismiss();
                listener.onTaskFailed(anError,mode);
            }
        });
    }

    public void DownloadModalFile(String url,String dirPath,String fileName,Context context,String mode,AsyncTaskCompleteListener listener){
        if (!Network.isConnected(context)) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        ProgressDialog progressdialog = initProgressDialog(context);
        progressdialog.show();
        AndroidNetworking.download(url,dirPath,fileName)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressdialog.dismiss();
                        listener.onTaskCompleted(new JSONObject(),mode,new JSONArray());
                    }
                    @Override
                    public void onError(ANError error) {
                        progressdialog.dismiss();
                        listener.onTaskFailed(error,mode);
                    }
                });
    }

    public ProgressDialog initProgressDialog(Context context) {
        ProgressDialog progress_dialog = new ProgressDialog(context);
        progress_dialog.setMessage("Please wait...");
        progress_dialog.setCancelable(true);
        progress_dialog.setCanceledOnTouchOutside(false);
        return progress_dialog;
    }
}






