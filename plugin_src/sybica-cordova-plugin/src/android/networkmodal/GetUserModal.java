package com.sybica.falconwings.networkmodal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserModal {

    @SerializedName("Pin")
    @Expose
    private String pin;
    @SerializedName("EmployeeId")
    @Expose
    private int employeeId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("MailId")
    @Expose
    private String mailId;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("Shift")
    @Expose
    private String shift;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Department")
    @Expose
    private String department;
    @SerializedName("Designation")
    @Expose
    private String designation;
    @SerializedName("EmployeeType")
    @Expose
    private String employeeType;
    @SerializedName("JoinDate")
    @Expose
    private String joinDate;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Zipcode")
    @Expose
    private String zipcode;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("AllowRemoteLogin")
    @Expose
    private boolean allowRemoteLogin;
    @SerializedName("ProfilePic")
    @Expose
    private String profilePic;
    @SerializedName("FRConfidenceThreshold")
    @Expose
    private String fRConfidenceThreshold;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public boolean isAllowRemoteLogin() {
        return allowRemoteLogin;
    }

    public void setAllowRemoteLogin(boolean allowRemoteLogin) {
        this.allowRemoteLogin = allowRemoteLogin;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getfRConfidenceThreshold() {
        return fRConfidenceThreshold;
    }

    public void setfRConfidenceThreshold(String fRConfidenceThreshold) {
        this.fRConfidenceThreshold = fRConfidenceThreshold;
    }
}
