package com.sybica.falconwings.networkmodal.attendanceLog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceLogData {

    @SerializedName("EmpAttnTransactionId")
    @Expose
    private int empAttnTransactionId;
    @SerializedName("EmployeeId")
    @Expose
    private int employeeId;
    @SerializedName("EmployeeName")
    @Expose
    private String employeeName;
    @SerializedName("TimeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("AttendanceStatus")
    @Expose
    private int attendanceStatus;
    @SerializedName("ElapsedTimeSinceBoot")
    @Expose
    private Object elapsedTimeSinceBoot;
    @SerializedName("SyncTime")
    @Expose
    private Object syncTime;
    @SerializedName("IsOfflineEntry")
    @Expose
    private boolean isOfflineEntry;
    @SerializedName("DeviceSN")
    @Expose
    private String deviceSN;
    @SerializedName("ImageURL")
    @Expose
    private String imageURL;
    @SerializedName("Latitude")
    @Expose
    private float latitude;
    @SerializedName("Longitude")
    @Expose
    private float longitude;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerAddress")
    @Expose
    private String customerAddress;
    @SerializedName("ApprovalStatus")
    @Expose
    private int approvalStatus;
    @SerializedName("ProbableFraud")
    @Expose
    private boolean probableFraud;

    public int getEmpAttnTransactionId() {
        return empAttnTransactionId;
    }

    public void setEmpAttnTransactionId(int empAttnTransactionId) {
        this.empAttnTransactionId = empAttnTransactionId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(int attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public Object getElapsedTimeSinceBoot() {
        return elapsedTimeSinceBoot;
    }

    public void setElapsedTimeSinceBoot(Object elapsedTimeSinceBoot) {
        this.elapsedTimeSinceBoot = elapsedTimeSinceBoot;
    }

    public Object getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(Object syncTime) {
        this.syncTime = syncTime;
    }

    public boolean isIsOfflineEntry() {
        return isOfflineEntry;
    }

    public void setIsOfflineEntry(boolean isOfflineEntry) {
        this.isOfflineEntry = isOfflineEntry;
    }

    public String getDeviceSN() {
        return deviceSN;
    }

    public void setDeviceSN(String deviceSN) {
        this.deviceSN = deviceSN;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public int getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(int approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public boolean isProbableFraud() {
        return probableFraud;
    }

    public void setProbableFraud(boolean probableFraud) {
        this.probableFraud = probableFraud;
    }
}
