package com.sybica.falconwings.networkmodal.attendanceLog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttendanceLogModal {
    @SerializedName("MyAttendance")
    @Expose
    private List<AttendanceLogData> myAttendance = null;
    @SerializedName("MyTeamAttendance")
    @Expose
    private List<Object> myTeamAttendance = null;

    public List<AttendanceLogData> getMyAttendance() {
        return myAttendance;
    }

    public void setMyAttendance(List<AttendanceLogData> myAttendance) {
        this.myAttendance = myAttendance;
    }

    public List<Object> getMyTeamAttendance() {
        return myTeamAttendance;
    }

    public void setMyTeamAttendance(List<Object> myTeamAttendance) {
        this.myTeamAttendance = myTeamAttendance;
    }

}

