
package com.sybica.falconwings.networkmodal.currentlocation;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address_ {

    @SerializedName("routeNumbers")
    @Expose
    private List<Object> routeNumbers = null;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("countrySubdivision")
    @Expose
    private String countrySubdivision;
    @SerializedName("countrySecondarySubdivision")
    @Expose
    private String countrySecondarySubdivision;
    @SerializedName("municipality")
    @Expose
    private String municipality;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("countryCodeISO3")
    @Expose
    private String countryCodeISO3;
    @SerializedName("freeformAddress")
    @Expose
    private String freeformAddress;
    @SerializedName("boundingBox")
    @Expose
    private BoundingBox boundingBox;
    @SerializedName("localName")
    @Expose
    private String localName;

    public List<Object> getRouteNumbers() {
        return routeNumbers;
    }

    public void setRouteNumbers(List<Object> routeNumbers) {
        this.routeNumbers = routeNumbers;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountrySubdivision() {
        return countrySubdivision;
    }

    public void setCountrySubdivision(String countrySubdivision) {
        this.countrySubdivision = countrySubdivision;
    }

    public String getCountrySecondarySubdivision() {
        return countrySecondarySubdivision;
    }

    public void setCountrySecondarySubdivision(String countrySecondarySubdivision) {
        this.countrySecondarySubdivision = countrySecondarySubdivision;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCodeISO3() {
        return countryCodeISO3;
    }

    public void setCountryCodeISO3(String countryCodeISO3) {
        this.countryCodeISO3 = countryCodeISO3;
    }

    public String getFreeformAddress() {
        return freeformAddress;
    }

    public void setFreeformAddress(String freeformAddress) {
        this.freeformAddress = freeformAddress;
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

}
